# README #

This package can be used either programmatically or as a command line tool. It is configured as a maven project, so doing a "maven install" should install it in your local maven repo, a "maven assembly:single" will create a single executable jar file. This can then be started with "java -jar *name_of_the_jar_file*" which will show you the different parameter settings.

### What is this repository for? ###

This piece of software is meant for fairly comprehensive text analysis using topic models. The input is always some sort of text corpus. This can be a collection of plain texts, web sites, emails or other custom data. Use the asv_corpus_utils package for data input and output as well as preprocessing.
The actual analysis then includes the application of a certain type of topic model and optionally the application of various test metrics, including a perplexity measurement and posterior predictive checking for the static models and timestamp prediction for the dynamic ones.

### License ###
This package is licensed under GPLv3. If you care to use it in a context not covered by this license, feel free to contact us.

### Contributors ###

The HDP CRF Gibbs sampler was written by Arnim Bleier, Patrick Jähnichen and Andreas Niekler as is documented in the source code. It has been modified to fit into our framework.
A standalone version can be obtained at [bitbucket](https://bitbucket.org/arnim_bleier/hdpgibbssampler)

The printtopics.py script was written by Matt Hoffman as part of the onlineldavb python package. It takes a vocabulary file and a matrix of topic-word-probabilities as input. It then sorts words in each topic by their probability and outputs them to stdout.
Onlineldavb can be found on [github](https://github.com/blei-lab/onlineldavb)

### Contributions ###

As this software comes from an academic setting it is poorly tested :D
You are warmly welcome to contribute testing scenarios and to review code.

### Who do I talk to? ###

If you want to contact me, write to [patrick.jaehnichen@hu-berlin.de](mailto:patrick.jaehnichen@hu-berlin.de)