/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.dtm.state;

import java.util.Arrays;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.ArrayUtils;

public class DTMTopicState {
    // properties

    private int W, T; // vocabulary size and sequence length

    // variational parameters

    private double[][] obs;             // observations, W x T

    // biproducts of the variational parameters

    private double obsVariance;         // observation variance
    private double chainVariance;       // chain variance
    private double[] zeta;            // extra variational parameter, T
    private double[][] eLogProb;      // E log prob(w | t), W x T

    // convenient quantities for inference

    private double[][] fwdMean;       // forward posterior mean, W x T
    private double[][] fwdVariance;   // forward posterior variance, W x T
    private double[][] mean;           // posterior mean, W x T
    private double[][] variance;       // posterior variance, W x T

    private double[][] meanT;         // W x T
    private double[][] varianceT;

    // Recent copy of w_phi_l.
    private double[][] wPhiL;         // W x T
    private double[][] wPhiSum;      // W x T
    private double[][] wPhiLSq;      // Square term involving various
    private double[][] mUpdateCoeff;  // Terms involving squares of
                                 // W, l, and phi.
    private double[][] mUpdateCoeffG;  // \sum_i=0..t phi_l(t) r(i-t)
	
    public DTMTopicState(int w, int t) {
		W = w;
		T = t;
		
		obs = new double[W][T];
		zeta = new double[T];
		eLogProb = new double[W][T];
		
		fwdMean = new double[W][T+1];
		fwdVariance = new double[W][T+1];
		mean = new double[W][T+1];
		variance = new double[W][T+1];
		
		meanT = new double[W][T];
		varianceT = new double[W][T];
		
		wPhiL = new double[W][T];
		wPhiSum = new double[W][T];
		wPhiLSq = new double[W][T];
		mUpdateCoeff = new double[W][T];
		mUpdateCoeffG = new double[W][T];
	}

	public double[][] getObs() {
		return obs;
	}

	public void setObs(double[][] obs) {
		this.obs = obs;
	}

	public double getObsVariance() {
		return obsVariance;
	}

	public void setObsVariance(double obs_variance) {
		this.obsVariance = obs_variance;
	}

	public double getChainVariance() {
		return chainVariance;
	}

	public void setChainVariance(double chain_variance) {
		this.chainVariance = chain_variance;
	}

	public double[] getZeta() {
		return zeta;
	}

	public void setZeta(double[] zeta) {
		this.zeta = zeta;
	}

	public double[][] geteLogProb() {
		return eLogProb;
	}

	public void seteLogProb(double[][] eLogProb) {
		this.eLogProb = eLogProb;
	}

	public double[][] getFwdMean() {
		return fwdMean;
	}

	public void setFwdMean(double[][] fwdMean) {
		this.fwdMean = fwdMean;
	}

	public double[][] getFwdVariance() {
		return fwdVariance;
	}

	public void setFwdVariance(double[][] fwdVariance) {
		this.fwdVariance = fwdVariance;
	}

	public double[][] getMean() {
		return mean;
	}

	public void setMean(double[][] mean) {
		this.mean = mean;
	}

	public double[][] getVariance() {
		return variance;
	}

	public void setVariance(double[][] variance) {
		this.variance = variance;
	}

	public double[][] getMeanT() {
		return meanT;
	}

	public void setMeanT(double[][] meanT) {
		this.meanT = meanT;
	}

	public double[][] getVarianceT() {
		return varianceT;
	}

	public void setVarianceT(double[][] varianceT) {
		this.varianceT = varianceT;
	}

	public double[][] getwPhiL() {
		return wPhiL;
	}

	public void setwPhiL(double[][] wPhiL) {
		this.wPhiL = wPhiL;
	}

	public double[][] getwPhiSum() {
		return wPhiSum;
	}

	public void setwPhiSum(double[][] wPhiSum) {
		this.wPhiSum = wPhiSum;
	}

	public double[][] getwPhiLSq() {
		return wPhiLSq;
	}

	public void setwPhiLSq(double[][] wPhiLSq) {
		this.wPhiLSq = wPhiLSq;
	}

	public double[][] getmUpdateCoeff() {
		return mUpdateCoeff;
	}

	public void setmUpdateCoeff(double[][] mUpdateCoeff) {
		this.mUpdateCoeff = mUpdateCoeff;
	}

	public double[][] getmUpdateCoeffG() {
		return mUpdateCoeffG;
	}

	public void setmUpdateCoeffG(double[][] mUpdateCoeffG) {
		this.mUpdateCoeffG = mUpdateCoeffG;
	}

	public void logNormCounts(double[] suffStatSlice) {
		double[] logNormCounts = new double[suffStatSlice.length];
		System.arraycopy(suffStatSlice, 0, logNormCounts, 0, suffStatSlice.length);
	    // normalize and take logs of the counts
		logNormCounts = VectorUtils.normalize(logNormCounts);
		logNormCounts = VectorUtils.addScalar(logNormCounts, 1./W);
		logNormCounts = VectorUtils.normalize(logNormCounts);
		logNormCounts = VectorUtils.log(logNormCounts);
	    // set variational observations to transformed counts

	    for (int t = 0; t < T; t++) {
	    	ArrayUtils.setColumn(obs, logNormCounts, t);
	    }
		
	}
    
	//forward-backward calculations
	public void computePosteriorVariance(int word) {
	    int t;
	    double w;


	    // forward
	    // note, initial variance set very large
	    fwdVariance[word][0] = chainVariance * 1000;
	    for (t = 1; t < T+1; t++) {
	    	// Eq. 1.16
	    	if (obsVariance > 0) {
	    		w = obsVariance /  (fwdVariance[word][t-1] + chainVariance + obsVariance);
	    	} else {
	    		w = 0.0;
	    	}
	    	fwdVariance[word][t] = w * (fwdVariance[word][t-1] + chainVariance);
	    }

	    // backward
	    variance[word][T] = fwdVariance[word][T];
	    for (t = T-1; t >= 0; t--) {
	    	// Eq. 1.21
	    	if (fwdVariance[word][t] > 0.0) {
	    		w = Math.pow(fwdVariance[word][t] / (fwdVariance[word][t] + chainVariance), 2);
	    	} else {
	    		w = 0.0;
	    	}
	    	variance[word][t] = w * (variance[word][t+1] - chainVariance) + (1-w) * fwdVariance[word][t];
	    }
	}

	public void computePosteriorMean(int word) {
	    int t;
	    double w;

	    // get the vectors for word w
//	    double[] obs = topic.getObs()[word];
//	    double[] mean = topic.getMean()[word];
//	    double[] fwdMean = topic.getFwdMean()[word];
//	    double[] fwdVariance = topic.getFwdVariance()[word];

	    // forward
	    // (note, the observation corresponding to mean t is at t-1)
	    fwdMean[word][0] = 0;
	    for (t = 1; t < T+1; t++) {
	    	// Eq. 1.14
	        assert(Math.abs(fwdVariance[word][t-1] + chainVariance + obsVariance) > 0.0);
	        w = obsVariance / (fwdVariance[word][t-1] + chainVariance + obsVariance);
	        fwdMean[word][t] = w * fwdMean[word][t-1] + (1-w) * obs[word][t-1];
	        if(Double.isNaN(fwdMean[word][t])) {
	        	//TODO: do appropriate logging
//			  outlog("t %d word %d w %f %f %f", t, word, w, vget(&fwd_mean, t - 1), vget(&obs, t-1));
//			  outlog("%f %f %f", var->obs_variance, vget(&fwd_variance, t-1),
//				 var->obs_variance);
			}
	    }

	    // backward
	    mean[word][T] = fwdMean[word][T];
	    for (t = T-1; t >= 0; t--) {
	    	// Eq. 1.18
	        if (chainVariance == 0.0) {
	        	w = 0.0;
	        } else {
	        	w = chainVariance / (fwdVariance[word][t] + chainVariance);
	        }
	        mean[word][t] = w * fwdMean[word][t] + (1-w) * mean[word][t+1];
	        if (Double.isNaN(mean[word][t])) {
	        	//TODO: do appropriate logging
//				outlog("t %d w %f %f %f", t, w, vget(&fwd_mean, t), vget(&mean, t+1));
			}
	        assert(!Double.isNaN(mean[word][t]));
	    }
	}

	public void updateZeta() {
	    int word;
	    int t;
	    
	    Arrays.fill(zeta, 0.);
	    for (word = 0; word < obs.length; word++) {
	        for (t = 0; t < obs[word].length; t++) {
	            double m = mean[word][t+1];
	            double v = variance[word][t+1];
	            double val = Math.exp(m + v/2.0);
	            // mset(var->zeta_terms, word, t, val);
	            zeta[t] += val;
	        }
	    }
	}

	public void computeExpectedLogProb() {
	    int t, w;
	    for (t = 0; t < T; t++)
	    	for (w = 0; w < W; w++)
	    		eLogProb[w][t] = mean[w][t+1] - Math.log(zeta[t]);
	}
 
    
    
}
