/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.dtm.state;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;

public class LDAPosterior<D extends Document> {

	D doc; // document associated to this posterior
	double[][] phi; // variational mult parameters (nterms x K)
	double[][] logPhi; // convenient for computation (nterms x K)
	double[] gamma; // variational dirichlet parameters (K)
	double[] lhood; // a K+1 vector, sums to the lhood bound
	private LDAModelState model;
	
	public LDAPosterior(int terms, int topics, LDAModelState m) {
		phi = new double[terms][topics];
		logPhi = new double[terms][topics];
		gamma = new double[topics];
		lhood = new double[topics+1];
		model = m;
	}
	
	private void initForNewDoc() {
	    for (int k = 0; k < model.getNumTopics(); k++) {
	        gamma[k] = model.getAlpha()[k] + ((double)doc.getLength()/model.getNumTopics());
	        for (int n = 0; n < doc.getNumberOfWordTypes(); n++)
	            phi[n][k] = 1./model.getNumTopics();
	    }
	
	}

	public D getDoc() {
		return doc;
	}

	public void setDoc(D doc) {
		this.doc = doc;
		initForNewDoc();
	}

	public double[][] getPhi() {
		return phi;
	}

	public double[][] getLogPhi() {
		return logPhi;
	}

	public double[] getGamma() {
		return gamma;
	}

	public double[] getLhood() {
		return lhood;
	}

	public LDAModelState getModel() {
		return model;
	}
	
	
}
