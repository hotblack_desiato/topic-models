/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.dtm.state;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState;

public class DTMDocumentState extends DocumentState {
	
	private double[] logLikelihoods;
	private double likelihood;
	private double[] lambda;
	
	
	public double[] getLogLikelihoods() {
		return logLikelihoods;
	}
	public void setLogLikelihoods(double[] logLikelihoods) {
		this.logLikelihoods = logLikelihoods;
	}
	public double getLikelihood() {
		return likelihood;
	}
	public void setLikelihood(double likelihood) {
		this.likelihood = likelihood;
	}
	public double[] getLambda() {
		return lambda;
	}
	public void setLambda(double[] lambda) {
		this.lambda = lambda;
	}
	
	

}
