/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.exporter.VocabularyExporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.CorpusImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicCorpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordTypeNotFoundException;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ExportSettings;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;
import de.uni_leipzig.informatik.asv.topicModels.model.GenericInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.cdtm.ContinuousTimeDynamicTopicModel;
import de.uni_leipzig.informatik.asv.topicModels.model.dlngap.DynamicLogNormalGammaPoissonModel;
import de.uni_leipzig.informatik.asv.topicModels.model.dtm.DynamicTopicModelInferencerVB;
import de.uni_leipzig.informatik.asv.topicModels.model.gap.GammaPoissonModel;
import de.uni_leipzig.informatik.asv.topicModels.model.gpdtm.GaussianProcessDynamicTopicModel;
import de.uni_leipzig.informatik.asv.topicModels.model.hdp.auxiliaryvar.HDPGibbsSamplerAux;
import de.uni_leipzig.informatik.asv.topicModels.model.hdp.crf.HDPGibbsSamplerCRF;
import de.uni_leipzig.informatik.asv.topicModels.model.hdp.online.HDPOnlineVBInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.hdp.online.HDPOnlineVBInferencerAdaptive;
import de.uni_leipzig.informatik.asv.topicModels.model.hdp.posteriorepresentation.HDPGibbsSamplerPR;
import de.uni_leipzig.informatik.asv.topicModels.model.hpy.HPYGibbsSamplerCRF;
import de.uni_leipzig.informatik.asv.topicModels.model.hpy.online.HPYOnlineVBInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.lda.LDAGibbsSampler;
import de.uni_leipzig.informatik.asv.topicModels.model.lda.LDAOnlineVBInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.lda.LDAOnlineVBInferencerAdaptiveLearning;
import de.uni_leipzig.informatik.asv.topicModels.model.lda.LDAVariationalBayesInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.lda.timeslice.LDAOnlineVBTimeslice;
import de.uni_leipzig.informatik.asv.topicModels.model.lngap.LogNormalGammaPoissonModel;
import de.uni_leipzig.informatik.asv.topicModels.model.logisticnormal.LogisticNormalTopicModel;
import de.uni_leipzig.informatik.asv.topicModels.model.sde.SDEDynamicTopicModel;
import de.uni_leipzig.informatik.asv.topicModels.utils.DocLineImporter;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;

public class Runner {
	private static InferencerProperties props;
	
	private static Logger logger = LogManager.getLogger(Runner.class);
	
	public static void main(String[] args) throws Exception {
		if(args.length < 2) {
			printUsage();
			return;
		}

		String propFile = args[0];
		if(!propFile.equals("null"))
			props = new InferencerProperties(propFile);
		else 
			props = new InferencerProperties();
		
		boolean success = false;
		File dir = new File(props.targetDir);
		if(!dir.exists())
			success = dir.mkdirs();
		if(success)
			logger.info("created target dir");
		else
			logger.warn("failed to create target dir");
			
		//copy properties file to target dir for later reference
		Files.copy(new File(propFile).toPath(), new File(props.targetDir + File.separator + "settings.properties").toPath(), StandardCopyOption.REPLACE_EXISTING);
		runModel(args);
		//end all threads when done
		System.exit(0);
	}

	private static void runModel(String[] args) {
		try {
			GenericInferencer model = null;
			DiachronicCorpus c = new DiachronicCorpus();
			DiachronicCorpus test = new DiachronicCorpus();
			DiachronicCorpus validation = new DiachronicCorpus();
			//TODO: this won't work with diachronic corpora, as new object assignment in a function are not valid outside the function
			loadCorpus(args, c, test, validation);
			switch(props.model) {
				case LDA_GIBBS: 
					model = new LDAGibbsSampler(c, test, validation, props); 
					break;
				case LDA_VB:
					model = new LDAVariationalBayesInferencer(c, test, validation, props); 
					break;
				case LDA_ONLINE_VB:
					model = new LDAOnlineVBInferencer(c, test, validation, props); 
					break;
				case LDA_ONLINE_VB_ADAPTIVE:
					model = new LDAOnlineVBInferencerAdaptiveLearning(c,test, validation, props); 
					break;
				case LDA_ONLINE_VB_TIMESLICE:
					model = new LDAOnlineVBTimeslice(c, test, validation, props);
					break;
				case HDP_AUX: 
					model = new HDPGibbsSamplerAux(c, test, validation, props);
					break;
				case HDP_CRF:
					model = new HDPGibbsSamplerCRF(c, test, validation, props);
					break;
				case HPY_CRF:
					model = new HPYGibbsSamplerCRF(c, test, validation, props);
					break;
				case HPY_ONLINE:
					model = new HPYOnlineVBInferencer(c, test, validation, props);
					break;
				case HDP_PR:
					model = new HDPGibbsSamplerPR(c, test, validation, props);
					break;
				case HDP_ONLINE:
					model = new HDPOnlineVBInferencer(c, test, validation, props);
					break;
				case HDP_ONLINE_ADAPTIVE:
					model = new HDPOnlineVBInferencerAdaptive(c, test, validation, props);
					break;
				case DTM_VB:
					model = new DynamicTopicModelInferencerVB(c, test, validation, props);
					break;
				case CTM_VB:
					model = new ContinuousTimeDynamicTopicModel(c, test, validation, props);
					break;
				case GAMMA_POISSON:
					model = new GammaPoissonModel(c, test, validation, props);
					break;
				case LNGAP:
					model = new LogNormalGammaPoissonModel(c, test, validation, props);
					break;
				case DLNGAP:
					model = new DynamicLogNormalGammaPoissonModel(c, test, validation, props);
					break;
				case LOGISTIC:
					model = new LogisticNormalTopicModel(c, test, validation, props);
					break;
				case SDE:
					model = new SDEDynamicTopicModel(c, test, validation, props);
					break;
				case GPDTM:
					model = new GaussianProcessDynamicTopicModel(c, test, validation, props);
					break;
			}
			System.out.println("corpus has " + c.getNumberOfDocuments() + " documents with " + c.getNumberOfWordTypes() + " words");
		
			long start = System.currentTimeMillis();
			model.doInference();
			long dur = System.currentTimeMillis() - start;
			System.out.println(props.iterations + " iterations took " + dur/1000 + "s");
			model.storeModel();
			if(props.doTesting)
				model.testModel();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void loadCorpus(String[] args, DiachronicCorpus c, DiachronicCorpus t, DiachronicCorpus v)
			throws IOException, ClassNotFoundException, WordTypeNotFoundException {
		String inputType = args[1];
		ImportSettings defaultImportSettings = new ImportSettings(null);
		if(inputType.equalsIgnoreCase(InputTypes.FEATURES.toString())) {
			if(args.length != 2) {
				printUsage();
				System.exit(-1);
			}
			String baseDir = props.sourceDir;
			//import training corpus, this always exists
			defaultImportSettings.vocabFile = baseDir + File.separator + "train" + File.separator + "vocabulary.dat";
			defaultImportSettings.dataFile = baseDir + File.separator + "train" + File.separator + "corpus.dat";
			CorpusImporter.importFeatureFrequencyCorpus(c, defaultImportSettings);
			
			//import test corpus (if present)
			String testCorpusDir = baseDir + File.separator + "test";
			File testDir = new File(testCorpusDir);
			if(testDir.exists() && testDir.isDirectory()) {
				defaultImportSettings.vocabFile = testCorpusDir + File.separator + "vocabulary.dat";
				defaultImportSettings.dataFile = testCorpusDir + File.separator + "corpus.dat";
				CorpusImporter.importFeatureFrequencyCorpus(t, defaultImportSettings);
			}
			
			//import validation corpus (if present)
			String validationCorpusDir = baseDir + File.separator + "validation";
			File validationDir = new File(validationCorpusDir);
			if(validationDir.exists() && validationDir.isDirectory()) {
				defaultImportSettings.vocabFile = validationCorpusDir + File.separator + "vocabulary.dat";
				defaultImportSettings.dataFile = validationCorpusDir + File.separator + "corpus.dat";
				CorpusImporter.importFeatureFrequencyCorpus(v, defaultImportSettings);
			}			
		} else if(inputType.equalsIgnoreCase(InputTypes.FEATURES_DIR.toString())) {
			if(args.length != 3) {
				printUsage();
				System.exit(-1);
			}
			defaultImportSettings.dataDir = props.sourceDir;
			defaultImportSettings.vocabFile = args[2];
			CorpusImporter.importFeatureFrequencyCorpusDir(c, defaultImportSettings);
		} else if(inputType.equalsIgnoreCase(InputTypes.FEATURES_DIACHRONOUS.toString())) {
			if(args.length != 4) {
				printUsage();
				System.exit(-1);
			}
			String baseDir = props.sourceDir;
			//import training corpus
			defaultImportSettings.vocabFile = baseDir + File.separator + "train" + File.separator + "vocabulary.dat";
			defaultImportSettings.dataFile = baseDir + File.separator + "train" + File.separator + "corpus_diachronic.dat";
			CorpusImporter.importDiachronicFeatureFrequencyCorpus(c, defaultImportSettings);
			
			//import test corpus
			String testCorpusDir = baseDir + File.separator + "test";
			File testDir = new File(testCorpusDir);
			if(testDir.exists() && testDir.isDirectory()) {
				defaultImportSettings.vocabFile = testCorpusDir + File.separator + "vocabulary.dat";
				defaultImportSettings.dataFile = testCorpusDir + File.separator + "corpus_diachronic.dat";
				CorpusImporter.importDiachronicFeatureFrequencyCorpus(t, defaultImportSettings);
			}
			
			//import validation corpus (if present)
			String validationCorpusDir = baseDir + File.separator + "validation";
			File validationDir = new File(validationCorpusDir);
			if(validationDir.exists() && validationDir.isDirectory()) {
				defaultImportSettings.vocabFile = validationCorpusDir + File.separator + "vocabulary.dat";
				defaultImportSettings.dataFile = validationCorpusDir + File.separator + "corpus_diachronic.dat";
				CorpusImporter.importDiachronicFeatureFrequencyCorpus(v, defaultImportSettings);
			}
		} else if(inputType.equalsIgnoreCase(InputTypes.VECTORS.toString())) {
			if(args.length != 2) {
				printUsage();
				System.exit(-1);
			}
			defaultImportSettings.dataDir = props.sourceDir;
			CorpusImporter.importTEVectorCorpus(c, defaultImportSettings);
		} else if(inputType.equalsIgnoreCase(InputTypes.NYT.toString())) {
			if(args.length != 2) {
				printUsage();
				System.exit(-1);
			}
			defaultImportSettings.minFreq = 10;
			defaultImportSettings.dataDir = props.sourceDir;
			c = new DiachronicCorpus();
			CorpusImporter.importNYTCorpus(c, defaultImportSettings);
		} else if(inputType.equalsIgnoreCase(InputTypes.DOC_LINES.toString())) {
			if(args.length != 3 && args.length != 4) {
				printUsage();
				System.exit(-1);
			}
			boolean allowExpand = false;
			if(args.length == 3) 
				allowExpand = true;
			else
				c.getDictionary().readDictionaryFile(args[3]);
			defaultImportSettings.dataFile = args[2];
			CorpusImporter importer = new DocLineImporter(allowExpand, defaultImportSettings);
			importer.doImport(c);
		} else if(inputType.equalsIgnoreCase(InputTypes.PLAIN.toString())) {
			if(args.length != 3) {
				printUsage();
				System.exit(-1);
			}
			String importSettingsFile = args[2];
			if(importSettingsFile.equals("null")) importSettingsFile = null;
			ImportSettings settings = new ImportSettings(importSettingsFile);
			settings.dataDir = props.sourceDir;

			
//			settings.preprocess = true;
//			settings.removePunctuation = true;
//			settings.normalizeCurrency = true;
//			settings.normalizeNumbers = true;
//			settings.removeStopwords = true;
//			settings.minFreq=75;
//			settings.minDocPercent=.1;
//			settings.minLength = 5;
//			settings.removeCurrency = true;
//			settings.removeNumbers = true;
//			settings.languages = new String[]{"de"};
			CorpusImporter.importPlainTextCorpus(c, settings);
			c.pruneDictionary();
			c.pruneDictionaryMinFreq(settings.minFreq);
			c.pruneDictionaryMinLength(settings.minLength);
			c.pruneDictionaryMinPercent(settings.minDocPercent);
		} else if(inputType.equalsIgnoreCase(InputTypes.DB.toString())) {
			if(args.length != 3) {
				printUsage();
				System.exit(-1);
			}
			ImportSettings settings = new ImportSettings(args[2]);
			CorpusImporter.importWortSchatzCorpus(c, settings);
		} else if(inputType.equalsIgnoreCase(InputTypes.ELASTIC.toString())) {
			long start = System.currentTimeMillis();
			ImportSettings s = new ImportSettings(args[2]);
			if(args.length != 3) {
				printUsage();
				System.exit(-1);
			}
			CorpusImporter.importElasticCorpus(c, s);
//			double minFreq = c.getNumberOfDocuments()*.05;
//			double maxFreq = c.getNumberOfDocuments()*.5;
//			c.pruneDictionaryMinMaxDocs((int)minFreq, (int)maxFreq);
//			c.pruneDictionaryMinLength(3);
			
			long dur = System.currentTimeMillis() - start;
			System.out.println("import took " + (dur/1000) + "s");
			
		}
		//take care of unused words in corpus and also remove them from test corpora
		HashSet<String> deletedTypeStrings = c.pruneDictionary();
		if(t != null) {
			t.pruneDictionary(deletedTypeStrings);
			t.pruneDictionary();
		}
		if(v != null) {
			v.pruneDictionary(deletedTypeStrings);
			v.pruneDictionary();
		}
		
		//keep track of actually used vocabulary, may change after pruning
		ExportSettings s = new ExportSettings(null);
		s.outputDir = props.targetDir;
		s.vocabFilename = "vocabulary_pruned.txt";
		s.maxFreq = Integer.MAX_VALUE;
		s.minFreq = -1;
		s.minLength = -1;
		VocabularyExporter dicEx = new VocabularyExporter(s);
		dicEx.doExport(c);
		
//		if(props.minFreq != -1) {
//			c.pruneDictionaryMinFreq(props.minFreq);
//		}
	}

	static void printUsage() {
		System.out.println("Usage:");
		String clazz = Thread.currentThread().getStackTrace()[0].getClassName();
		System.out.println(clazz + " samplerPropertiesFile features");
		System.out.println(clazz + " samplerPropertiesFile|null features_dir vocabularyFile");
		System.out.println(clazz + " samplerPropertiesFile|null features_diachronous vocabularyFile dataFile");
		System.out.println(clazz + " samplerPropertiesFile|null vectors");
		System.out.println(clazz + " samplerPropertiesFile|null nyt");
		System.out.println(clazz + " samplerPropertiesFile|null doc_lines filename [vocabularyFile]");
		System.out.println(clazz + " samplerPropertiesFile|null plain importSettingsFile");
		System.out.println(clazz + " samplerPropertiesFile|null db importSettingsFile");
		System.out.println(clazz + " samplerPropertiesFile|null elastic importSettingsFile");
	}
	
}

