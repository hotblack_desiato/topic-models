/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.topicModels.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cern.colt.map.OpenIntIntHashMap;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick
 *         Jaehnichen</a>
 * 
 */
public abstract class AbstractGenericInferencer<C extends Corpus> implements
		GenericInferencer {

	protected InferencerProperties props;

	protected C corpus;

	protected C testCorpus;

	protected C validationCorpus;

	// protected PrintStream logger;
	protected Logger logger;

	protected OpenIntIntHashMap localWordTypeIds;

	protected OpenIntIntHashMap localDocIds;

	protected ArrayList<Double> likelihood = new ArrayList<>();

	protected int K, V, M;

	protected ExecutorService threadPool;

	protected Random rng;

	protected AbstractGenericInferencer(C c, C t, C v, InferencerProperties p) {
		this.corpus = c;
		this.testCorpus = t;
		this.validationCorpus = v;
		this.props = p;
		int numThreads = this.props.numThreads;
		if (numThreads == -1)
			numThreads = Runtime.getRuntime().availableProcessors() * 2;
		this.threadPool = Executors.newFixedThreadPool(this.props.numThreads);
		// this.logger = new PrintStream(props.targetDir + File.separator +
		// "log.txt");

		this.logger = LogManager.getLogger(AbstractGenericStaticInferencer.class);


		// build local index
		this.localWordTypeIds = new OpenIntIntHashMap();
		int wtIdx = 0;
		for (WordType wt : this.corpus.getAllWordTypes())
			this.localWordTypeIds.put(wt.getId(), wtIdx++);

		this.localDocIds = new OpenIntIntHashMap();
		int docIdx = 0;
		for (Document d : this.corpus.getDocuments()) {
			this.localDocIds.put(d.getId(), docIdx);
			docIdx++;
		}
		this.K = this.props.numTopics;
		this.V = this.corpus.getNumberOfWordTypes();
		this.M = this.corpus.getNumberOfDocuments();
		if (this.props.randomSeed != 0)
			this.rng = new Random(this.props.randomSeed);
		else
			this.rng = new Random(System.currentTimeMillis());
	}

	/**
	 * @return the logger
	 */
	public Logger getLogger() {
		return this.logger;
	}

	/**
	 * @return the k
	 */
	public int getK() {
		return this.K;
	}

	/**
	 * @return the v
	 */
	public int getV() {
		return this.V;
	}

	/**
	 * @return the m
	 */
	public int getM() {
		return this.M;
	}

	/**
	 * @param k
	 *            the k to set
	 */
	public void setK(int k) {
		this.K = k;
	}

	/**
	 * @param v
	 *            the v to set
	 */
	public void setV(int v) {
		this.V = v;
	}

	/**
	 * @param m
	 *            the m to set
	 */
	public void setM(int m) {
		this.M = m;
	}

	public void storeModel() throws IOException {
		this.logger.info("storing the values...");
		this.logger.info("populating final values...");
		populateFinalValues();
		this.logger.info("writing topic top words...");
		writeTopWordsForTopics(this.props.topicTopWordsFile, this.props.topicWordlistSize);
		this.logger.info("writing topic-term proportions...");
		writeTopicTermProportions(this.props.topicWordProbabilityFile);
		this.logger.info("writing doc-topic proportions...");
		writeDocTopicProportions(this.props.documentTopicProbabilityFile);
		this.logger.info("writing overall topic proportions...");
		writeOverallTopicProportions(this.props.globalTopicProbabilityFile);
		this.logger.info("writing local to global type id translation...");
		writeLocalToGlobalWordTypeIds(this.props.localToGlobalTypeIdFile);
	}

	public OpenIntIntHashMap getLocalWordTypeIds() {
		return this.localWordTypeIds;
	}

	public OpenIntIntHashMap getLocalDocIds() {
		return this.localDocIds;
	}

	public C getCorpus() {
		return this.corpus;
	}

	/**
	 * @return the testCorpus
	 */
	public C getTestCorpus() {
		return this.testCorpus;
	}

	/**
	 * @return the props
	 */
	public InferencerProperties getProps() {
		return this.props;
	}

	/**
	 * @return the validationCorpus
	 */
	public C getValidationCorpus() {
		return this.validationCorpus;
	}

	/**
	 * @return the threadPool
	 */
	public ExecutorService getThreadPool() {
		return this.threadPool;
	}

	public abstract void populateFinalValues();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericInferencer#
	 * getTopicLikelihood()
	 */
	@Override
	public double getTopicLikelihood() {
		// TODO: workaround, overwrite in other classes;
		return 0;
	}

	protected abstract void writeTopWordsForTopics(String topicTopWordsFile,
			int numwords) throws IOException;

	protected abstract void writeDocTopicProportions(String filename)
			throws IOException;

	protected abstract void writeTopicTermProportions(
			String topicWordProbabilityFile) throws IOException;

	protected abstract void writeOverallTopicProportions(
			String globalTopicProbabilityFile) throws IOException;

	protected void writeLocalToGlobalWordTypeIds(String localToGlobalTypeIdFile)
			throws FileNotFoundException {
		StringBuffer sb = new StringBuffer();
		PrintStream ps = new PrintStream(this.props.targetDir + File.separator
				+ localToGlobalTypeIdFile);
		for (WordType wt : this.corpus.getAllWordTypes())
			sb.append(wt.getId()).append("->")
					.append(this.localWordTypeIds.get(wt.getId())).append("\n");
		ps.print(sb.toString());
		ps.close();

	}

	protected void writeLikelihood(String likelihoodFile)
			throws FileNotFoundException {
		PrintStream ll = new PrintStream(this.props.targetDir + File.separator
				+ likelihoodFile);
		for (int i = 0; i < this.likelihood.size(); i++) {
			ll.println(this.likelihood.get(i));
		}
		ll.close();
	}
}
