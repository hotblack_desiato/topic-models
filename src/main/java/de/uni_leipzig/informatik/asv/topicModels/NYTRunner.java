/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.CorpusImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.FileUtils;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.lda.LDAGibbsSampler;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;

public class NYTRunner {

	/**
	 * @param args
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws InterruptedException, IOException {
		String baseDir = args[0];
		String propsFile = args[1];

		InferencerProperties props = new InferencerProperties(propsFile);
		
		File[] corpora = null;
		if(props.corpusDirList.equals(""))
			 corpora = CorpusImporter.collectFiles(baseDir);
		else {
			String[] dirs = FileUtils.readLinesFromFile(props.corpusDirList);
			corpora = new File[dirs.length];
			for(int c = 0;c<dirs.length;c++)
				corpora[c] = CorpusImporter.collectFiles(baseDir + File.separator + dirs[c])[0];
		}
		ExecutorService pool = Executors.newFixedThreadPool(props.numThreads);
		for(File file : corpora)
			pool.execute(new MyThread(propsFile, file, baseDir));

		pool.shutdown();
		pool.awaitTermination(365, TimeUnit.DAYS);

		
		
	}
	
	
	public static class MyThread implements Runnable {
		
		private File corpusFile;
		private InferencerProperties props;
		private String vocabFileName;
		private String propertiesFile;
		
		public MyThread(String propsFile, File c, String baseDir) {
			propertiesFile = propsFile;
			this.corpusFile = c;
			this.vocabFileName = baseDir + File.separator + "vocabulary.dat";
		}

		@Override
		public void run() {
			long start = System.currentTimeMillis();
			props = new InferencerProperties(propertiesFile);
			props.targetDir += generateTargetDirSuffix(props, corpusFile);
			//make sure, target dir exists
			File f = new File(props.targetDir);
			f.mkdirs();

			Corpus dc = new Corpus(); 
			try {
				ImportSettings s = new ImportSettings(null);
				s.vocabFile = vocabFileName;
				s.dataFile = corpusFile.getAbsolutePath();
				CorpusImporter.importFeatureFrequencyCorpus(dc, s);
				//prune unused terms
				dc.pruneDictionary();
//				HDPGibbsSamplerCRF sampler = new HDPGibbsSamplerCRF((Corpus<DOCState, WordState>) dc, props);
				AbstractGenericInferencer<Corpus> sampler = new LDAGibbsSampler(dc, null, null, props);
				sampler.doInference();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("done processing file " + corpusFile.getAbsolutePath() + ", took " + (System.currentTimeMillis()-start)/1000 + "s");
		}
		
		private String generateTargetDirSuffix(InferencerProperties props, File c) {
			return File.separator + props.model.toString().toLowerCase() + "_beta_" + props.beta + "_sampling_" + props.sampleHyperParams + File.separator + c.getParentFile().getParentFile().getParentFile().getName()
			+ File.separator + c.getParentFile().getParentFile().getName()
			+ File.separator + c.getParentFile().getName();

		}
	}

}
