/*******************************************************************************
 * Copyright (c) 2011  Arnim Bleier, Patrick Jähnichen and Andreas Niekler
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.hdp.crf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;

import cern.jet.random.Gamma;
import cern.jet.random.engine.RandomEngine;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordToken;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.ArrayUtils;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericStaticInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.hdp.crf.state.DOCState;
import de.uni_leipzig.informatik.asv.topicModels.model.hdp.crf.state.WordState;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;

/**
 * Implements the Chinese Restaurant Franchise sampling scheme for inference in Hierarchical Dirichlet Processes.
 * 
 * For more information on the algorithm see:
 * <a href="http://www.gatsby.ucl.ac.uk/~ywteh/research/npbayes/TehJor2010a.pdf">Hierarchical Bayesian Nonparametric Models with Applications</a>. 
 * Y.W. Teh and M.I. Jordan. Bayesian Nonparametrics, 2010. Cambridge University Press.<br/>
 * 
 * A stand-alone implementation that was developed in parallel can be found at <a href="https://bitbucket.org/arnim_bleier/hdpgibbssampler">bitbucket</a>.
 * 
 * 
 * @author <a href="mailto:arnim.bleier+hdp@gmail.com">Arnim Bleier</a>,<a href="mailto:patrick.jaehnichen@hu-berlin.de">Patrick Jähnichen</a> 
 */
public class HDPGibbsSamplerCRF extends AbstractGenericStaticInferencer { 


	/**
	 * 
	 */
	private static final long serialVersionUID = -1534683719234882001L;
	protected double gamma; //corpus level concentration param
	protected double alpha; //document level concentration param

	
	private Gamma randGamma;
	protected double[] p;
	protected double[] f;
	
	protected List<Document> docs;
	protected int[] numberOfTablesByTopic;
	protected int[] wordCountByTopic;
	protected int[][] wordCountByTopicAndTerm;
	
	protected LinkedHashMap<Integer, DOCState> docStateMode = new LinkedHashMap<>();
	private LinkedHashMap<Integer, int[]> docWordTableAssignmentMode = new LinkedHashMap<>();
	public int numberOfTopicsMode;
	public int[] wordCountByTopicMode;
	public int[][] wordCountByTopicAndTermMode;
	
	public int sizeOfVocabulary;
	protected int totalNumberOfWords;
//	protected int numberOfTopics = 1;
	protected int totalNumberOfTables;
	
	public HDPGibbsSamplerCRF(Corpus c, Corpus test, Corpus validation, InferencerProperties _props) throws IOException {
		super(c, test, validation, _props);
		
		RandomEngine re = RandomEngine.makeDefault();
		this.randGamma = new Gamma(this.props.gamma_a, this.props.gamma_b, re);
		
		this.K = new Random().nextInt(this.props.maxInitialTopics) + 1;
		if(this.props.sampleHyperParams) {
			this.alpha = this.randGamma.nextDouble(this.props.alpha_a, 1./this.props.alpha_b);
			this.gamma = this.randGamma.nextDouble(this.props.gamma_a, 1./this.props.gamma_b);
		} else {
			this.alpha = this.props.alpha;
			this.gamma = this.props.gamma;
		}
		
		addInstances(c.getDocuments(), c.getNumberOfWordTypes());
	}
	

	/**
	 * Initially assign the words to tables and topics
	 * 
	 * @param corpus {@link CLDACorpus} on which to fit the model
	 */
	private void addInstances(Set<Document> documentsInput, int V) {
		this.sizeOfVocabulary = V;
		this.totalNumberOfWords = 0;
		for (Document doc : documentsInput) {
			doc.setState(new DOCState());
			for(WordToken w : doc.getWords())
				w.setState(new WordState(-1));
			if(doc.getLength() != doc.getWords().size())
				System.out.println("length mismatch");
			this.totalNumberOfWords += doc.getLength();
		}
		this.docs = new ArrayList<>(documentsInput);
		int k, i, j;
		Document doc;
		this.p = new double[20]; 
		this.f = new double[20];
		this.numberOfTablesByTopic = new int[this.K+1];
		this.wordCountByTopic = new  int[this.K+1];
		this.wordCountByTopicAndTerm = new int[this.K+1][];
		for (k = 0; k <= this.K; k++) 	// var initialization done
			this.wordCountByTopicAndTerm[k] = new int[this.sizeOfVocabulary];
		
		for (k = 0; k < this.K; k++) { 
			doc = this.docs.get(k);
			for (WordToken wt : doc.getWords()) 
				addWord(doc, wt, 0, k);
		} // all topics have now one document
		for (j = this.K; j < this.docs.size(); j++) {
			doc = this.docs.get(j); 
			k = this.rng.nextInt(this.K);
			for (WordToken wt : doc.getWords()) 
				addWord(doc, wt, 0, k);
		} // the words in the remaining documents are now assigned too
	}

	
	/**
	 * Step one step ahead
	 * 
	 */
	protected void nextGibbsSweep() {
		int table;
		for (int d = 0; d < this.docs.size(); d++) {
			Document doc = this.docs.get(d);
			for (WordToken wt : doc.getWords())  {
				removeWord(doc, wt); // remove the word i from the state
				table = sampleTable(doc, wt);
				if (table == doc.<DOCState>getState().getNumberOfTables()) // new Table
					addWord(doc, wt, table, sampleTopic()); // sampling its Topic
				else
					addWord(doc, wt, table, doc.<DOCState>getState().getTableToTopic()[table]); // existing Table
			}
		}
		defragment();
	}

	
	private void sampleHyperParamsFirstLevel() {
		double shape = this.props.gamma_a;
		double scale = this.props.gamma_b;
		int n = this.totalNumberOfTables;
		
//		double eta = randBeta.nextDouble(gamma + 1, n);
		double eta = MathUtils.beta(this.gamma + 1, n);
		double pi = shape + this.K - 1;
		double rate = 1. / scale - Math.log(eta);
		pi = pi / (pi + rate * n);
		
		if(MathUtils.bernoulliDraw(pi) == 1) {
			this.gamma = this.randGamma.nextDouble(shape + this.K, rate);
		} else {
			this.gamma = this.randGamma.nextDouble(shape + this.K - 1, rate);
		}
	}

	private void sampleHyperParamsSecondLevel() {
		double shape = this.props.alpha_a;
		double scale = this.props.alpha_b;
		
		int n = this.totalNumberOfTables;
		double rate, sumLogW, sumS;
		
		for(int step = 0;step < 20;step++) {
			sumLogW = 0;
			sumS = 0;
			for(int d = 0;d < this.docs.size();d++) {
				int docLength = this.docs.get(d).getLength();
				sumLogW += Math.log(MathUtils.beta(this.alpha + 1, docLength));
				sumS += (double)MathUtils.bernoulliDraw(docLength / (docLength + this.alpha));
			}
			rate = 1. / scale - sumLogW;
			this.alpha = this.randGamma.nextDouble(shape + n - sumS, rate);
		} 
	}
	

	private double computeJointLikelihood() {
		double lik = 0;
		for(int d=0;d<this.docs.size();d++) {
			lik += docLikelihood(this.docs.get(d));
		}
		lik += tableLikelihood();
		lik += data_likelihood();
		
		if(this.props.sampleHyperParams) {
			double shape = this.props.gamma_a;
			double scale = this.props.gamma_b;
			lik += (shape-1)*Math.log(this.gamma) - this.gamma/scale - shape*Math.log(scale) - MathUtils.loggamma(shape);
			
			shape = this.props.alpha_a;
			scale = this.props.alpha_b;
			lik += (shape-1)*Math.log(this.gamma) - this.gamma/scale - shape*Math.log(scale) - MathUtils.loggamma(shape);
		}
		return lik;
	}
	
	private double data_likelihood() {
		double l = this.K * MathUtils.loggamma(this.sizeOfVocabulary * this.props.beta);
		double lgamma_eta = MathUtils.loggamma(this.props.beta);
		
		for(int k=0;k<this.K;k++) {
			l -= MathUtils.loggamma(this.sizeOfVocabulary*this.props.beta + this.wordCountByTopic[k]);
			for(int w=0;w<this.sizeOfVocabulary;w++) {
				if(this.wordCountByTopicAndTerm[k][w] > 0)
					l += MathUtils.loggamma(this.wordCountByTopicAndTerm[k][w]+this.props.beta) - lgamma_eta;
			}
		}
		return l;
	}


	private double tableLikelihood() {
		double l = this.K * Math.log(this.gamma) - MathUtils.logFactorial(this.totalNumberOfTables, this.gamma);
		for(int k=0;k<this.K;k++) 
			l += MathUtils.loggamma(this.numberOfTablesByTopic[k]);
		return l;
	}


	private double docLikelihood(Document d) {
		double l = d.<DOCState>getState().getNumberOfTables() * Math.log(this.alpha) - MathUtils.logFactorial(d.getLength(), this.alpha);
		for(int t=0;t<d.<DOCState>getState().getNumberOfTables();t++) {
			int tableWordCount = d.<DOCState>getState().getWordCountByTable()[t];
			if(tableWordCount > 0)
				l += MathUtils.loggamma(tableWordCount);
		}
		return l;
	}


	/**
	 * Decide at which topic the table should be assigned to
	 * 
	 * @return the index of the topic
	 */
	protected int sampleTopic() {
		double u, pSum = 0.0;
		int k;
		this.p = ArrayUtils.ensureCapacity(this.p, this.K);
		double denom = this.gamma + this.totalNumberOfTables;
		for (k = 0; k < this.K; k++) {
			pSum += (this.numberOfTablesByTopic[k] * this.f[k])/denom;
			this.p[k] = pSum;
		}
		pSum += this.gamma / (this.sizeOfVocabulary*denom);
		this.p[this.K] = pSum;
		u = this.rng.nextDouble() * pSum;
		for (k = 0; k <= this.K; k++)
			if (u < this.p[k])
				break;
		return k;
	}
	

	/**	 
	 * Decide at which table the word should be assigned to
	 * 
	 * @param docID the index of the document of the current word
	 * @param i the index of the current word
	 * @return the index of the table
	 */
	protected int sampleTable(Document doc, WordToken wt) {	
		int k, j;
		double pSum = 0.0, vb = this.sizeOfVocabulary * this.props.beta, fNew, u;
		this.f = ArrayUtils.ensureCapacity(this.f, this.K);
		this.p = ArrayUtils.ensureCapacity(this.p, doc.<DOCState>getState().getNumberOfTables());
		double denom = this.gamma + this.totalNumberOfTables;
		fNew = this.gamma / (this.sizeOfVocabulary*denom);
		for (k = 0; k < this.K; k++) {
			this.f[k] = (this.wordCountByTopicAndTerm[k][this.localWordTypeIds.get(wt.getTypeId())] + this.props.beta) / 
					(this.wordCountByTopic[k] + vb);
			fNew += this.numberOfTablesByTopic[k] * this.f[k]/denom;
		}
		denom = (doc.getLength() + this.alpha);
		for (j = 0; j < doc.<DOCState>getState().getNumberOfTables(); j++) {
			if (doc.<DOCState>getState().getWordCountByTable()[j] > 0) 
				pSum += doc.<DOCState>getState().getWordCountByTable()[j] * this.f[doc.<DOCState>getState().getTableToTopic()[j]]/denom;
			this.p[j] = pSum;
		}
		pSum += this.alpha * fNew / denom; // Probability for t = tNew
		this.p[doc.<DOCState>getState().getNumberOfTables()] = pSum;
		u = this.rng.nextDouble() * pSum;
		for (j = 0; j <= doc.<DOCState>getState().getNumberOfTables(); j++)
			if (u < this.p[j]) 
				break;	// decided which table the word i is assigned to
		return j;
	}


	/**
	 * Method to call for fitting the model.
	 * 
	 * @param doShuffle
	 * @param shuffleLag
	 * @param maxIter number of iterations to run
	 * @param saveLag save interval 
	 * @param wordAssignmentsWriter {@link WordAssignmentsWriter}
	 * @param topicsWriter {@link TopicsWriter}
	 * @throws IOException 
	 */
	@Override
	public void doInference() 
	throws IOException {
		String header = "#iter\t#topics\t#tables\tgamma\talpha\tbeta\tjoint likelihood";
		this.logger.info(header);
		double maxLikelihood = Double.NEGATIVE_INFINITY;
		for (int iter = 0; iter < this.props.iterations; iter++) {
			if ((this.props.shuffleLag > 0) && (iter > 0) && (iter % this.props.shuffleLag == 0))
				doShuffle();
			nextGibbsSweep();
			if(this.props.sampleHyperParams && iter > 0){// && (iter % props.hyperSampleLag == 0)) {
				sampleHyperParamsFirstLevel();
				sampleHyperParamsSecondLevel();
			}
			double l = computeJointLikelihood();
			this.likelihood.add(l);
			String logString = iter + "\t" + this.K + "\t" + this.totalNumberOfTables + "\t" + this.gamma + "\t" + this.alpha + "\t" + this.props.beta + "\t" + l;
			this.logger.info(logString);
			if(iter > this.props.burnIn && l > maxLikelihood) {
				this.docStateMode.clear();
				this.docWordTableAssignmentMode.clear();
				for(int doc = 0;doc<this.docs.size();++doc) {
					Document d = this.docs.get(doc);
					this.docStateMode.put(d.getId(), d.<DOCState>getState().clone());
					int[] tabAssignment = new int[d.getLength()];
					for(int w=0;w<d.getLength();++w)
						tabAssignment[w] = d.getWords().get(w).<WordState>getState().getTableAssignment();
					this.docWordTableAssignmentMode.put(d.getId(), tabAssignment);
				}
				this.wordCountByTopicMode = this.wordCountByTopic.clone();
				this.wordCountByTopicAndTermMode = this.wordCountByTopicAndTerm.clone();
				this.numberOfTopicsMode = this.K;
				maxLikelihood = l;
				this.logger.info("new mode");
			}
			
		}
		populateFinalValues();
	}
		

	public void populateFinalValues() {
		this.K = this.numberOfTopicsMode;
		this.finalDocTopicWeights = new double[this.corpus.getNumberOfDocuments()][this.K];
		this.finalTopicWordWeights = new double[this.K][this.corpus.getNumberOfWordTypes()];
		
		for(Document d : this.corpus.getDocuments()) {
			
			int[] wc = this.docStateMode.get(d.getId()).getWordCountsByTopic(this.K);
			
			double length = d.getLength();
			for(int k=0;k<this.K;++k) {
				this.finalDocTopicWeights[this.localDocIds.get(d.getId())][k] = (double) wc[k] / length;
			}
			this.finalDocTopicWeights[this.localDocIds.get(d.getId())] = VectorUtils.normalize(this.finalDocTopicWeights[this.localDocIds.get(d.getId())]);
		}
		
		double vbeta = (double)this.sizeOfVocabulary*this.props.beta;
		Collection<WordType> types = this.corpus.getAllWordTypes();
		for(int k=0;k<this.K;++k) {
			for(WordType wt : types) {
				int typeId = this.localWordTypeIds.get(wt.getId());
				this.finalTopicWordWeights[k][typeId] = ((double)this.wordCountByTopicAndTermMode[k][typeId] + this.props.beta)/((double)this.wordCountByTopicMode[k] + vbeta);
			}
			this.finalTopicWordWeights[k] = VectorUtils.normalize(this.finalTopicWordWeights[k]);
		}
		
	}


	/**
	 * Removes a word from the bookkeeping
	 * 
	 * @param docID the id of the document the word belongs to 
	 * @param i the index of the word
	 */
	protected void removeWord(Document doc, WordToken wt){
		int table = wt.<WordState>getState().getTableAssignment();
		int k = doc.<DOCState>getState().getTableToTopic()[table];
		doc.<DOCState>getState().decrementTableWordCount(table); 
		this.wordCountByTopic[k]--; 		
		this.wordCountByTopicAndTerm[k][this.localWordTypeIds.get(wt.getTypeId())]--;
		if (doc.<DOCState>getState().getWordCountByTable()[table] == 0) { // table is removed
			this.totalNumberOfTables--; 
			this.numberOfTablesByTopic[k]--; 
			doc.<DOCState>getState().decrementTableToTopic(table); 
		}
	}
	
	
	
	/**
	 * Add a word to the bookkeeping
	 * 
	 * @param docID	docID the id of the document the word belongs to 
	 * @param i the index of the word
	 * @param table the table to which the word is assigned to
	 * @param k the topic to which the word is assigned to
	 */
	protected void addWord(Document doc, WordToken wt, int table, int k) {
		wt.<WordState>getState().setTableAssignment(table);
		doc.<DOCState>getState().incrementTableWordCount(table); 
		this.wordCountByTopic[k]++; 
		this.wordCountByTopicAndTerm[k][this.localWordTypeIds.get(wt.getTypeId())] ++;
		if (doc.<DOCState>getState().getWordCountByTable()[table] == 1) { // a new table is created
			doc.<DOCState>getState().incrementNumberOfTables();
			doc.<DOCState>getState().setTableToTopic(table, k);
			this.totalNumberOfTables++;
			this.numberOfTablesByTopic[k]++; 
			doc.<DOCState>getState().ensureTableToTopicCapacity(doc.<DOCState>getState().getNumberOfTables());
			doc.<DOCState>getState().ensureTableWordCountCapacity(doc.<DOCState>getState().getNumberOfTables());
			if (k == this.K) { // a new topic is created
				this.K++; 
				this.numberOfTablesByTopic = ArrayUtils.ensureCapacity(this.numberOfTablesByTopic, this.K); 
				this.wordCountByTopic = ArrayUtils.ensureCapacity(this.wordCountByTopic, this.K);
				this.wordCountByTopicAndTerm = ArrayUtils.add(this.wordCountByTopicAndTerm, new int[this.sizeOfVocabulary], this.K);
			}
		}
	}

	
	/**
	 * Removes topics from the bookkeeping that have no words assigned to
	 */
	protected void defragment() {
		int[] kOldToKNew = new int[this.K];
		int k, newNumberOfTopics = 0;
		for (k = 0; k < this.K; k++) {
			if (this.wordCountByTopic[k] > 0) {
				kOldToKNew[k] = newNumberOfTopics;
				ArrayUtils.swap(this.wordCountByTopic, newNumberOfTopics, k);
				ArrayUtils.swap(this.numberOfTablesByTopic, newNumberOfTopics, k);
				ArrayUtils.swap(this.wordCountByTopicAndTerm, newNumberOfTopics, k);
				newNumberOfTopics++;
			} 
		}
		this.K = newNumberOfTopics;
		for (Document d : this.docs) {
//			docs.get(j).defragment(kOldToKNew);
		    int numberOfTables = d.<DOCState>getState().getNumberOfTables();
			int[] tOldToTNew = new int[numberOfTables];
		    int t, newNumberOfTables = 0;
		    for (t = 0; t < numberOfTables; t++){
		        if (d.<DOCState>getState().getWordCountByTable()[t] > 0){
		            tOldToTNew[t] = newNumberOfTables;
		            d.<DOCState>getState().setTableToTopic(newNumberOfTables, kOldToKNew[d.<DOCState>getState().getTableToTopic()[t]]);
		            ArrayUtils.swap(d.<DOCState>getState().getWordCountByTable(), newNumberOfTables, t);
		            newNumberOfTables ++;
		        } else 
		        	d.<DOCState>getState().setTableToTopic(t, -1);
		    }
		    d.<DOCState>getState().setNumberOfTables(newNumberOfTables);
		    for (WordToken w : d.getWords())
		        w.<WordState>getState().setTableAssignment(tOldToTNew[w.<WordState>getState().getTableAssignment()]);
		}
	}
	
	
	/**
	 * Permute the ordering of documents and words in the bookkeeping
	 */
	protected void doShuffle(){
		Collections.shuffle(this.docs);
		for (int j = 0; j < this.docs.size(); j ++){
			Collections.shuffle(this.docs.get(j).getWords());
		}
	}
	
	
	private void writeTopicCounts() throws FileNotFoundException {
		PrintStream file = new PrintStream(this.props.targetDir + File.separator + "topicCounts.dat");
		for (int k = 0; k < this.numberOfTopicsMode; k++) {
			for (int w = 0; w < this.sizeOfVocabulary; w++)
				file.format("%05d ", this.wordCountByTopicAndTermMode[k][w]);
			file.println();
		}
		file.close();
	}
	
	private void writeAssignments() throws FileNotFoundException {
		PrintStream file = new PrintStream(this.props.targetDir + File.separator + "assignments.dat");
		file.println("d w z t");
		int t;
		for (Integer docId : this.docStateMode.keySet()) {
			Document doc = this.corpus.getDocumentById(docId);
			DOCState modeState = this.docStateMode.get(docId);
			int[] tabAssignments = this.docWordTableAssignmentMode.get(docId);
			for (int i = 0; i < doc.getLength(); i++) {
				t = tabAssignments[i];
				file.println(docId + " " + doc.getWords().get(i).getTypeId() + " " + modeState.getTableToTopic()[t] + " " + t); 
			}
		}
		file.close();
	}


	@Override
	public void storeModel() throws IOException {
		this.K = this.numberOfTopicsMode;
		super.storeModel();
		writeTopicCounts();
		writeAssignments();
	}


	@Override
	public void loadModel(String filename) throws IOException {
		// TODO Auto-generated method stub
		
	}


	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericStaticInferencer#doPrediction(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document, double[])
	 */
	@Override
	public double doPrediction(Document d, double[] topicPredictions) {
		double score = 0;
		//state init
		d.setState(new DOCState());
		for(WordToken w : d.getWords())
			w.setState(new WordState(-1));
		
		//random topic assignment init
		//for each word
		for(WordToken wt : d.getWords()) {
			//sample a table
			int table = sampleTable(d, wt);
			//book keeping in token and doc state
			wt.<WordState>getState().setTableAssignment(table);
			d.<DOCState>getState().incrementTableWordCount(table);
			
			if(d.<DOCState>getState().getWordCountByTable()[table] == 1) { //new table
				int topic = this.rng.nextInt(this.K); //random topic
				//book keeping in doc state
				d.<DOCState>getState().incrementNumberOfTables();
				d.<DOCState>getState().setTableToTopic(table, topic);
				d.<DOCState>getState().ensureTableToTopicCapacity(d.<DOCState>getState().getNumberOfTables());
				d.<DOCState>getState().ensureTableWordCountCapacity(d.<DOCState>getState().getNumberOfTables());
			}
		}
		//now do the sampling
		double oldScore = 1e-100;
		double converge = 1;
		for(int iter=0;iter<this.props.iterations;iter++) {
			for(WordToken wt : d.getWords()) {
				//remove current word from token and doc state
				//find current table of token
				int table = wt.<WordState>getState().getTableAssignment();
				//find topic assigned to that table
				int k = d.<DOCState>getState().getTableToTopic()[table];
				//remove token from table
				d.<DOCState>getState().decrementTableWordCount(table);
				//decrement table count if empty afterwards
				if (d.<DOCState>getState().getWordCountByTable()[table] == 0) { // table is removed
					d.<DOCState>getState().decrementTableToTopic(table); 
					d.<DOCState>getState().decrementNumberOfTables();
				}
				//sample new table for token
				table = sampleTable(d, wt);

				if (table == d.<DOCState>getState().getNumberOfTables()) { // new Table
					k = sampleTopic(); //sample topic for newly created table
//					d.<DOCState>getState().incrementNumberOfTables();
//					d.<DOCState>getState().setTableToTopic(table, k);
//					d.<DOCState>getState().ensureTableToTopicCapacity(d.<DOCState>getState().getNumberOfTables());
//					d.<DOCState>getState().ensureTableWordCountCapacity(d.<DOCState>getState().getNumberOfTables());
				} else
					k = d.<DOCState>getState().getTableToTopic()[table];

				//add new assignment to doc and token book keeping
				//assign to table
				wt.<WordState>getState().setTableAssignment(table);
				//increment #words for that table
				d.<DOCState>getState().incrementTableWordCount(table); 
				if (d.<DOCState>getState().getWordCountByTable()[table] == 1) { // a new table is created
					//increment table count for document
					d.<DOCState>getState().incrementNumberOfTables();
					//assign topic to table
					d.<DOCState>getState().setTableToTopic(table, k);
					//ensure capacity of underlying arrays
					d.<DOCState>getState().ensureTableToTopicCapacity(d.<DOCState>getState().getNumberOfTables());
					d.<DOCState>getState().ensureTableWordCountCapacity(d.<DOCState>getState().getNumberOfTables());
				}
			}
//			d.<DOCState>getState().defragment();
			score = docLikelihood(d);
			converge = (oldScore - score) / oldScore;
			oldScore = score;
			if(iter > 10 && converge < this.props.convergenceCriterion)
				break;
		}
		topicPredictions = VectorUtils.normalize(d.<DOCState>getState().getWordCountsByTopic(this.K));
		return score;
	}
	
	
		
}
