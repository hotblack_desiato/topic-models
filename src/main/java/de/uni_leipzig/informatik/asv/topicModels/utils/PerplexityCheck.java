/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.topicModels.utils;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordToken;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericStaticInferencer;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class PerplexityCheck {
	
	private AbstractGenericStaticInferencer model;
	
	public PerplexityCheck(AbstractGenericStaticInferencer _model) {
		this.model = _model;
	}
	
	public double computeDocumentCompletionLikelihood() {
		double ll = 0, wordCount = 0;
		//work on the test corpus that has been created by dividing original source documents
		for(Document d : model.getTestCorpus().getDocuments()) {
			int docId = model.getLocalDocIds().get(d.getSecondaryId());
			for(WordToken wt : d.getWords()) {
				int typeId = model.getLocalWordTypeIds().get(wt.getTypeId());
				double[] p_z = VectorUtils.multiply(model.getFinalDocTopicWeights()[docId], VectorUtils.viewColumn(model.getFinalTopicWordWeights(), typeId));
				ll += Math.log(VectorUtils.sum(p_z));
			}
			wordCount += d.getLength();
		}
		return (ll/wordCount);
	}
	
	public double computeValidationPerplexity() {
		double ll = 0;
		double[] topicProportions = new double[model.getK()];
		double wordCount = 0;
		for(Document d : model.getValidationCorpus().getDocuments()) {
			ll += model.doPrediction(d, topicProportions);
			wordCount += d.getLength();
		}
		return Math.exp(-(ll/wordCount));
	}
}
