/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.dtm.state;

import java.util.Arrays;

public class LDAModelState {
	
	private int numTopics;
	private int numTypes;
	private double[][] topics;
	private double[] alpha;
	
	public LDAModelState(int numTopics, int numTypes) {
		this.numTopics = numTopics;
		this.numTypes = numTypes;
		topics = new double[numTopics][numTypes];
		alpha = new double[numTopics];
	}

	public void setAllAlpha(double _alpha) {
		Arrays.fill(alpha, _alpha);
	}
	
	public int getNumTopics() {
		return numTopics;
	}

	public int getNumTypes() {
		return numTypes;
	}

	public double[][] getTopics() {
		return topics;
	}

	public double[] getAlpha() {
		return alpha;
	}
	
	
	
}
