/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.cdtm.state;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;
import de.uni_leipzig.informatik.asv.topicModels.model.cdtm.ContinuousTimeDynamicTopicModel;
import de.uni_leipzig.informatik.asv.topicModels.model.cdtm.state.Params.Pos;

public class OptFunction extends ObjectiveFunction {

	private boolean accurate = true;

	public OptFunction(Params p) {
		super(p.getOptSize(), p);
	}
	
	public double f(double[] x) {
		Params p = getParameters();
		ContinuousTimeDynamicTopicModel model = p.getModel();
		
		int segStart = p.getSegmentStart();
		int segEnd = p.getSegmentEnd();
		double initMean = p.getInitMean();
		
		double[] forwardMean = p.getForwardMean();
		double[] betaHat = p.getBetaHat();
		double[] forwardVariance = p.getForwardVariance();
		double[] backwardVariance = p.getBackwardVariance();
		double[] Pt = p.getPt();
		
		int k = p.getK();
		int w = p.getW();
		Pos pos = p.getPos();
		double f = 0;
		int s=0, t, tMinus, tPlus;
		double delta;
		
	    for (s = segStart; s < segEnd; s ++) {
	        t = model.getSparseTimes()[w][s];
	        if(accurate ) {
		        //update getBeta() and getZeta()
		        model.getZeta()[k][t] = MathUtils.logSubtract(model.getZeta()[k][t], model.getBeta()[k][w][s]+backwardVariance[s]/2.0);
		        model.getBeta()[k][w][s] = x[s-segStart];
		        model.getZeta()[k][t] = MathUtils.logAdd(model.getZeta()[k][t], model.getBeta()[k][w][s]+backwardVariance[s]/2.0);
		        
		        f += model.getStatsKWT()[k][w][s] * model.getBeta()[k][w][s] - model.getStatsKT()[k][t] * model.getZeta()[k][t];
	        } else {
		        model.getBeta()[k][w][s] = x[s-segStart];
		        f += model.getStatsKWT()[k][w][s] * model.getBeta()[k][w][s];
	        }
	    }
	    forwardMean[segEnd-1] = model.getBeta()[k][w][segEnd-1];
	    for (s = segEnd-1; s > segStart; s --) {
	        t = model.getSparseTimes()[w][s];
	        tMinus = model.getSparseTimes()[w][s-1];
	        delta = model.getVar()*(model.getTimes()[t] - model.getTimes()[tMinus]);
	        forwardMean[s-1] = (model.getBeta()[k][w][s-1]*Pt[s] - model.getBeta()[k][w][s]*forwardVariance[s-1])/delta;
	        betaHat[s] = ((Pt[s]+model.getVarHat())*forwardMean[s] - model.getVarHat()*forwardMean[s-1])/Pt[s];
	    }
	    s = segStart;
	    betaHat[s] = ((Pt[s]+model.getVarHat())*forwardMean[s] - model.getVarHat()*initMean)/Pt[s];
	    
	    for (s = segStart; s < segEnd; s ++) {
	        if (s == segStart) {
	            f += -0.5*Math.pow(betaHat[s]-initMean, 2.0)/(Pt[s]+model.getVarHat());
	        }
	        else {
	            f += -0.5*Math.pow(betaHat[s]-forwardMean[s-1], 2.0)/(Pt[s]+model.getVarHat());
	        }
	        f += 0.5 *Math.pow(betaHat[s]-model.getBeta()[k][w][s], 2.0)/model.getVarHat();
	    }
	    //take care the middle parts between the segments.
	    //most complicate parts
	    
	    double m_hat, v;
	    
	    if (pos == Pos.FIRST || pos == Pos.MIDDLE) {
	        t = model.getSparseTimes()[w][segEnd-1];
	        if (t+1 == model.getSparseTimes()[w][segEnd]) {
	            delta = model.getVar() * (model.getTimes()[t+1]-model.getTimes()[t]);
	            f += -0.5 * Math.pow(model.getBeta()[k][w][segEnd-1]-model.getBeta()[k][w][segEnd], 2.0)/delta;
	        }
	        else {
	            tPlus = model.getSparseTimes()[w][segEnd];
	            delta = model.getVar() * (model.getTimes()[tPlus]-model.getTimes()[t+1]);
	            v = forwardVariance[segEnd-1] + model.getVar() * (model.getTimes()[t+1] - model.getTimes()[t]);
	            m_hat = (model.getBeta()[k][w][segEnd-1]*delta + model.getBeta()[k][w][segEnd]*v)/Pt[segEnd];
	            delta = model.getVar() * (model.getTimes()[t+1]-model.getTimes()[t]);
	            f += -0.5 * Math.pow(model.getBeta()[k][w][segEnd-1]-m_hat, 2.0)/delta;
	        }
	    }
	    if (pos == Pos.LAST || pos == Pos.MIDDLE) {
	        t = model.getSparseTimes()[w][segStart];
	        if (t == model.getSparseTimes()[w][segStart-1]+1) {
	            delta = model.getVar() * (model.getTimes()[t] - model.getTimes()[t-1]);
	            f += - 0.5 * Math.pow(model.getBeta()[k][w][segStart]-model.getBeta()[k][w][segStart-1], 2.0)/delta;
	        }
	        else {
	            tMinus = model.getSparseTimes()[w][segStart-1]+1;
	            delta = model.getVar() *(model.getTimes()[t]-model.getTimes()[tMinus]);
	            v = forwardVariance[segStart-1] + model.getVar() *(model.getTimes()[tMinus]-model.getTimes()[tMinus-1]);
	            m_hat = (model.getBeta()[k][w][segStart-1]*delta + model.getBeta()[k][w][segStart]* v)/Pt[segStart];
	            delta = model.getVar() *(model.getTimes()[tMinus]-model.getTimes()[tMinus-1]);
	            f += -0.5 * Math.pow(m_hat-model.getBeta()[k][w][segStart-1], 2.0)/delta;
	        }
	    }

		
		return -f;
	}

	public void df(double[] x, double[] df) {
		Params p = getParameters();
		ContinuousTimeDynamicTopicModel model = p.getModel();
		
		int segStart = p.getSegmentStart();
		int segEnd = p.getSegmentEnd();
		double initMean = p.getInitMean();
		
		double[] forwardMean = p.getForwardMean();
		double[] betaHat = p.getBetaHat();
		double[] forwardVariance = p.getForwardVariance();
		double[] backwardVariance = p.getBackwardVariance();
		double[] Pt = p.getPt();
		double[] gradient = p.getGradient();
		
		int k = p.getK();
		int w = p.getW();
		Pos pos = p.getPos();
		int s=0, t, tMinus, tPlus;
		double delta;
		
	    for (s = segStart; s < segEnd; s ++) {
	        t = model.getSparseTimes()[w][s];
	        gradient[s] = 0.0;
			if(accurate )
		        gradient[s] += model.getStatsKWT()[k][w][s]
		        - model.getStatsKT()[k][t]*Math.exp(model.getBeta()[k][w][s]+backwardVariance[s]/2.0 - model.getZeta()[k][t]);
	        else {
	        	gradient[s] += model.getStatsKWT()[k][w][s];
	        }
	    }
	    forwardMean[segEnd-1] = model.getBeta()[k][w][segEnd-1];
	    for (s = segEnd-1; s > segStart; s --) {
	        t = model.getSparseTimes()[w][s];
	        tMinus = model.getSparseTimes()[w][s-1];
	        delta = model.getVar()*(model.getTimes()[t] - model.getTimes()[tMinus]);
	        forwardMean[s-1] = (model.getBeta()[k][w][s-1]*Pt[s] - model.getBeta()[k][w][s]*forwardVariance[s-1])/delta;
	        betaHat[s] = ((Pt[s]+model.getVarHat())*forwardMean[s] - model.getVarHat()*forwardMean[s-1])/Pt[s];
	    }
	    s = segStart;
	    betaHat[s] = ((Pt[s]+model.getVarHat())*forwardMean[s] - model.getVarHat()*initMean)/Pt[s];
	    
	    double gr = 0;
	    for (s = segStart; s < segEnd; s ++) {
	        if (s == segStart) {
	            gr = -(betaHat[s]-initMean)/Pt[s];
	            if (s == segEnd-1) {
	                gradient[s] += gr;
	            }
	            else {
	                delta = model.getVar()*(model.getSparseTimes()[w][s+1]-model.getSparseTimes()[w][s]);
	                gradient[s] += gr * Pt[s+1]/delta;
	                gradient[s+1] += gr *(-forwardVariance[s])/delta;
	            }
	        }
	        else if (s == segEnd-1) {
	            gr = -(betaHat[s]-forwardMean[s-1])/Pt[s];
	            gradient[s] += gr;
	            delta = model.getVar()*(model.getSparseTimes()[w][s]-model.getSparseTimes()[w][s-1]);
	            gradient[s] += gr * forwardVariance[s-1]/delta;
	            gradient[s-1] += gr * (-Pt[s])/delta;
	        }else {
	            gr = -(betaHat[s]-forwardMean[s-1])/Pt[s];
	            delta = model.getVar()*(model.getSparseTimes()[w][s+1]-model.getSparseTimes()[w][s]);
	            gradient[s] += gr * Pt[s+1]/delta;
	            gradient[s+1] += gr *(-forwardVariance[s])/delta;
	            delta = model.getVar()*(model.getSparseTimes()[w][s]-model.getSparseTimes()[w][s-1]);
	            gradient[s] += gr * forwardVariance[s-1]/delta;
	            gradient[s-1] += gr * (-Pt[s])/delta;
	        }
	    }
	    
	    for (s = segStart; s < segEnd; s ++) {
	        if (s == segStart) {
	            gr = (betaHat[s]-model.getBeta()[k][w][s])/model.getVarHat();
	            if (s == segEnd-1) {
	                gradient[s] += gr * model.getVarHat() / Pt[s];
	            }
	            else {
	                delta = model.getVar()*(model.getSparseTimes()[w][s+1]-model.getSparseTimes()[w][s]);
	                gradient[s] += gr * ((Pt[s+1]/delta) * (Pt[s]+model.getVarHat())/Pt[s]-1);
	                gradient[s+1] += gr * (-forwardVariance[s]/delta) * (Pt[s]+model.getVarHat())/Pt[s];
	            }
	        }
	        else if (s == segEnd-1) {
	            gr = (betaHat[s]-model.getBeta()[k][w][s])/model.getVarHat();
	            gradient[s] += gr * model.getVarHat() / Pt[s];
	            delta = model.getVar() * (model.getSparseTimes()[w][s]-model.getSparseTimes()[w][s-1]);
	            gradient[s] += gr * (forwardVariance[s-1]/delta) * (model.getVarHat()/Pt[s]);
	            gradient[s-1] += gr * (-Pt[s]/delta) * (model.getVarHat()/Pt[s]);
	        }else {
	            gr = (betaHat[s]-model.getBeta()[k][w][s])/model.getVarHat();
	            delta = model.getVar()*(model.getSparseTimes()[w][s+1]-model.getSparseTimes()[w][s]);
	            gradient[s]   += gr * ((Pt[s+1]/delta) * (Pt[s]+model.getVarHat())/Pt[s]-1);
	            gradient[s+1] += gr * (-forwardVariance[s]/delta) * (Pt[s]+model.getVarHat())/Pt[s];
	            
	            delta = model.getVar()*(model.getSparseTimes()[w][s]-model.getSparseTimes()[w][s-1]);
	            gradient[s] += gr * (forwardVariance[s-1]/delta) * (model.getVarHat()/Pt[s]);
	            gradient[s-1] += gr * (-Pt[s]/delta) * (model.getVarHat()/Pt[s]);
	        }
	    }
	    
	    
	    double m_hat, v;
	    if (pos == Pos.FIRST || pos == Pos.MIDDLE) {
	        t = model.getSparseTimes()[w][segEnd-1];
	        if (t+1 == model.getSparseTimes()[w][segEnd]) {
	            delta = model.getVar() * (model.getTimes()[t+1]-model.getTimes()[t]);
	            gradient[segEnd-1] += -(model.getBeta()[k][w][segEnd-1]-model.getBeta()[k][w][segEnd])/delta;
	        }
	        else {
	            tPlus = model.getSparseTimes()[w][segEnd];
	            delta = model.getVar() * (model.getTimes()[tPlus]-model.getTimes()[t+1]);
	            v = forwardVariance[segEnd-1] + model.getVar() * (model.getTimes()[t+1] - model.getTimes()[t]);
	            m_hat = (model.getBeta()[k][w][segEnd-1]*delta + model.getBeta()[k][w][segEnd]*v)/Pt[segEnd];
	            delta = model.getVar() * (model.getTimes()[t+1]-model.getTimes()[t]);
	            gradient[segEnd-1] += -(model.getBeta()[k][w][segEnd-1]-m_hat)/delta;
	        }
	    }
	    if (pos == Pos.LAST || pos == Pos.MIDDLE) {
	        t = model.getSparseTimes()[w][segStart];
	        if (t == model.getSparseTimes()[w][segStart-1]+1) {
	            delta = model.getVar() * (model.getTimes()[t] - model.getTimes()[t-1]);
	            gradient[segStart] += - (model.getBeta()[k][w][segStart]-model.getBeta()[k][w][segStart-1])/delta;
	        }
	        else {
	            tMinus = model.getSparseTimes()[w][segStart-1]+1;
	            delta = model.getVar() *(model.getTimes()[t]-model.getTimes()[tMinus]);
	            v = forwardVariance[segStart-1] + model.getVar() *(model.getTimes()[tMinus]-model.getTimes()[tMinus-1]);
	            m_hat = (model.getBeta()[k][w][segStart-1]*delta + model.getBeta()[k][w][segStart]* v)/Pt[segStart];
	            delta = model.getVar() *(model.getTimes()[tMinus]-model.getTimes()[tMinus-1]);
	            gradient[segStart] += -(v/Pt[segStart])*(m_hat-model.getBeta()[k][w][segStart-1])/delta;
	        }
	    }
	    
	    for (s = segStart; s < segEnd; s ++) {
	        df[s-segStart] = -gradient[s];
	    }
	}
	public double fdf(double[] x, double[] df) {
		Params p = getParameters();
		ContinuousTimeDynamicTopicModel model = p.getModel();
		int segStart = p.getSegmentStart();
		int segEnd = p.getSegmentEnd();
		
		double initMean = p.getInitMean();
		
		double[] forwardMean = p.getForwardMean();
		double[] forwardVariance = p.getForwardVariance();
		double[] betaHat = p.getBetaHat();
		double[] backwardVariance = p.getBackwardVariance();
		double[] Pt = p.getPt();
		
		double[] gradient = p.getGradient();
		
		int k = p.getK();
		int w = p.getW();
		Pos pos = p.getPos();
		double f = 0;
		int s=0, t, tMinus, tPlus;
		double delta;
	    for (s = segStart; s < segEnd; s ++) {
	        t = model.getSparseTimes()[w][s];
	        gradient[s] = 0.0;
	        if(accurate) {
		        //update getBeta() and getZeta()
		        model.getZeta()[k][t] = MathUtils.logSubtract(model.getZeta()[k][t], model.getBeta()[k][w][s]+backwardVariance[s]/2.0);
		        model.getBeta()[k][w][s] = x[s-segStart];
		        model.getZeta()[k][t] = MathUtils.logAdd(model.getZeta()[k][t], model.getBeta()[k][w][s]+backwardVariance[s]/2.0);
		        
		        f += model.getStatsKWT()[k][w][s] * model.getBeta()[k][w][s] - model.getStatsKT()[k][t] * model.getZeta()[k][t];
		        
		        gradient[s] += model.getStatsKWT()[k][w][s]
		        - model.getStatsKT()[k][t]*Math.exp(model.getBeta()[k][w][s]+backwardVariance[s]/2.0 - model.getZeta()[k][t]);
	        } else {
		        model.getBeta()[k][w][s] = x[s-segStart];
		        f += model.getStatsKWT()[k][w][s] * model.getBeta()[k][w][s];
		        gradient[s] += model.getStatsKWT()[k][w][s];
	        }
	    }
	    forwardMean[segEnd-1] = model.getBeta()[k][w][segEnd-1];
	    for (s = segEnd-1; s > segStart; s --) {
	        t = model.getSparseTimes()[w][s];
	        tMinus = model.getSparseTimes()[w][s-1];
	        delta = model.getVar()*(model.getTimes()[t] - model.getTimes()[tMinus]);
	        forwardMean[s-1] = (model.getBeta()[k][w][s-1]*Pt[s] - model.getBeta()[k][w][s]*forwardVariance[s-1])/delta;
	        betaHat[s] = ((Pt[s]+model.getVarHat())*forwardMean[s] - model.getVarHat()*forwardMean[s-1])/Pt[s];
	    }
	    s = segStart;
	    betaHat[s] =  ((Pt[s]+model.getVarHat())*forwardMean[s] - model.getVarHat()*initMean)/Pt[s];
	    
	    for (s = segStart; s < segEnd; s ++) {
	        if (s == segStart) {
	            f += -0.5*Math.pow(betaHat[s]-initMean, 2.0)/(Pt[s]+model.getVarHat());
	        }
	        else {
	            f += -0.5*Math.pow(betaHat[s]-forwardMean[s-1], 2.0)/(Pt[s]+model.getVarHat());
	        }
	        f += 0.5 * Math.pow(betaHat[s]-model.getBeta()[k][w][s], 2.0)/model.getVarHat();
	    }
	    
	    double gr = 0;
	    for (s = segStart; s < segEnd; s ++) {
	        if (s == segStart) {
	            gr = -(betaHat[s]-initMean)/Pt[s];
	            if (s == segEnd-1) {
	                gradient[s] += gr;
	            }
	            else {
	                delta = model.getVar()*(model.getSparseTimes()[w][s+1]-model.getSparseTimes()[w][s]);
	                gradient[s] += gr * Pt[s+1]/delta;
	                gradient[s+1] += gr *(-forwardVariance[s])/delta;
	            }
	        }
	        else if (s == segEnd-1) {
	            gr = -(betaHat[s]-forwardMean[s-1])/Pt[s];
	            gradient[s] += gr;
	            delta = model.getVar()*(model.getSparseTimes()[w][s]-model.getSparseTimes()[w][s-1]);
	            gradient[s] += gr * forwardVariance[s-1]/delta;
	            gradient[s-1] += gr * (-Pt[s])/delta;
	        }else {
	            gr = -(betaHat[s]-forwardMean[s-1])/Pt[s];
	            delta = model.getVar()*(model.getSparseTimes()[w][s+1]-model.getSparseTimes()[w][s]);
	            gradient[s] += gr * Pt[s+1]/delta;
	            gradient[s+1] += gr *(-forwardVariance[s])/delta;
	            delta = model.getVar()*(model.getSparseTimes()[w][s]-model.getSparseTimes()[w][s-1]);
	            gradient[s] += gr * forwardVariance[s-1]/delta;
	            gradient[s-1] += gr * (-Pt[s])/delta;
	        }
	    }
	    
	    for (s = segStart; s < segEnd; s ++) {
	        if (s == segStart) {
	            gr = (betaHat[s]-model.getBeta()[k][w][s])/model.getVarHat();
	            if (s == segEnd-1) {
	                gradient[s] += gr * model.getVarHat() / Pt[s];
	            }
	            else {
	                delta = model.getVar()*(model.getSparseTimes()[w][s+1]-model.getSparseTimes()[w][s]);
	                gradient[s] += gr * ((Pt[s+1]/delta) * (Pt[s]+model.getVarHat())/Pt[s]-1);
	                gradient[s+1] += gr * ((-forwardVariance[s])/delta) * (Pt[s]+model.getVarHat())/Pt[s];
	            }
	        }
	        else if (s == segEnd-1) {
	            gr = (betaHat[s]-model.getBeta()[k][w][s])/model.getVarHat();
	            gradient[s] += gr * model.getVarHat() / Pt[s];
	            delta = model.getVar() * (model.getSparseTimes()[w][s]-model.getSparseTimes()[w][s-1]);
	            gradient[s] += gr * (forwardVariance[s-1]/delta) * (model.getVarHat()/Pt[s]);
	            gradient[s-1] += gr * ((-Pt[s])/delta) * (model.getVarHat()/Pt[s]);
	        }else {
	            gr = (betaHat[s]-model.getBeta()[k][w][s])/model.getVarHat();
	            delta = model.getVar()*(model.getSparseTimes()[w][s+1]-model.getSparseTimes()[w][s]);
	            gradient[s] += gr * ((Pt[s+1]/delta) * (Pt[s]+model.getVarHat())/Pt[s]-1);
	            gradient[s+1] += gr * ((-forwardVariance[s])/delta) * (Pt[s]+model.getVarHat())/Pt[s];;
	            
	            delta = model.getVar()*(model.getSparseTimes()[w][s]-model.getSparseTimes()[w][s-1]);
	            gradient[s] += gr * (forwardVariance[s-1]/delta) * (model.getVarHat()/Pt[s]);
	            gradient[s-1] += gr * ((-Pt[s])/delta) * (model.getVarHat()/Pt[s]);
	        }
	    }
	    
	    
	    double m_hat, v;
	    if (pos == Pos.FIRST || pos == Pos.MIDDLE) {
	        t = model.getSparseTimes()[w][segEnd-1];
	        if (t+1 == model.getSparseTimes()[w][segEnd]) {
	            delta = model.getVar() * (model.getTimes()[t+1]-model.getTimes()[t]);
	            f += -0.5 * Math.pow(model.getBeta()[k][w][segEnd-1]-model.getBeta()[k][w][segEnd], 2.0)/delta;
	            gradient[segEnd-1] += -(model.getBeta()[k][w][segEnd-1]-model.getBeta()[k][w][segEnd])/delta;
	        }
	        else {
	            tPlus = model.getSparseTimes()[w][segEnd];
	            delta = model.getVar() * (model.getTimes()[tPlus]-model.getTimes()[t+1]);
	            v = forwardVariance[segEnd-1] + model.getVar() * (model.getTimes()[t+1] - model.getTimes()[t]);
	            m_hat = (model.getBeta()[k][w][segEnd-1]*delta + model.getBeta()[k][w][segEnd]*v)/Pt[segEnd];
	            delta = model.getVar() * (model.getTimes()[t+1]-model.getTimes()[t]);
	            f += -0.5 * Math.pow(model.getBeta()[k][w][segEnd-1]-m_hat, 2.0)/delta;
	            gradient[segEnd-1] += -(model.getBeta()[k][w][segEnd-1]-m_hat)/delta;
	        }
	    }
	    if (pos == Pos.LAST || pos == Pos.MIDDLE) {
	        t = model.getSparseTimes()[w][segStart];
	        if (t == model.getSparseTimes()[w][segStart-1]+1) {
	            delta = model.getVar() * (model.getTimes()[t] - model.getTimes()[t-1]);
	            f += - 0.5 * Math.pow(model.getBeta()[k][w][segStart]-model.getBeta()[k][w][segStart-1], 2.0)/delta;
	            gradient[segStart] += - (model.getBeta()[k][w][segStart]-model.getBeta()[k][w][segStart-1])/delta;
	        }
	        else {
	            tMinus = model.getSparseTimes()[w][segStart-1]+1;
	            delta = model.getVar() *(model.getTimes()[t]-model.getTimes()[tMinus]);
	            v = forwardVariance[segStart-1] + model.getVar() *(model.getTimes()[tMinus]-model.getTimes()[tMinus-1]);
	            m_hat = (model.getBeta()[k][w][segStart-1]*delta + model.getBeta()[k][w][segStart]* v)/Pt[segStart];
	            delta = model.getVar() *(model.getTimes()[tMinus]-model.getTimes()[tMinus-1]);
	            f += -0.5 * Math.pow(m_hat-model.getBeta()[k][w][segStart-1], 2.0)/delta;
	            gradient[segStart] += -(v/Pt[segStart])*(m_hat-model.getBeta()[k][w][segStart-1])/delta;
	        }
	    }
	    
	    for (s = segStart; s < segEnd; s ++) {
	    	df[s-segStart] = -gradient[s];
	    }
	    return -f;
	}


}
