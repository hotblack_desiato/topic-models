/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.commons.lang3.ArrayUtils;

import cern.colt.map.OpenIntIntHashMap;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicCorpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Dictionary;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.Type;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;
import de.uni_leipzig.informatik.asv.topicModels.utils.WordProb;

public abstract class AbstractGenericDynamicInferencer extends	AbstractGenericInferencer<DiachronicCorpus> implements Serializable, GenericDynamicInferencer{
	
	protected enum Dynamics {
			BROWNIAN_MOTION,
			ORNSTEIN_UHLENBECK,
			RBF,
			PERIODIC
	}
	
	public enum CovarianceModel {
		SINGLE,
		ADDITIVE,
		ADDITIVE_WEIGHTED,
		MULTIPLICATIVE;
	}



	/**
	 * final word probabilities in topic k at time t, TxKxW 
	 */
	protected double[][][] finalTopicWordWeights;
	/**
	 * final document topic weights for topic k at time t in document d, TxDxK
	 */
	protected double[][][] finalDocTopicWeights;
	
	protected Date[] dates;
	
	protected double[][] timeLocations;
	
	protected int T;
	
	protected int[] M;
	
	protected int[][] nonzeroFrequencyTimes;
	
	protected int[][] zeroFrequencyTimes;
	
	private double timeScale = 1;
	
	private double timeshift = 0;
	protected double maxScale;
	
	protected AbstractGenericDynamicInferencer(DiachronicCorpus c, DiachronicCorpus test, DiachronicCorpus validation, InferencerProperties props) throws IOException {
		super(c, test, validation, props);
		T = corpus.getNumberOfDates();
		M = new int[T];
		finalDocTopicWeights = new double[T][][];
		finalTopicWordWeights = ArrayFactory.doubleArray(Type.ZERO, T, K, V);

		
		HashMap<WordType, HashSet<Integer>> foundDatesForWords = new HashMap<>();
		for(WordType wt : corpus.getAllWordTypes())
			foundDatesForWords.put(wt, new HashSet<Integer>());
		
		//try different time transformations
		dates = corpus.getUsedDates().toArray(new Date[]{});
		timeLocations = new double[T][1];
		switch (props.granularity) {
		case DAILY:
			timeScale = 1000d*3600d*24d; //this scales to days
			break;
		case MONTHLY:
			timeScale = 1000d*3600d*24d*30d; //this scales to months
			break;
		case YEARLY:
			timeScale = 1000d*3600d*24d*30d*365d; //this scales to years
			break;
		default:
			break;
		}
		if(dates[0].getTime() < 0)
			timeshift = dates[0].getTime() - 1;

		for(int t=0;t<T;t++) {

			timeLocations[t][0] = (dates[t].getTime() - timeshift)/timeScale;
			
			//handle documents
			M[t] = corpus.getNumberOfDocumentsForDate(dates[t]);
			
			finalDocTopicWeights[t] = ArrayFactory.doubleArray(Type.ZERO, M[t], K);
			Set<Integer> docIds = corpus.getDocumentsForDate(dates[t]);
			int curTimeDocIdx = 0;
			for(Integer docId : docIds) {
				localDocIds.put(docId, curTimeDocIdx++);
				for(int typeId : corpus.getDocumentById(docId).getTypeList()) {
					WordType wt = corpus.getWordType(typeId);
					foundDatesForWords.get(wt).add(t);
				}
			}
		}
		maxScale = 1./(timeLocations[T-1][0] - timeLocations[0][0]);
		logger.info("maximum scaling: " + maxScale);
//		props.processVariance = maxScale;
			
		nonzeroFrequencyTimes = new int[V][];
		zeroFrequencyTimes = new int[V][];
		for(WordType wt : corpus.getAllWordTypes()) {
			int localId = localWordTypeIds.get(wt.getId());
			int[] times = ArrayUtils.toPrimitive(foundDatesForWords.get(wt).toArray(new Integer[]{}));
			Arrays.sort(times);
			nonzeroFrequencyTimes[localId] = times;
			zeroFrequencyTimes[localId] = new int[T-times.length];
			int ct = 0;
			for(int t=0;t<T;t++) {
				if(!foundDatesForWords.get(wt).contains(t)) {
					zeroFrequencyTimes[localId][ct] = t;
					ct++;
				}
			}
		}

	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8492399093881263496L;


	@Override
	protected void writeTopWordsForTopics(String topicTopWordsFile, int numWords) throws IOException {
		Date[] dates = corpus.getUsedDates().toArray(new Date[]{});
		PrintStream ps = new PrintStream(props.targetDir + File.separator + topicTopWordsFile);
		int sizeOfVocabulary = corpus.getNumberOfWordTypes();
		WordProb[] probs = new WordProb[sizeOfVocabulary];
		for(int k=0;k<props.numTopics;++k) {
			ps.println("Topic " + k);
			for(int t=0;t<finalTopicWordWeights.length;++t) {
				ps.print(dates[t].toString() + "\t");
				for(WordType wt : corpus.getAllWordTypes()) {
					int typeId = localWordTypeIds.get(wt.getId());
					probs[typeId] = new WordProb(wt.getValue(), finalTopicWordWeights[t][k][typeId], typeId);
				}
				Arrays.sort(probs);
				for(int i=0;i<numWords;i++)
					ps.print(probs[i].getTerm() + "\t");// + probs[i].getProb());
				ps.print("\n");
			}
			ps.println();
		}		
		ps.close();
	}

	

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericInferencer#writeDocTopicProportions(java.lang.String)
	 */
	@Override
	protected void writeDocTopicProportions(String filename) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(props.targetDir + File.separator + filename));
		for(int t=0;t<T;t++) {
			bw.write(dates[t].toString() + "\n");
			de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.ArrayUtils.saveArrayToWriter(bw, finalDocTopicWeights[t]);
			bw.write("\n");
		}
		bw.close();
	}



	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericInferencer#writeTopicTermProportions(java.lang.String)
	 */
	@Override
	protected void writeTopicTermProportions(String topicWordProbabilityFile)
			throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(props.targetDir + File.separator + topicWordProbabilityFile));
		StringBuilder sb = new StringBuilder();
		Date[] dates = corpus.getUsedDates().toArray(new Date[]{});
		for(Date d : dates)
			sb.append(d.getTime()/1000d).append(" ");
		sb.deleteCharAt(sb.length()-1).append("\n");
		bw.write(sb.toString());
		sb.setLength(0);
		for(int k=0;k<K;k++) {
			for(int w=0;w<V;w++) {
				for(int t=0;t<T;t++) {
					sb.append(finalTopicWordWeights[t][k][w]);
					if(t==T-1)
						sb.append("\n");
					else
						sb.append(" ");
				}
			}
			bw.write(sb.toString());
			sb.setLength(0);
		}
		bw.close();
	}



	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericInferencer#writeOverallTopicProportions(java.lang.String)
	 */
	@Override
	protected void writeOverallTopicProportions(
			String globalTopicProbabilityFile) throws IOException {
		double[][] topicProbs = ArrayFactory.doubleArray(Type.ZERO, T, K);
		for(int t=0;t<T;t++) {
			for(Integer doc : corpus.getDocumentsForDate(dates[t])) {
				int docLength = corpus.getDocumentById(doc).getLength();
				int d = localDocIds.get(doc);
				VectorUtils.add_(VectorUtils.multiplyWithScalar(finalDocTopicWeights[t][d], docLength), topicProbs[t]);
			}
			topicProbs[t] = VectorUtils.normalize(topicProbs[t]);
		}
		de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.ArrayUtils.saveArrayToFile(props.targetDir + File.separator + globalTopicProbabilityFile, topicProbs);
	}



	/**
	 * @return the dates
	 */
	public Date[] getDates() {
		return dates;
	}


	/**
	 * @return the timeLocations
	 */
	public double[][] getTimeLocations() {
		return timeLocations;
	}


	/**
	 * @return the t
	 */
	public int getT() {
		return T;
	}


	/**
	 * @return the nonzeroFrequencyTimes
	 */
	public int[][] getNonzeroFrequencyTimes() {
		return nonzeroFrequencyTimes;
	}



	/**
	 * @return the zeroFrequencyTimes
	 */
	public int[][] getZeroFrequencyTimes() {
		return zeroFrequencyTimes;
	}


	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericInferencer#testModel()
	 */
	@Override
	public void testModel() throws IOException {
		File testDir = new File(props.targetDir+File.separator+"test");
		testDir.mkdirs();
		
		OpenIntIntHashMap localWordTypesBackup = (OpenIntIntHashMap) localWordTypeIds.copy();
		//first translate the test corpus vocabulary to correct word ids
		Dictionary testDic = testCorpus.getDictionary();
		OpenIntIntHashMap testToTrainingVocabIds = new OpenIntIntHashMap();
		for(WordType testWt : testDic.getAllWordTypes()) {
			int localId = localWordTypesBackup.get(corpus.getDictionary().getWordTypeByString(testWt.getValue(), false).getId());
			testToTrainingVocabIds.put(testWt.getId(), localId);
		}
		localWordTypeIds = testToTrainingVocabIds;
		//map times
		Date[] testDates = testCorpus.getUsedDates().toArray(new Date[]{});
		Arrays.sort(testDates);
		int testTimes = testDates.length;
		OpenIntIntHashMap testToLocalTimes = new OpenIntIntHashMap();
		for(int t =0;t<testDates.length;t++) 
			testToLocalTimes.put(t, findClosestTimeStamp(testDates[t]));
		
		StringBuilder docLikelihoods = new StringBuilder();
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(props.targetDir + File.separator + "test" + File.separator + props.testDocumentTopicProbabilityFile));
		double perWordLikelihood = 0;
		double likelihood;
		int numTerms = 0;
		int[] testDocCounts = new int[testTimes];
		logger.info("heldout likelihood computation");
		//for each time, do prediction with "correct" topic setting
		for(int t=0;t<testTimes;t++) {
	        logger.info(100.0*t/(double)testTimes + "% ...");
			//get local time
			int localTime = findClosestTimeStamp(testDates[t]);
			Set<Integer> documents = testCorpus.getDocumentsForDate(testDates[t]);
			
			testDocCounts[t] = documents.size();
			
			bw.write(testDates[t].toString() + "\n");

			try {
				//predict the best time
				HashMap<Integer, Future<Double>> futures = new HashMap<>();
				HashMap<Integer, double[]> predictions = new HashMap<>();
				for(Integer docId : documents) {
					predictions.put(docId, ArrayFactory.doubleArray(Type.ZERO, K));													
					Document doc = (Document) testCorpus.getDocumentById(docId);
					futures.put(docId, threadPool.submit(new TestSetLikelihoodComputation(doc, predictions.get(docId), localTime)));
					numTerms += doc.getLength();
				}
				for(Integer docId : documents) {
					likelihood = futures.get(docId).get();
					docLikelihoods.append(likelihood).append(" ");
					perWordLikelihood += likelihood;
					double[] docPredictions = predictions.get(docId);
					for(int k=0;k<K;k++) 
						bw.write(docPredictions[k] + " ");
					bw.write("\n");
				}
			} catch(InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
			bw.write("\n");
			docLikelihoods.append("\n");
		}
		bw.close();
		perWordLikelihood += getTopicLikelihood();
		logger.info("per word likelihood: " + (perWordLikelihood/numTerms));

		bw = new BufferedWriter(new FileWriter(props.targetDir + File.separator + "test" + File.separator + props.testResultFilePrefix + "results.dat"));
		bw.write("per word likelihood: " + (perWordLikelihood/numTerms) + "\n");
		bw.write(docLikelihoods.toString() + "\n");

		//map type ids
		Dictionary valDic = validationCorpus.getDictionary();
		OpenIntIntHashMap validationToTrainingVocabIds = new OpenIntIntHashMap();
		for(WordType valWt : valDic.getAllWordTypes()) {
			int localId = localWordTypesBackup.get(corpus.getDictionary().getWordTypeByString(valWt.getValue(), false).getId());
			validationToTrainingVocabIds.put(valWt.getId(), localId);
		}
		localWordTypeIds = validationToTrainingVocabIds;

		
		logger.info("timestamp prediction");
		Date[][] bestTime = new Date[testTimes][];
		
		int validationTimes = validationCorpus.getNumberOfDates();
		Date[] validationDates = validationCorpus.getUsedDates().toArray(new Date[]{});
		int[] validationDocCounts = new int[validationTimes];
		try {
			//predict the best time
			Vector<Future<Date[]>> futures = new Vector<>();
			futures.setSize(validationTimes);
	
			for(int s=0;s<validationTimes;s++) {
				validationDocCounts[s] = validationCorpus.getDocumentsForDate(validationDates[s]).size();
				futures.set(s, threadPool.submit(new TimestampPredictorThread(validationDates[s], validationDocCounts[s])));
			}
			for(int s=0;s<validationTimes;s++) {
		        logger.info(100.0*s/(double)validationTimes + "% ...");
				bestTime[s] = futures.get(s).get();
			}
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		//find prediction error
		double error = 0, totalError = 0;
		double numDocs = 0;
		for(int s=0;s<validationTimes;s++) {
			error = 0;
			for(int d=0;d<validationDocCounts[s];d++) {
				double d1 = (validationDates[s].getTime() - timeshift)/timeScale;
				double d2 = (bestTime[s][d].getTime() - timeshift)/timeScale;
				error += Math.abs(d1 - d2);
				totalError += Math.abs(d1 - d2);
			}
			error /= validationDocCounts[s];
			bw.write(validationDates[s].toString() + ": " + error + "\n");
			logger.info(validationDates[s].toString() + ": " + error);
			numDocs += validationDocCounts[s];
		}
		totalError /= numDocs;
		bw.write("total error: " + totalError + "\n");
		logger.info("total error: " + totalError);
		
		//predictive check

		double predictiveLikelihood = 0;
		for(Date d : validationCorpus.getUsedDates()) {
			Set<Integer> valDocs = validationCorpus.getDocumentsForDate(d);
			for(Integer docId : valDocs) {
				int t = findClosestTimeStamp(d);
				Document doc = validationCorpus.getDocumentById(docId);
				int[] types = doc.getTypeList();
				int[] freqs = doc.getFrequencyList();
				Vector<Integer> freqLearn = new Vector<>();
				Vector<Integer> freqPredict = new Vector<>();
				for(int i =0;i<types.length;i++) {
					int learnFreq =(int) Math.ceil(freqs[i] * .5); 
					freqLearn.add(learnFreq);
					freqPredict.add(freqs[i] - learnFreq);
				}
				int[] freqLearnArray = ArrayUtils.toPrimitive(freqLearn.toArray(new Integer[]{}));
				int[] freqPredictArray = ArrayUtils.toPrimitive(freqPredict.toArray(new Integer[]{}));
				predictiveLikelihood += doValidation(types, freqLearnArray, freqPredictArray, t);
			}
		}
		logger.info("predictive doc likelihood on validation set: " + predictiveLikelihood);
		bw.write("predictive doc likelihood on validation set: " + predictiveLikelihood);
		
		bw.close();
	}
	
	
	/**
	 * @param publicationDate
	 * @return
	 */
	protected int findClosestTimeStamp(Date publicationDate) {
		int bestDateIdx = 0;
		Date bestDate = dates[bestDateIdx];
		for(int d=0;d<T;d++) {
			if(Math.abs(bestDate.getTime() - publicationDate.getTime()) > Math.abs(dates[d].getTime() - publicationDate.getTime())) {
				bestDate = dates[d];
				bestDateIdx = d;
			}
		}
		return bestDateIdx;
	}

	private class TestSetLikelihoodComputation implements Callable<Double> {

		private int[] typeList;
		private int[] freqList;
		private double[] predictions;
		private int localTime;

		/**
		 * @param doc
		 * @param ds
		 * @param localTime
		 */
		public TestSetLikelihoodComputation(
				Document doc,
				double[] ds, int localTime) {
			this.typeList = doc.getTypeList();
			this.freqList = doc.getFrequencyList();
			this.predictions = ds;
			this.localTime = localTime;
		}

		/* (non-Javadoc)
		 * @see java.util.concurrent.Callable#call()
		 */
		@Override
		public Double call() throws Exception {
			return doPrediction(typeList, freqList, predictions, localTime);
		}
		
	}

	private class TimestampPredictorThread implements Callable<Date[]> {
		private int nDocs;
		private Date testDate;
		
		public TimestampPredictorThread(Date testDate, int testDocs) {
			this.nDocs = testDocs;
			this.testDate = testDate;
		}
		/* (non-Javadoc)
		 * @see java.util.concurrent.Callable#call()
		 */
		@Override
		public Date[] call() throws Exception {
			Date[] bestTime = new Date[nDocs];
			double[] bestLikelihood = new double[nDocs];
			double[] predictions = new double[K];
			double curLikelihood = 0;
			Set<Integer> documents = validationCorpus.getDocumentsForDate(testDate);
			
			//map doc ids to array indexes
			Integer[] docIdArray = documents.toArray(new Integer[]{});
			Arrays.sort(docIdArray);
			
			for(int t=0;t<T;t++) {
				for(int d = 0;d<docIdArray.length;d++) {
					Document doc = (Document) validationCorpus.getDocumentById(docIdArray[d]);
					curLikelihood = doPrediction(doc.getTypeList(), doc.getFrequencyList(), predictions, t);
					if(t==0) {
						bestLikelihood[d] = curLikelihood;
						bestTime[d] = dates[t];
					} else {
						if(curLikelihood > bestLikelihood[d]) {
							bestLikelihood[d] = curLikelihood;
							bestTime[d] = dates[t];
						} 
					}
				}
			}
			return bestTime;
		}
		
	}
	
}
