/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.dtm;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.math3.optim.InitialGuess;
import org.apache.commons.math3.optim.MaxIter;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.SimpleValueChecker;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunctionGradient;
import org.apache.commons.math3.optim.nonlinear.scalar.gradient.NonLinearConjugateGradientOptimizer;
import org.apache.commons.math3.optim.nonlinear.scalar.gradient.NonLinearConjugateGradientOptimizer.Formula;
import org.apache.commons.math3.optim.nonlinear.vector.Target;

import cern.colt.map.OpenIntIntHashMap;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicCorpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.DoubleFunctionFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.ArrayUtils;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericDynamicInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.dtm.state.DTMTopicState;
import de.uni_leipzig.informatik.asv.topicModels.model.dtm.state.LDAModelState;
import de.uni_leipzig.informatik.asv.topicModels.model.dtm.state.LDAPosterior;
import de.uni_leipzig.informatik.asv.topicModels.model.dtm.state.ObservationFunction;
import de.uni_leipzig.informatik.asv.topicModels.model.dtm.state.ObservationFunctionGradient;
import de.uni_leipzig.informatik.asv.topicModels.model.dtm.state.OptimizationParams;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;

public class DynamicTopicModelInferencerVB extends AbstractGenericDynamicInferencer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3960574750197018790L;
	// vars
	private double[] alpha; // Dirichlet params
	private DTMTopicState[] topics; // topic chains

	// topic doc phis
	private double[][][] topicSuffStats;

	private double[][] gammas;

	private double[][] likelihoods;

	private HashMap<Integer, Date> dateMap;
	
	public DynamicTopicModelInferencerVB(
			DiachronicCorpus c, DiachronicCorpus test,
			DiachronicCorpus validation, InferencerProperties props) throws IOException {
		super(c, test, validation, props);
		init();
	}

	private void init() {
		gammas = new double[VectorUtils.max(M)][K];
		likelihoods = new double[VectorUtils.max(M)][K + 1];

		dateMap = new HashMap<>();
		int i = 0;
		for (Date d : corpus.getUsedDates()) {
			dateMap.put(i, d);
		}

		topicSuffStats = new double[K][V][T];

		// TODO: we could also use a few runs of lda on the whole corpus to
		// initialize the suffStats
		double[][] initialTopicSuffStats = new double[V][K];
		
		DoubleMatrix2D temp = new DenseDoubleMatrix2D(initialTopicSuffStats);
		temp.assign(DoubleFunctionFactory.gammaRand);
		initialTopicSuffStats = temp.toArray();
		
		topics = new DTMTopicState[K];
		alpha = new double[K];
		
		logger.trace(String.format("done with basic init, using %d topics, %d words, %d documents and %d timeslices\n", K, V, VectorUtils.sum(M), T));
		for (int k = 0; k < K; ++k) {
			logger.trace(String.format("initializing topic %d...", k+1));
			topics[k] = new DTMTopicState(V, T);
			if (k < props.dtmFixedTopics)
				topics[k].setChainVariance(1e-10);
			else
				topics[k].setChainVariance(props.dtmTopicChainVariance);
				
			alpha[k] = props.alpha;

			double[] suffStatSlice = ArrayUtils.getColumn(
					initialTopicSuffStats, k);

			topics[k].logNormCounts(suffStatSlice);

			// set variational parameters

			topics[k].setObsVariance(props.dtmObservationVariance);

			for (int w = 0; w < V; w++)
				topics[k].computePosteriorVariance(w);

			for (int w = 0; w < V; w++)
				// Run the forward-backward algorithm
				topics[k].computePosteriorMean(w);

			topics[k].updateZeta();
			topics[k].computeExpectedLogProb();

			// init topicSuffStats
			logger.trace(" done\n");
		}
		

	}

	@Override
	public void doInference() throws IOException {
		double[][] heldoutGammas = null;
		double[][] heldoutLikelihoods = null;
		// TODO: implement heldout stuff
		// if (props.useHeldoutData) {
		// heldout_gammas = new double[heldout.numberofdocs][K];
		// heldout_lhoods = gsl_matrix_calloc(heldout->ndocs, K + 1);
		// }

		double bound = 0, heldout_bound = 0, old_bound;
		double convergence = props.convergenceCriterion + 1;

		String seqDirRoot = props.targetDir + File.separator + "/lda-seq/";
		File seqDir = new File(seqDirRoot);
		if (!(seqDir.exists() && seqDir.isDirectory()))
			seqDir.mkdirs();

		PrintStream emLog = new PrintStream(props.targetDir + File.separator
				+ "em_log.dat");

		// run EM

		int iter = 0;
		// LDA_INFERENCE_MAX_ITER = 1;
		short final_iters_flag = 0;
		boolean last_iter = false;
		while (iter < props.dtmMinIterations
				|| ((final_iters_flag == 0 || convergence > props.convergenceCriterion) && iter <= props.dtmMaxIterations)
				&& !last_iter) {

			if (!(iter < props.dtmMinIterations || ((final_iters_flag == 0 || convergence > props.convergenceCriterion) && iter <= props.dtmMaxIterations))) {
				last_iter = true;
			}
			logger.trace(String.format("\nEM iter %3d\n", iter));
			logger.trace(String.format("%s", "E step\n"));
			emLog.format("%17.14e %5.3e\n", bound, convergence);

			old_bound = bound;
//			ArrayUtils.setAll(gammas, 0.);
//			ArrayUtils.setAll(likelihoods, 0.);

			// TODO: helodout stuff
			// if (heldout != NULL ) {
			// gsl_matrix_set_zero(heldout_gammas);
			// gsl_matrix_set_zero(heldout_lhoods);
			// }

			// for (int k = 0; k < K; k++) {
			// gsl_matrix_set_zero(topic_suffstats[k]);
			// }

			// compute the likelihood of a sequential corpus under an LDA
			// seq model and find the evidence lower bound.
			bound = computeBound(iter, last_iter);
			// if (heldout != NULL ) {
			// heldout_bound = lda_seq_infer(m, heldout, NULL, heldout_gammas,
			// heldout_lhoods, iter, last_iter, file_root);
			// }

			// print out the gammas and likelihoods.
//			ArrayUtils.saveArrayToFile(props.targetDir + File.separator
//					+ "gam.dat", gammas);
//			ArrayUtils.saveArrayToFile(props.targetDir + File.separator
//					+ "lhoods.dat", likelihoods);
			// if (heldout != NULL ) {
			// sprintf(name, "%s/heldout_lhoods.dat", root);
			// mtx_fprintf(name, heldout_lhoods);
			// sprintf(name, "%s/heldout_gam.dat", root);
			// mtx_fprintf(name, heldout_gammas);
			// }
			logger.trace(String.format("%s", "\nM step\n"));

			// fit the variational distribution
			double topicBound = fitLdaSeqTopics();
			bound += topicBound;

			//TODO: maybe follow original code and dump out some information here
//			write_lda_seq(m, root);
			if ((bound - old_bound) < 0) {
				if (props.dtmMaxIterations == 1)
					props.dtmMaxIterations = 2;
				if (props.dtmMaxIterations == 2)
					props.dtmMaxIterations = 5;
				if (props.dtmMaxIterations == 5)
					props.dtmMaxIterations = 10;
				if (props.dtmMaxIterations == 10)
					props.dtmMaxIterations = 20;

				logger.trace(String.format(
						"\nWARNING: bound went down %18.14f; increasing var iter to %d\n",
						bound - old_bound, props.dtmMaxIterations));
			}
			likelihood.add(bound);
			// check for convergence
			convergence = Math.abs((bound - old_bound) / old_bound);
			if (convergence < props.convergenceCriterion) {
				final_iters_flag = 1;
				props.dtmMaxIterations = 500;
				logger.trace(String.format("starting final iterations : max iter = %d\n",
						props.dtmMaxIterations));
				convergence = 1.0;
			}
			logger.trace(String.format(
					"\n(%02d) lda seq bound=% 15.7f; heldout bound=% 15.7f, conv=% 15.7e\n",
					iter, bound, heldout_bound, convergence));
			iter++;
		}
		emLog.close();
		//return bound;
	}
	
	private int[] getLocalTypeIds(int[] globalTypeIds) {
		int[] ret = new int[globalTypeIds.length];
		for(int i=0;i<globalTypeIds.length;++i)
			ret[i] = localWordTypeIds.get(globalTypeIds[i]);
		return ret;
	}

	private double fitLdaSeqTopics() {
		double lhood = 0, lhood_term = 0;
		for (int k = 0; k < K; k++) {
			logger.trace(String.format("\nfitting topic %02d\n", k));
			lhood_term = fitTopic(k);
			lhood += lhood_term;
		}
		return lhood;
	}

	private double fitTopic(int topic) {
		int iter, w;
		double bound = 0, old_bound = 0;
		double converged = props.convergenceCriterion + 1;
		double[] totals = new double[topicSuffStats[topic].length];

		for (w = 0; w < V; w++) {
			topics[topic].computePosteriorVariance(w);
		}

		totals = VectorUtils.sumColumnAxis(topicSuffStats[topic]);

		iter = 0;
		bound = computeTopicBound(topic, totals);

		logger.trace(String.format("initial sslm bound = %10.5f\n", bound));

		while ((converged > props.convergenceCriterion)
				&& (iter < props.dtmMaxIterations)) {
			iter++;
			old_bound = bound;

			updateObservations(topic, totals);

			bound = computeTopicBound(topic, totals);
			converged = Math.abs((bound - old_bound) / old_bound);
			logger.trace(String.format("(%02d) sslm bound = % 10.5f; conv = % 10.5e\n", iter,
					bound, converged));
		}

		// compute expected log probability
		topics[topic].computeExpectedLogProb();

		return bound;
	}

	private void updateObservations(int topic, double[] totals) {
	    int t, w, runs = 0;
	    double f_val, conv_val; int niter;
	    double[] obs, w_counts, mean_deriv;
	    double[] norm_cutoff_obs = null;
	    OptimizationParams params = new OptimizationParams();
	    // Matrix of mean derivatives:
	    // Row ~ s
	    double[][] mean_deriv_mtx = new double[T][T+1];

	    params.setTopic(topics[topic]);
	    params.setTotals(totals);

	    for (w = 0; w < V; w++)
	    {
	        if (w % 5000 == 0) {
	        	logger.trace(String.format("Updating term %d\n", w));
		}
	        w_counts = topicSuffStats[topic][w];
	        if (((w % 500) == 0) && (w > 0))
	        {
	            // outlog( "[SSLM] optimized %05d words (%05d cg runs)", w, runs);
	        }

	        // check norm of observation

	        double counts_norm = VectorUtils.norm(w_counts);
	        if ((counts_norm < props.dtmObservationNormCutoff) && (norm_cutoff_obs != null))
	        {
	            topics[topic].getObs()[w] = ArrayUtils.copyOf(norm_cutoff_obs);
	        }
	        else
	        {
	            if (counts_norm < props.dtmObservationNormCutoff)
	                Arrays.fill(w_counts, 0);

	            // compute mean deriv
	            for (t = 0; t < T; t++) {
	                computeMeanDeriv(w, t, topic, mean_deriv_mtx[t]);
	            }
	            // set parameters
	            params.setWord_counts(w_counts);
	            params.setWord(w);
	            params.setMean_deriv_mtx(mean_deriv_mtx);
	            obs = topics[topic].getObs()[w];
	            Target observations = new Target(obs);
	            ObjectiveFunction obs_f = new ObjectiveFunction(new ObservationFunction(params, props.dtmInitMult));
	            ObjectiveFunctionGradient obs_df = new ObjectiveFunctionGradient(new ObservationFunctionGradient(params));
	            InitialGuess ig = new InitialGuess(obs);
	            MaxIter mi = new MaxIter(props.dtmMaxIterations);
	            // optimize
	            NonLinearConjugateGradientOptimizer opt = new NonLinearConjugateGradientOptimizer(Formula.FLETCHER_REEVES, new SimpleValueChecker(props.convergenceCriterion, -1, props.dtmMaxIterations));
	            PointValuePair optimal = opt.optimize(obs_f, obs_df, GoalType.MAXIMIZE, observations, ig, mi);
	            double[] optimum = optimal.getPoint();
	            
	            runs++;
	            if (counts_norm < props.dtmObservationNormCutoff)
	            {
	            	norm_cutoff_obs = ArrayUtils.copyOf(obs);
	                // !!! this can be BLASified for speed
//	                gsl_vector_memcpy(norm_cutoff_obs, &obs);
	            }
	        }
	    }
	    topics[topic].updateZeta();
	}
	

	private void computeMeanDeriv(int word, int time, int topic, double[] deriv) {
		int t;
		double val, w;

		// get the vectors for word
		double[] fwd_variance = topics[topic].getVariance()[word];
		assert (deriv.length == T + 1);

		// forward
		// (note, we don't need to store the forward pass in this case.)
		deriv[0] = 0;
		for (t = 1; t <= T; t++) {
			// Eq. 1.37
			if (topics[topic].getObsVariance() > 0.0) {
				w = topics[topic].getObsVariance()
						/ (fwd_variance[t - 1]
								+ topics[topic].getChainVariance() + topics[topic]
									.getObsVariance());
			} else {
				w = 0.0;
			}
			val = w * deriv[t - 1];
			// note that observations are indexed 1 from the means/variances
			if (time == t - 1) {
				val += (1 - w);
			}
			deriv[t] = val;
		}

		// backward
		for (t = T - 1; t >= 0; t--) {
			// Eq. 1.39
			if (topics[topic].getChainVariance() == 0.0) {
				w = 0.0;
			} else {
				w = topics[topic].getChainVariance()
						/ (fwd_variance[t] + topics[topic].getChainVariance());
			}
			deriv[t] = w * deriv[t] + (1 - w) * deriv[t + 1];
		}
	}

	private double computeTopicBound(int topic, double[] totals) {
		int t, w;
		double term1 = 0, term2 = 0, term3 = 0;
		double val = 0, m, v, prev_m, prev_v;
		double w_phi_l, exp_i;
		double ent = 0;

		for (w = 0; w < V; w++) {
			// Run the forward-backward algorithm
			topics[topic].computePosteriorMean(w);
		}
		topics[topic].updateZeta();

		for (w = 0; w < V; w++) {
			val += (topics[topic].getVariance()[w][0] - topics[topic]
					.getVariance()[w][T])
					/ 2
					* topics[topic].getChainVariance();
		}

		logger.trace(String.format("Computing bound, all times%s.\n", ""));
		for (t = 1; t <= T; t++) {
			term1 = term2 = ent = 0.0;
			for (w = 0; w < V; w++) {
				m = topics[topic].getMean()[w][t];
				prev_m = topics[topic].getMean()[w][t - 1];
				v = topics[topic].getVariance()[w][t];

				// Values specifically related to document influence:
				// Note that our indices are off by 1 here.
				w_phi_l = topics[topic].getwPhiL()[w][t - 1];
				exp_i = Math.exp(-prev_m);

				term1 += Math.pow(m - prev_m - w_phi_l * exp_i, 2)
						/ (2 * topics[topic].getChainVariance()) - v
						/ topics[topic].getChainVariance()
						- Math.log(topics[topic].getChainVariance());

				term2 += topicSuffStats[topic][w][t - 1] * m;
				ent += Math.log(v) / 2; // note the 2pi's cancel with term1 (see
										// doc)
			}
			term3 = -totals[t - 1] * Math.log(topics[topic].getZeta()[t - 1]);

			val += -term1 + term2 + term3 + ent;
		}
		return (val);
	}

	private double computeBound(int iter, boolean last_iter) {
		logger.trace(String.format("computing bound at iteration %d ...\n", iter));
		double bound = 0.0;
		LDAModelState m = new LDAModelState(K, V);
		LDAPosterior<Document> post = new LDAPosterior<>(
				corpus.getMaximumDocumentSize(), K, m);
		for (int t = 0; t < T; t++) {
			// Prepare coefficients for the phi updates. This change is
			// relatively painless.
			logger.trace(String.format("processing timeslice %d, fitting lda posterior for each document\n", t));
			for (int k = 0; k < K; k++) {
				// get topic
				double[] s = ArrayUtils.getColumn(topics[k].geteLogProb(), t);
				post.getModel().getTopics()[k] = s;
//				ArrayUtils.setColumn(post.getModel().getTopics(), s, k);
			}
			for (int d = 0; d < M[t]; d++) {
				double doc_lhood;
				// For now, only do the standard, phi-based update.
				post.setDoc(corpus.getDocumentById(d));
				if (iter == 0) {
					doc_lhood = fitLdaPosterior(t, post);
				} else {
					doc_lhood = fitLdaPosterior(t, post);
				}
				if (topicSuffStats != null) {
					OpenIntIntHashMap wordFreqs = post.getDoc()
							.getWordFrequencies();
					for (int k = 0; k < K; k++) {
						for(int i=0;i<wordFreqs.size();++i){
//						for (int type : wordFreqs.keys().elements()) {
							int type = wordFreqs.keys().elements()[i];
							int c = wordFreqs.get(type);
							int local = localWordTypeIds.get(type);
							topicSuffStats[k][local][t] += c
									* post.getPhi()[i][k];
						}
					}
				}
				bound += doc_lhood;
				if((d+1)%10==0)
					logger.trace(String.format("done with %d if %d documents\n", d+1, M[t]));
			}
		}
		return bound;
	}

	private double fitLdaPosterior(int t,
			LDAPosterior<Document> post) {
		double lhood = computeLdaLikelihood(post);
		double lhood_old = 0;
		double converged = 0;
		int iter = 0;

		do {
			iter++;
			lhood_old = lhood;
			updateGamma(post);
			updatePhi(t, post);

			lhood = computeLdaLikelihood(post);
			converged = Math.abs((lhood_old - lhood)
					/ (lhood_old * post.getDoc().getLength()));
			logger.trace(String.format("converged = %f\n", converged));
		} while ((converged > props.convergenceCriterion)
				&& (iter <= props.dtmMaxIterations));

		return lhood;
	}

	private void updatePhi(int t,
			LDAPosterior<Document> post) {
		int k, K = post.getModel().getNumTopics();

		double[] dig = new double[K];

		for (k = 0; k < K; k++) {
			dig[k] = MathUtils.digamma(post.getGamma()[k]);
		}
		OpenIntIntHashMap freqs = post.getDoc().getWordFrequencies();
		for(int i=0;i<freqs.size();++i) {
//		for (Integer typeId : freqs.keys().elements()) {
			// compute log phi up to a constant
			int typeId = freqs.keys().elements()[i];
			int local = localWordTypeIds.get(typeId);
			for (k = 0; k < K; k++) {
				post.getLogPhi()[i][k] = dig[k]
						+ post.getModel().getTopics()[k][local];
			}

			// normalize in log space

			post.getLogPhi()[i] = MathUtils.normalizeLogDistribution(post
					.getLogPhi()[i]);
			post.getPhi()[i] = VectorUtils.exp(post.getLogPhi()[i]);
		}
	}

	private void updateGamma(
			LDAPosterior<Document> post) {

		System.arraycopy(post.getModel().getAlpha(), 0, post.getGamma(), 0,
				post.getModel().getAlpha().length);

		OpenIntIntHashMap freqs = post.getDoc().getWordFrequencies();
		for(int i=0;i<freqs.size();++i) {
			for (int k = 0; k < K; k++)
				post.getGamma()[k] += post.getPhi()[i][k] * freqs.values().elements()[i];
		}
	}

	private double computeLdaLikelihood(
			LDAPosterior<Document> post) {
		int k;
		int d = post.getDoc().getId();
		OpenIntIntHashMap wordFreqs = post.getDoc().getWordFrequencies();

		double gamma_sum = ArrayUtils.sum(gammas[d]);
		double lhood = MathUtils.loggamma(ArrayUtils.sum(alpha))
				- MathUtils.loggamma(gamma_sum);
		likelihoods[d][K] = lhood;

		double digsum = MathUtils.digamma(gamma_sum);
		for (k = 0; k < K; k++) {
			double e_log_theta_k = MathUtils.digamma(gammas[d][k]) - digsum;
			double lhood_term = (alpha[k] - gammas[d][k]) * e_log_theta_k
					+ MathUtils.loggamma(gammas[d][k])
					- MathUtils.loggamma(alpha[k]);

			for(int i=0;i<wordFreqs.size();++i) {
				int typeId = wordFreqs.keys().elements()[i];
//			for (Integer typeId : wordFreqs.keys().elements()) {
				int local = localWordTypeIds.get(typeId);
				if (post.getPhi()[i][k] > 0) {
					lhood_term += wordFreqs.get(typeId)
							* post.getPhi()[i][k]
							* (e_log_theta_k
									+ post.getModel().getTopics()[k][local] - post
										.getLogPhi()[i][k]);
				}
			}
			post.getLhood()[k] = lhood_term;
			lhood += lhood_term;
		}

		return lhood;
	}


	@Override
	public void loadModel(String filename) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void populateFinalValues() {
		// TODO fill this with the final values
		
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericDynamicInferencer#doPrediction(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicDocument, double[], int)
	 */
	@Override
	public double doPrediction(int[] types, int[] freqs, double[] topicPredictions, int time) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericDynamicInferencer#doValidation(int[], int[], int[], int)
	 */
	@Override
	public double doValidation(int[] types, int[] freqsLearn,
			int[] freqsPredict, int time) {
		// TODO Auto-generated method stub
		return 0;
	}

}
