/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.gap;

import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.digamma;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.divide;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.log;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.normalize;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.subtract;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.Optimizer;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.cg.FletcherReevesConjugateGradientOptimizer;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericStaticInferencer;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;

public class GammaPoissonModel extends AbstractGenericStaticInferencer {
	//stats
	double[][] statsKW;
	double[] statsK;
	
	//variational parameters
	double[][] lambda1;
	double[][] lambda2;
	double[][] gamma1;
	double[][] gamma2;
	double[][][] phi;
	
	//expectations
	double[][][] expZ;
	double[][] expTheta;
	double[][] expLogTheta;
	double[][] expBeta;
	double[][] expLogBeta;

	double zBound, betaBound, thetaBound;

	private OptimizationParams optP;
	

	public GammaPoissonModel(Corpus c, Corpus test, Corpus validation, InferencerProperties props)
			throws IOException {
		super(c, test, validation, props);
		init();
	}

	private void init() {
		K = props.numTopics;
		V = corpus.getNumberOfWordTypes();
		M = corpus.getNumberOfDocuments();
		
		lambda1 = new double[M][K];
		lambda2 = new double[M][K];
		expTheta = new double[M][K];
		expLogTheta = new double[M][K];

		for(int d=0;d<M;d++) {
			for(int k=0;k<K;k++) {
				lambda1[d][k] = props.gap_a + .01*Math.random();
				lambda2[d][k] = props.gap_b + .1*Math.random();
			}
			updateThetaExpectationsForDoc(d);
		}
		
		gamma1 = new double[K][V];
		gamma2 = new double[K][V];
		expBeta = new double[K][V];
		expLogBeta = new double[K][V];
		
		for(int w=0;w<V;w++) {
			for(int k=0;k<K;k++) {
				gamma1[k][w] = props.gap_c + .01*Math.random();
				gamma2[k][w] = props.gap_d + .1*Math.random();
			}
			updateBetaExpectationsForWord(w);
		}
		
		phi = new double[M][][];
		expZ = new double[M][][];
		for(Document d : corpus.getDocuments()) {
			int docIdx = localDocIds.get(d.getId());
			int Vd = d.getNumberOfWordTypes();
			phi[docIdx] = new double[Vd][K];
			expZ[docIdx] = new double[Vd][K];
		}
		
		statsK = new double[K];
		statsKW = new double[K][V];
		
		optP = new OptimizationParams();
		optP.setK(K);
		optP.setM(M);
		optP.setV(V);
		betaBound = thetaBound = zBound = 0;
		zeroStats();
		
	}

	/**
	 * 
	 */
	private void updateThetaExpectationsForDoc(int d) {
		for(int k=0;k<K;k++) {
			expTheta[d][k] = (lambda1[d][k]/lambda2[d][k]);
			expLogTheta[d][k] = MathUtils.digamma(lambda1[d][k]) - Math.log(lambda2[d][k]);
		}
	}

	/**
	 * 
	 */
	private void updateBetaExpectationsForWord(int w) {
		for(int k=0;k<K;k++) {
			expBeta[k][w] = (gamma1[k][w]/gamma2[k][w]);
			expLogBeta[k][w] = MathUtils.digamma(gamma1[k][w]) - Math.log(gamma2[k][w]);
		}
	}

	/**
	 * 
	 */
	private void zeroStats() {
		Arrays.fill(statsK, 0.);
		for(int k=0;k<K;k++)
			Arrays.fill(statsKW[k], 0.);
	}

	@Override
	public void doInference() throws IOException {
		double convergence = 1;
		double bound = 0;
		double oldBound = -1e200;
		int iter=0;
		StringBuilder sb = new StringBuilder();
		while(Math.abs(convergence) > props.convergenceCriterion && iter<props.iterations || iter < 7) {
			bound = updateParameters();
			convergence = (oldBound - bound) / oldBound;
			sb.append(bound).append("\t").append(convergence).append("\n");
			logger.info("bound: " + bound + ", convergence: " + convergence);
//			logger.info("betaBound = " + betaBound + ", thetaBound = " + thetaBound + ", zBound = " + zBound + ", completeBound = " + bound);
			betaBound = thetaBound = zBound = 0;
			if(convergence<0) {
				logger.info("Warning, bound decreasing");
//				break;
			}
			oldBound = bound;
			iter++;
		}
		BufferedWriter bw = new BufferedWriter(new FileWriter(props.targetDir+File.separator+"likelihood.txt"));
		bw.write(sb.toString());
		bw.close();
		props.iterations = iter;
	}

	
	private double updateParameters() {
//		logger.info("updating theta and phi...");
		
		double likelihood = 0;
		optP.setLikelihoodConstantPart(0);
		

		zeroStats();
		long start = System.currentTimeMillis();
		for(Document doc : corpus.getDocuments()) {
			likelihood += varEStep(doc);
		}
		long dur = System.currentTimeMillis() - start;
//		System.out.println("theta and phi took " + dur + "ms");

		//optimize topics
		likelihood += varMStep();

//		updateHyperparams();
		
		return likelihood;
	}
	
	private void updateHyperparams() {
		logger.info("updating hyper parameter");
		optP.seteBeta(divide(gamma1, gamma2));
		optP.seteTheta(divide(lambda1, lambda2));
		optP.seteLogBeta(subtract(digamma(gamma1), log(gamma2)));
		optP.seteLogTheta(subtract(digamma(lambda1), log(lambda2)));
		
		FletcherReevesConjugateGradientOptimizer minimizer = Optimizer.getOptimizer(FletcherReevesConjugateGradientOptimizer.class);
		boolean debug = true;
		HyperParamObjectiveFunction f = new HyperParamObjectiveFunction(4, optP);
		double[] guess = new double[]{props.gap_a,props.gap_b,props.gap_c,props.gap_d};
		logger.info("old values: " + Arrays.toString(guess));
		
		Optimizer.optimize(guess, .35, 1e-4, 1e-4, 30, f, minimizer);
		
		double[] optPoint = minimizer.getX();
		
		logger.info("new values: " + Arrays.toString(optPoint));
		props.gap_a = optPoint[0];
		props.gap_b = optPoint[1];
		props.gap_c = optPoint[2];
		props.gap_d = optPoint[3];

	}

	private double varEStep(Document doc) {
		int d = localDocIds.get(doc.getId());
		double converge = 1;
		double oldLikelihood = -1e200;
		double likelihood = 0;
		double phiSum = 0;
		int[] types = doc.getTypeList();
		int[] freqs = doc.getFrequencyList();
		while(converge > 1e-3) {
			setThetaToPrior(d);
			
			for(int n=0;n<types.length;++n) {
				int type = types[n];
				int wordFreq = freqs[n];
				int w = localWordTypeIds.get(type);
				for(int k=0;k<K;k++) {
					phi[d][n][k] = expLogTheta[d][k] + expLogBeta[k][w];
					if(k==0)
						phiSum = phi[d][n][k]; 
					else
						phiSum = MathUtils.logAdd(phiSum, phi[d][n][k]);
					
				}
				//needed when normalizing across words?
//				phi[d][n] = MathUtils.normalizeDistribution(phi[d][n]);
				for(int k=0;k<K;k++) {
					phi[d][n][k] = Math.exp(phi[d][n][k] - phiSum);
					expZ[d][n][k] = wordFreq*phi[d][n][k];
					lambda1[d][k] += expZ[d][n][k];
					lambda2[d][k] += expBeta[k][w];
				}
			}
			updateThetaExpectationsForDoc(d);
			
			likelihood = documentLikelihood(d, freqs, types);
			converge = (oldLikelihood - likelihood) / oldLikelihood;
			oldLikelihood = likelihood;
		}
		
		//udpdate suff stats
		for(int k=0;k<K;k++) {
			statsK[k] += expTheta[d][k];
			for(int n=0;n<types.length;n++) {
				int w = localWordTypeIds.get(types[n]);
				statsKW[k][w] += expZ[d][n][k];
			}
		}
		return likelihood;
	}
	
	/**
	 * @param d
	 */
	private void setThetaToPrior(int d) {
		for(int k=0;k<K;k++) {
			lambda1[d][k] = props.gap_a;
			lambda2[d][k] = props.gap_b;
		}
		
	}

	private double documentLikelihood(int d, int[] wordFreqs, int[] types) {
		double likelihood = 0;
		double curVal = 0;
		//compute bound
		for(int k=0;k<K;++k) {
			for(int n=0;n<types.length;n++) {
				int w = localWordTypeIds.get(types[n]);
				//likelihood z
				curVal = expZ[d][n][k]*(expLogTheta[d][k] + expLogBeta[k][w] - Math.log(phi[d][n][k])) - expTheta[d][k]*expBeta[k][w];
				likelihood += curVal;
				zBound += curVal;
			}
			//p(theta)
			curVal = props.gap_a * Math.log(props.gap_b) + (props.gap_a - 1) * expLogTheta[d][k]; 
			curVal -= props.gap_b*expTheta[d][k] + MathUtils.lgamma(props.gap_a);
			
			//q(theta)
			curVal -= lambda1[d][k]*Math.log(lambda2[d][k]) + (lambda1[d][k] - 1) * expLogTheta[d][k];
			curVal += lambda2[d][k]*expTheta[d][k] + MathUtils.lgamma(lambda1[d][k]);
			likelihood += curVal;
			thetaBound += curVal;
		}
		return likelihood;
	}
	
	
	private double varMStep() {
		double likelihood = 0;
		double curVal;
		int w;
		long start = System.currentTimeMillis();
		for(WordType type : corpus.getAllWordTypes()) {
			w = localWordTypeIds.get(type.getId());
			for(int k=0;k<K;++k) {
				gamma1[k][w] = props.gap_c + statsKW[k][w];
				gamma2[k][w] = props.gap_d + statsK[k];
			}

			updateBetaExpectationsForWord(w);
			curVal = computeBetaLikelihoodForWord(w); 
			likelihood += curVal;
			betaBound += curVal;
		}
		long dur = System.currentTimeMillis() - start;
//		logger.info("beta likelihood: " + likelihood + ", took " + dur + "ms");
		return likelihood;
	}

	/**
	 * @param w
	 * @return
	 */
	private double computeBetaLikelihoodForWord(int w) {
		double likelihood = 0;
		for(int k=0;k<K;++k) {
			likelihood += props.gap_c * Math.log(props.gap_d) + (props.gap_c-1)*expLogBeta[k][w]; 
			likelihood -= props.gap_d*expBeta[k][w] + MathUtils.lgamma(props.gap_c);
			
			likelihood -= gamma1[k][w]*Math.log(gamma2[k][w]) + (gamma1[k][w]-1)*expLogBeta[k][w]; 
			likelihood += gamma2[k][w]*expBeta[k][w] + MathUtils.lgamma(gamma1[k][w]);
		}
		return likelihood;
	}



	@Override
	public void populateFinalValues() {
		finalDocTopicWeights = expTheta;
		for(int d=0;d<corpus.getNumberOfDocuments();++d) 
			finalDocTopicWeights[d] = normalize(finalDocTopicWeights[d]);
		
		finalTopicWordWeights = expBeta;
		for(int k=0;k<K;++k)
			finalTopicWordWeights[k] = normalize(finalTopicWordWeights[k]);
	}

	@Override
	public void loadModel(String filename) throws IOException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericStaticInferencer#doPrediction(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document, double[])
	 */
	@Override
	public double doPrediction(Document d, double[] topicPredictions) {
		// TODO Auto-generated method stub
		return 0;
	}

}
