/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.cdtm;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.Set;

import org.apache.commons.math3.special.Gamma;

import cern.colt.map.OpenIntIntHashMap;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicCorpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.WordTokenState;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.Optimizer;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.cg.FletcherReevesConjugateGradientOptimizer;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericDynamicInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.cdtm.state.OptFunction;
import de.uni_leipzig.informatik.asv.topicModels.model.cdtm.state.Params;
import de.uni_leipzig.informatik.asv.topicModels.model.cdtm.state.Params.Pos;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;

public class ContinuousTimeDynamicTopicModel extends AbstractGenericDynamicInferencer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7318425678607106546L;
	private double timeStat, alpha, var0, var, varHat;
	private int segmentSize, W, K, T;
	private Date[] dates;
	private double[] times;
	private int[] sparseNumTimes;
	private int[][] sparseTimes;
	private double[][] zeta, statsKT;
	private double[][][] beta, statsKWT;
	private boolean accurate;
	private boolean debug = false;
	
	public ContinuousTimeDynamicTopicModel(DiachronicCorpus c, DiachronicCorpus test, DiachronicCorpus validation, InferencerProperties props) throws IOException {
		super(c, test, validation, props);
		alpha = props.alpha;
		var0 = 10;
		dates = corpus.getUsedDates().toArray(new Date[]{});
		double tStart = dates[0].getTime() / (24*60*60*1000);
		double tEnd = dates[dates.length - 1].getTime() / (24*60*60*1000);
		double maxScale = 1. / (tEnd - tStart);
//		var = maxScale * .5;
		var = props.processVariance;
		logger.info("using process variance " + var);
		varHat = .5;
		segmentSize = 2000;
		timeStat = 0;
		W = corpus.getNumberOfWordTypes();
		K = props.numTopics;
		T = c.getNumberOfDates();
		allocate();
	}
	
	
	
	@Override
	public void populateFinalValues() {
		updateZeta();
		for(int t=0;t<T;t++) {
			for(int k=0;k<K;k++) {
				for(int w=0;w<V;w++)
					finalTopicWordWeights[t][k][w] = Math.exp(finalTopicWordWeights[t][k][w] - zeta[k][t]);
			}
		}
	}



	private void allocate() {
		times = new double[T];
		for(int t=0;t<T;t++) 
			times[t] = dates[t].getTime() / (24*60*60*1000);
		
		sparseNumTimes = new int[W];
		for(int t=0;t<T;t++) {
			Set<Integer> docs = corpus.getDocumentsForDate(dates[t]);
			OpenIntIntHashMap freqs = corpus.getDocumentIdSubsetWordFrequencies(docs);
			for(int w=0;w<freqs.size();w++) {
				int word = freqs.keys().get(w);
				sparseNumTimes[word]++;
				assert(sparseNumTimes[word] <= (t+1));
			}
		}
		
		int maxSparseNumTimes = 0;
		double aveNumTimes = 0;
		for(int w=0;w<W;w++) {
			aveNumTimes += sparseNumTimes[w];
			if(maxSparseNumTimes < sparseNumTimes[w])
				maxSparseNumTimes = sparseNumTimes[w];
		}
		aveNumTimes /= W;
		
		System.out.println("average number of words over time line is " + aveNumTimes);
		System.out.println("sparsity is " + (1-(aveNumTimes/T)));
		
		sparseTimes = new int[W][];
		System.out.println("W="+ W);
		for(int w=0;w<W;w++)
			sparseTimes[w] = new int[sparseNumTimes[w]];
		
		Arrays.fill(sparseNumTimes, 0);
		for(int t=0;t<T;t++) {
			Set<Integer> docs = corpus.getDocumentsForDate(dates[t]);
			OpenIntIntHashMap freqs = corpus.getDocumentIdSubsetWordFrequencies(docs);
			for(int w=0;w<freqs.size();w++) {
				int word = freqs.keys().get(w);
				sparseTimes[word][sparseNumTimes[word]] = t;
				sparseNumTimes[word]++;
				assert(sparseNumTimes[word] <= (t+1));
			}
		}

		zeta = new double[K][T];
		statsKT = new double[K][T];

		beta = new double[K][W][];
		statsKWT = new double[K][W][];
		for(int k=0;k<K;k++) {
			for(int w=0;w<W;w++) {
				beta[k][w] = new double[sparseNumTimes[w]];
				statsKWT[k][w] = new double[sparseNumTimes[w]];
			}
		}
		
	}

	private void randInitializeModel() {
		Random rng = new Random(System.currentTimeMillis());
		
		for(int k=0;k<K;k++) {
			for(int w=0;w<W;w++) {
				for(int t=0;t<sparseNumTimes[w];t++) {
					beta[k][w][t] = rng.nextDouble() + .5;
				}
			}
		}
		updateZeta();
	}


	private void updateZeta() {
		System.out.println("updating zeta");
		double[] forwardMean = new double[T];
		double[] forwardVariance = new double[T];
		double[] backwardMean = new double[T];
		double[] backwardVariance = new double[T];
		
		double[] initMean = new double[K];
		double initVariance, initTime;
		int k,w,s,t,tMinus;
		double Pt, delta;
		
		for(k=0;k<K;k++)
			Arrays.fill(zeta[k], 0);
		for(w=0;w<W;w++) {
			initVariance = var0;
			initTime = 0;
			
			int start = 0;
			int end = 0;
	        int segStart = 0;
	        int segEnd = 0;
	        int nlen = sparseNumTimes[w];
	        
	        // segmental process
	        int size_step = (int) Math.ceil((double)nlen/(double)segmentSize);
	        for (int step = 0; step < size_step; step ++) {
	            start = end;
	            end = (step+1)*segmentSize;
	            if (end >= nlen)
	                end = T;
	            else
	                end = sparseTimes[w][end-1]+1;
	            
	            segStart = segEnd;
	            segEnd = (step+1) * segmentSize;
	            if (segEnd > nlen) segEnd = nlen;
	            
	            s = segStart;
	            for (t = start; t < end; t ++) {
	                if (t == start) {
	                    Pt = initVariance + var*(times[t]-initTime);
	                }
	                else {
	                    Pt = forwardVariance[t-1] + var*(times[t]-times[t-1]);
	                }
	                if (s < sparseNumTimes[w] && t == sparseTimes[w][s]) {
	                    forwardVariance[t] = varHat*Pt /(varHat+Pt);
	                    s ++;
	                }
	                else {
	                    forwardVariance[t] = Pt;
	                }
	            }
	            backwardVariance[end-1] = forwardVariance[end-1];
	            initVariance = forwardVariance[end-1]; //update init values
	            initTime = times[end-1];      //update init values
	            for (t = end-1; t > start; t --) {
	                Pt = forwardVariance[t-1] + var*(times[t]-times[t-1]);
	                backwardVariance[t-1] = forwardVariance[t-1]
	                        + Math.pow(forwardVariance[t-1]/Pt, 2)*(backwardVariance[t]-Pt);
	            }
	            for (k = 0; k < K; k ++) {
	                s = segEnd-1;
	                t = sparseTimes[w][s];
	                backwardMean[t] = beta[k][w][s];
	                forwardMean[t] = beta[k][w][s];
	                for (s = segEnd-1; s > segStart; s -- ) {
	                    t = sparseTimes[w][s];
	                    tMinus = sparseTimes[w][s-1];
	                    delta = var*(times[t] - times[tMinus]);
	                    Pt = forwardVariance[tMinus] + delta;
	                    backwardMean[tMinus] = beta[k][w][s-1];
	                    forwardMean[tMinus] =
	                            (backwardMean[tMinus]*Pt-backwardMean[t]*forwardVariance[tMinus])/delta;
	                }
	                s = segStart;
	                for (t = start; t < end; t ++) {
	                    if (s < sparseNumTimes[w] && t == sparseTimes[w][s]) {
	                        initMean[k] = forwardMean[t]; //update initMean
	                        s++;
	                    }
	                    else {
	                        forwardMean[t] = initMean[k];
	                    }
	                }
	                //printf("k %d:\n", k);
	                backwardMean[end-1] = forwardMean[end-1];
	                //printf("%lf\n", backwardMean[end-1]);
	                for (t = end-1; t > start; t --) {
	                    delta = var*(times[t]-times[t-1]);
	                    Pt = forwardVariance[t-1] + delta;
	                    backwardMean[t-1] = (forwardMean[t-1]*delta + backwardMean[t]*forwardVariance[t-1])/Pt;
	                    //printf("%lf\n", backwardMean[t-1]);
	                }
	                for (t = start; t < end; t ++) {
	                	if(accurate) {
		                    if (w>0) {
		                        finalTopicWordWeights[t][k][w] = backwardMean[t]+backwardVariance[t]/2;
		                        zeta[k][t] = MathUtils.logAdd(zeta[k][t], backwardMean[t]+backwardVariance[t]/2);
		                    }
		                    else {
		                        finalTopicWordWeights[t][k][w] = backwardMean[t]+backwardVariance[t]/2;
		                        zeta[k][t] = backwardMean[t]+backwardVariance[t]/2;
		                    }
	                	} else {
		                    if (w>0) {
		                        finalTopicWordWeights[t][k][w] = backwardMean[t]+backwardVariance[t]/2;
		                        zeta[k][t] = MathUtils.logAdd(zeta[k][t], backwardMean[t]);
		                    }
		                    else {
		                        finalTopicWordWeights[t][k][w] = backwardMean[t]+backwardVariance[t]/2;
		                        zeta[k][t] = backwardMean[t];
		                    }
	                	}
	                }
	            }
	        }
	    }

	}



	@Override
	public void doInference() throws IOException {
		randInitializeModel();
		double converged = 1d;
		double likelihood = 0, likelihoodOld = 0;
		int i=0;
		while((converged > props.convergenceCriterion || i <= 2) && (i < props.iterations)) {
			System.out.format("****** em iteration %d ******\n", ++i);
			likelihood = 0;
			zeroStat();
			System.out.println("**** e step ****");
			likelihood = eStep();
			likelihood += betaLikelihood();
			System.out.println("likelihood: " + likelihood);
			System.out.println("**** m step ****");
			mStep();
			this.likelihood.add(likelihood);
			converged = (likelihoodOld - likelihood) / likelihoodOld;
			likelihoodOld = likelihood;
			logger.info(likelihood+"\t" + converged + "\n");
			System.out.format("average optimizing time %.4f secondssss \n", timeStat/i);
			System.out.format("total optimizing time %.4f seconds \n", timeStat);
			
			if((i-1)%props.sampleLag == 0) {
				//TODO model saving
			}
			if(converged < 0)
				break;
		}
		
		
		//TODO save final model
	}




	private void mStep() {
		int optMaxIter = 3;
		int optimizeMaxIter = 20;
		
		double optimizerConvergence = 1e-3;
		
		double[] forwardMean = new double[T];
		double[] forwardVariance = new double[T];
		
		double[] betaHat = new double[T];
		double[] backwardVariance = new double[T];
		double[] Pt = new double[T];
		
		double[] gradient = new double[T];
		
		double initMean, initVariance, initTime;
		
		int i=0, iter, k, w, t, segStart=0, segEnd=0, nlen, numSegments;
		long startTime, endTime;
		
		startTime = System.currentTimeMillis();
		int suc = 0;
		FletcherReevesConjugateGradientOptimizer sMin = Optimizer.getOptimizer(FletcherReevesConjugateGradientOptimizer.class);
		while(i<optMaxIter) {
			i++;
			System.out.println("optimization round: " + i);
			for(k=0;k<K;k++) {
				for(w=0;w<W;w++) {
					computeSparseVariance(forwardVariance, backwardVariance, Pt, w);
					Params params = new Params();
					
					params.setForwardMean(forwardMean);
					params.setBetaHat(betaHat);
					params.setForwardVariance(forwardVariance);
					params.setBackwardVariance(backwardVariance);
					
					params.setPt(Pt);
					params.setGradient(gradient);
					
					params.setK(k);
					params.setW(w);
					
					params.setModel(this);
					
					
					initMean = 0;
					initVariance = var0;
					initTime = 0;
					nlen = sparseNumTimes[w];
					segStart = 0;
					segEnd = 0;
					numSegments = (int) Math.ceil((double)nlen/segmentSize);
					for(int step =0;step<numSegments;step++) {
						if(step == 0) {
							params.setPos(Pos.FIRST);
							if(numSegments == 1)
								params.setPos(Pos.FIRST_LAST);
						} else if(step == numSegments-1)
							params.setPos(Pos.LAST);
						else
							params.setPos(Pos.MIDDLE);
						
						segStart = segEnd;
						segEnd = (step+1) * segmentSize;
						if(segEnd > nlen) segEnd = nlen;
						params.setSegmentStart(segStart);
						params.setSegmentEnd(segEnd);
						params.setOptSize(segEnd-segStart);
						params.setInitMean(initMean);
						
						double[] x = new double[params.getOptSize()];
						for(int s=segStart;s<segEnd;s++)
							x[s-segStart] = beta[k][w][s];
						
						OptFunction f = new OptFunction(params);
						
						Optimizer.optimize(x, .35, .01, optimizerConvergence, optimizeMaxIter, f, sMin);
						
						for(int s=segStart;s<segEnd;s++)
							beta[k][w][s] = sMin.getX()[s-segStart];
						
						initMean = beta[k][w][segEnd-1];
						t = sparseTimes[w][segEnd-1];
						initTime = times[t];
						initVariance = forwardVariance[segEnd-1];
					}
				}
			}
		}
		endTime = System.currentTimeMillis();
		
		updateZeta();
		timeStat += (endTime-startTime)/1000.;
		
		
	}



	private double betaLikelihood() {
		double[] forwardMean = new double[T];
		double[] forwardVariance = new double[T];
		
		double[] betaHat = new double[T];
		double[] backwardVariance = new double[T];
		double[] Pt = new double[T];
		
		double initMean, initVariance, initTime, mHat, v, delta, likelihood = 0;
		int t, tPlus, tMinus, s, k, w;
		int segStart = 0, segEnd = 0;
		int nlen, numSegments;
		
		for(k=0;k<K;k++) {
			for(w=0;w<W;w++) {
				computeSparseVariance(forwardVariance, backwardVariance, Pt, w);
				
				initMean = 0;
				initVariance = var0;
				initTime = 0;
				nlen = sparseNumTimes[w];
				segStart = 0;
				segEnd = 0;
				numSegments = (int) Math.ceil((double) nlen / segmentSize);
				for(int step=0;step<numSegments;step++) {
					segStart = segEnd;
					segEnd = (step+1) * segmentSize;
					if(segEnd > nlen) segEnd = nlen;
	                if (numSegments > 1 && step < numSegments-1) {
	                    t = sparseTimes[w][segEnd-1];
	                    if (t+1 == sparseTimes[w][segEnd]) {
	                        delta = var * (times[t+1]-times[t]);
	                        likelihood += -0.5 * Math.pow(beta[k][w][segEnd-1]-beta[k][w][segEnd], 2.0)/delta;
	                    }
	                    else {
	                        tPlus = sparseTimes[w][segEnd];
	                        delta = var * (times[tPlus]-times[t+1]);
	                        v = forwardVariance[segEnd-1] + var * (times[t+1] - times[t]);
	                        mHat = (beta[k][w][segEnd-1]*delta + beta[k][w][segEnd]*v)/Pt[segEnd];
	                        delta = var * (times[t+1]-times[t]);
	                        likelihood += -0.5 * Math.pow(beta[k][w][segEnd-1]-mHat, 2.0)/delta;
	                    }
	                }
	                
	                forwardMean[segEnd-1] = beta[k][w][segEnd-1];
	                for (s = segEnd-1; s > segStart; s --) {
	                    t = sparseTimes[w][s];
	                    tMinus = sparseTimes[w][s-1];
	                    delta = var*(times[t] - times[tMinus]);
	                    forwardMean[s-1] = (beta[k][w][s-1]*Pt[s] - beta[k][w][s]*forwardVariance[s-1])/delta;
	                    betaHat[s] = ((Pt[s]+varHat)*forwardMean[s] - varHat*forwardMean[s-1])/Pt[s];
	                }
	                s = segStart;
	                betaHat[s] = ((Pt[s]+varHat)*forwardMean[s] - varHat*initMean)/Pt[s];
	                
	                for (s = segStart; s < segEnd; s ++) {
	                    if (s == segStart) {
	                        likelihood += -0.5*Math.pow(betaHat[s]-initMean, 2.0)/(Pt[s]+varHat);
	                    }
	                    else {
	                        likelihood += -0.5*Math.pow(betaHat[s]-forwardMean[s-1], 2.0)/(Pt[s]+varHat);
	                    }
	                    likelihood += 0.5 *Math.pow(betaHat[s]-beta[k][w][s], 2.0)/varHat;
	                }
	                
	                //update initial value for next step
	                initMean = beta[k][w][segEnd-1];
	                t = sparseTimes[w][segEnd-1];
	                initTime = times[t];
	                initVariance = forwardVariance[segEnd-1];
				}
			}
		}
		
		return likelihood;
	}



	private void computeSparseVariance(double[] forwardVariance,
			double[] backwardVariance, double[] Pt, int w) {
		double initVariance = var0;
		double initTime = 0;
		int t, tMinus, s;
		int segStart = 0;
		int segEnd = 0;
		int nlen = sparseNumTimes[w];
		int numSegments = (int) Math.ceil((double) nlen / segmentSize);
		for(int step=0;step<numSegments;step++) {
			segStart = segEnd;
			segEnd = (step+1) * segmentSize;
			if(segEnd > nlen) segEnd = nlen;
			
			for(s=segStart;s<segEnd;s++) {
				t = sparseTimes[w][s];
				if(s==segStart)
					Pt[s] = initVariance + var*(times[t] - initTime);
				else {
					tMinus = sparseTimes[w][s-1];
					Pt[s] = forwardVariance[s-1] + var*(times[t] - times[tMinus]);
				}
				forwardVariance[s] = varHat * Pt[s] / (varHat + Pt[s]);
			}
			backwardVariance[segEnd-1] = forwardVariance[segEnd-1];
			for(s=segEnd-1;s>segStart;s--) {
				backwardVariance[s-1] = forwardVariance[s-1] + Math.pow(forwardVariance[s-1]/Pt[s], 2) * (backwardVariance[s] - Pt[s]);
			}
			
			t = sparseTimes[w][segEnd-1];
			initTime = times[t];
			initVariance = forwardVariance[segEnd-1];
		}

	}



	private double eStep() {
		double likelihood = 0;
		int[] idx = new int[W];
		double[][] pi = new double[K][W];
		double[][] phi = new double[corpus.getMaximumDocumentSize()][K];
		double[] gamma = new double[K];
		finalDocTopicWeights = new double[T][corpus.getNumberOfDocuments()][K];
		for(int t=0;t<T;t++) {
			long start = System.currentTimeMillis();
			for(int k=0;k<K;k++) {
				Arrays.fill(pi[k], 0);
				for(int w=0;w<W;w++) {
					if(idx[w] >= sparseNumTimes[w]) continue;
					int curT = sparseTimes[w][idx[w]];
					if(t==curT)
						pi[k][w] = beta[k][w][idx[w]] - zeta[k][t];
				}
			}
			Set<Document> docs = corpus.getDocumentsByIds(corpus.getDocumentsForDate(dates[t]));
			for(Document d : docs) {
				likelihood += docEStep(idx, t, d, pi, phi, gamma);
				//populating finalDocTopicWeights
				System.arraycopy(gamma, 0, finalDocTopicWeights[t][localDocIds.get(d.getId())], 0, K);
			}
			OpenIntIntHashMap freqs = corpus.getDocumentSubsetWordFrequencies(docs);
			for(int id : freqs.keys().elements())
				idx[id]++;
			System.out.println("current likelihood: " + likelihood + ", timeslice " + (t+1) + " took " + (System.currentTimeMillis() - start)/1000. + "s");
		}
		
		return likelihood;
	}



	private double docEStep(int[] idx, int t,
			Document d, double[][] pi,
			double[][] phi, double[] gamma) {
		double likelihood = 0;
		likelihood = docInference(d, pi, phi, gamma);
		
		//update suff stats
		int w=0;
		double freq;
		int[] freqs = d.getFrequencyList();
		int[] types = d.getTypeList();
		for(int n=0;n<types.length;n++) {
			w = types[n];
			freq = freqs[n];
			for(int k=0;k<K;k++) {
				statsKWT[k][w][idx[w]] += freq * phi[n][k];
				statsKT[k][t] += freq * phi[n][k];
			}
		}
		return likelihood;
	}



	private Double docInference(Document d,
			double[][] pi, double[][] phi, double[] gamma) {
		Double likelihood = 0d;
		double converged = 1, phiSum = 0, likelihoodOld = 0;
		double[] oldPhi = new double[K];
		double[] digammaGam = new double[K];
		
//		OpenIntIntHashMap freqs = d.getWordFrequencies();
		int[] types = d.getTypeList();
		int[] freqs = d.getFrequencyList();
		double docSize = VectorUtils.sum(freqs);
		//posterior dirichlet
		for(int k=0;k<K;k++) {
			gamma[k] = alpha + (docSize / K);
			digammaGam[k] = MathUtils.digamma(gamma[k]);
			for(int n=0;n<d.getNumberOfWordTypes();n++) {
				phi[n][k] = 1./K;
			}
		}
		double varIter = 0;
		int freq;
		while((converged > 1e-3) && ((varIter < 50) || props.iterations == -1)) {
			varIter++;
			for(int n=0;n<d.getNumberOfWordTypes();n++) {
				freq = freqs[n];
				phiSum = 0;
				for(int k=0;k<K;k++) {
					oldPhi[k] = phi[n][k];
					phi[n][k] = digammaGam[k] + pi[k][types[n]];
					if(k>0)
						phiSum = MathUtils.logAdd(phiSum, phi[n][k]);
					else
						phiSum = phi[n][k];
				}
				for(int k=0;k<K;k++) {
					phi[n][k] = Math.exp(phi[n][k] - phiSum);
					gamma[k] = gamma[k] + freq * (phi[n][k] - oldPhi[k]);
					digammaGam[k] = MathUtils.digamma(gamma[k]);
				}
			}
			likelihood = computeLikelihood(d, pi, phi, gamma);
			if(varIter > 2)
				converged = Math.abs((likelihoodOld - likelihood) / likelihoodOld);
			likelihoodOld = likelihood;
		}
		assert(!likelihood.isNaN());
		return likelihood;
	}



	private double computeLikelihood(Document d,
			double[][] pi, double[][] phi, double[] gamma) {
		Double likelihood = 0d;
		double digsum = 0, gammaSum = 0;
		double[] dig = new double[K];
		
		for(int k=0;k<K;k++) {
			dig[k] = MathUtils.digamma(gamma[k]);
			gammaSum += gamma[k];
		}
		digsum = MathUtils.digamma(gammaSum);
		likelihood = Gamma.logGamma(alpha * K) - (K * Gamma.logGamma(alpha)) - Gamma.logGamma(gammaSum);
		for(int k=0;k<K;k++)
			likelihood += (alpha - 1) * (dig[k] - digsum) + Gamma.logGamma(gamma[k]) - (gamma[k] - 1) * (dig[k] - digsum);
		
		int[] types = d.getTypeList();
		int freq;
		
		for(int n=0;n<d.getNumberOfWordTypes();n++) {
			freq = d.getWordFrequencies().get(types[n]);
			for(int k=0;k<K;k++) {
				if(phi[n][k] > 0)
					likelihood += freq * (phi[n][k] * ((dig[k] - digsum) - MathUtils.safeLog(phi[n][k]) + pi[k][types[n]]));
			}
		}

		assert(!likelihood.isNaN());
			
		return likelihood;
	}



	private void zeroStat() {
		for(int k=0;k<K;k++) {
			for(int w=0;w<W;w++) {
				Arrays.fill(statsKWT[k][w], 0);
			}
			Arrays.fill(statsKT[k], 0);
		}
	}

	@Override
	public void loadModel(String filename) throws IOException {
		// TODO Auto-generated method stub
		
	}



	public double getAlpha() {
		return alpha;
	}



	public double getVar0() {
		return var0;
	}



	public double getVar() {
		return var;
	}



	public double getVarHat() {
		return varHat;
	}



	public double[] getTimes() {
		return times;
	}



	public int[][] getSparseTimes() {
		return sparseTimes;
	}



	public double[][] getZeta() {
		return zeta;
	}



	public double[][] getStatsKT() {
		return statsKT;
	}



	public double[][][] getBeta() {
		return beta;
	}



	public double[][][] getStatsKWT() {
		return statsKWT;
	}



	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericDynamicInferencer#doPrediction(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicDocument, double[], int)
	 */
	@Override
	public double doPrediction(int[] types, int[] freqs, double[] topicPredictions, int time) {
		// TODO Auto-generated method stub
		return 0;
	}



	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericDynamicInferencer#doValidation(int[], int[], int[], int)
	 */
	@Override
	public double doValidation(int[] types, int[] freqsLearn,
			int[] freqsPredict, int time) {
		// TODO Auto-generated method stub
		return 0;
	}
	

}
