/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.hdp.posteriorepresentation;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

import cern.jet.random.Gamma;
import cern.jet.random.engine.MersenneTwister;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordToken;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericStaticInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.hdp.posteriorepresentation.state.HDPPRDocState;
import de.uni_leipzig.informatik.asv.topicModels.model.hdp.posteriorepresentation.state.TopicAssignmentWithState;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;
import de.uni_leipzig.informatik.asv.topicModels.utils.WordProb;

public class HDPGibbsSamplerPR extends AbstractGenericStaticInferencer {

	private double alpha;
	private double gamma;
	private double eta;
	private double vEta;
	private double[] beta;
	private TopicAssignmentWithState[] topics;
	
	private int[][] wordCountByTopicAndTerm;
	private int[] wordCountByTopic;
	private int[] tableCountByTopic;
	private int iterations;
	private String targetDir;
	private double[][] phi;
	
	private Gamma randGamma;
	
	public HDPGibbsSamplerPR(Corpus c, Corpus test, Corpus validation, InferencerProperties props) throws IOException {
		super(c, null, null, props);
	}
	
	@Override
	public void doInference() throws IOException {
		init();
		doSampling();
	}

	private void doSampling() throws IOException {
		logger.trace("#iter\ttopics\tgamma\talpha");
		for(int iter = 0;iter < iterations;iter++) {
			doDocumentUpdates();
			resampleBeta();
			if(props.sampleHyperParams && iter > 0){// && (iter % props.hyperSampleLag == 0)) {
				sampleHyperParamsFirstLevel();
				sampleHyperParamsSecondLevel();
			}
			String str = iter + "\t" + K + "\t" + gamma + "\t" + alpha;
			System.out.println(str);
			logger.trace(str);
		}
		getPhi();
	}

	
	private void getPhi() {
		phi = new double[K][V];
		
		for(TopicAssignmentWithState t : topics) {
			for(WordType wt : corpus.getAllWordTypes()) {
				phi[t.getTopicId()][wt.getId()] = (wordCountByTopicAndTerm[t.getTopicId()][localWordTypeIds.get(wt.getId())] + eta) / (wordCountByTopic[t.getTopicId()] + vEta);
			}
		}		
	}

	public void getTopWordsForTopics(int numberOfWords) throws IOException {
		BufferedWriter wordWriter = new BufferedWriter(new FileWriter(targetDir + "/topWords_" + K + "_topics.txt"));
		StringBuffer buf = new StringBuffer();
		WordProb[] wps = new WordProb[corpus.getNumberOfWordTypes()];
		for(TopicAssignmentWithState t : topics) {
			for(WordType wt : corpus.getAllWordTypes()) {
				int localId = localWordTypeIds.get(wt.getId());
				wps[localId] = new WordProb(wt.getValue(), phi[t.getTopicId()][localId], localId);
			}
			Arrays.sort(wps);
			
			buf.append("Topic ").append(t.getTopicId()).append("\n");
			for(int word=0;word<numberOfWords;word++) {
				buf.append(wps[word].getTerm()).append("\t").append(wps[word].getProb()).append("\n");
			}
			buf.append("\n");
		}
		wordWriter.write(buf.toString());
		wordWriter.close();
	}

	
	private void doDocumentUpdates() {
		Arrays.fill(tableCountByTopic, 0);
		for(Document d : corpus.getDocuments()) {
			HDPPRDocState docState = d.getState();
			for(WordToken w : d.getWords()) {
				TopicAssignmentWithState typeState = w.getState();
				removeFromCounts(d, w);
				double[] pi = docState.getPi();
				double[] p = new double[pi.length];
				p[K] = pi[K]/(double)V;
				
				for(int k=0;k<K;k++) {
					p[k] = pi[k]*(wordCountByTopicAndTerm[k][localWordTypeIds.get(w.getTypeId())]+eta)/(wordCountByTopic[k] + vEta);
				}
				
				int newTopic = MathUtils.drawFromDistribution(p);
				int oldTopic = typeState.getTopicId();
				if(newTopic == K) { //new topic chosen
					addTopic(K);
					w.setState(topics[newTopic]);
				} else
					w.setState(topics[newTopic]);

				addToCounts(d, w);
				
				if(wordCountByTopic[oldTopic] == 0) {//remove empty topics from bookkeeping
					removeTopic(oldTopic);
				}
			}
			docState.resamplePi(alpha, beta);
			docState.resampleTableCounts(alpha, beta);
			tableCountByTopic = VectorUtils.addVector(tableCountByTopic, docState.getTableCounts());
		}
		
	}
	
	public void resampleBeta() {
		double[] concentrationParams = new double[K+1];
		concentrationParams[K] = gamma;
		for(int i=0;i<K;i++)
			concentrationParams[i] = tableCountByTopic[i];
		beta = MathUtils.drawFromDirichlet(concentrationParams);
	}
	
	private void sampleHyperParamsFirstLevel() {
		double shape = props.gamma_a;
		double scale = props.gamma_b;
		int n = VectorUtils.sum(tableCountByTopic);
		
		double eta = MathUtils.beta(gamma + 1, n);
		double pi = shape + K - 1;
		double rate = 1. / scale - Math.log(eta);
		pi = pi / (pi + rate * n);
		
		if(MathUtils.bernoulliDraw(pi) == 1) {
			gamma = randGamma.nextDouble(shape + K, rate);
		} else {
			gamma = randGamma.nextDouble(shape + K - 1, rate);
		}
	}

	private void sampleHyperParamsSecondLevel() {
		double shape = props.alpha_a;
		double scale = props.alpha_b;
		
		int n = VectorUtils.sum(tableCountByTopic);
		double rate, sumLogW, sumS;
		
		for(int step = 0;step < 20;step++) {
			sumLogW = 0;
			sumS = 0;
			for(Document d : corpus.getDocuments()) {
				int docLength = d.getLength();
				sumLogW += Math.log(MathUtils.beta(alpha + 1, docLength));
				sumS += (double)MathUtils.bernoulliDraw(docLength / (docLength + alpha));
			}
			rate = 1. / scale - sumLogW;
			alpha = randGamma.nextDouble(shape + n - sumS, rate);
		} 
	}


	private void addTopic(int newTopicId) {
		TopicAssignmentWithState newState = new TopicAssignmentWithState(newTopicId);
		TopicAssignmentWithState[] newTopics = new TopicAssignmentWithState[K+1];
		System.arraycopy(topics, 0, newTopics, 0, topics.length);
		newTopics[K] = newState;
		topics = newTopics;
		
		wordCountByTopic = VectorUtils.addElement(wordCountByTopic);
		tableCountByTopic = VectorUtils.addElement(tableCountByTopic);
		wordCountByTopicAndTerm = VectorUtils.addRow(wordCountByTopicAndTerm);
		
		beta = VectorUtils.addElement(beta);
		double v0 = cern.jet.random.Beta.staticNextDouble(gamma, 1.);
		double beta0 = beta[K];
		beta[K+1] = beta0*v0;
		beta[K] = beta0*(1-v0);
		
		for(Document d : corpus.getDocuments()) {
			HDPPRDocState docState = d.getState();
			docState.addTopic();
			double vj = cern.jet.random.Beta.staticNextDouble(alpha * beta0 * v0, alpha*beta0*(1-v0));
			docState.resetPi(vj);
		}
		K++;
		assert(wordCountByTopic.length == K);
		assert(tableCountByTopic.length == K);
		assert(wordCountByTopicAndTerm.length == K);
		assert(topics.length == K);
	}

	private void removeTopic(int oldTopic) {
		wordCountByTopic = VectorUtils.removeElement(wordCountByTopic, oldTopic);
		tableCountByTopic = VectorUtils.removeElement(tableCountByTopic, oldTopic);
		wordCountByTopicAndTerm = VectorUtils.removeRow(wordCountByTopicAndTerm, oldTopic);
		for(Document d : corpus.getDocuments()) 
			d.<HDPPRDocState>getState().removeTopic(oldTopic);
		
		TopicAssignmentWithState[] newTopics = new TopicAssignmentWithState[K-1];
		
		for(TopicAssignmentWithState t : topics) {
			if(t.getTopicId() == oldTopic)
				continue;
			if(t.getTopicId()>oldTopic)
				t.decreaseId();
			newTopics[t.getTopicId()] = t;
		}
		
		topics = newTopics;
		K--;
		assert(topics.length == K);
		assert(wordCountByTopic.length == K);
		assert(tableCountByTopic.length == K);
		assert(wordCountByTopicAndTerm.length == K);
	}

	private void init() {
		this.iterations = props.iterations;
		this.eta = props.beta;
		this.targetDir = props.targetDir;
		K = MathUtils.nextInt(props.maxInitialTopics);
		V = corpus.getNumberOfWordTypes();
		M = corpus.getNumberOfDocuments();
		randGamma = new Gamma(props.alpha_a, props.alpha_b, new MersenneTwister());

//		if(props.sampleHyperParams) {
//			this.alpha = randGamma.nextDouble(props.alpha_a, 1./props.alpha_b);
//			this.gamma = randGamma.nextDouble(props.gamma_a, 1./props.gamma_b);
//		} else {
			this.alpha = props.alpha;
			this.gamma = props.gamma;
//		}

		
		beta = new double[K+1];
		Arrays.fill(beta, 1.);
		topics = new TopicAssignmentWithState[K];
		
		wordCountByTopicAndTerm = new int[K][V];
		wordCountByTopic = new int[K];
		tableCountByTopic = new int[K];
		vEta = V*eta;
		ArrayList<Document> docs = new ArrayList<>(corpus.getDocuments());

		//for each topic, at least one document has all words assigned to it
		for(int k=0;k<K;k++) {
			initDocument(docs.get(localDocIds.keys().elements()[k]), k);
		}
		
		//assign rest of documents to random topics
		for(int d=K;d<M;d++) {
			initDocument(docs.get(localDocIds.keys().elements()[d]), MathUtils.nextInt(K));
		}
		//sample initial betas
		resampleBeta();
	}

	private void initDocument(Document doc,
			int k) {
		HDPPRDocState docState = new HDPPRDocState(K);
		//create topic k if it does not exist yet
		if(topics[k] == null) topics[k] = new TopicAssignmentWithState(k);
		//create a new state for the doc (with at most K topics)
		doc.setState(docState);
		//for initialization, assign all words the same topic
		for(WordToken w : doc.getWords()) {
			w.setState(topics[k]);
			addToCounts(doc, w);
		}
		//do sampling of pi and m
		docState.resamplePi(alpha, beta);
		docState.resampleTableCounts(alpha, beta);
		tableCountByTopic = VectorUtils.addVector(tableCountByTopic, docState.getTableCounts());
	}
	
	private void addToCounts(Document doc, WordToken w) {
		changeCounts(w, 1);
		doc.<HDPPRDocState>getState().increaseWordCountForTopic(w.<TopicAssignmentWithState>getState().getTopicId());
	}
	
	private void removeFromCounts(Document doc, WordToken w) {
		changeCounts(w, -1);
		doc.<HDPPRDocState>getState().decreaseWordCountForTopic(w.<TopicAssignmentWithState>getState().getTopicId());
	}

	private void changeCounts(WordToken w, int amount) {
		try {
			TopicAssignmentWithState typeState = w.getState();
			wordCountByTopic[typeState.getTopicId()] += amount;
			wordCountByTopicAndTerm[typeState.getTopicId()][localWordTypeIds.get(w.getTypeId())] += amount;
		} catch(Exception e) {
			System.out.println("ouch");
		}

	}


	@Override
	public void loadModel(String filename) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void populateFinalValues() {
		finalTopicWordWeights = phi;
		for(Document d : corpus.getDocuments()) {
			int docId = localDocIds.get(d.getId());
			int[] wordCountsByTopic = d.<HDPPRDocState>getState().getWordCountsByTopic();
			double[] topicDist = VectorUtils.normalize(wordCountsByTopic);
			finalDocTopicWeights[docId] = topicDist;
		}
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericStaticInferencer#doPrediction(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document, double[])
	 */
	@Override
	public double doPrediction(Document d, double[] topicPredictions) {
		// TODO Auto-generated method stub
		return 0;
	}

}
