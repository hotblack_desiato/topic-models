/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.hdp.online;

import cern.colt.matrix.DoubleMatrix2D;

public class OnlineHDPLocalStatisticsVO {

	double batchLikelihood;
	
	DoubleMatrix2D zetaD; 
	
	public double getBatchLikelihood() {
		return batchLikelihood;
	}

	public void setBatchLikelihood(double batchLikelihood) {
		this.batchLikelihood = batchLikelihood;
	}

	public DoubleMatrix2D getZetaD() {
		return zetaD;
	}

	public void setZetaD(DoubleMatrix2D zetaD) {
		this.zetaD = zetaD;
	}

	public DoubleMatrix2D getVarPhiD() {
		return varPhiD;
	}

	public void setVarPhiD(DoubleMatrix2D varPhiD) {
		this.varPhiD = varPhiD;
	}

	DoubleMatrix2D varPhiD;
	
	
}
