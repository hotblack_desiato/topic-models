/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.dtm.state;

import org.apache.commons.math3.analysis.MultivariateVectorFunction;

public class ObservationFunctionGradient implements MultivariateVectorFunction {

	private OptimizationParams params;
	private double initMult;

	public ObservationFunctionGradient(OptimizationParams params) {
		this.params = params;
	}
	
	@Override
	public double[] value(double[] x) throws IllegalArgumentException {
		params.getTopic().getObs()[params.getWord()] = x;
		params.getTopic().computePosteriorMean(params.getWord());
		double[] deriv = this.computeObsDerivative();
		
//		for(int i=0;i<deriv.length;++i)
//			deriv[i] = -deriv[i];
		
		
		return deriv;
	}
	
	private double[] computeObsDerivative() {
		  int t, u, T = params.getTopic().getObs()[0].length;
		  
		  double[] mean = params.getTopic().getMean()[params.getWord()];;
		  double[] variance = params.getTopic().getVariance()[params.getWord()];
		  double[] t_vct = new double[T];
		  double[] deriv = new double[T];
		  
		  // here the T vector in var is the zeta terms
		  for (u = 0; u < T; u++) {
			  t_vct[u] = Math.exp(mean[u+1] + variance[u+1]/2);
		  }

		  double[] w_phi_l = params.getTopic().getwPhiL()[params.getWord()];;
		  double[] m_update_coeff = params.getTopic().getmUpdateCoeff()[params.getWord()];
		  
		  for (t = 0; t < T; t++) {
		    double[] mean_deriv = params.getMean_deriv_mtx()[t];
		    double term1 = 0.0;
		    double term2 = 0.0;
		    double term3 = 0.0;
		    double term4 = 0.0;
		    for (u = 1; u <= T; u++) {
		      double mean_u = mean[u];
		      double var_u_prev = variance[u-1];
		      double mean_u_prev = mean[u-1];
		      double dmean_u = mean_deriv[u];
		      double dmean_u_prev = mean_deriv[u-1];
		      
		      double dmean_u_window_prev = 0.0;
		      double var_u_window_prev = 0.0;
		      double mean_u_window_prev = 0.0;
		      
		      term1 += (mean_u - mean_u_prev) * (dmean_u - dmean_u_prev);
		      
		      // note, observations indexed -1 from mean and variance
		      term2 +=
			(params.getWord_counts()[u-1] -
			 (params.getTotals()[u-1] *
			  t_vct[u-1] /
			  params.getTopic().getZeta()[u-1])) * dmean_u;
		      
		    }
		    if (params.getTopic().getChainVariance() > 0) {
		      term1 = -term1/params.getTopic().getChainVariance();
		      term1 = term1 -
			mean[0] * mean_deriv[0] /
			(initMult * params.getTopic().getChainVariance());
		    } else {
		      term1 = 0.0;
		    }
		    deriv[t] = term1 + term2 + term3 + term4;
		  }
		  return deriv;
	}

}
