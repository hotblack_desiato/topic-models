/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.lngap;

import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils.lgamma;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils.safeLog;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.add;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.divide;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.exp;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.multiplyWithScalar;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.normalize;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Set;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.WordTokenState;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.Type;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.Optimizer;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.cg.FletcherReevesConjugateGradientOptimizer;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.rprop.RPropOptimizer;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericStaticInferencer;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;
import de.uni_leipzig.informatik.asv.topicModels.utils.WordProb;

public class LogNormalGammaPoissonModel extends AbstractGenericStaticInferencer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//variational parameters
	double[][] lambda1;
	double[][] lambda2;
	
	double[][] mu;
	double[][] Sigma;
	
	//expectations
	double[][] expTheta;
	double[][] expLogTheta;
	double[][] expBeta;
	double[][] expEBeta;

	//sufficient statistics
	double[] statsK;
	double[][] statsKW;

	private double zBound;

	private double thetaBound;

	private double betaBound;
	
	
	public LogNormalGammaPoissonModel(Corpus c,
			Corpus test, Corpus validation, InferencerProperties props) throws IOException {
		super(c, test, validation, props);
		init();
	}

	private void init() {
		K = props.numTopics;
		V = corpus.getNumberOfWordTypes();
		M = corpus.getNumberOfDocuments();
		
		statsK = ArrayFactory.doubleArray(Type.ZERO, K);
		statsKW = ArrayFactory.doubleArray(Type.ZERO, K, V);
		
		lambda1 = new double[M][K];
		lambda2 = new double[M][K];
		expTheta = new double[M][K];
		expLogTheta = new double[M][K];
		
		for(int d=0;d<M;d++) {
			for(int k=0;k<K;k++) {
				lambda1[d][k] = props.lngap_a + .01*rng.nextDouble();
				lambda2[d][k] = props.lngap_b + .1*rng.nextDouble();
			}
			updateThetaExpectationsForDoc(d);
		}
		
		mu = new double[K][V];
		Sigma = new double[K][V];
		expBeta = new double[K][V];
		expEBeta = new double[K][V];
		
		//hack hack, set m0 to log(1/V)
		props.lngap_mu = Math.log(1./V);
		logger.info("using prior mean of " + props.lngap_mu);
		for(int w=0;w<V;w++) {
			for(int k=0;k<K;k++) {
				mu[k][w] = props.lngap_mu + .01*rng.nextDouble();
				Sigma[k][w] = props.lngap_sigma + .5*rng.nextDouble();
			}
			updateBetaExpectationsForWord(w);
		}
		zeroStats();
		zBound = thetaBound = betaBound = 0;
	}
	
	

	/**
	 * 
	 */
	private void zeroStats() {
		Arrays.fill(statsK, 0);
		for(int k=0;k<K;k++)
			Arrays.fill(statsKW[k], 0);		
	}

	/**
	 * 
	 */
	private void updateThetaExpectationsForDoc(int d) {
		for(int k=0;k<K;k++) {
			expTheta[d][k] = (lambda1[d][k]/lambda2[d][k]);
			expLogTheta[d][k] = MathUtils.digamma(lambda1[d][k]) - Math.log(lambda2[d][k]);
		}
	}

	/**
	 * 
	 */
	private void updateBetaExpectationsForWord(int w) {
		for(int k=0;k<K;k++) {
			expBeta[k][w] = mu[k][w];
			expEBeta[k][w] = Math.exp(mu[k][w] + .5*Sigma[k][w]);
		}
	}


	

	@Override
	public void doInference() throws IOException {
		//do initial "burn in"
		for(int i=0;i<10;i++)
			updateParameters();
		
		
		double convergence = 1;
		double bound = 0;
		double oldBound = -1e200;
		int iter=0;
		StringBuilder sb = new StringBuilder();
		while(convergence > 1e-4 && iter<props.iterations || iter < 10) {

			bound = updateParameters();
			
			likelihood.add(bound);
			convergence = (oldBound - bound) / oldBound;
			sb.append(bound).append("\t").append(convergence).append("\n");
//			logger.info("bound: " + bound + ", betaBound: " + betaBound + ", thetaBound: " + thetaBound + ", zBound: " + zBound + ", convergence: " + convergence);
			logger.info("bound: " + bound + ", convergence: " + convergence);
			zBound = 0;
			thetaBound = 0;
			betaBound = 0;
			if(convergence<0) {
				logger.info("Warning, bound decreasing");
//				break;
			}
			oldBound = bound;
			iter++;
		}
		BufferedWriter bw = new BufferedWriter(new FileWriter(props.targetDir+File.separator+"likelihood.txt"));
		bw.write(sb.toString());
		bw.close();
		props.iterations = iter;
	}

	private double updateParameters() {
		double likelihood = 0;
		zeroStats();
		for(Document doc : corpus.getDocuments()) {
			likelihood += varEStep(doc);
		}
//		System.out.println("e-step likelihood: " + likelihood);
		
		//optimize topics
		double stepLikelihood = 0;
		for(int step=0;step<3;step++) {
			stepLikelihood = varMStepSingleThread();
			System.out.println("m-step likelihood opt batch " + step + ": " + stepLikelihood);
		}
		likelihood += stepLikelihood;
//		likelihood += varMStepSingleThread();
//		assert(zBound+thetaBound+betaBound == likelihood);
		logger.info("boundSum : " + (zBound+thetaBound+betaBound) + ", likelihood : " + likelihood); 
		return likelihood;
	}

	
	private double varEStep(Document doc) {
		int d = localDocIds.get(doc.getId());
		int[] types = doc.getTypeList();
		int[] freqs = doc.getFrequencyList();

		double converge = 1;
		double oldLikelihood = -1e200;
		double likelihood = 0;
		double[][] phi = ArrayFactory.doubleArray(Type.ZERO, types.length, K);
		double[][] expZ = ArrayFactory.doubleArray(Type.ZERO, types.length, K);
		double phiSum = 0;
		
		while(converge > 1e-3) {
			setThetaToPrior(d);
			
			for(int n=0;n<types.length;++n) {
				int type = types[n];
				int wordFreq = freqs[n];
				int w = localWordTypeIds.get(type);
				for(int k=0;k<K;k++) {
					phi[n][k] = expLogTheta[d][k] + expBeta[k][w];
					if(k==0)
						phiSum = phi[n][k]; 
					else
						phiSum = MathUtils.logAdd(phiSum, phi[n][k]);
					
				}
				for(int k=0;k<K;k++) {
					phi[n][k] = Math.exp(phi[n][k] - phiSum);
					expZ[n][k] = wordFreq*phi[n][k];
					lambda1[d][k] += expZ[n][k];
					lambda2[d][k] += expEBeta[k][w];
				}
			}
			updateThetaExpectationsForDoc(d);
			
			likelihood = computeDocumentLikelihood(doc, phi, expZ);
			converge = (oldLikelihood - likelihood) / oldLikelihood;
			oldLikelihood = likelihood;
		}
		
		//udpdate suff stats
		for(int k=0;k<K;k++) {
			statsK[k] += expTheta[d][k];
			for(int n=0;n<types.length;n++) {
				int w = localWordTypeIds.get(types[n]);
				statsKW[k][w] += expZ[n][k];
			}
		}
		return likelihood;
	}
	
	/**
	 * @param d
	 */
	private void setThetaToPrior(int d) {
		for(int k=0;k<K;k++) {
			lambda1[d][k] = props.lngap_a;
			lambda2[d][k] = props.lngap_b;
		}
		
	}


	/**
	 * @param doc 
	 * @param phi
	 * @return
	 */
	private double computeDocumentLikelihood(Document doc, double[][] phi, double[][] expPhi) {
		double likelihood = 0;
		int d = localDocIds.get(doc.getId());
		int[] types = doc.getTypeList();
		double curVal;
		for(int k=0;k<K;k++) {
			for(int n=0;n<types.length;n++) {
				int w = localWordTypeIds.get(types[n]);
				curVal = expPhi[n][k]*(expLogTheta[d][k] + expBeta[k][w] - safeLog(phi[n][k])) - expTheta[d][k]*expEBeta[k][w];
				likelihood += curVal;
				zBound += curVal;
			}
			//p(theta)
			curVal = props.lngap_a * Math.log(props.lngap_b) + (props.lngap_a - 1) * expLogTheta[d][k]; 
			curVal -= props.lngap_b*expTheta[d][k] + lgamma(props.lngap_a);
			
			//q(theta)
			curVal -= lambda1[d][k]*Math.log(lambda2[d][k]) + (lambda1[d][k] - 1) * expLogTheta[d][k];
			curVal += lambda2[d][k]*expTheta[d][k] + lgamma(lambda1[d][k]);
			likelihood += curVal;
			thetaBound += curVal;
		}
		return likelihood;
	}
	


	
	private double varMStepSingleThread() {
		int optimizeMaxIter = 1000;
		double optimizerConvergence = 1e-6;
		double likelihood = 0;
		double altBound = 0;
		Objective objFun = new Objective(K, this);
		objFun.setMean0(props.lngap_mu);
		objFun.setVar0(props.lngap_sigma);

		Optimizer minimizer = Optimizer.getOptimizer(FletcherReevesConjugateGradientOptimizer.class);
//		Optimizer minimizer = Optimizer.getOptimizer(RPropOptimizer.class);
		for(WordType wt : corpus.getAllWordTypes()) {
			int w = wt.getId();
			objFun.setCurWord(w);
			//contruct value vector and subtract betaExpSums
			double[] guess = new double[K];
			for(int k=0;k<K;k++) {
				guess[k] = mu[k][w];
			}
			Optimizer.optimize(guess, .35, 1e-7, optimizerConvergence, optimizeMaxIter, objFun, minimizer);
			double[] optResult = minimizer.getX();
			
			//update mu and Sigma
			double[] df2 = new double[K]; 
			objFun.df2(optResult, df2);
			for(int k=0;k<K;k++) {
				Sigma[k][w] = -(1./df2[k]);
			}
			
			
			double[] df = new double[K];
			altBound -= objFun.fdf(optResult, df);
			for(int k=0;k<K;k++) {
				altBound -= df[k]*(mu[k][w] - optResult[k]);
				altBound += .5*df2[k]*Math.pow(mu[k][w] - optResult[k], 2);
				altBound += .5*(df2[k]*Sigma[k][w] + Math.log(Sigma[k][w]));
			}
			
			for(int k=0;k<K;k++) {
				mu[k][w] = optResult[k];
			}
			
//			likelihood -= minimizer.getF();
//			likelihood += altBound;
			
			updateBetaExpectationsForWord(w);
		}
		
		likelihood = computeBetaLikelihood(); 
//		logger.info("optimizer bound: " + likelihood + ", computed betaBound: " + betaBound);
		
		return likelihood;
	}
	
	private double computeBetaLikelihood() {
		betaBound = 0;
		double likelihood = 0;
		double curVal;
		for(int k=0;k<K;k++) {
			for(int w=0;w<V;w++) {
				curVal = -Math.log(props.lngap_sigma) - .5*(Math.pow(expBeta[k][w] - props.lngap_mu, 2) + Sigma[k][w])/props.lngap_sigma;
				curVal += Math.log(Sigma[k][w]) - .5;
				betaBound += curVal;
				likelihood += curVal;
			}
		}
		return likelihood;
	}



	@Override
	public void loadModel(String filename) throws IOException {
		// TODO Auto-generated method stub

	}
	
	@Override
	protected void writeTopWordsForTopics(String topicTopWordsFile, int numwords) throws IOException {
		PrintStream ps = new PrintStream(props.targetDir + File.separator + topicTopWordsFile);
		int sizeOfVocabulary = corpus.getNumberOfWordTypes();
		WordProb[] probs = new WordProb[sizeOfVocabulary];
		
		for(int k=0;k<finalTopicWordWeights.length;k++) {
			for(WordType wt : corpus.getAllWordTypes()) {
				int typeId = localWordTypeIds.get(wt.getId());
				probs[typeId] = new WordProb(wt.getValue(), finalTopicWordWeights[k][typeId], typeId);
			}
			Arrays.sort(probs);
			ps.println("Topic " + k);
			for(int i=0;i<numwords;i++)
				ps.println(probs[i].getTerm() + "\t" + probs[i].getProb() + "\t" + Sigma[k][probs[i].getType()]);
			ps.println();
		}
		ps.close();
	}

	@Override
	public void populateFinalValues() {
		finalDocTopicWeights = divide(lambda1, lambda2);
		for(int d=0;d<finalDocTopicWeights.length;++d)
			finalDocTopicWeights[d] = normalize(finalDocTopicWeights[d]);
		
		finalTopicWordWeights = exp(add(mu, multiplyWithScalar(Sigma, .5)));
		for(int k=0;k<finalTopicWordWeights.length;++k)
			finalTopicWordWeights[k] = normalize(finalTopicWordWeights[k]);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericStaticInferencer#doPrediction(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document, double[])
	 */
	@Override
	public double doPrediction(Document d, double[] topicPredictions) {
		//TODO
		return 0;
	}

}
