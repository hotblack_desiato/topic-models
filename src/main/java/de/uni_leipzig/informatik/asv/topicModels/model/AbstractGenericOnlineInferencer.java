/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model;

import java.io.IOException;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;

public abstract class AbstractGenericOnlineInferencer extends AbstractGenericStaticInferencer 
			implements GenericOnlineInferencer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Integer batchSize;
	protected double tau0;
	protected double kappa;

	protected AbstractGenericOnlineInferencer(Corpus c, Corpus test, Corpus validation, InferencerProperties props)
			throws IOException {
		super(c, test, validation, props);
		this.batchSize = props.batchSize;
		this.tau0 = props.tau0;
		this.kappa = props.kappa;
	}


}
