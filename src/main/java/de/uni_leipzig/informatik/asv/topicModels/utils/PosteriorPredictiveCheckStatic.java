/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.topicModels.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cern.colt.map.OpenIntIntHashMap;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordToken;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordTypeNotFoundException;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.ArrayUtils;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericStaticInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.lda.state.TopicAssignment;

/**
 * Performs posterior predictive checking using a realized discrepancy function, in this implementation 
 * mutual information. Algorithm is based on Mimno, D., & Blei, D. M. (2011). Bayesian checking for topic models, 227–237.
 * 
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class PosteriorPredictiveCheckStatic {
	
	private double[][] posteriorTopicWordWeights;
	private double[][] posteriorDocTopicWeights;
	private Corpus corpus;
	private OpenIntIntHashMap localWordTypeIds;
	private OpenIntIntHashMap localDocIds;
	private int K, M, V;
	private ExecutorService threadPool;
	private Logger logger = LogManager.getLogger(this.getClass());


	public PosteriorPredictiveCheckStatic(AbstractGenericStaticInferencer _model) {
		this.posteriorDocTopicWeights = _model.getFinalDocTopicWeights();
		this.posteriorTopicWordWeights = _model.getFinalTopicWordWeights();
		this.corpus = _model.getCorpus();
		this.localDocIds = _model.getLocalDocIds();
		this.localWordTypeIds = _model.getLocalWordTypeIds();
		this.K = _model.getK();
		this.M = _model.getM();
		this.V = _model.getV();
		this.threadPool = _model.getThreadPool();
	}
	
	
	
	public void doPosteriorPredictiveChecking(String testDir, int topicWordlistSize) throws IOException {
		new File(testDir + File.separator + "instantaneous_mi").mkdirs();

		//posterior predictive check via discrepancy functions
		double[][] n_kd = new double[K][M];
		double[][][] n_kdw = new double[K][M][];
		double[] n_k = new double[K];
		double[][] n_kw = new double[K][V];
		//draw a topic assignment sample from the posterior
		sampleTopicAssignment(corpus, n_kdw, n_kd, n_kw, n_k);
		logger.info("done with finding initial topic assignment");
		//mutual information and instant mutual information of observed realization
		double[] mi = new double[K];
		double[][] imi = new double[K][V];
		computeMutualInformationMeasures(corpus, mi, imi, n_kdw, n_kd, n_kw, n_k, false);
		logger.info("computed mutual information measures as found in observed data");
		try {
			double sampleSize = 100;
			double[][] miSample = new double[(int) sampleSize][K];
			double[][][] imiSample = new double[(int) sampleSize][K][V];
			Vector<Future<?>> futures = new Vector<>();
			for(int i=0;i<sampleSize;i++) 
				futures.add(threadPool.submit(new IMIComputation(miSample[i], imiSample[i])));
			for(int i=0;i<futures.size();i++) {
				futures.get(i).get();
				double fin = (i*100d)/sampleSize;
				logger.info("done with " + fin + "% of MI and IMI computation");
			}
			double[][] miData = new double[K][4];
			WordProb[] probs = new WordProb[V];
			for(int k=0;k<K;k++) {
				double[] miSeries = VectorUtils.viewColumn(miSample, k);
				double expectedMI = VectorUtils.mean(miSeries);
				double sdMI = Math.sqrt(VectorUtils.variance(miSeries));
				double deviance = (mi[k] - expectedMI) / sdMI;
				miData[k][0] = mi[k];
				miData[k][1] = expectedMI;
				miData[k][2] = sdMI;
				miData[k][3] = deviance;
				
				double[] imiSeries = new double[(int) (sampleSize/5)];
				getWordsForTopicSorted(probs, k);
				StringBuilder sb = new StringBuilder();
				for(int tw=0;tw<topicWordlistSize;tw++) {
					int w = probs[tw].getType();
					for(int i=0;i<imiSeries.length;i++) {
						imiSeries[i] = imiSample[i][k][w];
					}
					double imiMean = VectorUtils.mean(imiSeries);
					double sdImi = Math.sqrt(VectorUtils.variance(imiSeries));
					sb.append(probs[tw].getTerm()).append("\t");
					sb.append(imi[k][w]).append("\t");
					sb.append(imiMean).append("\t");
					sb.append(sdImi).append("\t");
					sb.append((imi[k][w] - imiMean) / sdImi).append("\n");
				}

				BufferedWriter bw = new BufferedWriter(new FileWriter(testDir + File.separator + "instantaneous_mi" + File.separator + "instantaneous_mi_topic" + (k+1) + ".dat"));
				bw.write(sb.toString());
				bw.close();
			}
			ArrayUtils.saveArrayToFile(testDir + File.separator + "mutual_information.dat", miData);
			
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			throw new IOException(e);
		}

	}

	private void getWordsForTopicSorted(WordProb[] probs, int k) {
		for(WordType wt : corpus.getAllWordTypes()) {
			int typeId = localWordTypeIds.get(wt.getId());
			probs[typeId] = new WordProb(wt.getValue(), posteriorTopicWordWeights[k][typeId], typeId);
		}
		Arrays.sort(probs);
	}

	
	/**
	 * @param n_k 
	 * @param n_kw 
	 * @param n_kd 
	 * @param n_kdw 
	 * @return
	 * @throws WordTypeNotFoundException 
	 */
	private Corpus sampleCorpusFromPosterior(double[][][] n_kdw, double[][] n_kd, double[][] n_kw, double[] n_k) throws WordTypeNotFoundException {
		ArrayUtils.setAll(n_kw, 0.);
		ArrayUtils.setAll(n_kd, 0.);
		Arrays.fill(n_k, 0.);
		Corpus c = new Corpus();
		c.setDictionary(corpus.getDictionary().clone());
		for(Document oldDoc : corpus.getDocuments()) {
			Document doc = new Document(c, oldDoc.getDescription());
			int localDocId = localDocIds.get(oldDoc.getId());
			//oldDoc.getId() and doc.getId() do not necessarily have the same value
			//circumvent using secondaryId
			doc.setSecondaryId(oldDoc.getId());
			for(int i=0;i<oldDoc.getLength();i++) {
				//sample a topic for the current token
				int topic = MathUtils.drawFromDistribution(posteriorDocTopicWeights[localDocId]);
				//sample a word from the sampled topic
				int localWordTypeId = MathUtils.drawFromDistribution(posteriorTopicWordWeights[topic]);
				int typeId = localWordTypeIds.keyOf(localWordTypeId);
				WordToken wt = new WordToken(c.getWordType(typeId), i);
				wt.setState(new TopicAssignment(topic));
				doc.addWord(wt);
			}
			assert(doc.getTypeList().length == doc.getNumberOfWordTypes());
			for(int k=0;k<K;k++)
				n_kdw[k][localDocId] = new double[doc.getNumberOfWordTypes()];
			for(WordToken wt : doc.getWords()) {
				int localWordTypeId = localWordTypeIds.get(wt.getTypeId());
				int typePosInDoc = org.apache.commons.lang3.ArrayUtils.indexOf(doc.getTypeList(), wt.getTypeId());
				int sampledTopic = wt.<TopicAssignment>getState().getId();
				n_kd[sampledTopic][localDocId]++;
				n_kdw[sampledTopic][localDocId][typePosInDoc]++;
				n_k[sampledTopic]++;
				n_kw[sampledTopic][localWordTypeId]++;
			}
		}
		return c;
	}

	private void computeMutualInformationMeasures(Corpus c, double[] mi, double[][] imi, double[][][] n_kdw, double[][] n_kd, double[][] n_kw, double[] n_k, boolean collect) throws IOException {
		int[] docTypeList;
		for(int k = 0;k < K; k++) {
			double H_D = 0;
			for(Document doc : c.getDocuments()) {
				int d = localDocIds.get(doc.getSecondaryId());
				//fraction of words assigned to the topic in current document and 
				//words assigned to that topic over all documents
				if(n_kd[k][d] != 0 && n_k[k] != 0)
					H_D -= (n_kd[k][d]/n_k[k])*Math.log(n_kd[k][d]/n_k[k]);
				docTypeList = doc.getTypeList();
				for(int wt = 0;wt < docTypeList.length;wt++) {
					int w = localWordTypeIds.get(docTypeList[wt]);
					if(n_kdw[k][d][wt] == 0 || n_kd[k][d] == 0 || n_k[k] == 0 || n_kw[k][w] == 0)
						continue;
					// subtract fraction of word assigned to the topic in current document and
					// word assigned to that topic over all documents
					imi[k][w] = (n_kdw[k][d][wt] / n_kd[k][d]) * Math.log((n_kdw[k][d][wt] * n_k[k])/ (n_kd[k][d] * n_kw[k][w]));
					mi[k] += n_kdw[k][d][wt]/n_k[k] * Math.log((n_kdw[k][d][wt] * n_k[k]) / (n_kd[k][d] * n_kw[k][w]));
				}
			}
			imi[k] = VectorUtils.addScalar(imi[k], H_D);
		}

	}

	private void sampleTopicAssignment(Corpus c, double[][][] n_kdw, double[][] n_kd, 
			double[][] n_kw, double[] n_k) {

		ArrayUtils.setAll(n_kw, 0.);
		ArrayUtils.setAll(n_kd, 0.);
		Arrays.fill(n_k, 0.);
		
		for(Document d : c.getDocuments()) {
			int localDocId = localDocIds.get(d.getId());
			for(int k=0;k<K;k++)
				n_kdw[k][localDocId] = new double[d.getNumberOfWordTypes()];
			for(WordToken wt : d.getWords()) {
				int localWordTypeId = localWordTypeIds.get(wt.getTypeId());
				int typePosInDoc = org.apache.commons.lang3.ArrayUtils.indexOf(d.getTypeList(), wt.getTypeId());
				//sample topic assignment and do bookkeeping
				double[] distributionOverTopics = VectorUtils.multiply(
						posteriorDocTopicWeights[localDocId],
						VectorUtils.viewColumn(posteriorTopicWordWeights, localWordTypeId)
						);
				distributionOverTopics = VectorUtils.normalize(distributionOverTopics);
				int sampledTopic = MathUtils.drawFromDistribution(distributionOverTopics);
				n_kd[sampledTopic][localDocId]++;
				n_kdw[sampledTopic][localDocId][typePosInDoc]++;
				n_k[sampledTopic]++;
				n_kw[sampledTopic][localWordTypeId]++;
			}
		}
	}
	
	private class IMIComputation implements Runnable {
		
		private double[][] imi;
		private double[] mi;
		/**
		 * 
		 */
		public IMIComputation(double[] globalMI, double[][] globalIMI) {
			this.mi = globalMI;
			this.imi = globalIMI;
		}
		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			double[][] n_kd = new double[K][M];
			double[][][] n_kdw = new double[K][M][];
			double[] n_k = new double[K];
			double[][] n_kw = new double[K][V];
			Corpus sample;
			long start = System.currentTimeMillis(), duration;
			try {
				sample = sampleCorpusFromPosterior(n_kdw, n_kd, n_kw, n_k);
				duration = (System.currentTimeMillis() - start)/1000l;
				logger.debug("created corpus sample from posterior predictive distribution, took " + duration + "s");
				start = System.currentTimeMillis();
				computeMutualInformationMeasures(sample, mi, imi, n_kdw, n_kd, n_kw, n_k, true);
				duration = (System.currentTimeMillis() - start)/1000l;
				logger.debug("computed mutual information scores for current sample, took " + duration + "s");
			} catch (WordTypeNotFoundException | IOException e) {
				logger.fatal(e.getMessage());
			} finally {
				logger.debug("cleaning up");
				n_k = null;
				n_kd = null;
				n_kw = null;
				n_kdw = null;
				sample = null;
				Runtime.getRuntime().gc();
				System.gc();
			}
			
		}
		
	}
	
}
