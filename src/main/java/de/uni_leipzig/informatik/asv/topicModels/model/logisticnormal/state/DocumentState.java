/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.logisticnormal.state;

import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.*;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils.*;
public class DocumentState extends de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1704586824817117690L;
	private int[] localWordTypeIds;
	private int localDocId;
	private int[] wordTypeFreqs;
	private double[] docLambda;
	private double[][] docPhi;
	private int K;
	
	public DocumentState(int localDocId, int[] localWordTypeIds, int[] freqs, int numTopics) {
		this.localWordTypeIds = localWordTypeIds;
		this.localDocId = localDocId;
		this.wordTypeFreqs = freqs;
		this.docLambda = new double[numTopics];
		this.docPhi = new double[localWordTypeIds.length][numTopics];
		this.K = numTopics;
//		for(int word : localWordTypeIds)
//			docPhi.put(word, new double[K]);
	}
	
	public int[] getWordTypeFrequencies() {
		return wordTypeFreqs;
	}

	public int[] getLocalWordTypeIds() {
		return localWordTypeIds;
	}

	public int getLocalDocId() {
		return localDocId;
	}
	
	public double getDocLambdaForTopic(int topic) {
		return docLambda[topic];
	}
	
	public void setDocLambdaForTopic(int topic, double newLambda) {
		docLambda[topic] = newLambda;
	}
	
	public double getLambdaSum() {
		return sum(docLambda);
	}
	
	public double[] getExpectedLogTheta() {
		return subtractScalar(digamma(docLambda), digamma(getLambdaSum()));
	}
	
	//TODO: maybe convert to a int-double list?
	public double getDocPhiForTopicAndWord(int topic, int wordId) {
		return docPhi[wordId][topic];
	}
		
	public void setDocPhiForTopicAndWord(int topic, int wordId, double newPhi) {
		docPhi[wordId][topic] = newPhi;
	}
	
	public void renormalizePhi() {
		for(int w=0;w<localWordTypeIds.length;++w) {
			docPhi[w] = normalize(docPhi[w]);
		}
	}
		
	private double computeLogLikelihoodW(double[][] pi) {
		double ll = 0;
		for(int n=0;n<localWordTypeIds.length;++n) {
			for(int k=0;k<K;++k)
				ll += wordTypeFreqs[n]*pi[k][localWordTypeIds[n]];
		}
			
		
		return ll;
	}
	
	private double computeLogLikelihoodZ() {
		double eLogPz = 0;
		double eLogQz = 0;
		double digSum = digamma(getLambdaSum());
		for(int w=0;w<localWordTypeIds.length;++w) {
			for(int k=0;k<K;++k) {
				eLogPz += wordTypeFreqs[w] * docPhi[w][k] * (digamma(docLambda[k]) - digSum);
				eLogQz += wordTypeFreqs[w] * docPhi[w][k] * safeLog(docPhi[w][k]);
			}
		}
		return (eLogPz - eLogQz);
	}
	
	private double computeLogLikelihoodTheta(double alpha) {
		double eLogPTheta = 0;
		double eLogQTheta = 0;
		double eLogPTheta2 = 0;
		double eLogQTheta2 = 0;
		double[] temp = getExpectedLogTheta();
		
		eLogPTheta2 = lgamma(K*alpha) - K * lgamma(alpha);
		eLogQTheta2 = -lgamma(sum(docLambda));
		for(int k=0;k<K;++k) {
			eLogPTheta2 += (alpha-1)*temp[k];
			eLogQTheta2 += lgamma(docLambda[k]) + (docLambda[k] - 1) * temp[k];
		}
		
		
		eLogPTheta = sum(multiplyWithScalar(temp, alpha-1));
		eLogQTheta = K*lgamma(alpha) - lgamma(K*alpha) + sum(multiply(temp, subtractScalar(docLambda, 1.)));
				
		
		return (eLogPTheta2 - eLogQTheta2);
	}
	
	public double computeDocLikelihood(double alpha, double[][] pi) {
		return (computeLogLikelihoodTheta(alpha) + computeLogLikelihoodZ() + computeLogLikelihoodW(pi));
	}


	public void recomputeVariationalParameters(double alpha, double[][] pi) {
		double docLen = sum(wordTypeFreqs);
		for(int k=0;k<K;++k) {
			docLambda[k] = alpha + docLen/K;
			for(int widx=0;widx<localWordTypeIds.length;++widx)
				docPhi[widx][k] = 1./K;
		}
		
		double[] digLambda = digamma(docLambda);
		
		double[] oldPhi = new double[K];
		double phiSum;
		for(int widx=0;widx<localWordTypeIds.length;++widx) {
			phiSum = 0;
			for(int k=0;k<K;++k) {
				oldPhi[k] = docPhi[widx][k];
				docPhi[widx][k] = digLambda[k] + pi[k][localWordTypeIds[widx]];
				if(k==0)
					phiSum = docPhi[widx][k];
				else
					phiSum = logAdd(phiSum, docPhi[widx][k]);
			}
			
			for(int k=0;k<K;++k) {
				docPhi[widx][k] = Math.exp(docPhi[widx][k] - phiSum);
				docLambda[k] += wordTypeFreqs[widx]*(docPhi[widx][k] - oldPhi[k]);
			}
			digLambda = digamma(docLambda);
		}
	}
}
