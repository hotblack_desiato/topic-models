/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.logisticnormal;

import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.*;

import java.util.Arrays;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.WordTokenState;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;
import de.uni_leipzig.informatik.asv.topicModels.model.logisticnormal.state.DocumentState;

public class Objective extends ObjectiveFunction {
	
	private int topic;
	
	protected Objective(int dim, LogisticNormalTopicModel p) {
		super(dim, p);
	}

	@Override
	public double f(double[] point) {
		LogisticNormalTopicModel p = getParameters();
		double f = 0;

		double[] logPi = subtractScalar(point, Math.log(sum(exp(point))));
		
		for(Document doc : p.getCorpus().getDocuments()) {
			int[] localTypes = doc.<DocumentState>getState().getLocalWordTypeIds();
			int[] wordFreqs = doc.<DocumentState>getState().getWordTypeFrequencies();
			
			for(int widx=0;widx < localTypes.length;++widx) {
				double scaledFreq = wordFreqs[widx] * doc.<DocumentState>getState().getDocPhiForTopicAndWord(topic, widx);
				f += logPi[localTypes[widx]]*scaledFreq;
			}
		}

		double[] xMinusMu = subtract(point, p.mean[topic]);
		double[][] tempSig = new double[p.getV()][p.getV()];
		for(int w=0;w<p.getV();++w)
			tempSig[w][w] = 1./p.var[topic][w];
		double[] muTimesSigma = dotProduct(xMinusMu, tempSig);
		f -= .5*(dotProduct(muTimesSigma, xMinusMu));
		return -f;
	}
	
	@Override
	public void df(double[] point, double[] df) {
		LogisticNormalTopicModel p = getParameters();
		Arrays.fill(df, 0);
		
		double[] expPoint = VectorUtils.exp(point);
		double sumExp = VectorUtils.sum(expPoint);
		double[] pi = VectorUtils.divideByScalar(expPoint, sumExp);
		
		for(Document doc : p.getCorpus().getDocuments()) {
			int[] localTypes = doc.<DocumentState>getState().getLocalWordTypeIds();
			int[] wordFreqs = doc.<DocumentState>getState().getWordTypeFrequencies();
			
			for(int widx=0;widx<localTypes.length;++widx) {
				double scaledFreq = wordFreqs[widx] * doc.<DocumentState>getState().getDocPhiForTopicAndWord(topic, widx);
				df[localTypes[widx]] += scaledFreq - scaledFreq*pi[localTypes[widx]];
			}
						
		}

		double[] xMinusMu = VectorUtils.subtract(point, p.mean[topic]);
		double[][] tempSig = new double[p.getV()][p.getV()];
		for(int w=0;w<p.getV();++w)
			tempSig[w][w] = 1./p.var[topic][w];
		VectorUtils.subtract_(df, VectorUtils.dotProduct(tempSig, xMinusMu));
		VectorUtils.neg_(df);
	}


	@Override
	public double fdf(double[] point, double[] df) {
		LogisticNormalTopicModel p = getParameters();
		double f = 0;
		Arrays.fill(df, 0);
		
		double[] expPoint = VectorUtils.exp(point);
		double sumExp = VectorUtils.sum(expPoint);
		double[] pi = VectorUtils.divideByScalar(expPoint, sumExp);
		double[] logPi = VectorUtils.subtractScalar(point, Math.log(sumExp));
		
		for(Document doc : p.getCorpus().getDocuments()) {
			int[] localTypes = doc.<DocumentState>getState().getLocalWordTypeIds();
			int[] wordFreqs = doc.<DocumentState>getState().getWordTypeFrequencies();

			for(int widx=0;widx < localTypes.length;++widx) {
				double scaledFreq = wordFreqs[widx] * doc.<DocumentState>getState().getDocPhiForTopicAndWord(topic, widx);
				f += logPi[localTypes[widx]]*scaledFreq;
				df[localTypes[widx]] += scaledFreq - scaledFreq*pi[localTypes[widx]];
			}
		}

		double[] xMinusMu = VectorUtils.subtract(point, p.mean[topic]);
		double[][] tempSig = new double[p.getV()][p.getV()];
		for(int w=0;w<p.getV();++w)
			tempSig[w][w] = 1./p.var[topic][w];
		double[] muTimesSigma = VectorUtils.dotProduct(xMinusMu, tempSig);
		f -= .5*(VectorUtils.dotProduct(muTimesSigma, xMinusMu));
		VectorUtils.subtract_(df, VectorUtils.dotProduct(tempSig, xMinusMu));
		VectorUtils.neg_(df);

		return -f;
	}

	/**
	 * @param topic the topic to set
	 */
	public void setTopic(int topic) {
		this.topic = topic;
	}


}
