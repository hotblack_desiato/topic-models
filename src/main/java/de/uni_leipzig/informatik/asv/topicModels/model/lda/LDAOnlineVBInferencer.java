/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.lda;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.jet.math.Functions;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordToken;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.DoubleFunctionFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MatrixUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericOnlineInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.lda.state.TopicAssignment;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;

public class LDAOnlineVBInferencer extends AbstractGenericOnlineInferencer {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double alphaInit, eta;
	
	protected DoubleMatrix2D lambda, ELogBeta, expELogBeta, gamma, ELogTheta, expELogTheta, suffStats;
	private DoubleMatrix1D alpha;
	
	//protected InferencerProperties props;
	
	private double updateCount;
	protected double rhoT;
	protected double curBound;
	
	public LDAOnlineVBInferencer(Corpus c, Corpus test, Corpus validation, InferencerProperties props) throws IOException {
		super(c, test, validation, props);
		initialize();
	}

	protected void initialize() {
		this.K = props.numTopics;
		this.M = corpus.getNumberOfDocuments();
		this.V = corpus.getNumberOfWordTypes();
		this.alphaInit = props.alpha;
		this.eta = props.eta;
		this.tau0 = props.tau0;
		this.kappa = props.kappa;
		this.batchSize = props.batchSize;
		
		alpha = new DenseDoubleMatrix1D(K).assign(alphaInit);
		
		//init var distribution q(beta|lambda)
		lambda = new DenseDoubleMatrix2D(K, V);
		lambda.assign(DoubleFunctionFactory.gammaRand);
		ELogBeta = MatrixUtils.logDirichletExpectation(lambda);
		expELogBeta = ELogBeta.copy().assign(Functions.exp);
		//allocate gamma
		gamma = new DenseDoubleMatrix2D(batchSize,K);
	}
	
	
	private void eStep(Set<Integer> docs) {
		int[] wordTypeIds, wordTypeCounts;
		DoubleMatrix1D wordTypeCountMatrix, gammaD;
		
		
		int counter = 0;
		
		//matrix of variational params for the document topic proportions of the document batch
		gamma.assign(DoubleFunctionFactory.gammaRand);
		
		ELogTheta = MatrixUtils.logDirichletExpectation(gamma);
		expELogTheta = ELogTheta.copy().assign(Functions.exp);
		
		//statistics for intermediate global variational params lambda
		suffStats = new DenseDoubleMatrix2D(lambda.rows(), lambda.columns()).assign(0);
		
		//optimize local parameters gamma and phi for each document
		for(Integer docId : docs) {
			Document d = corpus.getDocumentById(docId);
			
			wordTypeIds = d.getFrequencyList();
			
			int[] localTypes = new int[wordTypeIds.length];
			for(int i=0;i<wordTypeIds.length;i++)
				localTypes[i] = localWordTypeIds.get(wordTypeIds[i]);
			wordTypeCounts = d.getTypeList();
			
			wordTypeCountMatrix = getWordTypeCountMatrix(wordTypeCounts);
			
			
			
			OnlineLDALocalStatisticsVO batch = documentPrediction(wordTypeIds, wordTypeCountMatrix, localTypes, counter);
			gammaD = batch.getGammaD();
			DoubleMatrix1D phiNorm = batch.getPhiNorm();
			DoubleMatrix1D expELogThetaD = batch.getExpELogThetaD();
			//update gamma for the optimized document
			gamma.viewRow(counter).assign(gammaD);
			
			//contribution of the current document to the expected sufficient statistics for the m step
			DoubleMatrix1D normCts = wordTypeCountMatrix.copy().assign(phiNorm, Functions.div);
			DoubleMatrix2D outerProd = MatrixUtils.outerProduct(expELogThetaD, normCts);
			for(int k=0;k<K;k++) {
				suffStats.viewRow(k).viewSelection(localTypes).assign(outerProd.viewRow(k), Functions.plus);
			}
			counter++;
		}

		//finish computation of sufficient statistics for m step
		//suffStats[k,w] = \sum_d n_{dw} * phi_{dwk} = \sum_d n_{dw} * \exp{ELogTheta_{d_k} + ELogBeta_{kw}} / phiNorm_{dw}
		suffStats.assign(expELogBeta, Functions.mult);
	}

	protected OnlineLDALocalStatisticsVO documentPrediction(int[] wordTypeIds, DoubleMatrix1D wordTypeCountMatrix, int[] localTypes, int docId)
	{
		OnlineLDALocalStatisticsVO result = new OnlineLDALocalStatisticsVO();
		
		double meanchange = 0;
		
		DoubleMatrix1D gammaD, ELogThetaD, expELogThetaD, lastGamma;
		
		DoubleMatrix2D expELogBetaD;
		
		gammaD = new DenseDoubleMatrix1D(K).assign(DoubleFunctionFactory.gammaRand);
		ELogThetaD = MatrixUtils.logDirichletExpectation(gammaD);
		expELogThetaD = ELogThetaD.assign(Functions.exp);
		
		expELogBetaD = new DenseDoubleMatrix2D(K, wordTypeIds.length);
		for(int k=0;k<K;k++) {
			expELogBetaD.viewRow(k).assign(expELogBeta.viewRow(k).viewSelection(localTypes));
		}

		//the optimal phi_{dwk} is proportional to expELogThetaD_k * expELogBetaD_w, thus phiNorm is the normalizer
		DoubleMatrix1D phiNorm = MatrixUtils.dotProductOverColumns(expELogThetaD, expELogBetaD).assign(Functions.plus(1e-100));
		
		DoubleMatrix2D expELogBetaDTransposed = expELogBetaD.viewDice().copy();
		
		
		//iterate between gamma and phi until convergence
		for(int iter=0;iter<10000;iter++) {
			lastGamma = gammaD.copy();
			
			//compute gamma_d
			//first, normalize the wordcounts wrt to phi,
			DoubleMatrix1D normalizedWordCounts = wordTypeCountMatrix.copy().assign(phiNorm, Functions.div);
			DoubleMatrix1D dotProdNormCountsELogBetaD = MatrixUtils.dotProductOverColumns(normalizedWordCounts, expELogBetaDTransposed); 
			DoubleMatrix1D eLogThetaDTimesDotProd = expELogThetaD.copy().assign(dotProdNormCountsELogBetaD, Functions.mult);
			//phi is only represented implicitly through phiNorm, 
			//this update is derived by substituting the optimal phi into the update for gamma
			gammaD = eLogThetaDTimesDotProd.copy().assign(alpha, Functions.plus);
			
			//update the intermediate local distribution from the variational parameters
			ELogThetaD = MatrixUtils.logDirichletExpectation(gammaD);
			expELogThetaD = ELogThetaD.copy().assign(Functions.exp);
			
			//recompute phiNorm (implicit representation of phi)
			phiNorm = MatrixUtils.dotProductOverColumns(expELogThetaD, expELogBetaD).assign(Functions.plus(1e-100));

			//after reaching the convergence criterion we are done
			meanchange = MatrixUtils.mean(gammaD.copy().assign(lastGamma, Functions.minus).assign(Functions.abs));
			if(meanchange < props.convergenceCriterion)
				break;
		}
		
		result.setGammaD(gammaD);
		result.setPhiNorm(phiNorm);
		result.setExpELogThetaD(expELogThetaD);
		
		return result;
	}
	
	private DoubleMatrix1D getWordTypeCountMatrix(int[] wordTypeCounts) {
		DoubleMatrix1D wordTypeCountMatrix;
		wordTypeCountMatrix = new DenseDoubleMatrix1D(wordTypeCounts.length);
		for(int i=0;i<wordTypeCounts.length;++i)
			wordTypeCountMatrix.set(i, wordTypeCounts[i]);
		return wordTypeCountMatrix;
	}
	
	private double computeApproxBound(Set<Integer> docs) {
		double score = 0;
		DoubleMatrix2D ELogTheta = MatrixUtils.logDirichletExpectation(gamma);
		
		//E[log p(docs|theta, beta)]
		int counter = 0;
		for(Integer docId : docs) {
			Document d = corpus.getDocumentById(docId); 
			int[] wordIds = d.getWordFrequencies().keys().elements();
			int[] counts = d.getWordFrequencies().values().elements();
			
			DoubleMatrix1D wordTypeCountMatrix = getWordTypeCountMatrix(counts);
			
			DoubleMatrix1D phiNorm = new DenseDoubleMatrix1D(wordIds.length);
			
			for(int w=0;w<wordIds.length;w++) {
				DoubleMatrix1D temp = ELogTheta.viewRow(counter).copy().assign(ELogBeta.viewColumn(localWordTypeIds.get(wordIds[w])), Functions.plus);
				double tmax = VectorUtils.max(temp.toArray());
				double sum = temp.assign(Functions.minus(tmax)).assign(Functions.exp).zSum();
				phiNorm.set(w, Math.log(sum) + tmax);
			}
			score += wordTypeCountMatrix.assign(phiNorm, Functions.mult).zSum();
			counter++;
		}
		score += MatrixUtils.addRowWise(gamma.copy().assign(Functions.neg),alpha).assign(ELogTheta, Functions.mult).zSum();
		score += MatrixUtils.addRowWise(gamma.copy().assign(DoubleFunctionFactory.loggamma), alpha.copy().assign(DoubleFunctionFactory.loggamma).assign(Functions.neg)).zSum();
		score += MatrixUtils.sumRows(gamma).assign(DoubleFunctionFactory.loggamma).assign(Functions.neg).assign(Functions.plus(MathUtils.loggamma(alpha.zSum()))).zSum();
		
		score *= (double)M/(double)batchSize;
		
		score += lambda.copy().assign(Functions.neg).assign(Functions.plus(eta)).assign(ELogBeta, Functions.mult).zSum();
		score += lambda.copy().assign(DoubleFunctionFactory.loggamma).assign(Functions.minus(MathUtils.loggamma(eta))).zSum();
		score += MatrixUtils.sumRows(lambda).assign(DoubleFunctionFactory.loggamma).assign(Functions.neg).assign(Functions.plus(MathUtils.loggamma(V*eta))).zSum();
		
		return score;
	}
	
	public void runBatch(Set<Integer> docs) {
		eStep(docs);
		curBound = computeApproxBound(docs);
				
		DoubleMatrix2D newLambda = suffStats.copy();
		newLambda.assign(Functions.div(docs.size())).assign(Functions.mult(M)).assign(Functions.plus(eta));
		rhoT = computeLearningRate(newLambda);
		newLambda.assign(Functions.mult(rhoT));
		lambda.assign(Functions.mult(1-rhoT)).assign(newLambda, Functions.plus);
		
		ELogBeta = MatrixUtils.logDirichletExpectation(lambda);
		expELogBeta = ELogBeta.copy().assign(Functions.exp);

		if(props.sampleHyperParams) {
			//alpha
			DoubleMatrix2D ELogThetaBatchT = MatrixUtils.logDirichletExpectation(gamma).viewDice();
			DoubleMatrix1D ELogThetaSumOverD = MatrixUtils.sumRows(ELogThetaBatchT);//.assign(Functions.exp);
			
			DoubleMatrix1D grad = new DenseDoubleMatrix1D(K);
			double digammaAlphaSum = MathUtils.digamma(alpha.zSum());
			DoubleMatrix1D digammaAlpha = alpha.copy().assign(DoubleFunctionFactory.digamma);
			digammaAlpha.assign(Functions.neg).assign(Functions.plus(digammaAlphaSum));
			//?
			digammaAlpha.assign(Functions.mult(M));//.assign(Functions.div(M));
			
			grad.assign(digammaAlpha).assign(ELogThetaSumOverD, Functions.plus);
			grad.assign(Functions.div(M));
			grad.assign(Functions.mult(rhoT));
//			grad.assign(MathUtils.digamma(alpha.zSum())).assign(alpha.copy().assign(DoubleFunctionFactory.loggamma), Functions.minus).assign(Functions.mult(docs.size())).assign(Functions.div(M)).assign(Functions.exp);
//			grad.assign(ELogThetaSumOverD, Functions.mult).assign(Functions.mult(rhoT));//.assign(Functions.exp).assign(Functions.mult(rhoT));
			alpha.assign(grad, Functions.plus);
			//beta
//			OpenIntIntHashMap docSetWordFreqs = corpus.getDocumentSubsetWordFrequencies(docs);
//			int[] localTypes = new int[docSetWordFreqs.size()];
//			for(int i=0;i<localTypes.length;i++)
//				localTypes[i] = localWordTypeIds.get(docSetWordFreqs.keys().get(i));

//			double ELogBetaSum = ELogBeta.zSum();
//			for(int k=0;k<K;++k)
//				ELogBetaSum += ELogBeta.viewRow(k).viewSelection(localTypes).zSum();
//			ELogBetaSum = Math.exp(ELogBetaSum);
//			double usedWordsCountFraction = (double)docSetWordFreqs.size()/(double)W;
//			double gradient = K*(MathUtils.digamma(W*eta) - W*MathUtils.digamma(eta)) + ELogBetaSum;
//			gradient /= W;
//			gradient = gradient*rhoT;
//			eta = eta + gradient;//Math.exp(gradient)*rhoT;
		}

		
		
		updateCount++;
	}
	
	public double computeLearningRate(DoubleMatrix2D sampledGradient) {
		return Math.pow(tau0 + updateCount, -kappa);
	}
	

	@Override
	public void doInference() throws IOException {
		logger.info("#starting online inference");
		logger.info("#using window size: " + batchSize);
		
		Iterator<Document> docIter = corpus.getShuffledDocumentIterator();
		
		HashSet<Integer> curSlice = new HashSet<>();
		int processedDocs = 0;
		int wordCount = 0;
		int runs = 0;
		logger.info("#docs\tbound\tperplexity\tlearnRate\teta");
		while(docIter.hasNext()) {
			Document d = docIter.next();
			curSlice.add(d.getId());
			wordCount += d.getLength();
			processedDocs++;
			if(processedDocs%batchSize==0) {
				runs++;
				long start = System.currentTimeMillis();
				runBatch(curSlice);
				long dur = System.currentTimeMillis() - start;
				System.out.println(runs + ".: bound after processing " + processedDocs + ": " + curBound + ", took " + (dur/1000) + "s");
//				logger.println("current alpha = " + alpha + ", current eta = " + eta);
				double perWordBound = curBound*batchSize / ((double)M*(double)wordCount);
//				System.out.println(runs + ".: rho_t = " + rhoT + ", held-out perplexity estimate: " + Math.exp(-perWordBound));
				logger.info(runs*batchSize + "\t" + curBound + "\t" + Math.exp(-perWordBound) + "\t" + rhoT + "\t" + eta);
				curSlice.clear();
				wordCount = 0;
			}
		}
		
		gamma = new DenseDoubleMatrix2D(corpus.getNumberOfDocuments(),K);
		gamma.assign(DoubleFunctionFactory.gammaRand);
		logger.info("reinferring document topic distributions with final topics");
		reinferDocuments(corpus.getDocumentIds());
		logger.info("#done with online inference");
	}

	
	
	
	private void reinferDocuments(Set<Integer> set) {
		int[] wordTypeIds, wordTypeCounts;
		DoubleMatrix1D wordTypeCountMatrix, gammaD;
		
		
		//matrix of variational params for the document topic proportions of the document batch
		gamma.assign(DoubleFunctionFactory.gammaRand);
		
		ELogTheta = MatrixUtils.logDirichletExpectation(gamma);
		expELogTheta = ELogTheta.copy().assign(Functions.exp);
		//optimize local parameters gamma and phi for each document
		for(Integer docId : set) {
			int localId = localDocIds.get(docId);
			Document d = corpus.getDocumentById(docId);
			wordTypeIds = d.getTypeList();
			int[] localTypes = new int[wordTypeIds.length];
			for(int i=0;i<wordTypeIds.length;i++)
				localTypes[i] = localWordTypeIds.get(wordTypeIds[i]);
			wordTypeCounts = d.getFrequencyList();
			
			wordTypeCountMatrix = getWordTypeCountMatrix(wordTypeCounts);
			
			OnlineLDALocalStatisticsVO batch = documentPrediction(wordTypeIds, wordTypeCountMatrix, localTypes, localId);
			
			gammaD = batch.getGammaD();
			DoubleMatrix1D expELogThetaD = batch.getExpELogThetaD();
			//update gamma for the optimized document
			gamma.viewRow(localId).assign(gammaD);
			expELogTheta.viewRow(localId).assign(expELogThetaD);
		}
	}

	@Override
	public void populateFinalValues() {
		finalDocTopicWeights = expELogTheta.toArray();
		finalTopicWordWeights = expELogBeta.toArray();
	}

	
	
	private void writeTopicCountsTopWords() throws FileNotFoundException {
		
		StringBuffer sb = new StringBuffer();
		
		PrintStream file = new PrintStream(props.targetDir + File.separator + "topicCounts.dat");
		
		int[][] wordCountByTopicAndTerm = new int[K][];
		int[] wordCountByTopic = new int[K];
		for (int k = 0; k < K; k++) 	// var initialization done
		{
			wordCountByTopicAndTerm[k] = new int[V];
			
		}
			
		
		for(Document d : corpus.getDocuments())
		{
			for(WordToken t : d.getWords())
			{
				int localType = localWordTypeIds.get(t.getTypeId());
				int topic = t.<TopicAssignment>getState().getId();
				
				wordCountByTopicAndTerm[topic][localType]++;
				wordCountByTopic[topic]++;
			}
		}
		
		for (int k = 0; k < K; k++) {
			for (int w = 0; w < V; w++)
				sb.append(String.format("%05d ", wordCountByTopicAndTerm[k][w]));
				//file.format("%05d ", wordCountByTopicAndTerm[k][w]);
			sb.append("\n");
		}
		file.print(sb.toString());
		file.close();
	}
	
	private void writeAssignments() throws FileNotFoundException {
		
		StringBuffer sb = new StringBuffer();
		
		PrintStream file = new PrintStream(props.targetDir + File.separator + "assignments.dat");
		sb.append("d w z\n");
		//file.println("d w z");
		
		for (Document d : corpus.getDocuments()) {
			
			for(WordToken t : d.getWords())
			{
				int localType = localWordTypeIds.get(t.getTypeId());
				int topic = t.<TopicAssignment>getState().getId();
				
				sb.append(d.getId() + " " + localType + " " + topic + "\n");
			}
		}
		file.print(sb.toString());
		file.close();
	}
	


	
	public double[][] doPrediction(Set<Integer> documents) throws IOException {
		
		Set<Integer> set = corpus.getDocumentIds();
		
		double[][] docPredictions = new double[documents.size()][K];
		
		
		//optimize local parameters gamma and phi for each document
		int counter = 0;
		for(Integer docId : set) {
			Document d = corpus.getDocumentById(docId);
			doPrediction(d, docPredictions[counter]);
			counter++;
		}
		
		writeAssignments();
		writeTopicCountsTopWords();
//		double[][] gammaUnnormalized = gamma.toArray();
//		for(int i=0;i<gammaUnnormalized.length;i++) 
//			gammaUnnormalized[i] = VectorUtils.normalize(gammaUnnormalized[i]);
//		return gammaUnnormalized;
		return docPredictions;
	}
	
	@Override
	public void storeModel() throws IOException {
		super.storeModel();
		
	}

	@Override
	public void loadModel(String filename) throws IOException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericStaticInferencer#doPrediction(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document, double[])
	 */
	@Override
	public double doPrediction(Document d, double[] topicPredictions) {
		int localId = localDocIds.get(d.getId());
		int[] wordTypeIds = d.getTypeList();
		int[] localTypes = new int[wordTypeIds.length];
		for(int i=0;i<wordTypeIds.length;i++)
			localTypes[i] = localWordTypeIds.get(wordTypeIds[i]);
		int[] wordTypeCounts = d.getFrequencyList();

		//matrix of variational params for the document topic proportions of the document batch
		DoubleMatrix1D gammaLocal = new DenseDoubleMatrix1D(topicPredictions);
		gammaLocal.assign(DoubleFunctionFactory.gammaRand);
		
		DoubleMatrix1D ELogThetaLocal = MatrixUtils.logDirichletExpectation(gammaLocal);
		DoubleMatrix1D expELogThetaLocal = ELogThetaLocal.copy().assign(Functions.exp);
		
		DoubleMatrix1D wordTypeCountMatrix = getWordTypeCountMatrix(wordTypeCounts);
		
		OnlineLDALocalStatisticsVO batch = documentPrediction(wordTypeIds, wordTypeCountMatrix, localTypes, localId);
		
		DoubleMatrix1D gammaD = batch.getGammaD();
		DoubleMatrix1D phiNorm = batch.getPhiNorm();
		DoubleMatrix1D expELogThetaD = batch.getExpELogThetaD();
		//update gamma for the optimized document
		gammaLocal.assign(gammaD);

		expELogThetaLocal.assign(expELogThetaD);
		//contribution of the current document to the expected sufficient statistics for the m step
		
		DoubleMatrix1D normCts = wordTypeCountMatrix.copy().assign(phiNorm, Functions.div);
		
		//This is the document prediction
		DoubleMatrix2D outerProd = MatrixUtils.outerProduct(expELogThetaD, normCts);
		
		outerProd.assign(MatrixUtils.viewColumnSelection(expELogBeta, localTypes), Functions.mult);
		topicPredictions = expELogThetaLocal.toArray();
				
		//then sample from outerProd instead of suffStats
		for(int i = 0; i < d.getTypeList().length; i++)
		{
			for(WordToken t : d.getWords())
			{
				if(wordTypeIds[i] == t.getTypeId())
				{
					int topic = MathUtils.drawFromDistribution(outerProd.viewColumn(i).toArray());
					TopicAssignment a = new TopicAssignment(topic);
					t.setState(a);
				}
			}
		}
		
		int[] wordIds = d.getWordFrequencies().keys().elements();
//		int[] counts = d.getWordFrequencies().values().elements();
		
		
		for(int w=0;w<wordIds.length;w++) {
			DoubleMatrix1D temp = ELogThetaLocal.copy().assign(ELogBeta.viewColumn(localWordTypeIds.get(wordIds[w])), Functions.plus);
			double tmax = VectorUtils.max(temp.toArray());
			double sum = temp.assign(Functions.minus(tmax)).assign(Functions.exp).zSum();
			phiNorm.set(w, Math.log(sum) + tmax);
		}
		double score = wordTypeCountMatrix.assign(phiNorm, Functions.mult).zSum();

		return score;
	}




}
