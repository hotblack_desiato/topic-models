/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.gap;

public class OptimizationParams {
	
	private double[][] eLogTheta;
	private double[][] eLogBeta;
	private double[][] eTheta;
	private double[][] eBeta;
	
	private double likelihoodConstantPart;
	
	private int K, V, M;
	
	public double[][] geteLogTheta() {
		return eLogTheta;
	}
	public void seteLogTheta(double[][] eLogTheta) {
		this.eLogTheta = eLogTheta;
	}
	public double[][] geteLogBeta() {
		return eLogBeta;
	}
	public void seteLogBeta(double[][] eLogBeta) {
		this.eLogBeta = eLogBeta;
	}
	public double[][] geteTheta() {
		return eTheta;
	}
	public void seteTheta(double[][] eTheta) {
		this.eTheta = eTheta;
	}
	public double[][] geteBeta() {
		return eBeta;
	}
	public void seteBeta(double[][] eBeta) {
		this.eBeta = eBeta;
	}
	public int getK() {
		return K;
	}
	public void setK(int k) {
		K = k;
	}
	public int getV() {
		return V;
	}
	public void setV(int v) {
		V = v;
	}
	public int getM() {
		return M;
	}
	public void setM(int m) {
		M = m;
	}
	public double getLikelihoodConstantPart() {
		return likelihoodConstantPart;
	}
	public void setLikelihoodConstantPart(double likelihoodConstantPart) {
		this.likelihoodConstantPart = likelihoodConstantPart;
	}
	
	public void addLikelihoodConstantPart(double add) {
		this.likelihoodConstantPart += add;
	}
	
	
}
