/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.postprocessing;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.CorpusImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.DiachronicFeatureFrequencyImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicCorpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.FileUtils;

public class TopicAligner {
	
	private int backlook;
	private String modelBase;
	private String sourceBase;

	private Corpus c;
	private int dicSize;
	private File[] modelDirs;
	private Deque<double[][]> topicDefinitions;
	private Deque<File> topicDefinitionDirs;
	private HashMap<Integer, Integer> typeToPos;
	
	public TopicAligner(String sourceBase, String modelBase, int periodsToLookBack) throws IOException {
		this.backlook = periodsToLookBack;
		this.sourceBase = sourceBase;
		this.modelBase = modelBase;
		topicDefinitions = new ArrayDeque<>();
		topicDefinitionDirs = new ArrayDeque<>();
		typeToPos = new HashMap<>();
	}
	
	public void collectCorpus() throws IOException {
		c = new DiachronicCorpus();
		ImportSettings s = new ImportSettings(null);
		
		
		CorpusImporter ci = new DiachronicFeatureFrequencyImporter(s);
		String vocabFile = sourceBase + File.separator + "vocabulary.dat";
		
		System.out.print("reading dictionary...");
		c.getDictionary().readDictionaryFile(vocabFile);
		System.out.print(" done.\n");
		modelDirs = CorpusImporter.collectSubdirs(modelBase);
		Arrays.sort(modelDirs, new FileNameComparator());
		for(File d : modelDirs) {
			File f = CorpusImporter.collectFiles(d.getAbsolutePath().replace(modelBase, sourceBase))[0];
			System.out.print("importing file " + f.getAbsolutePath() + "...");
			s.dataFile = f.getAbsolutePath();
			ci.doImport(c);
			System.out.print(" done.\n");
		}
		c.pruneDictionary();
		dicSize = c.getNumberOfWordTypes();
		int count = 0;
		for(WordType wt : c.getAllWordTypes()) {
			typeToPos.put(wt.getId(), count++);
		}
		System.out.println("size after pruning: " + dicSize);
	}
	
	
	public void alignTopics() throws IOException {
		StringBuilder sb = new StringBuilder();
		for(File f : modelDirs) {
			System.out.println("aligning topics in dir " + f.getAbsolutePath());
			System.out.print("reading topics... ");
			double[][] topicDefs = readTopics(f);
			System.out.print("done.\n");
			//when deque is still empty, this is the first time slice --> do nothing
			if(!topicDefinitions.isEmpty()) {
				System.out.print("constructing match... ");
				for(int curTopic=0;curTopic<topicDefs.length;curTopic++) {
					sb.append(constructMatch(topicDefs[curTopic]));
				}
				System.out.print("done.\n");
			}
			topicDefinitions.addFirst(topicDefs);
			topicDefinitionDirs.addFirst(f);
			if(topicDefinitions.size() > backlook) {
				topicDefinitions.removeLast();
				topicDefinitions.removeLast();
			}
			writeTopicAlignment(sb.toString(), f);
			sb.setLength(0);
			System.out.println("done with dir " + f.getAbsolutePath());
		}
	}
	
	private void writeTopicAlignment(String string, File dir) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(dir.getAbsolutePath() + File.separator + "topicAlignment.dat"));
		bw.write(string);
		bw.close();		
	}

	private String constructMatch(double[] topic) {
		String result = "no match\n";
		Deque<double[][]> prevDays = new ArrayDeque<>(topicDefinitions);
		Deque<File> prevDirs = new ArrayDeque<>(topicDefinitionDirs);
		while(true) {
			double[][] curPrevDayTopics = prevDays.removeFirst();
			File curDir = prevDirs.removeFirst();

			double highestCos = 0;
			int highestTopicId = -1;
			for(int prevDayTopicId=0;prevDayTopicId<curPrevDayTopics.length;prevDayTopicId++) {
				double cosDist = VectorUtils.cosineDistance(topic, curPrevDayTopics[prevDayTopicId]);
				if(cosDist>highestCos) {
					highestCos = cosDist;
					highestTopicId = prevDayTopicId;
				}
			}

			int curTopicInPrevDay = highestTopicId;
			
			if(curTopicInPrevDay != -1) {
				result = curTopicInPrevDay + "\t#" + curDir.getAbsolutePath() + ", score=" + highestCos + "\n";
				break;
			}
		}
		return result;
	}

	private double[][] readTopics(File dir) throws IOException {
		ArrayList<double[]> topics = new ArrayList<>();
		double[] t = null;
		for(String line : FileUtils.readLinesFromFile(dir + File.separator + "topTopics.txt")) {
			if(line.startsWith("Topic")) {
				if(t!=null)
					topics.add(t);
				t = new double[dicSize];
				continue;
			}
			String[] parts = line.split("\t");
			if(parts.length != 2) {
				continue;
			}
			WordType wt = c.getWordType(parts[0], false);
			double prob = Double.parseDouble(parts[1]);
			t[typeToPos.get(wt.getId())] = prob;
		}
		return topics.toArray(new double[][]{});
	}
	
	public static void main(String[] args) throws Exception {
		if(args.length != 3) {
			printUsage();
			System.exit(0);
		}
		
		String sourceDir = args[0];
		String modelOutcomeDir = args[1];
		
		int backLook = Integer.parseInt(args[2]);
		
		TopicAligner ta = new TopicAligner(sourceDir, modelOutcomeDir, backLook);
		ta.collectCorpus();
		ta.alignTopics();
	}


	private static void printUsage() {
		System.out.println("usage: TopicAligner baseDirSrc baseDirModel daysToLookBack");
	}

}
