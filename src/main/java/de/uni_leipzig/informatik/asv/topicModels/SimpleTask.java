/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Vector;

import org.apache.commons.lang3.ArrayUtils;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.FileUtils;
import de.uni_leipzig.informatik.asv.topicModels.utils.WordProb;

public class SimpleTask {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
//		transposeTopicCounts();
//		translateDocs("./data/vectors_leipzig_short/corpus.dat", "./data/vl/corpus.dat");

		
		String workingDir = "cluster_s";
		int numTopics = 10;
		
//		hdp_workchain(workingDir, numTopics);
		bisecting_workchain(workingDir, numTopics);
//		lda_workchain(workingDir, numTopics);
		
	}

	private static void lda_workchain(String workingDir, int i) throws IOException {
		countStringLabels("./data/" + workingDir + "/topTopics_"+i+"_topics.txt", "./data/" + workingDir + "/labelCounts.dat", i);
		extractStringClusterDocCounts("./data/" + workingDir + "/topTopics_"+i+"_topics.txt", "./data/" + workingDir + "/topic-doc-counts.dat", i);
		computePrecisionRecallStringLabels("./data/" + workingDir + "/topic-doc-counts.dat", "./data/" + workingDir + "/labelCounts.dat", "./data/" + workingDir + "/topic-def.dat", "./data/" + workingDir + "/prec_recall.dat");
	}

	private static void bisecting_workchain(String workigDir, int i) throws IOException {
		computeHighProbWordsForClusters("../cluster_stuttgart/src/results/lp.cent", "./data/" + workigDir + "/topWords.dat");

		countStringLabels("../cluster_stuttgart/src/results/lp.asgn", "./data/" + workigDir + "/labelCounts.dat", i);
		extractStringClusterDocCounts("../cluster_stuttgart/src/results/lp.asgn", "./data/" + workigDir + "/topic-doc-counts.dat", i);
		computePrecisionRecallStringLabels("./data/" + workigDir + "/topic-doc-counts.dat", "./data/" + workigDir + "/labelCounts.dat", "./data/" + workigDir + "/topic-def.dat", "./data/" + workigDir + "/prec_recall.dat");
	}

	private static void computeHighProbWordsForClusters(String infile,
			String outfile) throws IOException {
		String[] centroids = FileUtils.readLinesFromFile(infile);
		
		ArrayList<WordProb> wps = new ArrayList<WordProb>();
		String curClust = "";
		PrintStream ps = new PrintStream(outfile);
		for(String c : centroids) {
			String[] parts = c.split("\\s");
			String clust = parts[0];
			if(!clust.equals(curClust) && !curClust.equals("")) {
				WordProb[] wpus = wps.toArray(new WordProb[]{});
				Arrays.sort(wpus);
				ps.println(curClust);
				for(int i=0;i<15;i++) {
					ps.println(wpus[i].getTerm());
				}
				ps.println();
				wps.clear();
			}
			curClust = clust;
			WordProb wp = new WordProb(parts[1], Double.parseDouble(parts[2]), -1);
			wps.add(wp);
		}

		WordProb[] wpus = wps.toArray(new WordProb[]{});
		Arrays.sort(wpus);
		ps.println(curClust);
		for(int i=0;i<15;i++) {
			ps.println(wpus[i].getTerm());
		}
		
		ps.close();

	}

	private static void hdp_workchain(String workigDir, int numTopics)
			throws IOException {
		exractDocTopicCounts("./data/" + workigDir + "/mode-word-assignments.dat", "./data/" + workigDir + "/doc-topic-proportions.dat", numTopics);
//		reorderDocTopicCounts("./data/" + workigDir + "/doc-topic-proportions.dat", "./data/" + workigDir + "/doc-topic-proportions-reordered.dat");
		countLabels("./data/" + workigDir + "/doc-topic-proportions.dat", "./data/" + workigDir + "/labelCounts.dat");
		extractTopicDocCounts("./data/" + workigDir + "/doc-topic-proportions-reordered.dat", "./data/" + workigDir + "/topic-doc-counts.dat", numTopics);
		computePrecisionRecall("./data/" + workigDir + "/topic-doc-counts.dat", "./data/" + workigDir + "/labelCounts.dat", "./data/" + workigDir + "/topic-def.dat", "./data/" + workigDir + "/prec_recall.dat");
	}

	private static void computePrecisionRecall(String topicDocFile, String labelCountFile,
			String topicDefFile, String outfile) throws IOException {
		int[] categoryCounts = new int[]{127,158,55,250,250,250,250,56,250,250};
		
		HashMap<Integer, int[]> topicDefs = new HashMap<Integer, int[]>();
		String[] topicDefContent = FileUtils.readLinesFromFile(topicDefFile);
		for(int i=0;i<topicDefContent.length;i++) {
			String[] parts = topicDefContent[i].split("\\s");
			int[] t = new int[parts.length];
			for(int j=0;j<parts.length;j++)
				t[j] = Integer.parseInt(parts[j]);
			topicDefs.put(i, t);
		}
		
		String[] topicDocContent = FileUtils.readLinesFromFile(topicDocFile);
		int[] topicDocSum = new int[topicDocContent.length];
		for(int i=0;i<topicDocSum.length;i++) {
			String[] parts = topicDocContent[i].split("\\s");
			topicDocSum[i] = Integer.parseInt(parts[1]);
		}
		
		String[] labelCountContent = FileUtils.readLinesFromFile(labelCountFile);
		
		int[][] labelCounts = new int[labelCountContent.length][topicDocSum.length];
		for(int i=0;i<labelCountContent.length;i++) {
			String[] parts = labelCountContent[i].split("\\s");
			for(int j=1;j<parts.length;j++) {
				String[] p = parts[j].split(":");
				int topic = Integer.parseInt(p[0]);
				int count = Integer.parseInt(p[1]);
				labelCounts[i][topic-1] = count;
			}
		}
		
		PrintStream ps = new PrintStream(outfile);
		double allCorrect = 0;
		double allSum = 0;
		double allCatCounts = 0;
		for(int i=0;i<categoryCounts.length;i++) {
			int[] topics = topicDefs.get(i);
			double sumCorrect = 0;
			double topicSum = 0;
			for(int topic : topics) {
				sumCorrect += labelCounts[i][topic];
				topicSum += topicDocSum[topic];
			}
			double precision = sumCorrect / topicSum;
			double recall = sumCorrect / (double)categoryCounts[i];
			ps.println(precision + "\t" + recall);
			allCorrect += sumCorrect;
			allSum += topicSum;
			allCatCounts += categoryCounts[i];
		}
		double overallPrecision = allCorrect / allSum;
		double overallRecall = allCorrect / allCatCounts;
		ps.println(overallPrecision + "\t" + overallRecall);
		
		ps.close();
		
		
	}
	
	private static void computePrecisionRecallStringLabels(String topicDocFile, String labelCountFile,
			String topicDefFile, String outfile) throws IOException {
		int[] categoryCounts = new int[]{127,158,55,250,250,250,250,56,250,250};
		
		HashMap<Integer, String[]> topicDefs = new HashMap<Integer, String[]>();
		String[] topicDefContent = FileUtils.readLinesFromFile(topicDefFile);
		for(int i=0;i<topicDefContent.length;i++) {
			String[] parts = topicDefContent[i].split("\\s");
			topicDefs.put(i, parts);
		}
		
		String[] topicDocContent = FileUtils.readLinesFromFile(topicDocFile);
		HashMap<String, Integer> topicDocSum = new HashMap<String, Integer>();
		for(int i=0;i<topicDocContent.length;i++) {
			String[] parts = topicDocContent[i].split("\\s");
			topicDocSum.put(parts[0], Integer.parseInt(parts[1]));
		}
		
		String[] labelCountContent = FileUtils.readLinesFromFile(labelCountFile);
		
		HashMap<Integer, HashMap<String, Integer>> labelCounts = new HashMap<Integer, HashMap<String,Integer>>();
		for(int i=0;i<labelCountContent.length;i++) {
			String[] parts = labelCountContent[i].split("\\s");
			HashMap<String, Integer> lc = new HashMap<String, Integer>();
			for(int j=1;j<parts.length;j++) {
				String[] p = parts[j].split(":");
				String clust = p[0];
				int count = Integer.parseInt(p[1]);
				lc.put(clust, count);
			}
			labelCounts.put(i, lc);
		}
		
		PrintStream ps = new PrintStream(outfile);
		double allCorrect = 0;
		double allSum = 0;
		double allCatCounts = 0;
		for(int i=0;i<categoryCounts.length;i++) {
			String[] topics = topicDefs.get(i);
			double sumCorrect = 0;
			double topicSum = 0;
			for(String topic : topics) {
				if(labelCounts.get(i).get(topic) != null)
					sumCorrect += labelCounts.get(i).get(topic);
				if(topicDocSum.get(topic) != null)
					topicSum += topicDocSum.get(topic);
			}
			double precision = sumCorrect / topicSum;
			double recall = sumCorrect / (double)categoryCounts[i];
			ps.println(precision + "\t" + recall);
			allCorrect += sumCorrect;
			allSum += topicSum;
			allCatCounts += categoryCounts[i];
		}
		double overallPrecision = allCorrect / allSum;
		double overallRecall = allCorrect / allCatCounts;
		ps.println(overallPrecision + "\t" + overallRecall);
		ps.close();
	}

	private static void extractTopicDocCounts(String infile, String outfile,
			int numTopics) throws IOException {
		String[] docTopics = FileUtils.readLinesFromFile(infile);
		int[] topicDocCounts = new int[numTopics];
		
		for(String s : docTopics) {
			String[] parts = s.split("\\s");
			String[] topicParts = parts[1].split(":");
			Integer topicId = Integer.parseInt(topicParts[0]);
			topicDocCounts[topicId-1]++;
		}
		PrintStream ps = new PrintStream(outfile);
		for(int i=0;i<topicDocCounts.length;i++)
			ps.println((i+1) + "\t" + topicDocCounts[i]);
		ps.close();
		
	}

	private static void extractStringClusterDocCounts(String infile, String outfile,
			int numTopics) throws IOException {
		String[] docTopics = FileUtils.readLinesFromFile(infile);
		HashMap<String, Integer> topicDocCounts = new HashMap<String, Integer>();
		
		for(String s : docTopics) {
			String[] parts = s.split("\\s");
			String cluster = parts[1];
			if(topicDocCounts.get(cluster) == null)
				topicDocCounts.put(cluster, 1);
			else
				topicDocCounts.put(cluster, topicDocCounts.get(cluster) + 1);
		}
		PrintStream ps = new PrintStream(outfile);
		for(String c : topicDocCounts.keySet())
			ps.println(c + "\t" + topicDocCounts.get(c));
		ps.close();
		
	}

	private static void countLabels(String infile, String outfile) throws IOException {
		String[] docTopics = FileUtils.readLinesFromFile(infile);
		HashMap<Integer, Integer> docTopicAssignments = new HashMap<Integer, Integer>();
		
		int highestTopic = 0;
		for(String s : docTopics) {
			String[] parts = s.split("\\s");
			Integer docId = Integer.parseInt(parts[0]);
			String[] topicParts = parts[1].split(":");
			Integer topicId = Integer.parseInt(topicParts[0]);
			if(topicId > highestTopic)
				highestTopic = topicId.intValue();
			docTopicAssignments.put(docId, topicId);
		}
		
		String[] realDocs = FileUtils.readLinesFromFile("../cluster_stuttgart/src/leipzig/vectors_class_title.txt");
		HashMap<Integer, String> realLabels = new HashMap<Integer, String>();
		for(String s : realDocs) {
			String[] parts = s.split("\\s");
			Integer docId = Integer.parseInt(parts[0]);
			String label = parts[1];
			realLabels.put(docId, label);
		}
		
		HashMap<String, int[]> topicLabelAssignments = new HashMap<String, int[]>();
		
		for(Integer docId : realLabels.keySet()) {
			String label = realLabels.get(docId);
			if(topicLabelAssignments.get(label) == null)
				topicLabelAssignments.put(label, new int[highestTopic+1]);
			int[] topicCounts = topicLabelAssignments.get(label);
			topicCounts[docTopicAssignments.get(docId)]++;
			topicLabelAssignments.put(label, topicCounts);
		}
		
		PrintStream ps = new PrintStream(outfile);
		for(String label : topicLabelAssignments.keySet()) {
			ps.print(label + "\t");
			int[] topics = topicLabelAssignments.get(label);
			for(int i=0;i<topics.length;i++) {
				if(topics[i]>1)
					ps.print(i + ":" + topics[i] + "\t");
			}
			ps.print("\n");
		}
		ps.close();
		
	}

	private static void countStringLabels(String infile, String outfile, int numclust) throws IOException {
		String[] docTopics = FileUtils.readLinesFromFile(infile);
		HashMap<Integer, String> docTopicAssignments = new HashMap<Integer, String>();
		
		for(String s : docTopics) {
			String[] parts = s.split("\\s");
			Integer docId = Integer.parseInt(parts[0]);
			String topicId = parts[1];
			docTopicAssignments.put(docId, topicId);
		}
		
		String[] realDocs = FileUtils.readLinesFromFile("../cluster_stuttgart/src/leipzig/vectors_class_title.txt");
		HashMap<Integer, String> realLabels = new HashMap<Integer, String>();
		for(String s : realDocs) {
			String[] parts = s.split("\\s");
			Integer docId = Integer.parseInt(parts[0]);
			String label = parts[1];
			realLabels.put(docId, label);
		}
		
		HashMap<String, HashMap<String, Integer>> topicLabelAssignments = new HashMap<String, HashMap<String, Integer>>();
		
		for(Integer docId : realLabels.keySet()) {
			String label = realLabels.get(docId);
			if(topicLabelAssignments.get(label) == null)
				topicLabelAssignments.put(label, new HashMap<String, Integer>());
			HashMap<String, Integer> topicCounts = topicLabelAssignments.get(label);
			if(topicCounts.get(docTopicAssignments.get(docId)) == null)
				topicCounts.put(docTopicAssignments.get(docId), 1);
			else
				topicCounts.put(docTopicAssignments.get(docId), topicCounts.get(docTopicAssignments.get(docId)) + 1);
			topicLabelAssignments.put(label, topicCounts);
		}
		
		PrintStream ps = new PrintStream(outfile);
		for(String label : topicLabelAssignments.keySet()) {
			ps.print(label + "\t");
			HashMap<String, Integer> topics = topicLabelAssignments.get(label);
			for(String clustLabel : topics.keySet()) {
				if(topics.get(clustLabel) != null && topics.get(clustLabel) > 0)
					ps.print(clustLabel + ":" + topics.get(clustLabel) + "\t");
			}
			ps.print("\n");
		}
		ps.close();
		
	}

	
	private static void reorderDocTopicCounts(String infile, String outfile) throws IOException {
		TreeMap<Integer, String> reordered = new TreeMap<Integer, String>();
		
		String[] translations = FileUtils.readLinesFromFile("./data/vl/translation.txt");
		String[] docs = FileUtils.readLinesFromFile(infile);
		for(int i=0;i<translations.length;i++)
			reordered.put(Integer.parseInt(translations[i]), docs[i].replaceFirst("\\d+", translations[i]));
		PrintStream ps = new PrintStream(outfile);
		for(String d : reordered.values())
			ps.println(d);
		ps.close();
	}

	private static void translateDocs(String wrongOrder, String correctOrder) throws IOException {
		String[] docsWrong = FileUtils.readLinesFromFile(wrongOrder);
		String[] docsCorrect = FileUtils.readLinesFromFile(correctOrder);
		
		int[] translation = new int[docsWrong.length];
		for(int i=0;i<docsWrong.length;i++) {
			translation[i] = ArrayUtils.indexOf(docsCorrect, docsWrong[i]);
		}
		PrintStream ps = new PrintStream("./data/vl/translation.txt");
		for(int i : translation)
			ps.println(i);
		ps.close();
	}

	private static void exractDocTopicCounts(String infile, String outfile, int numTopics) throws IOException {
		String[] lines = FileUtils.readLinesFromFile(infile);
		int curDoc = -1;
		double[] topicCounts = new double[numTopics];
		TreeMap<Integer, double[]> docTopicProps = new TreeMap<Integer, double[]>();
		int docTopicAssignments = 0;
		for(int i=1;i<lines.length;i++) {
			String[] parts = lines[i].split("\\s");
			int docId = Integer.parseInt(parts[0]);
			int topicId = Integer.parseInt(parts[2]);
			if(docId != curDoc && curDoc != -1) {
				for(int j=0;j<topicCounts.length;j++) {
					topicCounts[j] /= (double)docTopicAssignments;
				}
				docTopicProps.put(curDoc, topicCounts);
				topicCounts = new double[numTopics];
				docTopicAssignments = 0;
			}
			curDoc = docId;
			topicCounts[topicId]++;
			docTopicAssignments++;
		}
		//add last array
		for(int j=0;j<topicCounts.length;j++) {
			topicCounts[j] /= (double)docTopicAssignments;
		}
		docTopicProps.put(curDoc, topicCounts);

		
		PrintStream ps = new PrintStream(outfile);
		StringBuilder sb = new StringBuilder();
		for(Integer k : docTopicProps.keySet()) {
			sb.append(k).append("\t");
			//extract 3 topics with highest probability
			double[] topicProps = docTopicProps.get(k);
			double maxVal = 1;
			for(int i=0;i<3;i++) {
				int pos = maxValue(topicProps, maxVal);
				maxVal = topicProps[pos];
				sb.append(pos+1).append(":").append(maxVal).append("\t");
			}
			sb.deleteCharAt(sb.length()-1);
			ps.println(sb.toString());
			sb.setLength(0);
		}
		ps.close();
		
	}
	
	private static int maxValue(double[] a, double upperBound) {
		int maxPos = 0;
		double maxVal = 0;
		for(int i=0;i<a.length;i++) {
			if(a[i] > maxVal && a[i] < upperBound) {
				maxVal = a[i];
				maxPos = i;
			}
		}
		return maxPos;
	}

	private static void transposeTopicCounts() throws IOException,
			FileNotFoundException {
		String[] lines = FileUtils.readLinesFromFile("./data/train_dir_sampling2/mode-topics.dat");
		Vector<String> newLines = new Vector<String>();
		for(String line : lines) {
			String[] lineparts = line.split("\\s");
			for(int i=0;i<lineparts.length;i++) {
				if(newLines.size() <= i) {
					newLines.add(lineparts[i]);
				} else {
					newLines.set(i, newLines.get(i) + "\t" + lineparts[i]);
				}
			}
		}
		PrintStream ps = new PrintStream("./data/train_dir_sampling2/topicCountsTransposed.dat");
		for(String nl : newLines)
			ps.println(nl);
		ps.close();
	}

}
