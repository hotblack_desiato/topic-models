/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.logisticnormal;

import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.abs;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.add;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.dotProduct;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.exp;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.invert;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.log;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.mean;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.multiply;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.multiplyWithScalar;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.neg;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.normalize;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.subtract;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.subtractScalar;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.sum;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

import cern.colt.map.OpenIntDoubleHashMap;
import cern.jet.random.Gamma;
import cern.jet.random.Normal;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.WordTokenState;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.Optimizer;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.cg.FletcherReevesConjugateGradientOptimizer;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericStaticInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.logisticnormal.state.DocumentState;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;
import de.uni_leipzig.informatik.asv.topicModels.utils.WordProb;

public class LogisticNormalTopicModel extends AbstractGenericStaticInferencer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//hyper params
	double[][] mean;
	double[][] var;
	private double meanConst = 0;
	private double varConst = 1;
	private double alpha = 1.5;
	
	//variational parameters
	//for theta
//	double[][] lambda;
	
	//for beta
	double[][] mu;
	double[][] Sigma;

	//for z
//	double[][][] phi;
	
	//for E[log sum exp beta]
	private double[] zeta;
	private double[] constant;
	
	//utils
	private double[][] pi;
	private double[] expectedSumBeta;
	private double[] varianceSumBeta;
	private double[][] normalizedMu;
	
	
	
	public LogisticNormalTopicModel(Corpus c, Corpus test, Corpus validation, InferencerProperties props) throws IOException {
		super(c, test, validation, props);
		init();
	}

	private void init() {
		K = props.numTopics;
		V = corpus.getNumberOfWordTypes();
		M = corpus.getNumberOfDocuments();
		
		//TODO: read hyperparameter setting from props file
		alpha = props.alpha;
		
		Random rng = new Random(1);
		int docIdx = 0;
		for(Document d : corpus.getDocuments()) {
//			int[] localWordTypeIds = new int[d.getNumberOfWordTypes()];
//			int[] wordTypeFreq = new int[d.getNumberOfWordTypes()];
//			int[] types = d.getTypeList();
//			for(int ti=0;ti<d.getNumberOfWordTypes();++ti) {
//				localWordTypeIds[ti] = this.localWordTypeIds.get(types[ti]);
//				wordTypeFreq[ti] = d.getWordFrequencies().get(types[ti]);
//			}
//			DocumentState state = new DocumentState(docIdx++, localWordTypeIds, wordTypeFreq, K);
			DocumentState state = new DocumentState(d.getId(), d.getTypeList(), d.getFrequencyList(), K);
			d.setState(state);
			for(int k=0;k<K;k++) {
				d.<DocumentState>getState().setDocLambdaForTopic(k, Gamma.staticNextDouble(1, 1));
			}
//			for(int w=0;w<localWordTypeIds.length;++w) {
			for(int w=0;w<d.getNumberOfWordTypes();++w) {
				int randTopic = rng.nextInt(K);
				double prev = d.<DocumentState>getState().getDocPhiForTopicAndWord(randTopic, w);
				d.<DocumentState>getState().setDocPhiForTopicAndWord(randTopic, w, 1+prev);
			}
			d.<DocumentState>getState().renormalizePhi();
		}
		
		mean = new double[K][V];
		var = new double[K][V];
		
		
		mu = new double[K][V];
		Sigma = new double[K][V];
		
		for(int k=0;k<K;k++) {
			for(int w=0;w<V;w++) {
				mu[k][w] = Normal.staticNextDouble(meanConst, varConst);
				Sigma[k][w] = varConst + Math.random()*.1;
				mean[k][w] = meanConst;
				var[k][w] = varConst;
			}
		}

		zeta = new double[K];
		constant = new double[K];
		updateZeta();
		
		pi = new double[K][V];
		
		normalizedMu = new double[K][V];
		for(int k=0;k<K;++k)
			normalizedMu[k] = MathUtils.normalizeLogDistribution(mu[k]);
		
	}

	private void updateZeta() {
		logger.info("updating zeta");
		for(int k=0;k<K;++k) {
			for(int w=0;w<V;++w) {
				if(w==0)
					zeta[k] = mu[k][w] + .5*Sigma[k][w];
				else
					zeta[k] = MathUtils.logAdd(zeta[k], mu[k][w]+.5*Sigma[k][w]);
			}
		}
	}

	@Override
	public void doInference() throws IOException {
		double convergence = 1;
		double bound = 0, meanchange = 1.;
		double oldBound = 0;
		int iter=0;
//		while(meanchange > 1e-3) {
		while(Math.abs(convergence) > 1e-3 && iter<50) {
			bound = updateParameters();
//			meanchange = updateParameters();
			convergence = (oldBound - bound) / oldBound;
			logger.info("iteration " + iter + ": bound: " + bound + ", convergence: " + convergence);
//			logger.info("iteration " + iter + ": meanchange: " + meanchange);
			if(convergence<0 && iter > 30) {
				logger.info("Warning, bound decreasing");
				break;
			}
			oldBound = bound;
//			getTopWordsForTopics(props.topicWordlistSize, "iteration"+iter);
			iter++;
		}
		props.iterations = iter;
		getTopWordsForTopics(props.topicWordlistSize, null);
	}

	private double updateParameters() {
		logger.info("updating theta and phi...");
		OpenIntDoubleHashMap[][] expectedSuffStatsZ = new OpenIntDoubleHashMap[K][M];
		double likelihood = 0;
		expectedSumBeta = new double[K];
		varianceSumBeta = new double[K];
		for(int k=0;k<K;++k) {
			constant[k] = Math.log(zeta[k]) + 1 - sum(exp(add(mu[k], multiplyWithScalar(Sigma[k], .5)))) / zeta[k];
			pi[k] = subtractScalar(mu[k], zeta[k]);
			expectedSumBeta[k] = sum(
					exp(
							add(
									mu[k], 
									multiplyWithScalar(
											Sigma[k], .5
									)
							)
					)
			);
			varianceSumBeta[k] = sum(
					multiply(
							subtractScalar(exp(Sigma[k]), 1),
							exp(
									add(
											multiplyWithScalar(mu[k], 2),
											Sigma[k]
									)
							)
					)
			);
		}
		
		double[][] expBeta = new double[K][V]; 
		for(int k=0;k<K;++k)
			expBeta[k] = subtractScalar(mu[k], Math.log(expectedSumBeta[k]));
//			expBeta[k] = subtractScalar(mu[k], constant[k]);

		for(Document doc : corpus.getDocuments()) {
			likelihood += varEStep(expBeta, doc);
		}
		logger.info("sum of doc likelihoods: " + likelihood);
		//optimize topics
		likelihood += varMStepGSL(expectedSuffStatsZ);
//		double meanchange = varMStepGSL(expectedSuffStatsZ);

		updateZeta();
		
		for(int k=0;k<K;++k)
			normalizedMu[k] = MathUtils.normalizeLogDistribution(mu[k]);

		return likelihood;
//		return meanchange;
	}

	
	private double varMStepGSL(OpenIntDoubleHashMap[][] expectedSuffStatsZ) {
		logger.info("optimizing beta");
		double likelihood = 0, eLogPz, eLogQz, meanchange = 0;
		FletcherReevesConjugateGradientOptimizer opt = Optimizer.getOptimizer(FletcherReevesConjugateGradientOptimizer.class);
		Objective f = new Objective(V, this);
		
		for(int k=0;k<K;++k) {
			f.setTopic(k);
			
			double[] guess = new double[V];
			for(int w=0;w<V;++w)
				guess[w] = Normal.staticNextDouble(mu[k][w], Sigma[k][w]);
			
			Optimizer.optimize(guess, .35, .01, props.convergenceCriterion, props.iterations, f, opt);
			double[] absDiff = abs(subtract(mu[k], opt.getX()));
			double curChange =  mean(absDiff);
			meanchange += curChange;
			
			mu[k] = opt.getX();

		}
		double[] docSums = new double[V];
		for(Document d : corpus.getDocuments()) {
			int[] types = d.getTypeList();
			int[] freqs = d.getFrequencyList();
			//TODO: rewrite as dot product
			for(int i=0;i<types.length;++i) {
				for(int k=0;k<K;++k)
					docSums[k] += d.<DocumentState>getState().getDocPhiForTopicAndWord(k, i) * freqs[i]; 
			}
		}
		
		for(int k=0;k<K;++k) {
			f.setTopic(k);
			//update variance
            double[] piK = normalize(exp(mu[k]));
            
            double[] secDeriv = new double[V];
            for(WordType wt : corpus.getAllWordTypes()) {
//            	int w = localWordTypeIds.get(wt.getId());
            	int w = wt.getId();
            	secDeriv[w] = (-piK[w] + piK[w]*piK[w]) * docSums[k];
            }
    		
    		secDeriv = subtract(secDeriv, invert(Sigma[k]));
            
            
            Sigma[k] = neg(invert(secDeriv));
            
            double[][] tempSig = new double[V][V];
            for(int w=0;w<V;++w)
            	tempSig[w][w] = secDeriv[w];
            
            double[] muMinusMean = subtract(mu[k], mean[k]);
            
//            fr recode
            double[] gradient = new double[V];
            f.df(mu[k], gradient);
            
            likelihood += (-f.f(mu[k]) 
            		+ dotProduct(neg(gradient), muMinusMean) 
            		+ .5*dotProduct(dotProduct(muMinusMean,  tempSig), muMinusMean) 
            		+ .5*(dotProduct(secDeriv, var[k]) + sum(log(var[k]))));

		}
		logger.info("topic likelihood : " + likelihood);
		
		return likelihood;
	}

	private double varEStep(double[][] expBeta, Document doc) {
		double likelihood = 0, oldLikelihood = 1;
		double converge = 1;
		
		while(Math.abs(converge) > 1e-3) {
			likelihood = 0;
			//update phis and lambdas
			
			doc.<DocumentState>getState().recomputeVariationalParameters(alpha, pi);
						
			likelihood = doc.<DocumentState>getState().computeDocLikelihood(alpha, pi);
//			System.out.println("likelihood: " + likelihood);
			converge = (oldLikelihood - likelihood) / oldLikelihood;
			oldLikelihood = likelihood;
		}
		return likelihood;
	}
	
	
	public void getTopWordsForTopics(int numberOfWords, String postfix) throws IOException {
		String filename = props.targetDir + "/topWords_" + props.numTopics + "_topics";
		if(postfix != null)
			filename += "_" + postfix;
		filename += ".txt";
		BufferedWriter wordWriter = new BufferedWriter(new FileWriter(filename));
		StringBuffer buf = new StringBuffer();
		WordProb[] wps = new WordProb[corpus.getNumberOfWordTypes()];
		double[][] expectedBeta = add(mu, multiplyWithScalar(Sigma, .5));
		for(int topic = 0;topic < K;topic++) {
			double[] locgamma1 = MathUtils.normalizeDistribution(exp(expectedBeta[topic]));
//			double[] locgamma1 = MathUtils.normalizeDistribution(exp(mu[topic]));

			for(WordType wt : corpus.getAllWordTypes()) {
//				int locType = localWordTypeIds.get(wt.getId());
				int typeId = wt.getId();
				wps[typeId] = new WordProb(wt.getValue(), locgamma1[typeId], typeId);
			}
			Arrays.sort(wps);
			
			buf.append("Topic ").append(topic).append("\n");
			for(int word=0;word<numberOfWords;word++) {
				buf.append(wps[word].getTerm()).append("\t").append(wps[word].getProb()).append("\n");
			}
			buf.append("\n");
		}
		wordWriter.write(buf.toString());
		wordWriter.close();
	}


	@Override
	public void loadModel(String filename) throws IOException {
	}

	@Override
	public void populateFinalValues() {
		// TODO fill this
		
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericStaticInferencer#doPrediction(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document, double[])
	 */
	@Override
	public double doPrediction(Document d, double[] topicPredictions) {
		// TODO Auto-generated method stub
		return 0;
	}

}
