/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.utils;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.importer.TextImporter;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.util.ImportSettings;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.FileUtils;


public class DocLineImporter extends TextImporter {

	private boolean allowDicExpanding = false;
	
	public DocLineImporter(boolean allowExpandingDictionary, ImportSettings settings) {
		super(settings);
		this.allowDicExpanding = allowExpandingDictionary;
	}

	@Override
	public void doImport(Corpus c) throws IOException {
		String[] docs = FileUtils.readLinesFromFile(props.dataFile);
		for(String doc : docs) {
			doc = doc.replaceFirst("\\d+ \"", "");
			Pattern p = Pattern.compile("\\s+\\d{4}-\\d{2}-\\d{2}\\s+(\\d+\\.xml)\"$");
			Matcher m = p.matcher(doc);
			m.find();
			String sourcefile = m.group(1);
			doc = m.replaceAll("");
			createDocumentFromStringAndAddToCorpus(c, doc, sourcefile, allowDicExpanding);
		}
	}

}
