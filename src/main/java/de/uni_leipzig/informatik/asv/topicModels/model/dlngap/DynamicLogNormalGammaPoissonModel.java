/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.dlngap;

import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.doubleArray;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils.lgamma;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils.safeLog;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.normalize;

import java.io.IOException;
import java.util.Arrays;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicCorpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.WordTokenState;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunctionHadamardProduct;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunctionSum;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.brownian.BrownianKernel;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.ou.OrnsteinUhlenbeckKernel;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.periodic.PeriodicRBFKernel;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.rbf.GaussianKernel;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.Type;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.Optimizer;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.cg.FletcherReevesConjugateGradientOptimizer;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.rprop.RPropOptimizer;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericDynamicInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericDynamicInferencer.CovarianceModel;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;

public class DynamicLogNormalGammaPoissonModel extends AbstractGenericDynamicInferencer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1129648287907216510L;

	
	//poosterior topics (sparse)
	double[][][] mu;
	double[][][] Sigma;
	
	//posterior topics (dense)
	double[][][] posteriorMu;
	double[][][] posteriorSigma;

	//expectations
	double[][][] expBeta;
	double[][][] expEBeta;
	
	
	//hyper params
	double mean0;
	double var0;
	
	
	//sufficient statistics
	double[][] statsKT;
	double[][][] statsKWT;


	CovarianceFunction covFct;


	double nu;


	private double[][][] observationTimes;

	
	
	public DynamicLogNormalGammaPoissonModel(DiachronicCorpus c, DiachronicCorpus test, DiachronicCorpus validation,
			InferencerProperties props) throws IOException {
		super(c, test, validation, props);
		init();
	}

	private void init() {
		nu = props.measurementNoise;
		Dynamics dyn = Dynamics.valueOf(props.dynamics.toUpperCase());
		
		//initiate covariance function
		switch (dyn) {
		case BROWNIAN_MOTION:
			covFct = new BrownianKernel(props.processVariance, nu);
			break;
		case ORNSTEIN_UHLENBECK:
			covFct = new OrnsteinUhlenbeckKernel(nu, props.processVariance, props.lengthScale);
			break;
		case RBF:
			covFct = new GaussianKernel(props.processVariance, props.lengthScale, nu);
			break;
		case PERIODIC:
			covFct = new PeriodicRBFKernel(nu, props.processVariance, props.lengthScale);
			break;
		default:
			break;
		}
		Vector<CovarianceFunction> covFcs = new Vector<>();
		
		CovarianceFunction bm_base = new BrownianKernel(1, 0);
		covFcs.add(bm_base);

		CovarianceModel covModel = CovarianceModel.valueOf(props.covModel.toUpperCase());
		switch (covModel) {
		case SINGLE:
			covFct = covFcs.lastElement();
			double[] newHyperparameters = covFct.getHyperparameters();
			newHyperparameters[0] = nu;
			newHyperparameters[1] = props.processVariance;
			covFct.setHyperparameters(newHyperparameters);
			break;
		case ADDITIVE:
			covFct = new CovarianceFunctionSum(nu, props.processVariance, covFcs.toArray(new CovarianceFunction[]{})); 
			break;
		case ADDITIVE_WEIGHTED:
			double[] weights = new double[]{props.weight,(1-props.weight)};
			covFct = new CovarianceFunctionSum(nu, props.processVariance, covFcs.toArray(new CovarianceFunction[]{}), weights); 
			break;
		case MULTIPLICATIVE:
			covFct =  new CovarianceFunctionHadamardProduct(nu, props.processVariance, covFcs.toArray(new CovarianceFunction[]{}));
			break;
		default:
			covFct = new CovarianceFunctionSum(nu, props.processVariance, covFcs.toArray(new CovarianceFunction[]{})); 
			break;
		}

		statsKT = ArrayFactory.doubleArray(Type.ZERO, K, T);
		statsKWT = new double[K][V][];

		
		mean0 = props.lngap_mu;
		var0 = props.lngap_sigma;
		
		
		mu = new double[K][V][];
		Sigma = new double[K][V][];
		expBeta = new double[K][V][];
		expEBeta = new double[K][V][];
		//sparse topic word arrays
		for(int k=0;k<K;k++) {
			for(int w=0;w<V;w++) {
				int n = nonzeroFrequencyTimes[w].length;
				statsKWT[k][w] = ArrayFactory.doubleArray(Type.ZERO, n);
				mu[k][w] = ArrayFactory.doubleArray(Type.NORMAL, n);
				mu[k][w] = VectorUtils.addScalar(VectorUtils.multiplyWithScalar(mu[k][w], .01), mean0);
				Sigma[k][w] = ArrayFactory.doubleArray(Type.UNIFORM, n);
				Sigma[k][w] = VectorUtils.addScalar(VectorUtils.multiplyWithScalar(Sigma[k][w], .1), var0);
				expBeta[k][w] = ArrayFactory.doubleArray(Type.ZERO, n);
				expEBeta[k][w] = ArrayFactory.doubleArray(Type.ZERO, n);
				updateBetaExpectationsForWordAndTopic(mu[k][w], Sigma[k][w], expBeta[k][w], expEBeta[k][w]);
			}
		}
		
		observationTimes = new double[V][][];
		for(int w=0;w<V;w++) {
			int[] nnz = nonzeroFrequencyTimes[w];
			observationTimes[w] = new double[nnz.length][1];
			for(int n=0;n<nnz.length;n++)
				observationTimes[w][n][0] = nnz[n];
		} 
		
		posteriorMu = doubleArray(Type.ZERO, T, K, V);
		posteriorSigma = doubleArray(Type.ZERO, T, K, V);
		zeroStats();
		
	}

	/**
	 * 
	 */
	private void zeroStats() {
		for(int k=0;k<K;k++) {
			Arrays.fill(statsKT[k], 0);
			for(int w=0;w<V;w++)
				Arrays.fill(statsKWT[k][w], 0);
		}
		
	}

	@Override
	public void doInference() throws IOException {
		for(int i=0;i<10;i++)
			updateParameters();
		
		double convergence = 1;
		double bound = 0;
		double oldBound = 0;
		int iter=0;
		while(Math.abs(convergence) > 1e-3 && iter<props.iterations) {
			bound = updateParameters();
			likelihood.add(bound);
			convergence = (oldBound - bound) / oldBound;
			logger.info("bound: " + bound + ", convergence: " + convergence);
			if(convergence<0 && iter > 2) {
				logger.info("Warning, bound decreasing");
				break;
			}
			oldBound = bound;
			iter++;
		}
		props.iterations = iter;
	}

	private double updateParameters()  {
		logger.info("updating theta and phi...");
		double likelihood = 0;
		int[] sparseIndex = new int[V];
		zeroStats();
		try {
			for(int t=0;t<T;t++) {
				likelihood += varEStep(t, sparseIndex);
				//update sparse indexes
			}
			logger.debug("likelihood after doc processing: " + likelihood);
			likelihood += varMStepMulitThread();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		return likelihood;
	}



	private double varEStep(int t, int[] sparseIndex) throws InterruptedException, ExecutionException {
		Set<Integer> docIdSet = corpus.getDocumentsForDate(dates[t]);
		double[][] expTheta = new double[docIdSet.size()][K];
		double[][] pi = doubleArray(Type.ZERO, K, V);
		double[][] ePi = doubleArray(Type.ZERO, K, V);
		double[][][] phi = new double[docIdSet.size()][][];
		for(int k=0;k<K;k++) {
			for(int w=0;w<V;w++) {
				if(sparseIndex[w] < nonzeroFrequencyTimes[w].length && nonzeroFrequencyTimes[w][sparseIndex[w]] == t) {
					pi[k][w] = expBeta[k][w][sparseIndex[w]];
					ePi[k][w] = expEBeta[k][w][sparseIndex[w]];
				}
			}
		}

		double likelihood = 0;
		Vector<Future<Double>> futures = new Vector<>();
		futures.setSize(docIdSet.size());
		for(Integer docId : docIdSet) {
			Document doc = (Document) corpus.getDocumentById(docId);
			int d = localDocIds.get(docId);
			int[] types = doc.getTypeList();
			int[] freqs = doc.getFrequencyList();
			phi[d] = ArrayFactory.doubleArray(Type.ZERO, types.length, K);
			futures.set(d, threadPool.submit(new DocumentInferenceThread(doc, phi[d], expTheta[d], pi, ePi, types, freqs)));
//			likelihood += documentInference(doc, phi[d], expTheta[d], pi, ePi, types, freqs);
		}
		for(Integer docId : docIdSet) {
			Document doc = (Document) corpus.getDocumentById(docId);
			int d = localDocIds.get(docId);
			//make sure computation for this doc is done
			likelihood += futures.get(d).get();
			int[] types = doc.getTypeList();
			int[] freqs = doc.getFrequencyList();
			// update sufficient statistics
			for(int k=0;k<K;k++) {
				statsKT[k][t] += expTheta[d][k];
				for(int n=0;n<types.length;n++) {
					int w = localWordTypeIds.get(types[n]);
					int sparseIdx = sparseIndex[w];
					statsKWT[k][w][sparseIdx] += freqs[n]*phi[d][n][k];				
				}
			}
			//update the final values
			finalDocTopicWeights[t][d] = expTheta[d];
		}
		for(int wt : corpus.getDocumentIdSubsetWordFrequencies(docIdSet).keys().elements()) {
			sparseIndex[localWordTypeIds.get(wt)]++;
		}

		return likelihood;
	}

	private double documentInference(double[][] phi, double[] expTheta, double[][] pi, double[][] ePi, int[] types, int[] freqs) {
 		double[] lambda1 = ArrayFactory.doubleArray(Type.UNIFORM, K);
 		double[] lambda2 = ArrayFactory.doubleArray(Type.UNIFORM, K);
		double[] expLogTheta = new double[K];
 		updateThetaExpectationsForDoc(lambda1,  lambda2, expTheta, expLogTheta);
 		double phiSum = 0;
		double converge = 1;
		double oldLikelihood = 1;
		double likelihood = 0;
		while(converge > 1e-3) {
			//reset lambdas to priors
			setThetaToPrior(lambda1, lambda2);
			
			//update phi and lambda
			for(int n=0;n<types.length;++n) {
				int type = types[n];
				int w = localWordTypeIds.get(type);
				int wordFreq = freqs[n];
				for(int k=0;k<K;k++) {
					phi[n][k] = expLogTheta[k] + pi[k][w];
					if(k==0)
						phiSum = phi[n][k];
					else
						phiSum = MathUtils.logAdd(phiSum, phi[n][k]);
				}

				//update lambdas
				for(int k=0;k<K;k++) {
					phi[n][k] = Math.exp(phi[n][k] - phiSum);
					lambda1[k] += wordFreq*phi[n][k];
					lambda2[k] += ePi[k][w];
				}
			}
			//update expectations using new parameter settings
			updateThetaExpectationsForDoc(lambda1,  lambda2, expTheta, expLogTheta);
			//recompute likelihood
			likelihood = computeDocumentLikelihood(types, freqs, phi, lambda1, lambda2, pi, ePi);
			
			converge = (oldLikelihood - likelihood) / oldLikelihood;
			oldLikelihood = likelihood;
		}
		return likelihood;
	}
	
	
		
	

	/**
	 * @param d
	 */
	private void setThetaToPrior(double[] lambda1, double[] lambda2) {
		for(int k=0;k<K;k++) {
			lambda1[k] = props.lngap_a;
			lambda2[k] = props.lngap_b;
		}
		
	}
	/**
	 * 
	 */
	private void updateThetaExpectationsForDoc(double[] lambda1, double[] lambda2, double[] expTheta, double[] expLogTheta) {
		for(int k=0;k<K;k++) {
			expTheta[k] = (lambda1[k]/lambda2[k]);
			expLogTheta[k] = MathUtils.digamma(lambda1[k]) - Math.log(lambda2[k]);
		}
	}

	
	/**
	 * @param k
	 * @param w
	 */
	private void updateBetaExpectationsForWordAndTopic(double[] mu, double[] Sigma, double[] expBeta, double[] expEBeta) {
		for(int t=0;t<mu.length;t++) {
			expBeta[t] = mu[t];
			expEBeta[t] = Math.exp(mu[t] + .5*Sigma[t]);
		}
	}


	/**
	 * @param doc 
	 * @param phi
	 * @param sparseIndex 
	 * @return
	 */
	private double computeDocumentLikelihood(int[] types, int[] freqs, double[][] phi, double[] lambda1, double[] lambda2, double[][] pi, double[][] ePi) {
		double likelihood = 0;
		double[] expTheta = new double[K];
		double[] expLogTheta = new double[K];
		updateThetaExpectationsForDoc(lambda1, lambda2, expTheta, expLogTheta);
		double curVal;
		for(int k=0;k<K;k++) {
			for(int n=0;n<types.length;n++) {
				int w = localWordTypeIds.get(types[n]);
				curVal = freqs[n]*phi[n][k]*(expLogTheta[k] + pi[k][w] - safeLog(phi[n][k])) - expTheta[k]*ePi[k][w];
				likelihood += curVal;
//				zBound += curVal;
			}
			//p(theta)
			curVal = props.lngap_a * Math.log(props.lngap_b) + (props.lngap_a - 1) * expLogTheta[k]; 
			curVal -= props.lngap_b*expTheta[k] + lgamma(props.lngap_a);
			
			//q(theta)
			curVal -= lambda1[k]*Math.log(lambda2[k]) + (lambda1[k] - 1) * expLogTheta[k];
			curVal += lambda2[k]*expTheta[k] + lgamma(lambda1[k]);
			likelihood += curVal;
//			thetaBound += curVal;
		}
		return likelihood;
	}

	
	private double varMStepSingleThread() {
		logger.info("optimzing beta");
		double likelihood = 0;
		for(int k=0;k<K;k++) {
			for(WordType wt : corpus.getAllWordTypes()) {
				int w = localWordTypeIds.get(wt.getId());
				likelihood += doGPRegressionStep(k, w);
			}
		}
		return likelihood;
	}

	private double varMStepMulitThread() throws InterruptedException, ExecutionException {
		logger.info("optimzing beta (multithreaded)");
		double likelihood = 0;
		Vector<Future<Double>> futures = new Vector<>();
		for(int k=0;k<K;k++) {
			for(WordType wt : corpus.getAllWordTypes()) {
				int w = localWordTypeIds.get(wt.getId());
				Future<Double> f = threadPool.submit(new OptimizationRunner(k, w, this));
				futures.add(f);
			}
		}
		for(Future<Double> f : futures)
			likelihood += f.get();
		return likelihood;
	}
	
	
	
	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericInferencer#getTopicLikelihood()
	 */
	@Override
	public double getTopicLikelihood() {
		double likelihood = 0;
		for(int w = 0;w<V;w++) {
			int[] nonzeroTimes = nonzeroFrequencyTimes[w];
			for(int k=0;k<K;k++) {
				DLNGAPObjective f = new DLNGAPObjective(nonzeroTimes.length, this);
				f.setObservationData(observationTimes[w], k, w);
				likelihood += f.getLikelihood();
			}
		}
		return likelihood;
	}

	private double doGPRegressionStep(int k, int w) {
		int optimizeMaxIter = 1000;
		double optimizerConvergence = 1e-6;
//		Optimizer minimizer = Optimizer.getOptimizer(FletcherReevesConjugateGradientOptimizer.class);
		Optimizer minimizer = Optimizer.getOptimizer(RPropOptimizer.class);
		int[] nonZeros = nonzeroFrequencyTimes[w];
		DLNGAPObjective objFun = new DLNGAPObjective(nonZeros.length, this);
		objFun.setCurWord(w);
		objFun.setCurTopic(k);
		objFun.setObservationData(observationTimes[w], k, w);
		double[] guess = mu[k][w].clone();
		
//		double[] guess = VectorUtils.multiplyWithScalar(ArrayFactory.doubleArray(Type.NORMAL, nonzeroFrequencyTimes[w].length), var0);
		
		Optimizer.optimize(guess, .35, 1e-4, optimizerConvergence, optimizeMaxIter, objFun, minimizer);
		double[] optResult = minimizer.getX();
		
		//update mu
		mu[k][w] = optResult;
		//TODO Sigma update
		updateBetaExpectationsForWordAndTopic(mu[k][w], Sigma[k][w], expBeta[k][w], expEBeta[k][w]);
		return objFun.getLikelihood();
	}


	@Override
	public void loadModel(String filename) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void populateFinalValues() {
		for(int t=0;t<T;t++) {
			for(int d=0;d<finalDocTopicWeights[t].length;++d)
				finalDocTopicWeights[t][d] = normalize(finalDocTopicWeights[t][d]);
		}
		double[] prediction;
		for(int k=0;k<K;++k) {
			for(int w=0;w<V;w++) {
				DLNGAPObjective o = new DLNGAPObjective(nonzeroFrequencyTimes[w].length, this);
				o.setObservationData(observationTimes[w], k, w);
				prediction = o.predictFromPosteriors(mu[k][w]);
				for(int t =0;t<T;t++)
					finalTopicWordWeights[t][k][w] = prediction[t];
			}
		}
		for(int t=0;t<T;t++) {
			for(int k=0;k<K;k++) {
				finalTopicWordWeights[t][k] = normalize(finalTopicWordWeights[t][k]);
			}
		}
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericDynamicInferencer#doPrediction(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicDocument, double[], int)
	 */
	@Override
	public double doPrediction(int[] types, int[] freqs, double[] predictions, int time) {
		double[][] phi = doubleArray(Type.ZERO, types.length, K);
		
		double[][] expEBeta = VectorUtils.exp(VectorUtils.add(posteriorMu[time], VectorUtils.multiplyWithScalar(posteriorSigma[time], .5)));

		
		return documentInference(phi, predictions, posteriorMu[time], expEBeta, types, freqs);
	}
	
	private class DocumentInferenceThread implements Callable<Double> {
		private Document doc;
		private double[][] phi;
		private double[] expTheta;
		private double[][] pi;
		private double[][] ePi;
		private int[] wordIds;
		private int[] freqs;

		public DocumentInferenceThread(Document doc, double[][] phi, double[] expTheta, double[][] pi, double[][] ePi, int[] types, int[] freqs) {
			this.doc = doc;
			this.phi = phi;
			this.expTheta = expTheta;
			this.pi = pi;
			this.ePi = ePi;
			this.wordIds = types;
			this.freqs = freqs;
		}

		/* (non-Javadoc)
		 * @see java.util.concurrent.Callable#call()
		 */
		@Override
		public Double call() throws Exception {
			return documentInference(phi, expTheta, pi, ePi, wordIds, freqs);
		}
	}
	
	private class OptimizationRunner implements Callable<Double> {
		private int k;
		private int w;
		
		private DynamicLogNormalGammaPoissonModel model;

		public OptimizationRunner(int topic, int word, DynamicLogNormalGammaPoissonModel m) {
			this.k = topic;
			this.w = word;
			this.model = m;
			
		}
		/* (non-Javadoc)
		 * @see java.util.concurrent.Callable#call()
		 */
		@Override
		public Double call() throws Exception {
			int optimizeMaxIter = 1000;
			double optimizerConvergence = 1e-6;
			Optimizer minimizer = Optimizer.getOptimizer(FletcherReevesConjugateGradientOptimizer.class);
//			Optimizer minimizer = Optimizer.getOptimizer(RPropOptimizer.class);
			int[] nonZeros = nonzeroFrequencyTimes[w];
			DLNGAPObjective objFun = new DLNGAPObjective(nonZeros.length, model);
			objFun.setCurWord(w);
			objFun.setCurTopic(k);
			objFun.setObservationData(observationTimes[w], k, w);
			double[] guess = mu[k][w].clone();
			
//			double[] guess = VectorUtils.multiplyWithScalar(ArrayFactory.doubleArray(Type.NORMAL, nonzeroFrequencyTimes[w].length), var0);
			
			Optimizer.optimize(guess, .35, 1e-4, optimizerConvergence, optimizeMaxIter, objFun, minimizer);
			double[] optResult = minimizer.getX();
			
			//update mu
			mu[k][w] = optResult;
			//TODO Sigma update
			updateBetaExpectationsForWordAndTopic(mu[k][w], Sigma[k][w], expBeta[k][w], expEBeta[k][w]);

			return objFun.getLikelihood();
		}
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericDynamicInferencer#doValidation(int[], int[], int[], int)
	 */
	@Override
	public double doValidation(int[] types, int[] freqsLearn,
			int[] freqsPredict, int time) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
