/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.topicModels.model.gpdtm;

import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.doubleArray;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils.digamma;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils.lgamma;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils.logAdd;
import static java.lang.Math.exp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.commons.math3.linear.CholeskyDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import cern.colt.map.OpenIntIntHashMap;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicCorpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.WordTokenState;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunction;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunctionHadamardProduct;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.CovarianceFunctionSum;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.brownian.BrownianKernel;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.ou.OrnsteinUhlenbeckKernel;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.periodic.PeriodicRBFKernel;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.gp.cov.rbf.GaussianKernel;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.Type;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.Optimizer;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.cg.FletcherReevesConjugateGradientOptimizer;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.rprop.RPropOptimizer;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericDynamicInferencer;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class GaussianProcessDynamicTopicModel extends AbstractGenericDynamicInferencer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	double nu;
	double initMean = 0;
	double initVar = 5;
	double[][][] ssKWT;
	double[][] ssKT;
	double[][][] m;
	double[][][] v;
	double[][] zeta;
	CovarianceFunction covFct;
	
	
	/**
	 * @param c
	 * @param props
	 * @throws IOException
	 */
	public GaussianProcessDynamicTopicModel(DiachronicCorpus c, DiachronicCorpus test, DiachronicCorpus validation,
			InferencerProperties props) throws IOException {
		super(c, test, validation, props);
		init();
	}

	/**
	 * 
	 */
	private void init() {
		nu = props.measurementNoise;
		Dynamics dyn = Dynamics.valueOf(props.dynamics.toUpperCase());
		
		Vector<CovarianceFunction> covFcs = new Vector<>();
		
		CovarianceFunction bm_base = new BrownianKernel(1, 0);
		covFcs.add(bm_base);

		//initiate covariance function
		switch (dyn) {
		case BROWNIAN_MOTION:
//			covFct = new BrownianKernel(props.processVariance, nu);
			//do nothing
			break;
		case ORNSTEIN_UHLENBECK:
//			covFct = new OrnsteinUhlenbeckKernel(nu, props.processVariance, props.lengthScale);
			covFcs.add(new OrnsteinUhlenbeckKernel(0, 1, props.lengthScale));
			break;
		case RBF:
//			covFct = new GaussianKernel(props.processVariance, props.lengthScale, nu);
			covFcs.add(new GaussianKernel(1, props.lengthScale, 0));
			break;
		case PERIODIC:
//			covFct = new PeriodicRBFKernel(nu, props.processVariance, props.lengthScale);
			covFcs.add(new PeriodicRBFKernel(0, 1, props.lengthScale));
			break;
		default:
			break;
		}
		CovarianceModel covModel = CovarianceModel.valueOf(props.covModel.toUpperCase());
		switch (covModel) {
		case SINGLE:
			covFct = covFcs.lastElement();
			double[] newHyperparameters = covFct.getHyperparameters();
			newHyperparameters[0] = nu;
			newHyperparameters[1] = props.processVariance;
			covFct.setHyperparameters(newHyperparameters);
			break;
		case ADDITIVE:
			covFct = new CovarianceFunctionSum(nu, props.processVariance, covFcs.toArray(new CovarianceFunction[]{})); 
			break;
		case ADDITIVE_WEIGHTED:
			double[] weights = new double[]{props.weight,(1-props.weight)};
			covFct = new CovarianceFunctionSum(nu, props.processVariance, covFcs.toArray(new CovarianceFunction[]{}), weights); 
			break;
		case MULTIPLICATIVE:
			covFct =  new CovarianceFunctionHadamardProduct(nu, props.processVariance, covFcs.toArray(new CovarianceFunction[]{}));
			break;
		default:
			covFct = new CovarianceFunctionSum(nu, props.processVariance, covFcs.toArray(new CovarianceFunction[]{})); 
			break;
		}
		
//		nu=.001;
		ssKT = ArrayFactory.doubleArray(Type.ZERO, K, T);
		ssKWT = new double[K][V][];
		m = new double[K][V][];
		v = new double[K][V][];
		for(WordType wt : corpus.getAllWordTypes()) {
			int w = localWordTypeIds.get(wt.getId());
			int n = nonzeroFrequencyTimes[w].length;
			for(int k=0;k<K;k++) {
				ssKWT[k][w] = ArrayFactory.doubleArray(Type.ZERO, n);
				m[k][w] = ArrayFactory.doubleArray(Type.NORMAL, n);
				v[k][w] = ArrayFactory.doubleArray(Type.UNIFORM, n);
			}
		}
		
		zeta = ArrayFactory.doubleArray(Type.ZERO, K, T);
		
		try {
			updateZeta();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}

	private double eStep() throws InterruptedException, ExecutionException {
		double likelihood = 0;
		//set word indices to zero
		int[] wIdx = new int[V];
		//for all times
		for(int t=0;t<T;t++) {
			likelihood += timeEStep(t, wIdx);
//			logger.info("done with time " + t + ", current likelihood: " + likelihood);
		}
		return likelihood;
	}

	private double timeEStep(int t, int[] wIdx) throws InterruptedException, ExecutionException {
		double likelihood = 0.;
		double[][] pi = doubleArray(Type.ZERO, K, V);
		//compute the logistic projection at time t (in logspace)
		for(int k=0;k<K;k++) {
			//for all words
			for(int w=0;w<V;w++) {
				//if nonzerotime of current word equals current time update pi (the word appears at t)
				if(wIdx[w] < nonzeroFrequencyTimes[w].length && nonzeroFrequencyTimes[w][wIdx[w]] == t) {
					pi[k][w] = m[k][w][wIdx[w]] - zeta[k][t];
				}
			}
		}
		Set<Integer> currentTimeDocIdSet = corpus.getDocumentsForDate(dates[t]);
		Vector<Future<Double>> futures = new Vector<>();
		futures.setSize(currentTimeDocIdSet.size());
		double[][][] phi = new double[currentTimeDocIdSet.size()][][];
		double[][] docGamma = ArrayFactory.doubleArray(Type.ZERO, currentTimeDocIdSet.size(), K);
		for(Integer docId : currentTimeDocIdSet) {
			Document doc = (Document) corpus.getDocumentById(docId);
			int d = localDocIds.get(docId);
			phi[d] = doubleArray(Type.ZERO, doc.getNumberOfWordTypes(), K);
			futures.setElementAt(threadPool.submit(new DocumentInferenceThread(docId, pi, phi[d], docGamma[d])), d);
		}
		
		for(Integer docId : currentTimeDocIdSet) {
			Document doc = (Document) corpus.getDocumentById(docId);
			int d = localDocIds.get(doc.getId());
			likelihood += futures.get(d).get();
			int[] wordIds = doc.getTypeList();
			int[] freqs = doc.getFrequencyList();

			int w, freq;
			for(int n=0;n<wordIds.length;n++) {
				w = localWordTypeIds.get(wordIds[n]);
				freq = freqs[n];
				for(int k=0;k<K;k++) {
					ssKWT[k][w][wIdx[w]] += freq * phi[d][n][k];
					ssKT[k][t] += freq * phi[d][n][k];
				}
			}
			
			finalDocTopicWeights[t][d] = docGamma[d].clone();
		}
		//update the time indices for used words in this timeslice
		for(Integer typeId : corpus.getDocumentIdSubsetWordFrequencies(currentTimeDocIdSet).keys().elements())
			wIdx[localWordTypeIds.get(typeId)]++;
		return likelihood;
	}

	private double doDocumentEStep(Integer docId, double[][] pi, double[][] phi, double[] docGamma) {
		double docLikelihood = 0.;

		Document doc = (Document) corpus.getDocumentById(docId);
		int[] wordIds = doc.getTypeList();
		int[] freqs = doc.getFrequencyList();
		//prepare document latent vars
		//posterior dirichlet
		for(int k=0;k<K;k++) {
			docGamma[k] = props.alpha + ((double)doc.getLength() / (double)K);
			for(int n=0;n<wordIds.length;n++) {
				phi[n][k] = 1./K;
			}
		}

		
		doDocumentInference(phi, docGamma, pi, wordIds, freqs);
		docLikelihood = computeDocumentLikelihood(phi, docGamma, pi, wordIds, freqs);
		
		return docLikelihood;
	}

	private void doDocumentInference(double[][] phi, double[] docGamma,
			double[][] pi, int[] wordIds, int[] freqs) {
		double converged = 1, phiSum = 0, likelihoodOld = Double.NEGATIVE_INFINITY;
		double[] oldPhi = new double[K];
		double[] digammaGam = new double[K];
		//compute digammas once for all words
		for(int k=0;k<K;k++)
			digammaGam[k] = digamma(docGamma[k]);
		
		double varIter = 0;
		int freq;
		double likelihood = 0;

		while((converged > 1e-3) && ((varIter < 50) || props.iterations == -1)) {
			varIter++;
			for(int n=0;n<wordIds.length;n++) {
				freq = freqs[n];
				phiSum = 0;
				for(int k=0;k<K;k++) {
					oldPhi[k] = phi[n][k];
					phi[n][k] = digammaGam[k] + pi[k][localWordTypeIds.get(wordIds[n])];
					if(k>0)
						phiSum = logAdd(phiSum, phi[n][k]);
					else
						phiSum = phi[n][k];
				}
				for(int k=0;k<K;k++) {
					phi[n][k] = exp(phi[n][k] - phiSum) + 1e-100;
					docGamma[k] = docGamma[k] + freq * (phi[n][k] - oldPhi[k]);
					digammaGam[k] = digamma(docGamma[k]);
				}
			}
			likelihood = computeDocumentLikelihood(phi, docGamma, pi, wordIds, freqs);
			if(varIter > 2)
				converged = (likelihoodOld - likelihood) / likelihoodOld;
			likelihoodOld = likelihood;
		}
		assert(!Double.valueOf(likelihood).isNaN());
	}

	private double computeDocumentLikelihood(double[][] phi, double[] docGamma,
			double[][] pi, int[] wordIds, int[] freqs) {
		double likelihood = 0d;
		double digsum = 0, gammaSum = 0;
		double[] dig = new double[K];
		
		for(int k=0;k<K;k++) {
			dig[k] = digamma(docGamma[k]);
			gammaSum += docGamma[k];
		}
		digsum = digamma(gammaSum);
		
		likelihood = lgamma(props.alpha * K) - (K * lgamma(props.alpha));
		likelihood -= lgamma(gammaSum);

		for(int k=0;k<K;k++) {
			likelihood += (props.alpha - docGamma[k]) * (dig[k] - digsum) + lgamma(docGamma[k]);
		}
		
		int freq;
		for(int n=0;n<wordIds.length;n++) {
			freq = freqs[n];
			for(int k=0;k<K;k++) {
				if(phi[n][k] > 0)
					likelihood += freq * (phi[n][k] * ((dig[k] - digsum) - MathUtils.safeLog(phi[n][k]) + pi[k][localWordTypeIds.get(wordIds[n])]));
			}
		}

		assert(!Double.valueOf(likelihood).isNaN());
			
		return likelihood;
	}

	
	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericInferencer#doInference()
	 */
	@Override
	public void doInference() throws IOException {
		double convergence = 1.;
		double likelihood = 0, oldLikelihood = -1e300, mStep = 0;
		int iter = 0;
		while(iter < props.iterations) {
			iter++;
			zeroStats();
			try {
				likelihood = eStep();
				logger.info("eStep likelihood: " + likelihood);
				mStep = mStepMultiThread();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
			likelihood += mStep;
			logger.info("mStep likelihood: " + mStep);
			convergence = (oldLikelihood - likelihood)/oldLikelihood;
			logger.info("likelihood: " + likelihood + ", convergence: " + convergence);
			oldLikelihood = likelihood;
			
			if((Double.valueOf(likelihood).isNaN() || convergence < props.convergenceCriterion) && iter > 3)
				break;
		}
	}

	/**
	 * @return
	 */
	private double mStep() {
		// take the sufficient statistics collected from the e-step and feed them to a GP regression step
		//for all topics and words:
		double likelihood = 0;
		long start, dur = 0;
		for(int k=0;k<K;++k) {
			for(int w=0;w<V;++w) {
				start = System.currentTimeMillis();
				likelihood += doGPRegressionStep(k,w);
				dur += System.currentTimeMillis() - start;
//				if(w%100==0)
//					System.out.println("done with word " + w);
			}
//			System.out.println("done with topic " + k);
		}
		System.out.println("average time per word: " + ((double)dur/(double)(K*V)) + "ms");
		return likelihood;
	}
	
	private double mStepMultiThread() throws InterruptedException, ExecutionException {
		double likelihood = 0;
		Vector<Future<Double>> futures = new Vector<>();
		for(int k=0;k<K;k++) {
			for(WordType wt : corpus.getAllWordTypes()) {
				int w = localWordTypeIds.get(wt.getId());
				Future<Double> f = threadPool.submit(new OptimizationRunner(k, w, this));
				futures.add(f);
			}
		}
		for(Future<Double> f : futures) {
			if(f == null)
				System.err.println("wtf");
			likelihood += f.get();
		}
		return likelihood;
	}

	/**
	 * @param k2
	 * @param w2
	 */
	private double doGPRegressionStep(int k, int w) {
//		Optimizer opt = Optimizer.getOptimizer(FletcherReevesConjugateGradientOptimizer.class); 
		Optimizer opt = Optimizer.getOptimizer(RPropOptimizer.class); 
		int[] nonzeroTimes = nonzeroFrequencyTimes[w];
		double[] guess = new double[nonzeroTimes.length];
		
		double[][] observationTimes = extractObservationTimes(w);
		for(int t=0;t<nonzeroTimes.length;t++)
			guess[t] = m[k][w][t];
		OptimizeTopicObjectiveFunction objective = new OptimizeTopicObjectiveFunction(nonzeroTimes.length, this);
		objective.setObservationData(observationTimes, k, w);
//		objective.testGradients();
		//subtract current beta from the zeta sum of exp(beta)
		for(int t=0;t<nonzeroTimes.length;t++)
			zeta[k][nonzeroTimes[t]] = MathUtils.logSubtract(zeta[k][nonzeroTimes[t]], m[k][w][t] + .5*v[k][w][t]);
		
		//optimization
		Optimizer.optimize(guess, .35, props.convergenceCriterion, props.convergenceCriterion, props.iterations, objective, opt);
		m[k][w] = opt.getX();
		
		//add optimized beta back to zeta
		for(int t=0;t<nonzeroTimes.length;t++)
			zeta[k][nonzeroTimes[t]] = MathUtils.logAdd(zeta[k][nonzeroTimes[t]], m[k][w][t] + .5*v[k][w][t]);

		return objective.getLikelihood();
		
	}

	private double[][] extractObservationTimes(int w) {
		int[] nonzeroTimes = nonzeroFrequencyTimes[w];
		double[][] observationTimes = new double[nonzeroTimes.length][1];
		for(int t=0;t<nonzeroTimes.length;t++) {
			double timeLocation = timeLocations[nonzeroTimes[t]][0];
			observationTimes[t][0] = timeLocation;
		}
		return observationTimes;
	}

	
	
	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericInferencer#getTopicLikelihood()
	 */
	@Override
	public double getTopicLikelihood() {
		double likelihood = 0;
		for(int w=0;w<V;w++) {
			double[][] observations = extractObservationTimes(w);
			for(int k=0;k<K;k++) {
				OptimizeTopicObjectiveFunction f = new OptimizeTopicObjectiveFunction(observations.length, this);
				f.setObservationData(observations, k, w);
				likelihood += f.getLikelihood();
			}
		}
		return likelihood;
	}

	private void updateZeta() throws InterruptedException, ExecutionException {
		for(int k =0;k<K;k++) {
			Arrays.fill(zeta[k], 0);
		}
		Vector<Future<?>> futures = new Vector<>();
		for(int w=0;w<V;w++) {
			futures.add(threadPool.submit(new ZetaUpdater(w)));
		}
		for(int i=0;i<futures.size();i++)
			futures.get(i).get();
	}

	private synchronized void updateZetaCritical(int w, int k, double expectedMean, int t) {
		if(w == 0) {
			zeta[k][t] = expectedMean;
		} else
			zeta[k][t] = MathUtils.logAdd(zeta[k][t], expectedMean);
	}

	/**
	 * 
	 */
	private void zeroStats() {
		for(int k=0;k<K;++k) {
			Arrays.fill(ssKT[k], 0);
			for(int w=0;w<V;++w)
				Arrays.fill(ssKWT[k][w], 0);
		}
		
	}
	
	private synchronized void updateZetaParts(double[] oldVals, int k, int w) {
		int[] nonzeroTimes = nonzeroFrequencyTimes[w];
		for(int t=0;t<nonzeroTimes.length;t++) {
			zeta[k][nonzeroTimes[t]] = MathUtils.logSubtract(zeta[k][nonzeroTimes[t]], oldVals[t]);
			zeta[k][nonzeroTimes[t]] = MathUtils.logAdd(zeta[k][nonzeroTimes[t]], m[k][w][t] + .5*v[k][w][t]);
		}

	}
	
	public double doPrediction(Date time, double[][] predictedGammas) throws IOException {
		//get local time
		int t = findClosestTimeStamp(time);
		
		Set<Integer> documents = testCorpus.getDocumentsForDate(time);
		
		//map doc ids to array indexes
		OpenIntIntHashMap indexesToDocs = new OpenIntIntHashMap();
		Integer[] docIdArray = documents.toArray(new Integer[]{});
		Arrays.sort(docIdArray);
		for(int i =0;i<docIdArray.length;i++) 
			indexesToDocs.put(i, docIdArray[i]);
		
		
		double likelihood = 0;
		int numTerms = 0;
		for(int d = 0;d<docIdArray.length;d++) {
			Document doc = (Document) corpus.getDocumentById(indexesToDocs.get(d));
			int[] wordIds = doc.getTypeList();
			int[] freqs = doc.getFrequencyList();
			double[][] phi = doubleArray(Type.ZERO, wordIds.length, K);
			//prepare document latent vars
			//posterior dirichlet
			for(int k=0;k<K;k++) {
				predictedGammas[d][k] = props.alpha + ((double)doc.getLength() / (double)K);
				for(int n=0;n<wordIds.length;n++) {
					phi[n][k] = 1./K;
				}
			}
			doDocumentInference(phi, predictedGammas[d], finalDocTopicWeights[t], wordIds, freqs);
			likelihood += computeDocumentLikelihood(phi, predictedGammas[d], finalDocTopicWeights[t], wordIds, freqs);
			numTerms += doc.getLength();
		}
		logger.info("per word likelihood: " + (likelihood/numTerms));
		return likelihood/numTerms;
	}
	
	public double doPrediction(int[] types, int[] freqs, double[] predictions, int time) {
		int length = VectorUtils.sum(freqs);
		double[][] phi = doubleArray(Type.ZERO, types.length, K);
		//prepare document latent vars
		//posterior dirichlet
		for(int k=0;k<K;k++) {
			predictions[k] = props.alpha + ((double)length / (double)K);
			for(int n=0;n<types.length;n++) {
				phi[n][k] = 1./K;
			}
		}
		double[][] pi = VectorUtils.log(finalTopicWordWeights[time]);
		doDocumentInference(phi, predictions, pi, types, freqs);
		return computeDocumentLikelihood(phi, predictions, pi, types, freqs);
	}
	
	
	

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericDynamicInferencer#doValidation(int[], int[], int[], int)
	 */
	@Override
	public double doValidation(int[] types, int[] freqsLearn,
			int[] freqsPredict, int time) {
		int length = VectorUtils.sum(freqsLearn);
		double[] predictions = doubleArray(Type.ZERO, K);
		double[][] phi = doubleArray(Type.ZERO, types.length, K);
		//prepare document latent vars
		//posterior dirichlet
		for(int k=0;k<K;k++) {
			predictions[k] = props.alpha + ((double)length / (double)K);
			for(int n=0;n<types.length;n++) {
				phi[n][k] = 1./K;
			}
		}
		double[][] pi = VectorUtils.log(finalTopicWordWeights[time]);
		doDocumentInference(phi, predictions, pi, types, freqsLearn);
		return computeDocumentLikelihood(phi, predictions, pi, types, freqsPredict);
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericInferencer#loadModel(java.lang.String)
	 */
	@Override
	public void loadModel(String filename) throws IOException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericInferencer#populateFinalValues()
	 */
	@Override
	public void populateFinalValues() {
		try {
			updateZeta();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
//		double mean = 0;
//		double[] correctedPoint;
//		for(int w=0;w<V;w++) {
//			int[] nonzeroTimes = nonzeroFrequencyTimes[w];
//			RealMatrix noise = MatrixUtils.createRealIdentityMatrix(nonzeroTimes.length).scalarAdd(nu);
//			double[][] observationTimes = new double[nonzeroTimes.length][1];
//			for(int t=0;t<nonzeroTimes.length;t++) { 
//				observationTimes[t][0] = timeLocations[nonzeroTimes[t]][0];
//			}
//			RealMatrix Kbm = MatrixUtils.createRealMatrix(covFct.computeCrossCovarianceMatrix(observationTimes, timeLocations));
//			RealMatrix Kbb = MatrixUtils.createRealMatrix(covFct.computeCovarianceMatrix(observationTimes));
//			CholeskyDecomposition cd = new CholeskyDecomposition(Kbb.add(noise));
//			RealMatrix KbbPlusNoiseInv = cd.getSolver().getInverse();
//			
//			RealMatrix KmbTimesKbbPlusNoiseInv = Kbm.transpose().multiply(KbbPlusNoiseInv);
//
//			//compute variance
//			RealMatrix var = Kmm.subtract(Kbm.transpose().multiply(KbbPlusNoiseInv).multiply(Kbm));
//			double[] diagVar = new double[var.getRowDimension()];
//			for(int i=0;i<diagVar.length;i++)
//				diagVar[i] = var.getEntry(i, i);
//			
//			for(int k=0;k<K;k++) {
//				mean = VectorUtils.mean(m[k][w]);
//				correctedPoint = VectorUtils.subtractScalar(m[k][w], mean);
//				//compute mean
//				RealMatrix newMean = KmbTimesKbbPlusNoiseInv.multiply(MatrixUtils.createColumnRealMatrix(correctedPoint)).scalarAdd(mean);
//				for(int t=0;t<T;t++)
//					finalTopicWordWeights[t][k][w] = newMean.getEntry(t, 0);
//			}
//		}

		for(int t=0;t<T;t++) {
			for(int k=0;k<K;k++) {
				for(int w=0;w<V;w++) {
					finalTopicWordWeights[t][k][w] = Math.exp(finalTopicWordWeights[t][k][w] - zeta[k][t]);
				}
			}
		}
	}
	
	private class DocumentInferenceThread implements Callable<Double> {
		private Integer docId;
		private double[][] pi;
		private double[][] phi;
		private double[] docGamma;

		public DocumentInferenceThread(Integer docId, double[][] pi, double[][] phi, double[] docGamma) {
			this.docId = docId;
			this.pi = pi;
			this.phi = phi;
			this.docGamma = docGamma;
		}

		/* (non-Javadoc)
		 * @see java.util.concurrent.Callable#call()
		 */
		@Override
		public Double call() throws Exception {
			return doDocumentEStep(docId, pi, phi, docGamma);
		}
	}
	
	
	private class OptimizationRunner implements Callable<Double> {
		private int k;
		private int w;
		
		private GaussianProcessDynamicTopicModel model;

		public OptimizationRunner(int topic, int word, GaussianProcessDynamicTopicModel m) {
			this.k = topic;
			this.w = word;
			this.model = m;
			
		}
		/* (non-Javadoc)
		 * @see java.util.concurrent.Callable#call()
		 */
		@Override
		public Double call() throws Exception {
//			Optimizer opt = Optimizer.getOptimizer(FletcherReevesConjugateGradientOptimizer.class); 
			Optimizer opt = Optimizer.getOptimizer(RPropOptimizer.class); 
			int[] nonzeroTimes = nonzeroFrequencyTimes[w];
			double[][] observationTimes = new double[nonzeroTimes.length][1];
			double[] guess = new double[nonzeroTimes.length];
			
			for(int t=0;t<nonzeroTimes.length;t++) {
				double timeLocation = timeLocations[nonzeroTimes[t]][0];
				observationTimes[t][0] = timeLocation;
				guess[t] = m[k][w][t];
			}
			OptimizeTopicObjectiveFunction objective = new OptimizeTopicObjectiveFunction(nonzeroTimes.length, model);
			objective.setObservationData(observationTimes, k, w);
			double[] localZeta = new double[nonzeroTimes.length];
			double[] oldVals = VectorUtils.add(m[k][w], VectorUtils.multiplyWithScalar(v[k][w], .5));
			for(int t=0;t<nonzeroTimes.length;t++)
				localZeta[t] = MathUtils.logSubtract(zeta[k][nonzeroTimes[t]], m[k][w][t] + .5*v[k][w][t]);
			objective.setLocalZeta(localZeta);
//			objective.testGradients();
			//subtract current beta from the zeta sum of exp(beta)
			
			//optimization
			Optimizer.optimize(guess, .35, props.convergenceCriterion, props.convergenceCriterion, props.iterations, objective, opt);
			m[k][w] = opt.getX();
			
			updateZetaParts(oldVals, k, w);
			return objective.getLikelihood();
		}
	}
	
	private class ZetaUpdater implements Runnable {
		
		private int w;

		public ZetaUpdater(int w) {
			this.w = w;
		}
		
		@Override
		public void run() {
			boolean predictionsNeeded = false;
			double[] postVar = new double[T];
			
			RealMatrix preTerm = null;
			RealMatrix KbbPlusNoiseInv = null;
			RealMatrix KPredObs = null;
			//things that need to be done anyways
			int[] nonzeroTimes = nonzeroFrequencyTimes[w];
			int[] zeroTimes = zeroFrequencyTimes[w];
			if(nonzeroTimes.length != T) {
				predictionsNeeded = true;
				//this word appears sparsely, predict states at unknown times
				double[][] observationTimes = new double[nonzeroTimes.length][1];
				double[][] predictionTimes = new double[zeroTimes.length][1];
				//collect unknown times
				for(int t=0;t<nonzeroTimes.length;t++)
					observationTimes[t][0] = timeLocations[nonzeroTimes[t]][0];
				for(int t=0;t<zeroTimes.length;t++)
					predictionTimes[t][0] = timeLocations[t][0];
				
				//precomputations
				RealMatrix Kbb = MatrixUtils.createRealMatrix(covFct.computeCovarianceMatrix(observationTimes));
				CholeskyDecomposition cd = new CholeskyDecomposition(Kbb);
				RealMatrix KbbInv = cd.getSolver().getInverse();
				
				RealMatrix noise = MatrixUtils.createRealIdentityMatrix(nonzeroTimes.length).scalarAdd(nu);
				cd = new CholeskyDecomposition(Kbb.add(noise));
				KbbPlusNoiseInv = cd.getSolver().getInverse();
				
				preTerm = Kbb.add(noise).multiply(KbbInv);
				
				//compute predicted variance, constant for each word
				RealMatrix KPred = MatrixUtils.createRealMatrix(covFct.computeCovarianceMatrix(predictionTimes));
				KPredObs = MatrixUtils.createRealMatrix(covFct.computeCrossCovarianceMatrix(predictionTimes, observationTimes));
				
				RealMatrix var = KPred.subtract(KPredObs.multiply(KbbPlusNoiseInv).multiply(KPredObs.transpose()));
				for(int t=0;t<zeroTimes.length;t++)
					postVar[zeroTimes[t]] = var.getEntry(t, t);
			}
			for(int k=0;k<K;k++) {
				double[] curMean = new double[T];
				for(int t=0;t<nonzeroTimes.length;t++) {
					curMean[nonzeroTimes[t]] = m[k][w][t];
					postVar[nonzeroTimes[t]] = v[k][w][t];
				}

				if(predictionsNeeded) {
					//do the predictions at unknown points and complete curMean
					double postMean = VectorUtils.mean(m[k][w]);
					RealMatrix derivedObservations = preTerm.multiply(MatrixUtils.createColumnRealMatrix(m[k][w]).scalarAdd(-postMean));//.scalarAdd(postMean);
//					double obsMean = VectorUtils.mean(derivedObservations.getColumn(0));
//					double[] predictions = KPredObs.multiply(KbbPlusNoiseInv).multiply(derivedObservations.scalarAdd(-obsMean)).scalarAdd(obsMean).getColumn(0);
					double[] predictions = KPredObs.multiply(KbbPlusNoiseInv).multiply(derivedObservations).getColumn(0);

					for(int t=0;t<zeroTimes.length;t++)
						curMean[zeroTimes[t]] = predictions[t] + postMean;
				} 
					
				double[] expectedMean = VectorUtils.add(curMean, VectorUtils.multiplyWithScalar(postVar, .5));	
				
				for(int t=0;t<T;t++) {
					finalTopicWordWeights[t][k][w] = expectedMean[t];
					updateZetaCritical(w, k, expectedMean[t], t);
				}
			}
			
		}
	}

}
