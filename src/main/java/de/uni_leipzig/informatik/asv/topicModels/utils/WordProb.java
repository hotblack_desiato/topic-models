/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.utils;

public class WordProb implements Comparable<WordProb>{
	
	private double prob;
	private String term;
	private int type;
	
	public WordProb(String t, double p, int typeId) {
		term = t;
		prob = p;
		type = typeId;
	}

	public int compareTo(WordProb o) {
		return this.prob < o.getProb() ? 1 : this.prob == o.getProb() ? 0 : -1;
	}

	public double getProb() {
		return prob;
	}

	public String getTerm() {
		return term;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}
	
	

}
