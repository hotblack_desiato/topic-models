/*******************************************************************************
 * Copyright (c) 2011  Arnim Bleier, Patrick Jähnichen and Andreas Niekler
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.hdp.crf.state;

import java.util.ArrayList;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.ArrayUtils;

public class DOCState extends DocumentState{
	
	private int numberOfTables;
	private int[] tableToTopic; 
    private int[] wordCountByTable;

	public DOCState() {
	    numberOfTables = 0;  
	    wordCountByTable = new int[2];
	    tableToTopic = new int[2];
	}


	public int[] getWordCountsByTopic(int numTopics) {
		int[] c = new int[numTopics];
		for(int t=0;t<numberOfTables;t++) {
			if(tableToTopic[t] == -1)
				continue;
			c[tableToTopic[t]] += wordCountByTable[t];
		}
		return c;
	}


	public int getNumberOfTables() {
		return numberOfTables;
	}

	public void setNumberOfTables(int newNumberOfTables) {
		numberOfTables = newNumberOfTables;
	}

	public int[] getTableToTopic() {
		return tableToTopic;
	}


	public int[] getWordCountByTable() {
		return wordCountByTable;
	}

	public void incrementTableWordCount(int table) {
		wordCountByTable[table]++;
	}
	
	public void decrementTableWordCount(int table) {
		wordCountByTable[table]--;
	}
	
	public void ensureTableWordCountCapacity(int capacity) {
		wordCountByTable = ArrayUtils.ensureCapacity(wordCountByTable, capacity);
	}
	
	public void setTableToTopic(int table, int topic) {
		tableToTopic[table] = topic;
	}
	
	public void decrementTableToTopic(int table)  {
		tableToTopic[table]--;
	}
	
	public void ensureTableToTopicCapacity(int capacity) {
		tableToTopic = ArrayUtils.ensureCapacity(tableToTopic, capacity);
	}
	
	public void incrementNumberOfTables() {
		numberOfTables++;
	}
	
	public void decrementNumberOfTables() {
		numberOfTables--;
	}
	
	public void defragment() {
		ArrayList<Integer> occTables = new ArrayList<>();
		for(int t=0;t<numberOfTables;t++) {
			if(wordCountByTable[t] > 0)
				occTables.add(t);
		}
		int[] occTableArray = org.apache.commons.lang3.ArrayUtils.toPrimitive(occTables.toArray(new Integer[]{}));
		int[] newTableToTopic = new int[occTableArray.length+1]; 
		int[] newWordCountByTable = new int[occTableArray.length+1];
		System.arraycopy(VectorUtils.viewSelection(tableToTopic, occTableArray), 0, newTableToTopic, 0, occTableArray.length);
		System.arraycopy(VectorUtils.viewSelection(wordCountByTable, occTableArray), 0, newWordCountByTable, 0, occTableArray.length);
		
		tableToTopic = newTableToTopic;
		wordCountByTable = newWordCountByTable;
		numberOfTables -= (numberOfTables - occTableArray.length);
	}


	@Override
	public DOCState clone() {
		DOCState newOne = new DOCState();
		newOne.numberOfTables = numberOfTables;
		newOne.tableToTopic = tableToTopic.clone();
		newOne.wordCountByTable = wordCountByTable.clone();
		return newOne;
	}
	
	
}
