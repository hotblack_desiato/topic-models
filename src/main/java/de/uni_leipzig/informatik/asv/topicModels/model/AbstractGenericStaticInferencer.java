/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.Arrays;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.Type;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.ArrayUtils;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;
import de.uni_leipzig.informatik.asv.topicModels.utils.PerplexityCheck;
import de.uni_leipzig.informatik.asv.topicModels.utils.PosteriorPredictiveCheckStatic;
import de.uni_leipzig.informatik.asv.topicModels.utils.WordProb;

public abstract class AbstractGenericStaticInferencer extends AbstractGenericInferencer<Corpus>
		implements Serializable, GenericStaticInferencer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected double[][] finalTopicWordWeights;

	protected double[][] finalDocTopicWeights;

	protected AbstractGenericStaticInferencer(Corpus c, Corpus test, Corpus validation, InferencerProperties props)
			throws IOException {
		super(c, test, validation, props);
		finalDocTopicWeights = ArrayFactory.doubleArray(Type.ZERO, M, K);
		finalTopicWordWeights = ArrayFactory.doubleArray(Type.ZERO, K, V);
	}

	/**
	 * @return the finalTopicWordWeights
	 */
	public double[][] getFinalTopicWordWeights() {
		return finalTopicWordWeights;
	}

	/**
	 * @return the finalDocTopicWeights
	 */
	public double[][] getFinalDocTopicWeights() {
		return finalDocTopicWeights;
	}

	protected void writeTopWordsForTopics(String topicTopWordsFile, int numwords) throws IOException {
		PrintStream ps = new PrintStream(props.targetDir + File.separator + topicTopWordsFile);
		int sizeOfVocabulary = corpus.getNumberOfWordTypes();
		WordProb[] probs = new WordProb[sizeOfVocabulary];

		for (int k = 0; k < finalTopicWordWeights.length; k++) {
			getWordsForTopicSorted(probs, k);
			ps.println("Topic " + k);
			for (int i = 0; i < numwords; i++)
				ps.println(probs[i].getTerm() + "\t" + probs[i].getProb());
			ps.println();
		}
		ps.close();
	}

	private void getWordsForTopicSorted(WordProb[] probs, int k) {
		for (WordType wt : corpus.getAllWordTypes()) {
			int typeId = localWordTypeIds.get(wt.getId());
			probs[typeId] = new WordProb(wt.getValue(), finalTopicWordWeights[k][typeId], typeId);
		}
		Arrays.sort(probs);
	}

	protected void writeDocTopicProportions(String filename) throws IOException {
		ArrayUtils.saveArrayToFile(props.targetDir + File.separator + filename, finalDocTopicWeights);
	}

	protected void writeTopicTermProportions(String topicWordProbabilityFile) throws IOException {
		ArrayUtils.saveArrayToFile(props.targetDir + File.separator + topicWordProbabilityFile, finalTopicWordWeights);
	}

	protected void writeOverallTopicProportions(String globalTopicProbabilityFile) throws IOException {
		StringBuilder sb = new StringBuilder();
		double corpusSize = (double) corpus.getNumberOfWordTokens();
		for (int k = 0; k < finalTopicWordWeights.length; ++k) {
			double prop = 0;
			for (Document d : corpus.getDocuments()) {
				double l = (double) d.getLength();
				prop += l * finalDocTopicWeights[localDocIds.get(d.getId())][k];
			}
			sb.append((prop / corpusSize)).append("\n");
		}
		BufferedWriter bw = new BufferedWriter(
				new FileWriter(props.targetDir + File.separator + globalTopicProbabilityFile));
		bw.write(sb.toString());
		bw.close();

	}

	@Override
	public void testModel() throws IOException {
		String testDir = props.targetDir + File.separator + "test";
		File testDirFile = new File(testDir);
		if(!testDirFile.exists())
			testDirFile.mkdirs();
		// posterior predictive checking
		if (props.doPosteriorPredictiveChecking) {
			PosteriorPredictiveCheckStatic ppc = new PosteriorPredictiveCheckStatic(this);
			ppc.doPosteriorPredictiveChecking(testDir, props.topicWordlistSize);
		}

		// implement perplexity metric and held-out log likelihood
		PerplexityCheck ppx = new PerplexityCheck(this);
		double perplexity = -1, docCompletion = -1;
		if(props.doPerplexityTest)
			perplexity = ppx.computeValidationPerplexity();
		if(props.doDocumentCompletionTest)
			docCompletion = ppx.computeDocumentCompletionLikelihood();
		BufferedWriter bw = new BufferedWriter(new FileWriter(testDir + File.separator + "perplexity.dat"));
		bw.write(perplexity + "\n");
		bw.write(docCompletion + "\n");
		bw.close();
	}

}
