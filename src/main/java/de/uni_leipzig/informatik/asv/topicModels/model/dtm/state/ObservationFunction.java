/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.dtm.state;

import org.apache.commons.math3.analysis.MultivariateFunction;

public class ObservationFunction implements MultivariateFunction{

	
	private OptimizationParams params;
	private double initMult;

	public ObservationFunction(OptimizationParams params, double initMult) {
		this.params = params;
		this.initMult = initMult;
	}
	
	@Override
	public double value(double[] x) {
	    int t, T = x.length;
	    double val = 0, term1 = 0, term2 = 0, term3 = 0, term4 = 0;
	    double[] mean, variance, w_phi_l, m_update_coeff;

	    params.getTopic().getObs()[params.getWord()] = x;

	    // Run the forward-backward algorithm
	    params.getTopic().computePosteriorMean(params.getWord());
	    
	    mean = params.getTopic().getMean()[params.getWord()];
	    variance = params.getTopic().getVariance()[params.getWord()];
	    w_phi_l = params.getTopic().getwPhiL()[params.getWord()];
	    m_update_coeff = params.getTopic().getmUpdateCoeff()[params.getWord()];

	    // Only compute the objective if the chain variance 
	    for (t = 1; t <= T; t++) {
	        double mean_t = mean[t];
	        double mean_t_prev = mean[t-1];
	        double var_t_prev = variance[t - 1];
	        val = mean_t - mean_t_prev;
	        term1 += val * val;

	        // note, badly indexed counts
	        term2 +=
	            params.getWord_counts()[t-1] * mean_t -
	            params.getTotals()[t-1] *
	            (Math.exp(mean_t + variance[t]/2) / params.getTopic().getZeta()[t-1]);
	            // log(vget(p->var->zeta, t-1)));

	    }
	    // note that we multiply the initial variance by INIT_MULT
	    if (params.getTopic().getChainVariance() > 0.0) {
	      term1 = - term1 / (2 * params.getTopic().getChainVariance());
	      term1 = (term1 -
		       mean[0] * mean[0]) /
		       (2 * initMult * params.getTopic().getChainVariance());      
	    } else {
	      term1 = 0.0;
	    }

	    return (term1 + term2 + term3 + term4);

	}

}
