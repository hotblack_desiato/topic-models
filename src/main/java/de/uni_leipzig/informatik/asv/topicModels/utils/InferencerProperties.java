/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class InferencerProperties {

	public int iterations;
	public int sampleLag;
	public double alpha;
	public double alpha_a;
	public double alpha_b;
	public double beta;
	public double beta_a;
	public double beta_b;
	public double gamma;
	public double gamma_a;
	public double gamma_b;
	public double eta;
	public double d;
	public double tau0;
	public double kappa;
	public double omega;
	public int numTopics;
	public int burnIn;
	public String sourceDir;
	public String targetDir;
	public int shuffleLag;
	public Integer batchSize;
	public boolean sampleHyperParams;
	public int hyperSampleLag;
	public int maxInitialTopics;
	public int maxFreq;
	public int minFreq;
	public int globalTopicTruncate;
	public int localTopicTruncate;
	public Model model;
	public double convergenceCriterion;
	public String corpusDirList;
	public int numThreads;
	public int topicWordlistSize;
	public int[] day, month, year;
	
	public Long randomSeed;
	
	//gamma poisson
	public double gap_a;
	public double gap_b;
	public double gap_c;
	public double gap_d;
	
	//lognormal gamma poisson
	public double lngap_a;
	public double lngap_b;
	public double lngap_mu;
	public double lngap_sigma;
	
	public Granularity granularity;
	
	//DTM
	public int dtmFixedTopics;
	public double dtmInitMult;
	public double dtmTopicChainVariance;
	public double dtmObservationVariance;
	public double dtmObservationNormCutoff;
	public int dtmMinIterations;
	public int dtmMaxIterations;
	
	//GPDTM
	public double processVariance;
	public double measurementNoise;
	public double lengthScale;
	public String dynamics;
	public String covModel;
	
	//files
	public String topicTopWordsFile;
	public String topicWordProbabilityFile;
	public String documentTopicProbabilityFile;
	public String testDocumentTopicProbabilityFile;
	public String testResultFilePrefix;
	public String globalTopicProbabilityFile;
	public String localToGlobalTypeIdFile;
	public String likelihoodFile;
	
	public enum PropertyNames {
		//files
		TOPIC_WORD_PROBABILITY_FILE,
		DOCUMENT_TOPIC_PROBABILITY_FILE,
		TEST_DOCUMENT_TOPIC_PROBABILITY_FILE,
		TEST_RESULT_FILE_PREFIX,
		TOPIC_TOP_WORDS_FILE,
		GLOBAL_TOPIC_PROBABILITY_FILE,
		LOCAL_TO_GLOBAL_TYPE_ID_FILE,
		LIKELIHOOD_FILE,
		//misc
		ITERATIONS, 
		SAMPLE_LAG, 
		SHUFFLE_LAG, 
		HYPER_SAMPLE_LAG, 
		NUM_TOPICS, 
		MAX_INITIAL_TOPICS, 
		MIN_FREQ,
		MAX_FREQ,
		GLOBAL_TOPIC_TRUNCATE, 
		LOCAL_TOPIC_TRUNCATE, 
		BURN_IN, 
		SOURCE_DIR, 
		TARGET_DIR, 
		SAMPLE_HYPER, 
		MODEL, 
		CONVERGENCE_CRITERION, 
		CORPUS_DIR_LIST, 
		NUM_THREADS, 
		TOPIC_WORDLIST_SIZE, 
		DAY, 
		MONTH, 
		YEAR,
		RANDOM_SEED,
		DO_TESTING,
		PPC_TEST,
		PERPLEXITY_TEST,
		DOC_COMPLETION_TEST,
		//hyper params
		ALPHA, 
		ALPHA_A, 
		ALPHA_B, 
		BETA, 
		BETA_A, 
		BETA_B, 
		GAMMA, 
		GAMMA_A, 
		GAMMA_B, 
		TAU0, 
		KAPPA, 
		OMEGA, 
		BATCH_SIZE, 
		ETA, 
		D, 
		//GAP model
		GAP_A,
		GAP_B,
		GAP_C,
		GAP_D,
		//LNGAP model
		LNGAP_A,
		LNGAP_B,
		LNGAP_MU,
		LNGAP_SIGMA,
		//SDE
		SDE_TYPE,
		//GPDTM
		PROCESS_VARIANCE,
		MEASUREMENT_NOISE,
		LENGTH_SCALE,
		DYNAMICS,
		COVARIANCE_MODEL,
		//DTM
		DTM_FIXED_TOPICS,
		DTM_INITIAL_VARIANCE_MULTIPLIER,
		DTM_TOPIC_CHAIN_VARIANCE,
		DTM_OBSERVATION_VARIANCE,
		DTM_OBSERVATION_NORM_CUTOFF,
		DTM_MIN_ITERATIONS,
		DTM_MAX_ITERATIONS, 
		
		GRANULARITY, WEIGHT;
	}

	public enum Model {
		//LDA batch
		LDA_GIBBS, 
		LDA_VB, 
		//LDA SVI
		LDA_ONLINE_VB, 
		LDA_ONLINE_VB_TIMESLICE, 
		LDA_ONLINE_VB_ADAPTIVE, 
		//HDP batch
		HDP_AUX, 
		HDP_CRF, 
		HDP_PR, 
		//HDP SVI
		HDP_ONLINE, 
		HDP_ONLINE_ADAPTIVE,
		//HPY batch
		HPY_CRF, 
		//HPY SVI
		HPY_ONLINE,
		//DTM batch
		DTM_VB,
		
		//CTM
		CTM_VB,
		
		//gamma poisson
		GAMMA_POISSON,
		
		//log normal gamma poisson
		LNGAP,
		
		//dynamic log normal gamma poisson
		DLNGAP,
		
		//Logistic normal prior
		LOGISTIC,
		
		//GP driven
		GPDTM,
		
		//SDE driven
		SDE;
	}
	
	public enum Granularity {
		DAILY,
		MONTHLY,
		YEARLY;
	}

	private Properties properties;
	public double weight;
	public boolean doTesting;
	public boolean doPosteriorPredictiveChecking;
	public boolean doPerplexityTest;
	public boolean doDocumentCompletionTest;
	
	public InferencerProperties() {
		loadFromPropertiesFile("./conf/default.properties");
	}

	public InferencerProperties(String propertiesFile) {
		loadFromPropertiesFile(propertiesFile);
	}

	public void saveToFile(String filename) throws IOException {
		FileWriter fw = new FileWriter(filename);
		properties.store(fw, null);
		fw.close();
	}

	private void loadFromPropertiesFile(String filename) {
		if (properties == null) {
			properties = new Properties();
		}
		try {
			if(filename != null) {
				FileInputStream fis = new FileInputStream(new File(filename));
				properties.load(fis);
				fis.close();
			}
			model = Model.valueOf(properties.getProperty(
					PropertyNames.MODEL.toString(), "LDA_GIBBS").toUpperCase());
			iterations = Integer.valueOf(properties.getProperty(
					PropertyNames.ITERATIONS.toString(), "1000"));
			maxInitialTopics = Integer.valueOf(properties.getProperty(
					PropertyNames.MAX_INITIAL_TOPICS.toString(), "50"));
			minFreq = Integer.valueOf(properties.getProperty(
					PropertyNames.MIN_FREQ.toString(), "-1"));
			maxFreq = Integer.valueOf(properties.getProperty(
					PropertyNames.MAX_FREQ.toString(), "-1"));
			globalTopicTruncate = Integer.valueOf(properties.getProperty(
					PropertyNames.GLOBAL_TOPIC_TRUNCATE.toString(), "1000"));
			localTopicTruncate = Integer.valueOf(properties.getProperty(
					PropertyNames.LOCAL_TOPIC_TRUNCATE.toString(), "50"));
			sampleLag = Integer.valueOf(properties.getProperty(
					PropertyNames.SAMPLE_LAG.toString(), "50"));
			burnIn = Integer.valueOf(properties.getProperty(
					PropertyNames.BURN_IN.toString(), "300"));
			shuffleLag = Integer.valueOf(properties.getProperty(
					PropertyNames.SHUFFLE_LAG.toString(), "50"));
			hyperSampleLag = Integer.valueOf(properties.getProperty(
					PropertyNames.HYPER_SAMPLE_LAG.toString(), "20"));
			alpha = Double.valueOf(properties.getProperty(
					PropertyNames.ALPHA.toString(), "0.5"));
			alpha_a = Double.valueOf(properties.getProperty(
					PropertyNames.ALPHA_A.toString(), "0.5"));
			alpha_b = Double.valueOf(properties.getProperty(
					PropertyNames.ALPHA_B.toString(), "0.5"));
			beta = Double.valueOf(properties.getProperty(
					PropertyNames.BETA.toString(), "0.1"));
			beta_a = Double.valueOf(properties.getProperty(
					PropertyNames.BETA_A.toString(), "0.1"));
			beta_b = Double.valueOf(properties.getProperty(
					PropertyNames.BETA_B.toString(), "0.1"));
			gamma = Double.valueOf(properties.getProperty(
					PropertyNames.GAMMA.toString(), "0.1"));
			gamma_a = Double.valueOf(properties.getProperty(
					PropertyNames.GAMMA_A.toString(), "0.1"));
			gamma_b = Double.valueOf(properties.getProperty(
					PropertyNames.GAMMA_B.toString(), "0.1"));
			eta = Double.valueOf(properties.getProperty(
					PropertyNames.ETA.toString(), "0.1"));
			d = Double.valueOf(properties.getProperty(
					PropertyNames.D.toString(), "-0.1"));
			tau0 = Double.valueOf(properties.getProperty(
					PropertyNames.TAU0.toString(), "1"));
			kappa = Double.valueOf(properties.getProperty(
					PropertyNames.KAPPA.toString(), "0.9"));
			omega = Double.valueOf(properties.getProperty(
					PropertyNames.OMEGA.toString(), "0.05"));
			batchSize = Integer.valueOf(properties.getProperty(
					PropertyNames.BATCH_SIZE.toString(), "100"));
			sampleHyperParams = Boolean.valueOf(properties.getProperty(
					PropertyNames.SAMPLE_HYPER.toString(), "false"));
			convergenceCriterion = Double.valueOf(properties.getProperty(
					PropertyNames.CONVERGENCE_CRITERION.toString(), "0.0"));
			numTopics = Integer.valueOf(properties.getProperty(
					PropertyNames.NUM_TOPICS.toString(), "10"));
			sourceDir = properties.getProperty(
					PropertyNames.SOURCE_DIR.toString(), "./source");
			targetDir = properties.getProperty(
					PropertyNames.TARGET_DIR.toString(), "./data");
			corpusDirList = properties.getProperty(
					PropertyNames.CORPUS_DIR_LIST.toString(), "");
			numThreads = Integer
					.parseInt(properties.getProperty(PropertyNames.NUM_THREADS
							.toString(), String.valueOf(Runtime.getRuntime()
							.availableProcessors()*2)));
			topicWordlistSize = Integer.parseInt(properties.getProperty(
					PropertyNames.TOPIC_WORDLIST_SIZE.toString(), "25"));
			day = toIntArray(properties.getProperty(
					PropertyNames.DAY.toString(), ""));
			month = toIntArray(properties.getProperty(
					PropertyNames.MONTH.toString(), ""));
			year = toIntArray(properties.getProperty(
					PropertyNames.YEAR.toString(), ""));
			randomSeed = Long.valueOf(properties.getProperty(PropertyNames.RANDOM_SEED.toString(), "0"));
			
			doTesting = Boolean.valueOf(properties.getProperty(PropertyNames.DO_TESTING.toString(), "false")).booleanValue();
			doPosteriorPredictiveChecking = Boolean.valueOf(properties.getProperty(PropertyNames.PPC_TEST.toString(), "false")).booleanValue();
			doPerplexityTest = Boolean.valueOf(properties.getProperty(PropertyNames.PERPLEXITY_TEST.toString(), "false")).booleanValue();
			doDocumentCompletionTest = Boolean.valueOf(properties.getProperty(PropertyNames.DOC_COMPLETION_TEST.toString(), "false")).booleanValue();
			//gamma poisson
			gap_a = Double.valueOf(properties.getProperty(PropertyNames.GAP_A.toString(), "1.0"));
			gap_b = Double.valueOf(properties.getProperty(PropertyNames.GAP_B.toString(), "1.0"));
			gap_c = Double.valueOf(properties.getProperty(PropertyNames.GAP_C.toString(), "1.0"));
			gap_d = Double.valueOf(properties.getProperty(PropertyNames.GAP_D.toString(), "1.0"));
			
			//lognormal gamma poisson
			lngap_a = Double.valueOf(properties.getProperty(PropertyNames.LNGAP_A.toString(), ".05"));
			lngap_b = Double.valueOf(properties.getProperty(PropertyNames.LNGAP_B.toString(), "1.0"));
			lngap_mu = Double.valueOf(properties.getProperty(PropertyNames.LNGAP_MU.toString(), "0"));
			lngap_sigma = Double.valueOf(properties.getProperty(PropertyNames.LNGAP_SIGMA.toString(), "1.0"));
			
			granularity = Granularity.valueOf(properties.getProperty(PropertyNames.GRANULARITY.toString(), "MONTHLY").toUpperCase());
			
			//SDE
			
			
			//DTM
			dtmFixedTopics = Integer.parseInt(properties.getProperty(PropertyNames.DTM_FIXED_TOPICS.toString(), "5"));
			dtmInitMult = Double.parseDouble(properties.getProperty(PropertyNames.DTM_INITIAL_VARIANCE_MULTIPLIER.toString(), "1000"));
			dtmMaxIterations = Integer.parseInt(properties.getProperty(PropertyNames.DTM_MAX_ITERATIONS.toString(), "1000"));
			dtmMinIterations = Integer.parseInt(properties.getProperty(PropertyNames.DTM_MIN_ITERATIONS.toString(), "300"));
			dtmObservationNormCutoff = Double.parseDouble(properties.getProperty(PropertyNames.DTM_OBSERVATION_NORM_CUTOFF.toString(), "2"));
			dtmObservationVariance = Double.parseDouble(properties.getProperty(PropertyNames.DTM_OBSERVATION_VARIANCE.toString(), ".2"));
			dtmTopicChainVariance = Double.parseDouble(properties.getProperty(PropertyNames.DTM_TOPIC_CHAIN_VARIANCE.toString(), ".1"));
			
			//GPDTM
			processVariance = Double.parseDouble(properties.getProperty(PropertyNames.PROCESS_VARIANCE.toString(), "1.0"));
			measurementNoise = Double.parseDouble(properties.getProperty(PropertyNames.MEASUREMENT_NOISE.toString(), "0.01"));
			lengthScale = Double.parseDouble(properties.getProperty(PropertyNames.LENGTH_SCALE.toString(), "1.0"));
			dynamics = properties.getProperty(PropertyNames.DYNAMICS.toString(), "RBF");
			covModel = properties.getProperty(PropertyNames.COVARIANCE_MODEL.toString(), "SINGLE");
			weight = Double.parseDouble(properties.getProperty(PropertyNames.WEIGHT.toString(), ".05"));
			//files
			topicTopWordsFile = properties.getProperty(PropertyNames.TOPIC_TOP_WORDS_FILE.toString(), "topicTopWords.txt");
			topicWordProbabilityFile = properties.getProperty(PropertyNames.TOPIC_WORD_PROBABILITY_FILE.toString(), "topicWordWeights.dat");
			documentTopicProbabilityFile = properties.getProperty(PropertyNames.DOCUMENT_TOPIC_PROBABILITY_FILE.toString(), "documentTopicWeights.dat");
			globalTopicProbabilityFile = properties.getProperty(PropertyNames.GLOBAL_TOPIC_PROBABILITY_FILE.toString(), "globalTopicProportions.dat");
			localToGlobalTypeIdFile = properties.getProperty(PropertyNames.LOCAL_TO_GLOBAL_TYPE_ID_FILE.toString(), "localToGlobalTypeIds.dat");
			likelihoodFile = properties.getProperty(PropertyNames.LIKELIHOOD_FILE.toString(), "likelihood.dat");
			testDocumentTopicProbabilityFile = properties.getProperty(PropertyNames.TEST_DOCUMENT_TOPIC_PROBABILITY_FILE.toString(), "inferred_gamma.dat");
			testResultFilePrefix = properties.getProperty(PropertyNames.TEST_RESULT_FILE_PREFIX.toString(), "test_result_");
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	private int[] toIntArray(String input) {
		if (input.trim().equals(""))
			return new int[] {};
		String[] parts = input.split(",");
		int[] ret = new int[parts.length];
		for (int i = 0; i < parts.length; ++i)
			ret[i] = Integer.parseInt(parts[i]);
		return ret;
	}
}
