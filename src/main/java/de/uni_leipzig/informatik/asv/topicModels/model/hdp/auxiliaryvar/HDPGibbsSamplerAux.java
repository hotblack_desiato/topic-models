/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.hdp.auxiliaryvar;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;

import cern.jet.random.Beta;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordToken;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericStaticInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.lda.state.TopicAssignment;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;
import de.uni_leipzig.informatik.asv.topicModels.utils.WordProb;

public class HDPGibbsSamplerAux extends AbstractGenericStaticInferencer {

	private TopicAssignment[] topics;

	private double[] topLevelMixingProportions; // [corpusLevelTopicIndex] = proportion, named beta in Teh's paper
	private double[][] docTopicCount; // [docIndex][corpusLevelTopicIndex] = # of words in doc assigned to topic

	private double[] topicSum; // [topicIndex] = number of assignments to a topic
	private double[][] topicWordCounts; // [topicIndex][wordIndex] = number of assignment of a type to a topic

	private int J;
	private int N;
	private double logAlpha_0;
	private double gamma;
	private double logGamma;
	private double[] beta; // hyperparameter for topics' word distributions
	private double sumBeta;

	private int maxInitialK = 10;

	private int iterations = 1000;
	private int burnIn = 300;

	public HDPGibbsSamplerAux(Corpus c, Corpus test, Corpus validation, InferencerProperties props) throws IOException {
		super(c, test, validation, props);
		logAlpha_0 = MathUtils.safeLog(props.alpha);
		gamma = props.gamma;
		logGamma = Math.log(gamma);
		beta = new double[]{props.beta};

		iterations = props.iterations;

		initialiseVariables();
	}

	private void initialiseVariables() {
		System.out.print("initialising sampler...");
		long start = System.currentTimeMillis();
		K = rng.nextInt(maxInitialK) + 1;
		V = corpus.getNumberOfWordTypes();
		J = corpus.getNumberOfDocuments();
		N = corpus.getNumberOfWordTokens();

		double scalarBeta = beta[0];
		beta = new double[V];
		Arrays.fill(beta, scalarBeta);
		sumBeta = V * scalarBeta;
		
		topics = new TopicAssignment[K];

		topicSum = new double[K];
		topicWordCounts = new double[K][V];
		for (int k = 0; k < K; k++)
			topics[k] = new TopicAssignment(k);
		

		topLevelMixingProportions = new double[K+1];

		// break stick initially
		double stickLength = 1d;
		for (int k = 0; k < K; k++) {
			double b = Beta.staticNextDouble(1, gamma);
			double curPart = b * stickLength;
			topLevelMixingProportions[k] = Math.log(curPart);
			stickLength -= curPart;
		}
		topLevelMixingProportions[K] = Math.log(stickLength);

		docTopicCount = new double[J][K];

		for (Document d : corpus.getDocuments()) {
			for (WordToken wt : d.getWords()) {
				TopicAssignment topic = topics[rng.nextInt(K)];
				wt.setState(topic);
				addToCounts(d, wt);
			}
		}

		doCorpusUpdates();
		System.out.print(" done. (" + (System.currentTimeMillis() - start) + "ms)");
	}

	public void doInference() {
		logger.trace("doing inference...");
		for (int iter = 0; iter < iterations; iter++) {
			long start = System.currentTimeMillis();
			Iterator<Document> docIterator = corpus.getShuffledDocumentIterator();
			while (docIterator.hasNext()) {
				Document curDoc = docIterator.next();
				doDocumentUpdates(curDoc);
			}
			doCorpusUpdates();
			if (iter >= burnIn) {
				// TODO: infer hyper params beta_topic, beta, gamma_0
			}
//			if (iter % 100 == 0) {
				logger.trace("done with " + iter + " iterations (last one took " + (System.currentTimeMillis() - start) + "ms), current K=" + K);
//			}
		}
		// get theta
		WordProb[][] theta = getTheta();
		String tw = printTopWords(theta, 20);
		logger.trace(tw);

	}

	
	@Override
	public void populateFinalValues() {
		finalTopicWordWeights = new double[K][V];
		finalDocTopicWeights = new double[J][K];
		double[] denoms = computeMarginalProbDenominators();
		for (Document doc : corpus.getDocuments()) {
			int d = localDocIds.get(doc.getId());
			finalDocTopicWeights[d] = VectorUtils.normalize(docTopicCount[d]);

			for (WordToken wt : doc.getWords()) {
				int type = wt.getTypeId();
				int topic = wt.<TopicAssignment>getState().getId();
				finalTopicWordWeights[topic][type] += computeF(topic, type, denoms[topic]);
			}
			for(int k=0;k<K;++k) {
				finalTopicWordWeights[k] = VectorUtils.normalize(finalTopicWordWeights[k]);				
			}
		}
	}

	private String printTopWords(WordProb[][] theta, int numTopWords) {
		StringBuilder sb = new StringBuilder();
		for (int k = 0; k < theta.length; k++) {
			Arrays.sort(theta[k]);
			sb.append("Topic ").append(k).append("\n");
			for (int i = 0; i < numTopWords; i++)
				sb.append(theta[k][i].getTerm()).append("\t").append(theta[k][i].getProb()).append("\n");
			sb.append("\n");
		}
		return sb.toString();
	}

	private WordProb[][] getTheta() {
		WordProb[][] theta = new WordProb[K][V];
		double[][] thetaBase = new double[K][V];
		
//		double sumTopLevel = VectorUtils.logSum(topLevelMixingProportions);
//		
//		double[] n_kTopLevel = VectorUtils.addLogDistributions(VectorUtils.toLog(topicSum),topLevelMixingProportions);
//		
//		double[] thetaBaseVals = VectorUtils.subtractScalar(n_kTopLevel, ComputationUtils.logAdd(Math.log(N), sumTopLevel));
//		
//		for (int k = 0; k < K; k++) {
//			Arrays.fill(thetaBase[k], thetaBaseVals[k]);
//		}
		
		double[] denoms = computeMarginalProbDenominators();

		for (Document d : corpus.getDocuments()) {
			for (WordToken wt : d.getWords()) {
				int type = wt.getTypeId();
				int topic = wt.<TopicAssignment>getState().getId();
				thetaBase[topic][type] += computeF(topic, type, denoms[topic]);
			}
		}
		for (int v = 0; v < V; v++) {
			String term = corpus.getWordType(v).getValue();
			for (TopicAssignment t : topics) {
				int k = t.getId();
				double wordProb = thetaBase[k][v] == 0.0 ? 0.0 : Math.exp(thetaBase[k][v]);
				theta[k][v] = new WordProb(term, wordProb, v);
			}
		}
		return theta;
	}

	private void doCorpusUpdates() {
		// sample m
		double[] betaParams = new double[K + 1];
		for (Document d : corpus.getDocuments()) {
			for (TopicAssignment t : topics) {
				int k = t.getId();
				double n_j_k = docTopicCount[d.getId()][k];
				//ignore topics with count zero
				if(n_j_k <= 1) {
					betaParams[k] += n_j_k;
					continue;
				}
				double alphaBeta = logAlpha_0 + topLevelMixingProportions[k];
				try {
					double[] stirlingNumbers = VectorUtils.log(MathUtils.stirlingNumber1stKind((int)n_j_k));
					for (int m = 0; m < stirlingNumbers.length; m++) {
						//when m=0, m is actually the stirlingNumber s(n_j_k,1), 
						//ignoring all values for m=0 as s(n_j_k,0)=0 for n_j_k > 0
						stirlingNumbers[m] += m*alphaBeta; 
					}
					//add one for same reason as above
					int chosenM = MathUtils.drawFromLogDistribution(stirlingNumbers) + 1;
					betaParams[k] += chosenM;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		// sample top level beta
		betaParams[K] = gamma;
		double[] drawnDir = MathUtils.drawFromDirichlet(betaParams);
		topLevelMixingProportions = VectorUtils.log(drawnDir);
//		System.out.println("concentration params: " + Arrays.toString(betaParams));
//		System.out.println("sampled beta: " + Arrays.toString(drawnDir));
	}

	private void doDocumentUpdates(Document curDoc) {
		int docIndex = curDoc.getId();
		double[] f = createFArray();
		double[] p_z = createPZArray(f);
		// sample z
		for (WordToken wt : curDoc.getWords()) {
			removeFromCounts(curDoc, wt);
			double[] denoms = computeMarginalProbDenominators();
			int type = wt.getTypeId();
			for (int k = 0; k < K; k++) {
				
				f[k] = computeF(k, type, denoms[k]);
				p_z[k] = MathUtils.logAdd(logAlpha_0 + topLevelMixingProportions[k], MathUtils.safeLog(docTopicCount[docIndex][k])) + f[k];
			}
			int chosenTopic = MathUtils.drawFromLogDistribution(p_z);
			boolean newTopic = false;
			if (chosenTopic >= topics.length) { // new topic chosen
				//add topic to bookkeeping
				addTopic();
				newTopic = true;
			}
			wt.setState(topics[chosenTopic]);
			addToCounts(curDoc, wt);

			if(newTopic) {
				//expand prob arrays iff new topic has been constructed
				//do this after adding new assignment to the counts 
				//to avoid division by zero in computeMarginalProbDenominators
				denoms = computeMarginalProbDenominators();
				f = createFArray();
				p_z = createPZArray(f);
			}
			
			// prune unused topics
			for (int topic = 0; topic < K; topic++) {
				if (topicSum[topic] == 0)
					removeTopic(topic);
			}
		}
	}

	private double[] createPZArray(double[] f) {
		double[] p_z = new double[K + 1];
		p_z[K] = logAlpha_0 + topLevelMixingProportions[K] + f[K];
		return p_z;
	}

	private double[] createFArray() {
		double[] f = new double[K + 1];
		f[K] = -MathUtils.safeLog((double) V);
		return f;
	}

	private double[] computeMarginalProbDenominators() {
		return VectorUtils.log(VectorUtils.addScalar(topicSum, sumBeta));
	}

	private void addTopic() {
		// add new topic
		TopicAssignment[] newTopics = new TopicAssignment[K+1];
		System.arraycopy(topics, 0, newTopics, 0, K);
		newTopics[topics.length] = new TopicAssignment(topics.length);
		topics = newTopics;
		
		// update counts
		for (int i=0;i<docTopicCount.length;i++) {
			double[] docList = docTopicCount[i];
			double[] newDocList = new double[docList.length+1];
			System.arraycopy(docList, 0, newDocList, 0, docList.length);
			docTopicCount[i] = newDocList;
		}
		
		double[][] newTopicWordCounts = new double[topicWordCounts.length+1][V];
		for(int i=0;i<topicWordCounts.length;i++) {
			newTopicWordCounts[i] = topicWordCounts[i];
		}
		topicWordCounts = newTopicWordCounts;

		double[] newTopicSum = new double[topicSum.length+1];
		System.arraycopy(topicSum, 0, newTopicSum, 0, topicSum.length);
		topicSum = newTopicSum;

		K++;
		
		double[] toplevelNew = new double[topLevelMixingProportions.length+1];
		System.arraycopy(topLevelMixingProportions, 0, toplevelNew, 0, topLevelMixingProportions.length);
		double b = Beta.staticNextDouble(1, gamma);
		toplevelNew[topLevelMixingProportions.length-1] = Math.log(b) + topLevelMixingProportions[topLevelMixingProportions.length-1];
		toplevelNew[topLevelMixingProportions.length] = Math.log(1 - b) + topLevelMixingProportions[topLevelMixingProportions.length-1];
		topLevelMixingProportions = toplevelNew;
//		System.out.println("adding new topic, K = " + K);
	}

	private void removeTopic(int topicToDelete) {
		for(int i=0;i<docTopicCount.length;i++) {
			docTopicCount[i] = VectorUtils.removeElement(docTopicCount[i], topicToDelete);
		}
		topicWordCounts = VectorUtils.deleteRow(topicWordCounts, topicToDelete);
		topicSum = VectorUtils.removeElement(topicSum, topicToDelete);
		topLevelMixingProportions = VectorUtils.removeElement(topLevelMixingProportions, topicToDelete);
		topics = deleteElement(topics, topicToDelete);
		for (int tt = topicToDelete; tt < topics.length; tt++) {
			topics[tt].setId(tt);
		}
		K--;
//		System.out.println("removing topic " + topicToDelete + ", Knew = " + K);
	}

	private double computeF(int topic, int typeId, double denom) {
		double numerator = topicWordCounts[topic][typeId] + beta[typeId];
		return MathUtils.safeLog(numerator) - denom;
	}

	private void removeFromCounts(Document doc, WordToken token) {
		changeCount(doc, token, -1);
	}

	private void addToCounts(Document doc, WordToken token) {
		changeCount(doc, token, 1);
	}

	private void changeCount(Document doc, WordToken token, int plusOrMinus) {
		docTopicCount[doc.getId()][token.<TopicAssignment>getState().getId()] += plusOrMinus;
		topicWordCounts[token.<TopicAssignment>getState().getId()][token.getTypeId()] += plusOrMinus;
		topicSum[token.<TopicAssignment>getState().getId()] += plusOrMinus;
	}

	private TopicAssignment[] deleteElement(TopicAssignment[] a, int el) {
		TopicAssignment[] r = new TopicAssignment[a.length-1];
		int count = 0;
		for(int i=0;i<a.length;i++) {
			if(i==el) continue;
			r[count++] = a[i];
		}
		return r;
	}



	@Override
	public void loadModel(String filename) throws IOException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericStaticInferencer#doPrediction(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document, double[])
	 */
	@Override
	public double doPrediction(Document d, double[] topicPredictions) {
		// TODO Auto-generated method stub
		return 0;
	}
}
