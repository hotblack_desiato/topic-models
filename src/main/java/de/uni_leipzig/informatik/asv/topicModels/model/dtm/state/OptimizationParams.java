/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.dtm.state;

public class OptimizationParams {
    private DTMTopicState topic;
    private double[] word_counts;
    private double[] totals;
    private double[][] mean_deriv_mtx;
    private int word;
	
    
    public DTMTopicState getTopic() {
		return topic;
	}
	public void setTopic(DTMTopicState topic) {
		this.topic = topic;
	}
	public double[] getWord_counts() {
		return word_counts;
	}
	public void setWord_counts(double[] word_counts) {
		this.word_counts = word_counts;
	}
	public double[] getTotals() {
		return totals;
	}
	public void setTotals(double[] totals) {
		this.totals = totals;
	}
	public double[][] getMean_deriv_mtx() {
		return mean_deriv_mtx;
	}
	public void setMean_deriv_mtx(double[][] mean_deriv_mtx) {
		this.mean_deriv_mtx = mean_deriv_mtx;
	}
	public int getWord() {
		return word;
	}
	public void setWord(int word) {
		this.word = word;
	}
    
    

}
