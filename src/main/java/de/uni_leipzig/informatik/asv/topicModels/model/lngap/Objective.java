/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.lngap;

import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils.safeLog;

import java.util.Arrays;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericStaticInferencer;

public class Objective extends ObjectiveFunction {

	private int curWord;
	double mean0;
	double var0;
	
	public Objective() {}
	
	public Objective(int dim, AbstractGenericStaticInferencer p) {
		super(dim, p);
	}


	@Override
	public double f(double[] point) {
		LogNormalGammaPoissonModel m = getParameters();
		
		double f = 0;
		for(int k=0;k<m.getK();k++) {
			f -= .5*(safeLog(var0) + (Math.pow(point[k] - mean0, 2) + m.Sigma[k][curWord])/var0);
			f -= Math.exp(point[k] + .5*m.Sigma[k][curWord])*m.statsK[k];
			f += point[k]*m.statsKW[k][curWord];
		}
		return -f;
	}

	@Override
	public void df(double[] point, double[] df) {
		LogNormalGammaPoissonModel m = getParameters();
		
		for(int k=0;k<m.getK();k++) {
			double expEBeta = Math.exp(point[k] + .5*m.Sigma[k][curWord]);
			df[k] = -1./(var0) * (point[k] - mean0) + m.statsKW[k][curWord] - expEBeta*m.statsK[k];
		}
		VectorUtils.neg_(df);
	}

	
	@Override
	public double fdf(double[] point, double[] df) {
		LogNormalGammaPoissonModel m = getParameters();

		double f = 0;
		for(int k=0;k<m.getK();k++) {
			double expEBeta = Math.exp(point[k] + .5*m.Sigma[k][curWord]);

			f -= .5*(safeLog(var0) + (Math.pow(point[k] - mean0, 2) + m.Sigma[k][curWord])/var0);
			f -= expEBeta*m.statsK[k];
			f += point[k]*m.statsKW[k][curWord];

			df[k] = -1./(var0) * (point[k] - mean0) + m.statsKW[k][curWord] - expEBeta*m.statsK[k];
		}
		VectorUtils.neg_(df);
		return -f;
	}
	
	public void df2(double[] point, double[] df2) {
		LogNormalGammaPoissonModel m = getParameters();
		
		Arrays.fill(df2, 0);
		for(int k=0;k<m.getK();k++) {
			double expEBeta = Math.exp(point[k] + .5*m.Sigma[k][curWord]);
			df2[k] = -expEBeta*m.statsK[k] - 1./var0;
		}
		
	}

	/**
	 * @param curWord the curWord to set
	 */
	public void setCurWord(int curWord) {
		this.curWord = curWord;
	}

	/**
	 * @param mean0 the mean0 to set
	 */
	public void setMean0(double mean0) {
		this.mean0 = mean0;
	}

	/**
	 * @param var0 the var0 to set
	 */
	public void setVar0(double var0) {
		this.var0 = var0;
	}

}
