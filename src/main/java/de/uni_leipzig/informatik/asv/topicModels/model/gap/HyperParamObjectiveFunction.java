/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.gap;

import java.util.Arrays;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;

public class HyperParamObjectiveFunction extends ObjectiveFunction {

	protected HyperParamObjectiveFunction(int dim, OptimizationParams p) {
		super(dim, p);
	}

	@Override
	public double f(double[] point) {
		OptimizationParams p = getParameters();
		double f = p.getLikelihoodConstantPart();
		double a = point[0], b = point[1], c = point[2], d = point[3];
		if(a < 0)
			a = -a;
		if(b<0)
			b = -b;
		if(c<0)
			c = -c;
		if(d<0)
			d = -d;
		double aLogB = a*Math.log(b);
		double loggammaA = MathUtils.loggamma(a);
		double cLogD = c*Math.log(d);
		double loggammaC = MathUtils.loggamma(c);

		for(int k=0;k<p.getK();++k) {
			for(int doc=0;doc<p.getM();++doc) {
				//a and b
				f += aLogB + (a-1)*p.geteLogTheta()[doc][k] - b*p.geteTheta()[doc][k] - loggammaA;
				if(Double.isNaN(f))
					System.out.println("NaN here");
			}
			for(int w=0;w<p.getV();++w) {
				//c and d
				f += cLogD + (c-1)*p.geteLogBeta()[k][w] - d*p.geteBeta()[k][w] - loggammaC;
				if(Double.isNaN(f))
					System.out.println("NaN here");
			}
		}
		return -f;
	}
	
	
	@Override
	public void df(double[] point, double[] df) {
		OptimizationParams p = getParameters();
		Arrays.fill(df, 0);
		
		double a = point[0], b = point[1], c = point[2], d = point[3];
		if(a < 0)
			a = -a;
		if(b<0)
			b = -b;
		if(c<0)
			c = -c;
		if(d<0)
			d = -d;
		
		double logB = Math.log(b);
		double digammaA = MathUtils.digamma(a);
		double aOverB = a/b;
		
		double logD = Math.log(d);
		double digammaC = MathUtils.digamma(c);
		double cOverD = c/d;
		
		
		for(int k=0;k<p.getK();++k) {
			for(int doc=0;doc<p.getM();++doc) {
				//a and b
				df[0] += logB + p.geteLogTheta()[doc][k] - digammaA;
				df[1] += aOverB - p.geteTheta()[doc][k];
			}
			for(int w=0;w<p.getV();++w) {
				//c and d
				df[2] += logD + p.geteLogBeta()[k][w] - digammaC;
				df[3] += cOverD - p.geteBeta()[k][w];
			}
		}
		VectorUtils.neg_(df);
	}


	@Override
	public double fdf(double[] point, double[] df) {
		OptimizationParams p = getParameters();
		double f = p.getLikelihoodConstantPart();
		Arrays.fill(df, 0);
		
		double a = point[0], b = point[1], c = point[2], d = point[3];
		
		if(a < 0)
			a = -a;
		if(b<0)
			b = -b;
		if(c<0)
			c = -c;
		if(d<0)
			d = -d;

		double logB = Math.log(b);
		double aLogB = a*logB;
		double loggammaA = MathUtils.loggamma(a);
		double digammaA = MathUtils.digamma(a);
		double aOverB = a/b;
		
		double logD = Math.log(d);
		double cLogD = c*logD;
		double loggammaC = MathUtils.loggamma(c);
		double digammaC = MathUtils.digamma(c);
		double cOverD = c/d;
		
		
		for(int k=0;k<p.getK();++k) {
			for(int doc=0;doc<p.getM();++doc) {
				//a and b
				f += aLogB + (a-1)*p.geteLogTheta()[doc][k] - b*p.geteTheta()[doc][k] - loggammaA;
				df[0] += logB + p.geteLogTheta()[doc][k] - digammaA;
				df[1] += aOverB - p.geteTheta()[doc][k];
			}
			for(int w=0;w<p.getV();++w) {
				//c and d
				f += cLogD + (c-1)*p.geteLogBeta()[k][w] - d*p.geteBeta()[k][w] - loggammaC;
				df[2] += logD + p.geteLogBeta()[k][w] - digammaC;
				df[3] += cOverD - p.geteBeta()[k][w];
			}
		}
		VectorUtils.neg_(df);
		
		return -f;
	}

}
