/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.sde;

import static de.uni_leipzig.informatik.asv.topicModels.model.sde.Diffusion.getDiffusionDerivativeA;
import static de.uni_leipzig.informatik.asv.topicModels.model.sde.Diffusion.getDiffusionDerivativeMean;
import static de.uni_leipzig.informatik.asv.topicModels.model.sde.Diffusion.getDiffusionDerivativeVariance;
import static de.uni_leipzig.informatik.asv.topicModels.model.sde.Diffusion.getDiffusionDerivativeb;
import static de.uni_leipzig.informatik.asv.topicModels.model.sde.Diffusion.getDiffusionEnergy;
import static java.lang.Math.exp;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.*;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils.*;
import static de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils.*;

import java.io.IOException;
import java.util.Date;
import java.util.Set;

import org.apache.commons.math3.special.Gamma;

import cern.colt.Arrays;
import cern.colt.map.OpenIntIntHashMap;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicCorpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.WordTokenState;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.Type;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericDynamicInferencer;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;


public class SDEInfinitesimalDynamicTopicModel extends AbstractGenericDynamicInferencer {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//some constants
	private int T, K, W;

	//SDE type
	Diffusion.Type diffusionType = Diffusion.Type.ORNSTEIN_UHLENBECK;
	
	// define the model specific parameters
	private double initialMean = 0;
	private double initialVariance = 5;
	//the system variance
	private double[][] sigma;
	//parameters for the diffusion process
	private double[][][] params;
	// functions in variational distribution over topic probabilities in time, K x W x T
	private double[][][] a;
	private double[][][] b;
	//variational mean and variance for distribution over topic probabilities in time, K x W x T
	private double[][][] m;
	private double[][][] s;
	//lagrange multipliers for optimization in m-step
	private double[][][] lambda;
	private double[][][] psi;
	// variational parameters zeta for approximation of intractable expectation, K x T
	private double zeta[][];
	//sufficient statistics
	private double[][][] ssKWT;
	private double[][] ssKT;
	//structures for sparse word occurrences
	private int[][] sparseTimes;
	//timestamps
	private Date[] dates;
	private double[] times;
	private double dt;
	//the learning rate in gradient descent
	private double lr;


	
	
	public SDEInfinitesimalDynamicTopicModel(DiachronicCorpus c, InferencerProperties props) throws IOException {
		super(c, null, null, props);
		allocate();
		zeroStats();
	}

	private void zeroStats() {
		ssKWT = null;
		ssKT = null;
		
		ssKWT = doubleArray(Type.ZERO, K, W, T);
		ssKT = doubleArray(Type.ZERO, K, T);
	}

	private void allocate() {
		logger.info("allocating arrays");
		//cache a few numbers
		K = props.numTopics;
		T = corpus.getNumberOfDates();
		W = corpus.getNumberOfWordTypes();


		//times (in ms) -> days
		dates = corpus.getUsedDates().toArray(new Date[T]);
		times = doubleArray(Type.ZERO, T);
		int dateCount = 0;
		for(Date d : corpus.getUsedDates()) {
			times[dateCount++] = d.getTime() / (1000*60*60*24);
		}
		//scale times to [0,1]
		double timeSpan = times[T-1] - times[0];
		times = divideByScalar(times, timeSpan);
		
		//find the "infinitesimal" time step
//		dt = (times[T-1] - times[0])/T;
		dt = .01;
		logger.info("infinitesimal time step is " + dt);
		//build up the sparse structures for word occurrences
		sparseTimes = new int[W][];
		
		for(int t=0;t<T;t++) {
			OpenIntIntHashMap timePointWordFreqs = corpus.getDocumentIdSubsetWordFrequencies(corpus.getDocumentsForDate(dates[t]));
			for(int wIdx : timePointWordFreqs.keys().elements()) {
				//get the local wordId \in {0,...,W}
				int w = localWordTypeIds.get(wIdx);
				if(sparseTimes[w] == null || sparseTimes[w].length == 0)
					sparseTimes[w] = new int[]{t};
				else {
					sparseTimes[w] = addElement(sparseTimes[w]);
					sparseTimes[w][sparseTimes[w].length-1] = t;
				}
					
			}
		}
		double aveNumTimes = 0;
		for(int w=0;w<W;w++) {
			aveNumTimes += sparseTimes[w].length;
		}
		aveNumTimes /= W;
		
		logger.info("average number of words over time line is " + aveNumTimes);
		logger.info("sparsity is " + (1-(aveNumTimes/T)));

		
		//allocate the parameter arrays
		switch(diffusionType) {
		case BROWNIAN:
			params = new double[K][W][];
			break;
		case ORNSTEIN_UHLENBECK:
			params = new double[K][W][];
			for(int k=0;k<K;k++) {
				for(int w=0;w<W;w++) {
					params[k][w] = doubleArray(Type.ONE, 1);
					params[k][w] = multiplyWithScalar(params[k][w], 1.);
				}
			}
		}
		
		sigma = doubleArray(Type.ONE, K, W);
		sigma = VectorUtils.multiplyWithScalar(sigma, .5);
		
		a = doubleArray(Type.NORMAL, K, W, T);
		b = doubleArray(Type.NORMAL, K, W, T);
		
		m = doubleArray(Type.UNIFORM, K, W, T);
//		for(int k=0;k<K;k++)
//			m[k] = VectorUtils.addScalar(m[k], .5);
		
		s = doubleArray(Type.UNIFORM, K, W, T);
		
		lambda = doubleArray(Type.ZERO, K, W, T);
		psi = doubleArray(Type.ZERO, K, W, T);
		
		ssKWT = doubleArray(Type.ZERO, K, W, T);
		ssKT = doubleArray(Type.ZERO, K, T);
		
		lr = .001;
		
		updateZeta();
	}

	private void updateZeta() {
		zeta = doubleArray(Type.ZERO, K, T);
		for(int k=0;k<K;k++) {
			for(int t=0;t<T;t++) {
				for(int w=0;w<W;w++) {
					if(w==0)
						zeta[k][t] = m[k][w][t] + .5 * s[k][w][t]; 
					else
						zeta[k][t] = logAdd(zeta[k][t], m[k][w][t] + .5*s[k][w][t]);
				}
			}
		}
		
	}


	@Override
	public void doInference() throws IOException {
		double convergence = 1.;
		double likelihood, oldLikelihood = -1e300, mStep;
		int iter = 0;
		while(iter < props.iterations) {
			iter++;
			zeroStats();
			likelihood = eStep();
			logger.info("eStep likelihood: " + likelihood);
			mStep();
			mStep = computeBetaLikelihood();
			likelihood += mStep;
			logger.info("mStep likelihood: " + mStep);
			convergence = (oldLikelihood - likelihood)/oldLikelihood;
			logger.info("likelihood: " + likelihood + ", convergence: " + convergence);
			oldLikelihood = likelihood;
			if((Double.valueOf(likelihood).isNaN() || convergence < props.convergenceCriterion) && iter > 3)
				break;
		}
		
	}

	private double computeBetaLikelihood() {
		//the upper bound on the marginal likelihood is the negative free energy
		double likelihood = 0;
		for(int k=0;k<K;k++) {
			for(int w=0;w<W;w++) {
				for(int t=0;t<T;t++) {
					likelihood -= getDiffusionEnergy(diffusionType, m[k][w][t], s[k][w][t], a[k][w][t], b[k][w][t], sigma[k][w], params[k][w]);
//					if(Double.valueOf(likelihood).isNaN())
//						logger.warn("likelihood is nan");
//					likelihood -= getObservationEnergy(k, w, t);
				}
			}
		}
		return likelihood;
	}

	private double eStep() {
		double likelihood = 0.;
		double[][] pi = doubleArray(Type.ZERO, K, W);
		//for all times
		for(int t=0;t<T;t++) {
			//compute the logistic projection at time t
			double piSum;
			for(int k=0;k<K;k++) {
				piSum = 0;
				for(int w=0;w<W;w++) {
					pi[k][w] = m[k][w][t] - zeta[k][t];
					piSum += exp(pi[k][w]);
				}
//				logger.info("pisum for topic " + k + " is " + piSum);
			}
			
			for(Integer docId : corpus.getDocumentsForDate(dates[t])) {
				likelihood += doDocumentEStep(docId, pi, t);
			}
			logger.info("done with time " + t + ", current likelihood: " + likelihood);
		}
		return likelihood;
	}

	private double doDocumentEStep(Integer docId, double[][] pi, int time) {
		double docLikelihood = 0.;

		Document doc = (Document) corpus.getDocumentById(docId);
		int[] wordIds = doc.getTypeList();
		int[] freqs = doc.getFrequencyList();
		double[][] phi = doubleArray(Type.ZERO, wordIds.length, K);
		double[] docGamma = doubleArray(Type.ONE, K);
		//prepare document latent vars
		//posterior dirichlet
		for(int k=0;k<K;k++) {
			docGamma[k] = props.alpha + ((double)doc.getLength() / (double)K);
			for(int n=0;n<wordIds.length;n++) {
				phi[n][k] = 1./K;
			}
		}

		
		doDocumentInference(phi, docGamma, pi, wordIds, freqs);
		docLikelihood = computeDocumentLikelihood(phi, docGamma, pi, wordIds, freqs);
		
		int w, freq;
		for(int n=0;n<wordIds.length;n++) {
			w = localWordTypeIds.get(wordIds[n]);
			freq = freqs[n];
			for(int k=0;k<K;k++) {
				ssKWT[k][w][time] += freq * phi[n][k];
				ssKT[k][time] += freq * phi[n][k];
			}
		}
		
		return docLikelihood;
	}

	private void doDocumentInference(double[][] phi, double[] docGamma,
			double[][] pi, int[] wordIds, int[] freqs) {
		double converged = 1, phiSum = 0, likelihoodOld = Double.NEGATIVE_INFINITY;
		double[] oldPhi = new double[K];
		double[] digammaGam = new double[K];
		//compute digammas once for all words
		for(int k=0;k<K;k++)
			digammaGam[k] = digamma(docGamma[k]);
		
		double varIter = 0;
		int freq;
		double likelihood = 0;

		while((converged > 1e-3) && ((varIter < 50) || props.iterations == -1)) {
			varIter++;
			for(int n=0;n<wordIds.length;n++) {
				freq = freqs[n];
				phiSum = 0;
				for(int k=0;k<K;k++) {
					oldPhi[k] = phi[n][k];
					phi[n][k] = digammaGam[k] + pi[k][localWordTypeIds.get(wordIds[n])];
					if(k>0)
						phiSum = logAdd(phiSum, phi[n][k]);
					else
						phiSum = phi[n][k];
				}
				for(int k=0;k<K;k++) {
					phi[n][k] = exp(phi[n][k] - phiSum) + 1e-100;
					if(phi[n][k] == 0.0)
						logger.warn("phi is 0.0");
					docGamma[k] = docGamma[k] + freq * (phi[n][k] - oldPhi[k]);
					if(docGamma[k] < 0) {
						logger.warn("gamma is negative in topic " + k);
					}
//					docGamma[k] += freq * phi[n][k];
					digammaGam[k] = digamma(docGamma[k]);
				}
			}
			likelihood = computeDocumentLikelihood(phi, docGamma, pi, wordIds, freqs);
			if(varIter > 2)
				converged = (likelihoodOld - likelihood) / likelihoodOld;
			likelihoodOld = likelihood;
		}
		assert(!Double.valueOf(likelihood).isNaN());
	}

	private double computeDocumentLikelihood(double[][] phi, double[] docGamma,
			double[][] pi, int[] wordIds, int[] freqs) {
		double likelihood = 0d;
		double digsum = 0, gammaSum = 0;
		double[] dig = new double[K];
		
		for(int k=0;k<K;k++) {
			dig[k] = digamma(docGamma[k]);
			gammaSum += docGamma[k];
		}
		digsum = digamma(gammaSum);
		
		likelihood = Gamma.logGamma(props.alpha * K) - (K * Gamma.logGamma(props.alpha));
		
		likelihood -= Gamma.logGamma(gammaSum);
		if(Double.valueOf(likelihood).isNaN()) {
			logger.info("gamma = " + Arrays.toString(docGamma));
			logger.info("gammaSum = " + gammaSum);
			logger.info("digsum = " + digsum);
		}
		assert(!Double.valueOf(likelihood).isNaN());

		for(int k=0;k<K;k++) {
			likelihood += (props.alpha - docGamma[k]) * (dig[k] - digsum) + Gamma.logGamma(docGamma[k]);
			if(Double.valueOf(likelihood).isNaN()) {
				logger.warn("likelihood is nan, current values are:");
				logger.warn("dig[k] = " + dig[k]);
				logger.warn("digsum = " + digsum);
				logger.warn("docGamma[k] = " + docGamma[k]);
				logger.warn("loggamma(gamma[k]) = " + Gamma.logGamma(docGamma[k]));
				logger.warn("phi[k] = " + Arrays.toString(VectorUtils.transpose(phi)[k]));
			}
			assert(!Double.valueOf(likelihood).isNaN());
		}
		
		int freq;

		assert(!Double.valueOf(likelihood).isNaN());
		
		for(int n=0;n<wordIds.length;n++) {
			freq = freqs[n];
			for(int k=0;k<K;k++) {
				if(phi[n][k] > 0)
					likelihood += freq * (phi[n][k] * ((dig[k] - digsum) - MathUtils.safeLog(phi[n][k]) + pi[k][localWordTypeIds.get(wordIds[n])]));
			}
		}

		assert(!Double.valueOf(likelihood).isNaN());
			
		return likelihood;
	}

	private void mStep() {
		//following the algorithm as given in "Variational Inference for Diffusion Processes", Archambeau et.al.
		double derivM, derivS, derivA, derivB, Esde, Eobs, dLambdaDt, dPsiDt;;
		//initialise a, b 
		//TODO: done once or in every m-step?
//		a = doubleArray(Type.NORMAL, K, W, T);
//		b = doubleArray(Type.NORMAL, K, W, T);
		
		int t, st, allIter = 0;
		// foreach word in each topic
		for(int k=0;k<K;k++) {
			for(int w=0;w<W;w++) {
				
				double objective = 1e100, oldObjective = Double.POSITIVE_INFINITY, converge = 1;
				int iter = 0;
				//optimization loop
				while(Math.abs(converge) > 1e-3 && iter < props.iterations && objective < oldObjective) {
					iter++;
					//update mean and variance
//					m[k][w][0] -= .0001*(initialMean - lambda[k][w][0]*initialVariance);
//					s[k][w][0] -= .0001/(2*psi[k][w][0] + 1./initialVariance);
					for(t=0;t<T-1;t++) {
						zeta[k][t+1] = logSubtract(zeta[k][t+1], m[k][w][t+1] + .5*s[k][w][t+1]);
						double meanChange = (a[k][w][t] * m[k][w][t] - b[k][w][t])*dt;
						m[k][w][t+1] = m[k][w][t] - meanChange;
						double varChange = (2*a[k][w][t]*s[k][w][t] - sigma[k][w])*dt;
						s[k][w][t+1] = s[k][w][t] - varChange; 
						zeta[k][t+1] = logAdd(zeta[k][t+1], m[k][w][t+1] + .5*s[k][w][t+1]);
//						if(k==0&&w==0)
//							logger.info("t = " + t + ", meanchange = " + meanChange + ", varchange = " + varChange);
					}
					//update the lagrange multipliers
					st = sparseTimes[w].length-1;
					if(sparseTimes[w][st] == T-1)
						st--;
					for(t=T-1;t>0;t--) {
						derivM = getDiffusionDerivativeMean(diffusionType, m[k][w][t], s[k][w][t], a[k][w][t], b[k][w][t], sigma[k][w], params[k][w]);
						derivS = getDiffusionDerivativeVariance(diffusionType, m[k][w][t], s[k][w][t], a[k][w][t], b[k][w][t], sigma[k][w], params[k][w]);
						lambda[k][w][t-1] = lambda[k][w][t] + (derivM - a[k][w][t]*lambda[k][w][t])*dt;
						psi[k][w][t-1] = psi[k][w][t] + (derivS - 2*psi[k][w][t]*a[k][w][t])*dt;
						//if observation present (i.e. we have seen word w at time t-1)
						if(st >= 0 && sparseTimes[w][st] == t-1) {
							lambda[k][w][t-1] = lambda[k][w][t-1] + getObservationDerivativeMean(k,w,t-1);
							psi[k][w][t-1] = psi[k][w][t] + getObservationDerivativeVariance(k,w,t-1);
							st--;
						}
					}
					
					
					//update the variational functions
					for(t=0;t<T;t++) {
						derivA = getDiffusionDerivativeA(diffusionType, m[k][w][t], s[k][w][t], a[k][w][t], b[k][w][t], sigma[k][w], params[k][w]);
						a[k][w][t] = a[k][w][t] - lr*(derivA - lambda[k][w][t]*m[k][w][t] - 2*psi[k][w][t]*s[k][w][t]);
						
						derivB = getDiffusionDerivativeb(diffusionType, m[k][w][t], s[k][w][t], a[k][w][t], b[k][w][t], sigma[k][w], params[k][w]);
						b[k][w][t] = b[k][w][t] - lr*(derivB + lambda[k][w][t]);
						if(Math.abs(a[k][w][t]) > 1000 || Math.abs(b[k][w][t]) > 1000) {
							double dummy = 1;
//							logger.warn("susceptible values for a or b");
						}
					}
					
					//TODO take gradient steps for parameters and noise
					
					//compute the new objective value, Eq. (16)
					oldObjective = objective;
					objective = 0;
					st = 0;
					for(t=0;t<T;t++) {
						derivM = getDiffusionDerivativeMean(diffusionType, m[k][w][t], s[k][w][t], a[k][w][t], b[k][w][t], sigma[k][w], params[k][w]);
						derivS = getDiffusionDerivativeVariance(diffusionType, m[k][w][t], s[k][w][t], a[k][w][t], b[k][w][t], sigma[k][w], params[k][w]);

						Esde = getDiffusionEnergy(diffusionType, m[k][w][t], s[k][w][t], a[k][w][t], b[k][w][t], sigma[k][w], params[k][t]);
						if(st < sparseTimes[w].length && sparseTimes[w][st] == t) {
							Eobs = getObservationEnergy(k, w, t);
							st++;
						} else
							Eobs = 0;

						dLambdaDt = -derivM + a[k][w][t]*lambda[k][w][t];
						dPsiDt = -derivS + 2*psi[k][w][t]*a[k][w][t];
						
						objective += (Esde + Eobs)*dt; // accounts for F_Sigma(q, theta), TODO add KL(q_0||p_0)
						if(objective > 1000) {
							double dummy = 1.;
//							logger.warn("high objective value, unlikely...");
						}
						objective -= (lambda[k][w][t]*(a[k][w][t]*m[k][w][t] - b[k][w][t]) - dLambdaDt*m[k][w][t])*dt;
						objective -= (psi[k][w][t]*(2*a[k][w][t]*s[k][w][t] - sigma[k][w]) - dPsiDt*s[k][w][t])*dt;
						//TODO add m_0 and s_0
					}
//					if(k==0&&w==0)
//						logger.info("objective: " + objective);
					converge = (oldObjective - objective) / oldObjective;
				}
				allIter += iter;
			}
		}
		logger.info("average optimization steps: " + allIter/(K*W));
	}
	
	private double getObservationEnergy(int topic, int word, int time) {
		double ret = 0;
		ret = -ssKWT[topic][word][time]*m[topic][word][time] + ssKWT[topic][word][time]*zeta[topic][time];
		return ret;
	}
	
	private double getObservationDerivativeMean(int topic, int word, int time) {
		double ret = 0;
		ret = -ssKWT[topic][word][time] + (ssKWT[topic][word][time]*(exp(m[topic][word][time] + .5*s[topic][word][time] - zeta[topic][time]) + 1e-100) ); // / exp(zeta[topic][time]);
		return ret;
	}

	private double getObservationDerivativeVariance(int topic, int word, int time) {
		double ret = (ssKWT[topic][word][time]*.5*(exp(m[topic][word][time] + .5*s[topic][word][time] - zeta[topic][time]) +1e-100 ));// / exp(zeta[topic][time]);
		return ret;
	}


	@Override
	public void loadModel(String filename) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void populateFinalValues() {
		updateZeta();
		finalTopicWordWeights = doubleArray(Type.ZERO, T, K, W);
		for(int t=0;t<T;t++) {
			for(int k=0;k<K;k++) {
				for(int w=0;w<W;w++) {
					finalTopicWordWeights[t][k][w] = exp(m[k][w][t] + .5*s[k][w][t] - zeta[k][t]) + 1e-100;
				}
			}
		}
		
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericDynamicInferencer#doPrediction(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicDocument, double[], int)
	 */
	@Override
	public double doPrediction(int[] types, int[] freqs, double[] topicPredictions, int time) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericDynamicInferencer#doValidation(int[], int[], int[], int)
	 */
	@Override
	public double doValidation(int[] types, int[] freqsLearn,
			int[] freqsPredict, int time) {
		// TODO Auto-generated method stub
		return 0;
	}

}
