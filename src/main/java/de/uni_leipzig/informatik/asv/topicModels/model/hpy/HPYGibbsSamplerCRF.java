/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/*
 * Copyright 2011 Arnim Bleier, Andreas Niekler and Patrick Jaehnichen
 * Licensed under the GNU Lesser General Public License.
 * http://www.gnu.org/licenses/lgpl.html
 */

package de.uni_leipzig.informatik.asv.topicModels.model.hpy;

import java.io.IOException;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordToken;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.utils.ArrayUtils;
import de.uni_leipzig.informatik.asv.topicModels.model.hdp.crf.HDPGibbsSamplerCRF;
import de.uni_leipzig.informatik.asv.topicModels.model.hdp.crf.state.DOCState;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;

/**
 * Hierarchical Pitman-Yor Processes  
 * Chinese Restaurant Franchise Sampler
 * 
 * For more information on the algorithm see:
 * Hierarchical Bayesian Nonparametric Models with Applications. 
 * Y.W. Teh and M.I. Jordan. Bayesian Nonparametrics, 2010. Cambridge University Press.
 * http://www.gatsby.ucl.ac.uk/~ywteh/research/npbayes/TehJor2010a.pdf
 * 
 * For other known implementations see README.txt
 * 
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jähnichen</a>
 */
public class HPYGibbsSamplerCRF extends HDPGibbsSamplerCRF { 


	/**
	 * 
	 */
	private static final long serialVersionUID = 2099429999994408756L;


	public HPYGibbsSamplerCRF(Corpus c, Corpus test, Corpus validation, InferencerProperties props) throws IOException {
		super(c, test, validation, props);
	}
	


	/**
	 * Decide at which topic the table should be assigned to
	 * 
	 * @return the index of the topic
	 */
	@Override
	protected int sampleTopic() {
		double u, pSum = 0.0;
		int k;
		p = ArrayUtils.ensureCapacity(p, K);
		double denom = gamma + totalNumberOfTables;
		for (k = 0; k < K; k++) {
			pSum += ((numberOfTablesByTopic[k] - props.eta) * f[k])/denom;
			p[k] = pSum;
		}
		pSum += (gamma + K*props.eta) / (sizeOfVocabulary*denom);
		p[K] = pSum;
		u = rng.nextDouble() * pSum;
		for (k = 0; k <= K; k++)
			if (u < p[k])
				break;
		return k;
	}
	

	/**	 
	 * Decide at which table the word should be assigned to
	 * 
	 * @param docID the index of the document of the current word
	 * @param i the index of the current word
	 * @return the index of the table
	 */
	@Override
	protected int sampleTable(Document doc, WordToken wt) {	
		int k, j;
		DOCState docState = doc.getState();
		double pSum = 0.0, vb = sizeOfVocabulary * props.beta, fNew, u;
		f = ArrayUtils.ensureCapacity(f, K);
		p = ArrayUtils.ensureCapacity(p, docState.getNumberOfTables());
		double denom = gamma + totalNumberOfTables;
		fNew = (gamma + K*props.eta) / (sizeOfVocabulary*denom);
		for (k = 0; k < K; k++) {
			f[k] = (wordCountByTopicAndTerm[k][localWordTypeIds.get(wt.getTypeId())] + props.beta) / 
					(wordCountByTopic[k] + vb);
			fNew += (numberOfTablesByTopic[k] - props.eta) * f[k]/denom;
		}
		denom = (doc.getLength() + alpha);
		for (j = 0; j < docState.getNumberOfTables(); j++) {
			if (docState.getWordCountByTable()[j] > 0) 
				pSum += (docState.getWordCountByTable()[j] - props.d) * f[docState.getTableToTopic()[j]]/denom;
			p[j] = pSum;
		}
		pSum += (alpha + docState.getNumberOfTables()*props.d)* fNew / denom; // Probability for t = tNew
		p[docState.getNumberOfTables()] = pSum;
		u = rng.nextDouble() * pSum;
		for (j = 0; j <= docState.getNumberOfTables(); j++)
			if (u < p[j]) 
				break;	// decided which table the word i is assigned to
		return j;
	}


		
}
