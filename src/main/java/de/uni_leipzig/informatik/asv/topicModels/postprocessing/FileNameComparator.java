/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.postprocessing;

import java.io.File;
import java.util.Comparator;

public class FileNameComparator implements Comparator<File> {

	@Override
	public int compare(File f1, File f2) {
		//look for day
		int day = Integer.parseInt(f1.getName());
		//look for month
		int month = Integer.parseInt(f1.getParentFile().getName());
		//look for the year
		int year = Integer.parseInt(f1.getParentFile().getParentFile().getName());

		//look for day
		int day2 = Integer.parseInt(f2.getName());
		//look for month
		int month2 = Integer.parseInt(f2.getParentFile().getName());
		//look for the year
		int year2 = Integer.parseInt(f2.getParentFile().getParentFile().getName());
		
		if(year < year2)
			return -1;
		else if(year > year2)
			return 1;
		else {
			if(month < month2)
				return -1;
			else if(month > month2)
				return 1;
			else {
				if(day < day2)
					return -1;
				else if(day > day2)
					return 1;
				else 
					return 0;
			}
		}
	}

}
