/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.lda.timeslice;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.DiachronicCorpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.topicModels.model.lda.LDAOnlineVBInferencer;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;

public class LDAOnlineVBTimeslice extends LDAOnlineVBInferencer{
	private DiachronicCorpus localCorpus;
	public LDAOnlineVBTimeslice(Corpus c, Corpus test, Corpus validation, InferencerProperties props) throws IOException {
		super(c, test, validation, props);
		this.localCorpus = (DiachronicCorpus) c;
	}

	@Override
	public void doInference() throws IOException {
		logger.trace("starting online inference");
		logger.trace("using window size: " + batchSize);
		
		//get daily documents
		Set<Set<Integer>> dayDocSet = new LinkedHashSet<>();
		if(props.year.length == 0) {
			// TODO write getter for all documents on daily basis in DiachronicCorpus
			System.err.println("define at least a year (by now)");
		} else {
			Arrays.sort(props.year);
			if(props.month.length == 0) {
				for(int year : props.year)
					dayDocSet.addAll(localCorpus.getDailyDocumentsByYear(year));
			} else {
				Arrays.sort(props.month);
				if(props.day.length == 0)
					for(int year : props.year) {
						for(int month : props.month)
							dayDocSet.addAll(localCorpus.getDailyDocumentsByMonthAndYear(month, year));
					}
				else {
					Arrays.sort(props.day);
					for(int year : props.year) {
						for(int month : props.month) {
							for(int day : props.day) {
								dayDocSet.add(localCorpus.getDocumentsByDayAndMonthAndYear(day, month, year));
							}
						}
					}
					
				}
			}
		}
		
		
		HashSet<Integer> curSlice = new HashSet<>();
		int processedDocs = 0;
		int wordCount = 0;
		int runs = 0;
		int dayCount = 0;
		for(Set<Integer> dayDocIds : dayDocSet) {
			Iterator<Integer> docIter = dayDocIds.iterator();
			while(docIter.hasNext()) {
				Document d = corpus.getDocumentById(docIter.next());
				curSlice.add(d.getId());
				wordCount += VectorUtils.sum(d.getWordFrequencies().values().elements());
				processedDocs++;
				if(processedDocs%batchSize==0 || !docIter.hasNext()) {
					runs++;
					long start = System.currentTimeMillis();
					runBatch(curSlice);
					long dur = System.currentTimeMillis() - start;
					logger.trace(runs + ".: bound after processing " + processedDocs + ": " + curBound + ", took " + (dur/1000) + "s");
					double perWordBound = curBound*batchSize / ((double)M*(double)wordCount);
					logger.trace(runs + ".: rho_t = " + rhoT + ", held-out perplexity estimate: " + Math.exp(-perWordBound));
					//logger.trace("");
					curSlice.clear();
					wordCount = 0;
				}
			}
			logger.trace("done with day " + dayCount);
			doPrediction(null);
			dayCount++;
		}
		logger.trace("done with online inference");
	}

	
	
}
