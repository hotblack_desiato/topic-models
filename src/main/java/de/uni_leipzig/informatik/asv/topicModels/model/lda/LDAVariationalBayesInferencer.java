/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.lda;

import java.io.IOException;
import java.util.Set;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.jet.math.Functions;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.DoubleFunctionFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MatrixUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericStaticInferencer;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;

public class LDAVariationalBayesInferencer extends AbstractGenericStaticInferencer {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DoubleMatrix2D gamma, lambda, ELogTheta, ELogBeta, expELogTheta, expELogBeta, suffStats;
	private double alpha, eta, curBound;
	
	private int W;
	public LDAVariationalBayesInferencer(Corpus c, Corpus test, Corpus validation, InferencerProperties props) throws IOException {
		super(c, test, validation, props);
		K = props.numTopics;
		M = corpus.getNumberOfDocuments();
		W = corpus.getNumberOfWordTypes();

		
		initialize();
	}
	private void initialize() {
		this.K = props.numTopics;
		this.M = corpus.getNumberOfDocuments();
		this.W = corpus.getNumberOfWordTypes();
		this.alpha = props.alpha;
		this.eta = props.eta;
		
		//init var distribution q(beta|lambda)
		lambda = new DenseDoubleMatrix2D(K, W);
		lambda.assign(DoubleFunctionFactory.gammaRand);
		ELogBeta = MatrixUtils.logDirichletExpectation(lambda);
		expELogBeta = ELogBeta.copy().assign(Functions.exp);
		//allocate gamma
		gamma = new DenseDoubleMatrix2D(M,K);
		
	}

	private void eStep(Set<Document> docs) {
		int[] wordTypeIds, wordTypeCounts;
		DoubleMatrix1D gammaD, ELogThetaD, expELogThetaD, lastGamma, wordTypeCountMatrix;
		DoubleMatrix2D expELogBetaD;
		double meanchange = 0;
		int counter = 0;
		
		gamma.assign(DoubleFunctionFactory.gammaRand);
		
		ELogTheta = MatrixUtils.logDirichletExpectation(gamma);
		expELogTheta = ELogTheta.copy().assign(Functions.exp);
		
		suffStats = new DenseDoubleMatrix2D(lambda.rows(), lambda.columns()).assign(0);
		
		for(Document d : docs) {
			wordTypeIds = d.getWordFrequencies().keys().elements();
			int[] localTypes = new int[wordTypeIds.length];
			for(int i=0;i<wordTypeIds.length;i++)
				localTypes[i] = localWordTypeIds.get(wordTypeIds[i]);
			wordTypeCounts = d.getWordFrequencies().values().elements();
			
			wordTypeCountMatrix = getWordTypeCountMatrix(wordTypeCounts);
			
			gammaD = gamma.viewRow(counter).copy();
			ELogThetaD = ELogTheta.viewRow(counter).copy();
			expELogThetaD = expELogTheta.viewRow(counter);
			
			expELogBetaD = new DenseDoubleMatrix2D(K, wordTypeIds.length);
			for(int k=0;k<K;k++) {
				expELogBetaD.viewRow(k).assign(expELogBeta.viewRow(k).viewSelection(localTypes));
			}

			DoubleMatrix1D phiNorm = MatrixUtils.dotProductOverColumns(expELogThetaD, expELogBetaD).assign(Functions.plus(1e-100));
			
			DoubleMatrix2D expELogBetaDTransposed = expELogBetaD.viewDice().copy();
			
			for(int iter=0;iter<100;iter++) {
				lastGamma = gammaD.copy();
				
				//compute gamma_d
				DoubleMatrix1D normalizedWordCounts = wordTypeCountMatrix.copy().assign(phiNorm, Functions.div);
				DoubleMatrix1D dotProdNormCountsELogBetaD = MatrixUtils.dotProductOverColumns(normalizedWordCounts, expELogBetaDTransposed); 
				DoubleMatrix1D eLogThetaDTimesDotProd = expELogThetaD.copy().assign(dotProdNormCountsELogBetaD, Functions.mult);
				
				gammaD = eLogThetaDTimesDotProd.copy().assign(Functions.plus(alpha));
				
				ELogThetaD = MatrixUtils.logDirichletExpectation(gammaD);
				expELogThetaD = ELogThetaD.copy().assign(Functions.exp);
				
				phiNorm = MatrixUtils.dotProductOverColumns(expELogThetaD, expELogBetaD).assign(Functions.plus(1e-100));
				
				meanchange = MatrixUtils.mean(gammaD.copy().assign(lastGamma, Functions.minus).assign(Functions.abs));
				if(meanchange < props.convergenceCriterion)
					break;
			}
			gamma.viewRow(counter).assign(gammaD);
			
			DoubleMatrix1D normCts = wordTypeCountMatrix.copy().assign(phiNorm, Functions.div);
			DoubleMatrix2D outerProd = MatrixUtils.outerProduct(expELogThetaD, normCts);
			for(int k=0;k<K;k++) {
				suffStats.viewRow(k).viewSelection(localTypes).assign(outerProd.viewRow(k), Functions.plus);
			}
//			if(counter%100==0)
//				System.out.println("done with " + counter + " docs");
			counter++;
		}
		
		suffStats.assign(expELogBeta, Functions.mult);
	}
	private DoubleMatrix1D getWordTypeCountMatrix(int[] wordTypeCounts) {
		DoubleMatrix1D wordTypeCountMatrix;
		wordTypeCountMatrix = new DenseDoubleMatrix1D(wordTypeCounts.length);
		for(int i=0;i<wordTypeCounts.length;++i)
			wordTypeCountMatrix.set(i, wordTypeCounts[i]);
		return wordTypeCountMatrix;
	}
	
	private double computeApproxBound(Set<Document> docs) {
		double score = 0;
		DoubleMatrix2D ELogTheta = MatrixUtils.logDirichletExpectation(gamma);
//		DoubleMatrix2D ELogBetaTransposed = ELogBeta.viewDice().copy();
		
		//E[log p(docs|theta, beta)]
		int counter = 0;
		for(Document d : docs) {
			int[] wordIds = d.getWordFrequencies().keys().elements();
			int[] counts = d.getWordFrequencies().values().elements();
			
			DoubleMatrix1D wordTypeCountMatrix = getWordTypeCountMatrix(counts);
			
			DoubleMatrix1D phiNorm = new DenseDoubleMatrix1D(wordIds.length);
			
			for(int w=0;w<wordIds.length;w++) {
				DoubleMatrix1D temp = ELogTheta.viewRow(counter).copy().assign(ELogBeta.viewColumn(localWordTypeIds.get(wordIds[w])), Functions.plus);
				double tmax = VectorUtils.max(temp.toArray());
				double sum = temp.assign(Functions.minus(tmax)).assign(Functions.exp).zSum();
				phiNorm.set(w, Math.log(sum) + tmax);
			}
			score += wordTypeCountMatrix.assign(phiNorm, Functions.mult).zSum();
			counter++;
		}
		score += gamma.copy().assign(Functions.neg).assign(Functions.plus(alpha)).assign(ELogTheta, Functions.mult).zSum();
		score += gamma.copy().assign(DoubleFunctionFactory.loggamma).assign(Functions.minus(MathUtils.loggamma(alpha))).zSum();
		score += MatrixUtils.sumRows(gamma).assign(DoubleFunctionFactory.loggamma).assign(Functions.neg).assign(Functions.plus(MathUtils.loggamma(alpha*K))).zSum();
		
		score += lambda.copy().assign(Functions.neg).assign(Functions.plus(eta)).assign(ELogBeta, Functions.mult).zSum();
		score += lambda.copy().assign(DoubleFunctionFactory.loggamma).assign(Functions.minus(MathUtils.loggamma(eta))).zSum();
		score += MatrixUtils.sumRows(lambda).assign(DoubleFunctionFactory.loggamma).assign(Functions.neg).assign(Functions.plus(MathUtils.loggamma(eta*W))).zSum();
		
		return score;
	}
	
	public double runIteration(Set<Document> docs) {
		eStep(docs);
		curBound = computeApproxBound(docs);
		DoubleMatrix2D lastLambda = lambda.copy();
		
		lambda.assign(suffStats.copy());
		lambda.assign(Functions.plus(eta));
		
		double meanChange = MatrixUtils.mean(lastLambda.assign(lambda, Functions.minus).assign(Functions.abs));
		
		ELogBeta = MatrixUtils.logDirichletExpectation(lambda);
		expELogBeta = ELogBeta.copy().assign(Functions.exp);
		return meanChange;
	}

	@Override
	public void doInference() throws IOException {
		logger.trace("starting vb inference");
		
		double curChange = 1;
		int runs = 0;
		while(curChange > props.convergenceCriterion) {
			runs++;
			long start = System.currentTimeMillis();
			curChange = runIteration(corpus.getShuffledDocumentSet());
			long dur = System.currentTimeMillis() - start;
			logger.trace(runs + ".: bound: " + curBound + ", change: " + curChange + " took " + (dur/1000) + "s");
			double perWordBound = curBound / ((double)W);
			logger.trace(runs + ".: held-out perplexity estimate: " + Math.exp(-perWordBound));
			likelihood.add(curBound);
			
			//logger.trace();
		}
		logger.trace("done with vb inference");
	}


	@Override
	public void loadModel(String filename) throws IOException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void populateFinalValues() {
		finalTopicWordWeights = lambda.toArray();
		finalDocTopicWeights = gamma.toArray();
	}
	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericStaticInferencer#doPrediction(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document, double[])
	 */
	@Override
	public double doPrediction(Document d, double[] topicPredictions) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	
}
