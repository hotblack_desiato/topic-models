/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.lda;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeMap;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordToken;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordType;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.WordTypeNotFoundException;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericStaticInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.lda.state.TopicAssignment;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;
import de.uni_leipzig.informatik.asv.topicModels.utils.WordProb;

public class LDAGibbsSampler extends AbstractGenericStaticInferencer {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private double tAlpha;
	private double wBeta;
	
	private double[][] nw;
	private double[][] nd;
	private double[] nwsum;
	
	private double[][] phi;
	private double[][] theta;
	private double numStats = 0;
	private double numLogs = 0;
	
	private long start = System.currentTimeMillis();
	
	private double jointLogProb = 0;
	private double lastJointLogProb = 0;
	private double harmonicLikelihood = 0;
	private double gammaConstant;
	
	private BufferedWriter jointLogProbFileWriter;
	
	public LDAGibbsSampler(Corpus c, Corpus test, Corpus validation, InferencerProperties props) throws IOException {
		super(c, test, validation, props);
		File targetDirObj = new File(props.targetDir);
		if(!targetDirObj.exists())
			targetDirObj.mkdirs();
	}
	
	private void initialize() throws IOException {
		if(props.alpha == 0)
			 props.alpha = 50d/props.numTopics;
		tAlpha = props.numTopics*props.alpha;
		wBeta = corpus.getNumberOfWordTypes() * props.beta;
		
		double loggammaWBeta = MathUtils.loggamma(wBeta);
		double loggammaBetaToW = corpus.getNumberOfWordTypes() * MathUtils.loggamma(props.beta);
		gammaConstant = props.numTopics * (loggammaWBeta-loggammaBetaToW);

		nd = new double[corpus.getNumberOfDocuments()][props.numTopics];
		nw = new double[corpus.getNumberOfWordTypes()][props.numTopics];
		nwsum = new double[props.numTopics];

		
		phi = new double[props.numTopics][corpus.getNumberOfWordTypes()];
		theta = new double[corpus.getNumberOfDocuments()][props.numTopics];
		
		for(Document doc : corpus.getDocuments()) {
			int topic = rng.nextInt(props.numTopics);
			int docId = localDocIds.get(doc.getId());
			for(WordToken wt : doc.getWords()) {
				int localTypeId = localWordTypeIds.get(wt.getTypeId());
				wt.setState(new TopicAssignment(topic));
				nd[docId][topic]++;
				nw[localTypeId][topic]++;
				nwsum[topic]++;
			}
		}
		
		jointLogProbFileWriter = new BufferedWriter(new FileWriter(props.targetDir + "/jointLogProb_" + props.numTopics + "_topics.txt"));
	}
	
	public void getTopWordsForTopics(int numberOfWords) throws IOException {
		getTopWordsForTopics(numberOfWords, null);
	}
	
	public void getTopWordsForTopics(int numberOfWords, String prefix) throws IOException {
		BufferedWriter wordWriter = new BufferedWriter(new FileWriter(props.targetDir + "/topWords_" + props.numTopics + "_topics.txt"));
		StringBuffer buf = new StringBuffer();
		WordProb[] wps = new WordProb[corpus.getNumberOfWordTypes()];
		for(int topic = 0;topic < props.numTopics;topic++) {
			for(WordType wt : corpus.getAllWordTypes()) {
				int typeId = localWordTypeIds.get(wt.getId());
				wps[typeId] = new WordProb(wt.getValue(), phi[topic][typeId], typeId);
			}
			Arrays.sort(wps);
			
			buf.append("Topic ").append(topic).append("\n");
			for(int word=0;word<numberOfWords;word++) {
				buf.append(wps[word].getTerm()).append("\t").append(wps[word].getProb()).append("\n");
			}
			buf.append("\n");
		}
		wordWriter.write(buf.toString());
		wordWriter.close();
	}
	
	public void getTopTopicsForDocs(int numberOfTopics) throws FileNotFoundException {
		PrintStream ps = new PrintStream(props.targetDir + File.separator + "topTopics_" + props.numTopics + "_topics.txt");
		WordProb[] tps = new WordProb[props.numTopics];
		TreeMap<Integer, int[]> topTopics = new TreeMap<>();
		for(Document d : corpus.getDocuments()) {
			int docId = localDocIds.get(d.getId());
			for(int topic=0;topic<props.numTopics;topic++)
				tps[topic] = new WordProb(String.valueOf(topic), theta[docId][topic], topic);
			Arrays.sort(tps);
			int[] tpsFinal = new int[numberOfTopics];
			for(int i=0;i<numberOfTopics;i++) {
				tpsFinal[i] = Integer.parseInt(tps[i].getTerm());
			}
			topTopics.put(docId, tpsFinal);
		}
		
		for(Integer k : topTopics.keySet()) {
			ps.print(k + "\t");
			for(int t : topTopics.get(k))
				ps.print(t + " ");
			ps.println();
		}
		ps.close();
		
	}
	
	public void doInference() throws IOException {
		initialize();
		doSampling();
		free();
	}
	
	
	
	@Override
	public void populateFinalValues() {
		finalDocTopicWeights = theta;
		finalTopicWordWeights = phi;
	}

	public void doSampling() throws IOException {
		boolean converged = false;
		long estimTot=0;
		int estimCount = 0;
		for(int iter=0;iter<props.iterations;iter++) {
			if(iter%100 == 0 && iter != 0) {
				long curTime = System.currentTimeMillis() - start;
				System.out.println(props.numTopics + " topic model at iteration " + iter + ", avg: " + (curTime/(double)iter) + "ms, tot: " + (curTime/1000) + "s");
			}
			sampleIteration();
			if(iter != 0 && !converged && checkConvergence()) {
				props.iterations = Math.min(iter+200, props.iterations);
				System.out.println("converged after " + iter + " iterations, going for another 200 iterations or until max iterations, whichever is reached first");
				converged = true;
				//do not sample hyper params anymore
				props.sampleHyperParams = false;
			}
			logJointProbabilityDistribution();
			if(iter == 0)
				likelihood.add(jointLogProb);
			else
				likelihood.add(lastJointLogProb);
			if(iter%props.hyperSampleLag==0 && props.sampleHyperParams) {
				long estimStart = System.currentTimeMillis();
				estimateBetaAlpha();
				long estimDur = System.currentTimeMillis() - estimStart;
				estimTot += estimDur;
				estimCount++;
				System.out.println("new alpha: " + props.alpha + ", new beta: " + props.beta + ", last: " + estimDur + "ms, avg: " + (estimTot/estimCount) + "ms");
			}
			if(iter%props.sampleLag==0 && iter > props.burnIn) {
//				collectHarmonicLikelihoodMean();
				doLogging();
			}
		}
		phi = VectorUtils.divideByScalar(phi, numStats);
		theta = VectorUtils.divideByScalar(theta, numStats);
		
	}
	
	private boolean checkConvergence() {
		double smoothedLogJointPosterior = jointLogProb / numLogs;
		double diff = Math.abs(lastJointLogProb - smoothedLogJointPosterior);
		diff /= Math.max(Math.abs(lastJointLogProb), Math.abs(smoothedLogJointPosterior));
		if(diff <= props.convergenceCriterion)
			return true;
		return false;
	}


	private void logJointProbabilityDistribution() throws IOException {
		numLogs++;
		//joint likelihood
		double smoothed = jointLogProb / numLogs;
		lastJointLogProb = smoothed;
	}

	private void free() throws IOException {
		jointLogProbFileWriter.close();
	}

	private void sampleIteration() throws IOException {
		double[] topicWeights = new double[props.numTopics];
		double[] wordProbs = new double[props.numTopics];
		for(Document doc : corpus.getDocuments()) {
			double constant = (doc.getLength() -1) + tAlpha;
			int docId = localDocIds.get(doc.getId());
			for(WordToken wt : doc.getWords()) {
				//remove from bookkeeping
				removeFromCounts(docId, wt);
				int localTypeId = localWordTypeIds.get(wt.getTypeId());
				for(int curTopic = 0; curTopic < props.numTopics;curTopic++) {
					double wordProb = (nw[localTypeId][curTopic] + props.beta) / (nwsum[curTopic] + wBeta);
					wordProbs[curTopic] = wordProb;
					topicWeights[curTopic] =  wordProb * (nd[docId][curTopic] + props.alpha) / constant;
				}
				int newTopic = MathUtils.drawFromDistribution(topicWeights);
				wt.<TopicAssignment>getState().setId(newTopic);
				addToCounts(docId, wt);
				
				jointLogProb += Math.log(topicWeights[newTopic]);
			}
		}
	}

	private void addToCounts(int docId, WordToken wt) {
		int localTypeId = localWordTypeIds.get(wt.getTypeId());
		int topic = wt.<TopicAssignment>getState().getId();

		nd[docId][topic]++;
		nw[localTypeId][topic]++;
		nwsum[topic]++;
	}

	private void removeFromCounts(int docId, WordToken wt) {
		int localTypeId = localWordTypeIds.get(wt.getTypeId());
		int topic = wt.<TopicAssignment>getState().getId();
		
		nd[docId][topic]--;
		nw[localTypeId][topic]--;
		nwsum[topic]--;
	}
	
	private void doLogging() throws IOException {
		double[] dist = new double[props.numTopics];
		
		for(Document doc : corpus.getDocuments()) {
			double constant = (doc.getLength() - 1) + K*props.alpha;
			int docId = localDocIds.get(doc.getId());
			for(int topic = 0; topic<props.numTopics;topic++) {
				dist[topic] = (nd[docId][topic] + props.alpha) / constant; 
			}
			dist = VectorUtils.normalize(dist);
			
			theta[docId] = VectorUtils.add(theta[docId], dist);
		}
		
		dist = new double[corpus.getNumberOfWordTypes()];
		
		for(int topic = 0;topic<props.numTopics;topic++) {
			for(WordType wt : corpus.getAllWordTypes()) {
				int localTypeId = localWordTypeIds.get(wt.getId());
				dist[localTypeId] = (nw[localTypeId][topic] + props.beta) / (nwsum[topic] + V*props.beta);
			}
			dist = VectorUtils.normalize(dist);
			phi[topic] = VectorUtils.add(phi[topic], dist);
		}
		numStats++;
	}
	
	private void estimateBetaAlpha() {
		double temp_beta = props.beta;
		double temp_alpha = props.alpha;
		for (int iter=0; iter<30; iter++) {
			double sum_nvk_beta = 0.0;
			double sum_nv_Vbeta = 0.0;
			double sum_nmk_alpha = 0.0;
			double sum_nm_Kalpha = 0.0;
			for(Document doc : corpus.getDocuments()) 
				 sum_nm_Kalpha += MathUtils.digamma(doc.getLength() + (props.numTopics*temp_alpha));
			for(int m = 0;m<corpus.getNumberOfDocuments();m++) {
				 for(int k = 0;k<props.numTopics;k++) {
					 sum_nmk_alpha += MathUtils.digamma(nd[m][k]+temp_alpha);
				 }
			 }

			double denominator = props.numTopics*(sum_nm_Kalpha-(corpus.getNumberOfDocuments()*MathUtils.digamma(props.numTopics*temp_alpha)));
			double enumerator = temp_alpha*(sum_nmk_alpha - (props.numTopics*corpus.getNumberOfDocuments()*MathUtils.digamma(temp_alpha)));
			
			temp_alpha = enumerator/denominator;
			
			for(int k = 0;k<props.numTopics;k++) {
				sum_nv_Vbeta += MathUtils.digamma(nwsum[k] + (corpus.getNumberOfWordTypes()*temp_beta));
				for(int v = 0;v<corpus.getNumberOfWordTypes();v++) {
					 sum_nvk_beta += MathUtils.digamma(nw[v][k]+temp_beta);
				 }
			}
			denominator = corpus.getNumberOfWordTypes()*(sum_nv_Vbeta-(props.numTopics*MathUtils.digamma(corpus.getNumberOfWordTypes()*temp_beta)));
			enumerator = temp_beta*(sum_nvk_beta - (props.numTopics*corpus.getNumberOfWordTypes()*MathUtils.digamma(temp_beta)));
			
			temp_beta = enumerator/denominator;
		}
		props.beta = temp_beta;
		props.alpha = temp_alpha;
	}


//	@Override
//	public void storeModel() throws IOException {
//		super.storeModel();
//		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(props.targetDir + File.separator + "model.obj"));
//		oos.writeObject(this);
//		oos.close();
//	}

	@Override
	public void loadModel(String filename) throws IOException {
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename));
		try {
			LDAGibbsSampler sampler = (LDAGibbsSampler) ois.readObject();
			props = sampler.props;
			corpus = sampler.corpus;
			gammaConstant = sampler.gammaConstant;
			harmonicLikelihood = sampler.harmonicLikelihood;
			jointLogProb = sampler.jointLogProb;
			props.numTopics = sampler.props.numTopics;
			lastJointLogProb = sampler.lastJointLogProb;
			localWordTypeIds = sampler.localWordTypeIds;
			nd = sampler.nd;
			nw = sampler.nw;
			numLogs = sampler.numLogs;
			numStats = sampler.numStats;
			nwsum = sampler.nwsum;
			phi = sampler.phi;
			rng = sampler.rng;
			start = sampler.start;
			tAlpha = sampler.tAlpha;
			theta = sampler.theta;
			wBeta = sampler.wBeta;
		} catch (ClassNotFoundException e) {
			System.err.println("the file \"" + props.targetDir + File.separator + "model.obj" + "\" is not an LDAGibbsSampler model file");
		}
		ois.close();
	}

	public double testModelPerplexity() throws IOException {
		double perplexity = Double.POSITIVE_INFINITY;
		
		ArrayList<Document> heldOutDocuments = new ArrayList<>();
		
		for(Document doc : corpus.getDocuments()) {
			int heldOutTokens = doc.getLength() / 5;
			
			ArrayList<WordToken> restDocument = new ArrayList<>();
			Document heldOutDocument = new Document(corpus, doc.getDescription());
			ArrayList<WordToken> heldOutDocumentWords = new ArrayList<>();
			
			int startSelectTerms = (doc.getLength() - heldOutTokens)/2;
			ArrayList<WordToken> words = doc.getWords();
			for(int i = 0; i < words.size();i++) {
				if((i>=startSelectTerms) && (i < startSelectTerms + heldOutTokens))
					heldOutDocumentWords.add(words.get(i));
				else
					restDocument.add(words.get(i));
			}
			doc.clearWords();
			try {
				for(WordToken wt : restDocument)
					doc.addWord(wt);
				heldOutDocument.clearWords();
				for(WordToken wt : heldOutDocumentWords)
					heldOutDocument.addWord(wt);
			} catch(WordTypeNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		//init again cause documents where changed
		this.initialize();
		
		this.doSampling();
		
		int[][] test_z = new int[corpus.getNumberOfDocuments()][];
		int[][] test_nd = new int[corpus.getNumberOfDocuments()][props.numTopics];
		int[][] test_nw = new int[corpus.getNumberOfWordTypes()][corpus.getNumberOfDocuments()];
		
		for (int m = 0; m < corpus.getNumberOfDocuments(); m++) {
            
			int Nd = heldOutDocuments.get(m).getLength();
            ArrayList<WordToken> docWords = heldOutDocuments.get(m).getWords();
			test_z[m] = new int[Nd];
           	
            for (int n = 0; n < Nd; n++) {
                int topic = (int)(Math.random() * props.numTopics);
                test_z[m][n] = topic;
                test_nd[m][topic]++;
                test_nw[localWordTypeIds.get(docWords.get(n).getTypeId())][m]++;
            }
        }	
        double[] p = new double[props.numTopics];
        ArrayList<WordToken> heldOutwords;
		for (int iter=0; iter<props.iterations; iter++) {
			
			for (int m=0; m<corpus.getNumberOfDocuments(); m++) {
				
				for(int n=0; n< heldOutDocuments.get(m).getLength();n++){
					
					heldOutwords = heldOutDocuments.get(m).getWords();
					
					int topic = test_z[m][n];
					
			        test_nd[m][topic]--;
			        
					int typeId = localWordTypeIds.get(heldOutwords.get(n).getTypeId());
			        
					for(int z=0;z<props.numTopics;z++){
						p[z] = phi[z][typeId] * (test_nd[m][z] + props.alpha);
					}
					
					topic = MathUtils.drawFromDistribution(p);
					
			        test_nd[m][topic]++;
			        test_z[m][n] = topic;
			      
				}
			}
			if(iter%10==0)
				System.out.println("done with " + iter + " heldout inference iterations");
		}
		
		double[][] thetaHeldOut = new double[corpus.getNumberOfDocuments()][props.numTopics];
		
		for (int m=0; m<corpus.getNumberOfDocuments(); m++) {
			double constant = heldOutDocuments.get(m).getLength() + (props.numTopics*props.alpha);
			for(int k=0; k<props.numTopics;k++){
				thetaHeldOut[m][k] = (test_nd[m][k]+props.alpha)/constant;
			}
			
		}
		double sum_log_p = 0.0;
		int sum_N = 0;
		
		for (int m=0; m<corpus.getNumberOfDocuments(); m++) {
			
			double log_p = 0.0;
			
			for (int t=0; t<corpus.getNumberOfWordTypes(); t++) {
			
				int n_m_t = test_nw[t][m];
				if(n_m_t == 0) continue;

//				ArrayList<WordToken> heldOutWords = heldOutDocuments.get(m).getWords();
//				
//				for(int i = 0; i < heldOutDocuments.get(m).getLength();i++)
//				{
//					if(heldOutWords.get(t).getTypeId() == heldOutWords.get(i).getTypeId())
//						numberTDoc++;
//				}
				
				double sum_phi_theta = 0.0;
			
				for(int k=0; k<props.numTopics;k++){
					sum_phi_theta += phi[k][t] * thetaHeldOut[m][k];
				}
				Double res = n_m_t * Math.log(sum_phi_theta);
				log_p += res;
			
			}
			sum_log_p += log_p;
			sum_N += heldOutDocuments.get(m).getLength();
		}
		
		perplexity = Math.exp(-(sum_log_p/sum_N));
		
		return perplexity;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericStaticInferencer#doPrediction(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document, double[])
	 */
	@Override
	public double doPrediction(Document d, double[] topicPredictions) {
		double ll = 0;
		double constant = (d.getLength() -1) + props.alpha*K;
		//prediction statistics
		double[] pred_nd = new double[K];
		//initialize
		for(WordToken wt : d.getWords()) {
			int topic = rng.nextInt(K);
			wt.setState(new TopicAssignment(topic));
			pred_nd[topic]++;
		}
		//do sampling
		double[] p_z = new double[K];
		for(int iter=0;iter<props.iterations;iter++) {
			for(WordToken wt : d.getWords()) {
				//remove from book keeping
				int topic = wt.<TopicAssignment>getState().getId();
				pred_nd[topic]--;
				int typeId = localWordTypeIds.get(wt.getTypeId());
		        
				for(int curTopic =0;curTopic<K;curTopic++) {
					//at first, only collect the theta part of the assignment probability
					p_z[curTopic] = (pred_nd[curTopic] + props.alpha)/constant;
				}
				//multiply with final phi
				p_z = VectorUtils.multiply(p_z, VectorUtils.viewColumn(finalTopicWordWeights, typeId));
				topic = MathUtils.drawFromDistribution(p_z);
				
				pred_nd[topic]++;
				wt.<TopicAssignment>getState().setId(topic);
				
			}
		}
		
		//recompute document's theta
		for(int k=0; k<props.numTopics;k++){
			topicPredictions[k] = (pred_nd[k]+props.alpha)/constant;
		}

		//log likelihood, i.e. p(w_d|beta, z)
		for(WordToken wt : d.getWords()) {
			double sum_phi_theta = 0.0;
			
			for(int k=0; k<K;k++){
				//phi_kw * theta_dk
				sum_phi_theta += finalTopicWordWeights[k][localWordTypeIds.get(wt.getTypeId())] * topicPredictions[k];
			}
			ll += Math.log(sum_phi_theta);
		}
		return ll;
	}


}
