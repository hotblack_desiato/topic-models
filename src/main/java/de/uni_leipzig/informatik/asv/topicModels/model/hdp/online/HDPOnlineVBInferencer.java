/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.topicModels.model.hdp.online;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.jet.math.Functions;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.DoubleFunctionFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MatrixUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.topicModels.model.AbstractGenericOnlineInferencer;
import de.uni_leipzig.informatik.asv.topicModels.model.hdp.online.state.DOCState;
import de.uni_leipzig.informatik.asv.topicModels.model.hdp.online.state.WordState;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;

/**
 * @author jaehnichen
 *
 */
public class HDPOnlineVBInferencer extends AbstractGenericOnlineInferencer {


	private double beta; // default only
	
	protected List<Document> docs;
	
	protected int totalNumberOfWords;
	protected int totalNumberOfTables;
	
	
	protected DoubleMatrix2D lambda;

	private DoubleMatrix2D ELogPhi;

	//private DoubleMatrix2D expELogPhi;

	private DoubleMatrix2D suffStatsLambda;

	private DoubleMatrix1D suffStatsStickBeta;
	private DoubleMatrix1D u, v, ElogStickBeta, ElogStickPi;
	
	
	private int T;


	protected double alpha_0, gamma;
	
	
	private double updateCount;
	private double rhoT;
	private double curBound;

	private DoubleMatrix1D expELogStickBeta;
	
	public HDPOnlineVBInferencer(Corpus c, Corpus test, Corpus validation, InferencerProperties props) throws IOException {
		super(c, test, validation, props);
		initialize();
	}

	private void initialize() {
		this.K = props.globalTopicTruncate;
		this.T = props.localTopicTruncate;
		this.M = corpus.getNumberOfDocuments();
		this.V = corpus.getNumberOfWordTypes();
		
		//doc-level DP concentration param
		this.alpha_0 = props.alpha;
		//corpus-level dirichlet param to base measure
		this.beta = props.beta;
		//corpus-level DP concentration param
		this.gamma = props.gamma;
		
		this.batchSize = props.batchSize;

		//learning rate params
		this.tau0 = props.tau0;
		this.kappa = props.kappa;
		
		//init var distribution q(beta|lambda)
		lambda = new DenseDoubleMatrix2D(K, V);
		lambda.assign(DoubleFunctionFactory.gammaRand);
		ELogPhi = MatrixUtils.logDirichletExpectation(lambda);
		//expELogPhi = ELogPhi.copy().assign(Functions.exp);
		
		//corpus-level stick-breaking params
		u = new DenseDoubleMatrix1D(K-1).assign(getCorpusLevelBetaParam1());
		v = getCorpusLevelBetaParam2(K-1);
		
		ElogStickBeta = MatrixUtils.logStickExpectation(u, v);
		expELogStickBeta = new DenseDoubleMatrix1D(ElogStickBeta.toArray()).assign(Functions.exp);
	}
	
	
	/*
	private double eStep(Set<Integer> docs) {
		//optimize: a,b - the doc-level stick-breaking parameters; phi - doc-level indicator vars; zeta - topic assignments (to local topics)
		int[] wordTypeIds, wordTypeCounts;
		DoubleMatrix2D zetaD, varPhiD = null;
		//DoubleMatrix2D lastVarPhi, lastZeta, zetaD, varPhiD = null;
		DoubleMatrix1D wordTypeCountMatrix;
		int counter = 0;
		
		//collect sufficient statistics for m-step updates of lambda (topics) and global stick-breaking parameters omega = (u,v)
		suffStatsLambda = new DenseDoubleMatrix2D(lambda.rows(), lambda.columns()).assign(0);
		suffStatsStickBeta = new DenseDoubleMatrix1D(K).assign(0);
		
		HashSet<Integer> seenWords = new HashSet<>();
		
		double batchLikelihood = 0;
		
		for(Integer docId : docs) {
			Document<DOCState, WordState> d = corpus.getDocumentById(docId);
			
			//get terms used in this documents and extract the local ids
			wordTypeIds = d.getWordFrequencies().keys().elements();
			List<Integer> _list = Arrays.asList(org.apache.commons.lang3.ArrayUtils.toObject(wordTypeIds));
			seenWords.addAll(_list);
			
			
			
			int[] localTypes = new int[wordTypeIds.length];
			for(int i=0;i<wordTypeIds.length;i++)
				localTypes[i] = localWordTypeIds.get(wordTypeIds[i]);
			
			//get term freqs of used words and construct a matrix from that
			wordTypeCounts = d.getWordFrequencies().values().elements();
			wordTypeCountMatrix = getWordTypeCountMatrix(wordTypeCounts);
			
			
			//start with uniform distribution, each word has equal probability to be assigned to any local topic (n x T)
			//start with uniform distribution, each word has equal probability to be assigned to any local topic (n x T)
			
			
			OnlineHDPLocalStatisticsVO batch = documentPrediction(wordTypeIds,wordTypeCountMatrix, localTypes);
			
			zetaD = batch.getZetaD();
			varPhiD = batch.getVarPhiD();
			batchLikelihood += batch.getBatchLikelihood();
			
			
			DoubleMatrix2D zetaTimesWordCounts = MatrixUtils.multRowElements(zetaD.viewDice(), wordTypeCountMatrix).viewDice();
			DoubleMatrix2D varPhiDotZeta = MatrixUtils.dotProduct(varPhiD.viewDice(), zetaTimesWordCounts.viewDice());

			suffStatsStickBeta.assign(MatrixUtils.sumColumns(varPhiD), Functions.plus);
			for(int t=0;t<K;t++) {
				suffStatsLambda.viewRow(t).viewSelection(localTypes).assign(varPhiDotZeta.viewRow(t), Functions.plus);
			}
			counter++;
		}
//		System.out.println("seen words: " + seenWords.size() + ", vocab length: " + W + ", cardinality of suffStatsLambda (first row): " + suffStatsLambda.viewRow(0).cardinality() + ", last row: " + suffStatsLambda.viewRow(suffStatsLambda.rows()-1).cardinality());
//		System.out.println("suffStatsLambda row 0 cardinality: " + suffStatsLambda.viewRow(0).cardinality());
		return batchLikelihood;
	}

	protected OnlineHDPLocalStatisticsVO documentPrediction(int[] wordTypeIds, DoubleMatrix1D wordTypeCountMatrix, int[] localTypes)
	{
		double batchLikelihood = 0.;
		
		DoubleMatrix2D zetaD = new DenseDoubleMatrix2D(wordTypeIds.length,T).assign(1./T); 
		
		DoubleMatrix1D a = new DenseDoubleMatrix1D(T-1).assign(1.), b = new DenseDoubleMatrix1D(T-1).assign(getDocumentLevelBetaParam2(T-1));
		
		
		//init local stick-breaking weights
		ElogStickPi = MatrixUtils.logStickExpectation(a, b);
		
		DoubleMatrix2D varPhiD = null;
		
		OnlineHDPLocalStatisticsVO result = new OnlineHDPLocalStatisticsVO();
		
		double likelihood = 0, oldLikelihood = -1e100;
		
		DoubleMatrix2D eLogPhiD;
		//extract expectations of words under topics, we deal with types so multiply with term freq (K x n)
		eLogPhiD = MatrixUtils.viewColumnSelection(ELogPhi, localTypes);
		
		//\sum_n E[log p(w_n|\phi_k)]
		DoubleMatrix2D eLogPhiDTimesWordCounts = MatrixUtils.multRowElements(eLogPhiD.copy(),wordTypeCountMatrix);
		
		
		for(int iter=0;iter<100;iter++) {

			//compute initial mapping from local to global topics varPhi_d \propto exp{ sum_n ( zeta_n,t * E[log p(w_nj|phi_k)] ) }
			//from third iteration on, include global stick-breaking weights
			varPhiD = MatrixUtils.dotProduct(zetaD.viewDice(), eLogPhiDTimesWordCounts.viewDice());
			if(iter > 2)
				MatrixUtils.addRowWise(varPhiD, ElogStickBeta);
			//varPhiNormalizer = MatrixUtils.logNormalizeRows(varPhiD);
			DoubleMatrix2D logVarPhi = varPhiD.copy();
			varPhiD.assign(Functions.exp);

			//compute initial zeta (local topic assignments) zeta_d \propto exp{ sum_k ( var_phi_t,k * E[log p(w_nj|phi_k)] ) }
			//from third iteration on, include global stick-breaking weights
			zetaD = MatrixUtils.dotProduct(varPhiD, eLogPhiD).viewDice();
			if(iter > 2)
				MatrixUtils.addRowWise(zetaD, ElogStickPi);
			//zetaNormalizer = MatrixUtils.logNormalizeRows(zetaD);
			DoubleMatrix2D logZetaD = zetaD.copy();
			zetaD.assign(Functions.exp);			
			
			//recompute local stick-breaking variational params
			DoubleMatrix2D zetaDTimesWordcounts = MatrixUtils.multRowElements(zetaD.viewDice(), wordTypeCountMatrix).viewDice();
			DoubleMatrix1D zetaSumN = MatrixUtils.sumColumns(zetaDTimesWordcounts.viewPart(0, 0, zetaD.rows(), zetaD.columns()-1));
			a.assign(getDocumentLevelBetaParam1()).assign(zetaSumN, Functions.plus);
			
			DoubleMatrix1D phiCum = MatrixUtils.sumColumns(zetaDTimesWordcounts.viewPart(0, 1, zetaDTimesWordcounts.rows(), zetaDTimesWordcounts.columns()-1)).viewFlip();
			b.assign(getDocumentLevelBetaParam2(b.size())).assign(MatrixUtils.cumulativeSum(phiCum).viewFlip(), Functions.plus);

			//reestimate local stick-breaking weights given the new parameters
			ElogStickPi = MatrixUtils.logStickExpectation(a, b);
			
			//check for convergence
			likelihood += MatrixUtils.addRowWise(logVarPhi.assign(Functions.neg), ElogStickBeta).assign(varPhiD, Functions.mult).zSum();
			likelihood += getDocumentLevelBetaParam2(T-1).assign(Functions.log).zSum();

			likelihood += stickBreakingLikelihood(a, b);
			
			DoubleMatrix2D temp = MatrixUtils.addRowWise(logZetaD.assign(Functions.neg), ElogStickPi);
			likelihood += temp.assign(zetaD, Functions.mult).zSum();
			
			DoubleMatrix2D dotProd = MatrixUtils.dotProduct(varPhiD, eLogPhiDTimesWordCounts);
			likelihood += zetaD.viewDice().copy().assign(dotProd, Functions.mult).zSum();
			
			double converge = (likelihood - oldLikelihood)/Math.abs(oldLikelihood);

			//store likelihood in oldLikelihood and reset it
			oldLikelihood = likelihood;
			likelihood = 0;
			if(converge < -0.00001)
//				System.out.println("warning: likelihood decreasing");
			if(converge < props.convergenceCriterion && converge > 0) {
				batchLikelihood += likelihood;
				break;
			}
		}
		
		result.setZetaD(zetaD);
		result.setVarPhiD(varPhiD);
		result.setBatchLikelihood(batchLikelihood);
		
		return result;
	}
	
	*/
	
	
	
	protected DoubleMatrix1D getDocumentLevelBetaParam2(int dim) {
		return new DenseDoubleMatrix1D(dim).assign(alpha_0);
	}

	protected double getDocumentLevelBetaParam1() {
		return 1.;
	}

	private double stickBreakingLikelihood(DoubleMatrix1D a, DoubleMatrix1D b) {
		double likelihood = 0;
		DoubleMatrix1D abSum = a.copy().assign(b, Functions.plus);
		DoubleMatrix1D digSum = abSum.copy().assign(DoubleFunctionFactory.digamma);
		DoubleMatrix1D aDig = a.copy().assign(DoubleFunctionFactory.digamma);
		DoubleMatrix1D bDig = b.copy().assign(DoubleFunctionFactory.digamma);
		
		aDig.assign(digSum, Functions.minus);
		bDig.assign(digSum, Functions.minus);
		
		likelihood += a.copy().assign(Functions.neg).assign(Functions.plus(1.)).assign(aDig, Functions.mult).zSum();
		likelihood += b.copy().assign(Functions.neg).assign(getDocumentLevelBetaParam2(b.size()), Functions.plus).assign(bDig, Functions.mult).zSum();
		likelihood -= abSum.copy().assign(DoubleFunctionFactory.loggamma).zSum() - (a.copy().assign(DoubleFunctionFactory.loggamma).zSum() + b.copy().assign(DoubleFunctionFactory.loggamma).zSum());
		return likelihood;
	}

	private DoubleMatrix1D getWordTypeCountMatrix(int[] wordTypeCounts) {
		DoubleMatrix1D wordTypeCountMatrix;
		wordTypeCountMatrix = new DenseDoubleMatrix1D(wordTypeCounts.length);
		for(int i=0;i<wordTypeCounts.length;++i)
			wordTypeCountMatrix.set(i, wordTypeCounts[i]);
		return wordTypeCountMatrix;
	}
	
	private double computeApproxBound(Set<Document> docs) {
		double score = 0;
//		DoubleMatrix2D ELogTheta = MatrixUtils.logDirichletExpectation(gamma);
//		DoubleMatrix2D ELogBetaTransposed = ELogBeta.viewDice().copy();
		
		//E[log p(docs|theta, beta)]
		int counter = 0;
		for(Document d : docs) {
			int[] wordIds = d.getWordFrequencies().keys().elements();
			int[] counts = d.getWordFrequencies().values().elements();
			
			DoubleMatrix1D wordTypeCountMatrix = getWordTypeCountMatrix(counts);
			
			DoubleMatrix1D phiNorm = new DenseDoubleMatrix1D(wordIds.length);
			
			for(int w=0;w<wordIds.length;w++) {
//				DoubleMatrix1D temp = ELogTheta.viewRow(counter).copy().assign(ELogBeta.viewColumn(localWordTypeIds.get(wordIds[w])), Functions.plus);
//				double tmax = VectorUtils.max(temp.toArray());
//				double sum = temp.assign(Functions.minus(tmax)).assign(Functions.exp).zSum();
//				phiNorm.set(w, Math.log(sum) + tmax);
			}
			score += wordTypeCountMatrix.assign(phiNorm, Functions.mult).zSum();
			counter++;
		}
//		score += gamma.copy().assign(Functions.neg).assign(Functions.plus(alpha)).assign(ELogTheta, Functions.mult).zSum();
//		score += gamma.copy().assign(DoubleFunctionFactory.loggamma).assign(Functions.minus(MathUtils.loggamma(alpha))).zSum();
//		score += MatrixUtils.sumRows(gamma).assign(DoubleFunctionFactory.loggamma).assign(Functions.neg).assign(Functions.plus(MathUtils.loggamma(alpha*K))).zSum();
		
		score *= (double)M/(double)batchSize;
		
		score += lambda.copy().assign(Functions.neg).assign(Functions.plus(beta)).assign(ELogPhi, Functions.mult).zSum();
		score += lambda.copy().assign(DoubleFunctionFactory.loggamma).assign(Functions.minus(MathUtils.loggamma(beta))).zSum();
		score += MatrixUtils.sumRows(lambda).assign(DoubleFunctionFactory.loggamma).assign(Functions.neg).assign(Functions.plus(MathUtils.loggamma(beta*V))).zSum();
		
		return score;
	}
	
	private double eStep(Set<Integer> docs) {
		//optimize: a,b - the doc-level stick-breaking parameters; phi - doc-level indicator vars; zeta - topic assignments (to local topics)
		int[] wordTypeIds, wordTypeCounts;

		
		DoubleMatrix2D lastVarPhi, lastZeta, zetaD, varPhiD = null;
		
		DoubleMatrix1D wordTypeCountMatrix, last_a, last_b, zetaNormalizer, varPhiNormalizer;
		//stick-breaking parameters, dimensionality is # of local topics T
		DoubleMatrix1D a = new DenseDoubleMatrix1D(T-1).assign(1.), b = new DenseDoubleMatrix1D(T-1).assign(getDocumentLevelBetaParam2(T-1));
		DoubleMatrix2D eLogPhiD;
		int counter = 0;
		//collect sufficient statistics for m-step updates of lambda (topics) and global stick-breaking parameters omega = (u,v)
		suffStatsLambda = new DenseDoubleMatrix2D(lambda.rows(), lambda.columns()).assign(0);
		suffStatsStickBeta = new DenseDoubleMatrix1D(K).assign(0);
		
		HashSet<Integer> seenWords = new HashSet<>();
		
		double batchLikelihood = 0;
		
		for(Integer docId : docs) {
			int localDocId = localDocIds.get(docId);
			Document d = corpus.getDocumentById(localDocId);
			//init likelihood vals
			double likelihood = 0, oldLikelihood = -1e100;

			//init local stick-breaking weights
			ElogStickPi = MatrixUtils.logStickExpectation(a, b);
			
			//get terms used in this documents and extract the local ids
			wordTypeIds = d.getWordFrequencies().keys().elements();
			List<Integer> _list = Arrays.asList(org.apache.commons.lang3.ArrayUtils.toObject(wordTypeIds));
			seenWords.addAll(_list);
			int[] localTypes = new int[wordTypeIds.length];
			for(int i=0;i<wordTypeIds.length;i++)
				localTypes[i] = localWordTypeIds.get(wordTypeIds[i]);
			
			//get term freqs of used words and construct a matrix from that
			wordTypeCounts = d.getWordFrequencies().values().elements();
			wordTypeCountMatrix = getWordTypeCountMatrix(wordTypeCounts);
			
			//extract expectations of words under topics, we deal with types so multiply with term freq (K x n)
			eLogPhiD = MatrixUtils.viewColumnSelection(ELogPhi, localTypes);
			//\sum_n E[log p(w_n|\phi_k)]
			DoubleMatrix2D eLogPhiDTimesWordCounts = MatrixUtils.multRowElements(eLogPhiD.copy(),wordTypeCountMatrix);
			
			//start with uniform distribution, each word has equal probability to be assigned to any local topic (n x T)
			zetaD = new DenseDoubleMatrix2D(wordTypeIds.length,T).assign(1./T); 
			
			for(int iter=0;iter<100;iter++) {

				//compute initial mapping from local to global topics varPhi_d \propto exp{ sum_n ( zeta_n,t * E[log p(w_nj|phi_k)] ) }
				//from third iteration on, include global stick-breaking weights
				varPhiD = MatrixUtils.dotProduct(zetaD.viewDice(), eLogPhiDTimesWordCounts.viewDice());
				if(iter > 2)
					MatrixUtils.addRowWise(varPhiD, ElogStickBeta);
				varPhiNormalizer = MatrixUtils.logNormalizeRows(varPhiD);
				DoubleMatrix2D logVarPhi = varPhiD.copy();
				varPhiD.assign(Functions.exp);

				//compute initial zeta (local topic assignments) zeta_d \propto exp{ sum_k ( var_phi_t,k * E[log p(w_nj|phi_k)] ) }
				//from third iteration on, include global stick-breaking weights
				zetaD = MatrixUtils.dotProduct(varPhiD, eLogPhiD).viewDice();
				if(iter > 2)
					MatrixUtils.addRowWise(zetaD, ElogStickPi);
				zetaNormalizer = MatrixUtils.logNormalizeRows(zetaD);
				DoubleMatrix2D logZetaD = zetaD.copy();
				zetaD.assign(Functions.exp);			
				
				//recompute local stick-breaking variational params
				DoubleMatrix2D zetaDTimesWordcounts = MatrixUtils.multRowElements(zetaD.viewDice(), wordTypeCountMatrix).viewDice();
				DoubleMatrix1D zetaSumN = MatrixUtils.sumColumns(zetaDTimesWordcounts.viewPart(0, 0, zetaD.rows(), zetaD.columns()-1));
				a.assign(getDocumentLevelBetaParam1()).assign(zetaSumN, Functions.plus);
				
				DoubleMatrix1D phiCum = MatrixUtils.sumColumns(zetaDTimesWordcounts.viewPart(0, 1, zetaDTimesWordcounts.rows(), zetaDTimesWordcounts.columns()-1)).viewFlip();
				b.assign(getDocumentLevelBetaParam2(b.size())).assign(MatrixUtils.cumulativeSum(phiCum).viewFlip(), Functions.plus);

				//reestimate local stick-breaking weights given the new parameters
				ElogStickPi = MatrixUtils.logStickExpectation(a, b);
				
				//check for convergence
				likelihood += MatrixUtils.addRowWise(logVarPhi.assign(Functions.neg), ElogStickBeta).assign(varPhiD, Functions.mult).zSum();
				likelihood += getDocumentLevelBetaParam2(T-1).assign(Functions.log).zSum();

				likelihood += stickBreakingLikelihood(a, b);
				
				DoubleMatrix2D temp = MatrixUtils.addRowWise(logZetaD.assign(Functions.neg), ElogStickPi);
				likelihood += temp.assign(zetaD, Functions.mult).zSum();
				
				DoubleMatrix2D dotProd = MatrixUtils.dotProduct(varPhiD, eLogPhiDTimesWordCounts);
				likelihood += zetaD.viewDice().copy().assign(dotProd, Functions.mult).zSum();
				
				double converge = (likelihood - oldLikelihood)/Math.abs(oldLikelihood);

				//store likelihood in oldLikelihood and reset it
				oldLikelihood = likelihood;
				likelihood = 0;
				if(converge < -0.00001)
//					System.out.println("warning: likelihood decreasing");
				if(converge < props.convergenceCriterion && converge > 0) {
					batchLikelihood += likelihood;
					break;
				}
			}
			
			DoubleMatrix2D zetaTimesWordCounts = MatrixUtils.multRowElements(zetaD.viewDice(), wordTypeCountMatrix).viewDice();
			DoubleMatrix2D varPhiDotZeta = MatrixUtils.dotProduct(varPhiD.viewDice(), zetaTimesWordCounts.viewDice());

			suffStatsStickBeta.assign(MatrixUtils.sumColumns(varPhiD), Functions.plus);
			for(int t=0;t<K;t++) {
				suffStatsLambda.viewRow(t).viewSelection(localTypes).assign(varPhiDotZeta.viewRow(t), Functions.plus);
			}
			counter++;
		}
//		System.out.println("seen words: " + seenWords.size() + ", vocab length: " + W + ", cardinality of suffStatsLambda (first row): " + suffStatsLambda.viewRow(0).cardinality() + ", last row: " + suffStatsLambda.viewRow(suffStatsLambda.rows()-1).cardinality());
//		System.out.println("suffStatsLambda row 0 cardinality: " + suffStatsLambda.viewRow(0).cardinality());
		return batchLikelihood;
	}

	
	public void runBatch(Set<Integer> docs) {
		curBound = eStep(docs);
		//update lambda
		DoubleMatrix2D newLambda = suffStatsLambda.copy();
		newLambda.assign(Functions.div(docs.size())).assign(Functions.mult(M)).assign(Functions.plus(beta));
//		System.out.println("cardinality of newLambda (first row): " + newLambda.viewRow(0).cardinality() + ", last row: " + newLambda.viewRow(newLambda.rows()-1).cardinality());
		rhoT = computeLearningRate(newLambda);
		
		newLambda.assign(Functions.mult(rhoT));
		lambda.assign(Functions.mult(1-rhoT)).assign(newLambda, Functions.plus);
		
		
		//update stick vars
		DoubleMatrix1D newStickVars = suffStatsStickBeta.copy();
		newStickVars.assign(Functions.div(docs.size())).assign(Functions.mult(M));
		u = newStickVars.viewPart(0, K-1).copy().assign(Functions.plus(getCorpusLevelBetaParam1()));
		DoubleMatrix1D cumSum = newStickVars.viewPart(1, K-1).copy().viewFlip();
		v = MatrixUtils.cumulativeSum(cumSum).viewFlip().assign(getCorpusLevelBetaParam2(v.size()), Functions.plus);
		
		curBound += getCorpusLevelBetaParam2(v.size()).assign(Functions.log).zSum();
		curBound += stickBreakingLikelihood(u, v);
		
		//TODO: check if likelihood computation is correct (curBound)
		
		ELogPhi = MatrixUtils.logDirichletExpectation(lambda);
		//expELogPhi = ELogPhi.copy().assign(Functions.exp);
		ElogStickBeta = MatrixUtils.logStickExpectation(u, v);
		expELogStickBeta = ElogStickBeta.copy().assign(Functions.exp);
		updateCount++;
	}

	protected DoubleMatrix1D getCorpusLevelBetaParam2(int dim) {
		return new DenseDoubleMatrix1D(dim).assign(gamma);
	}

	protected double getCorpusLevelBetaParam1() {
		return 1.;
	}
	

	@Override
	public double computeLearningRate(DoubleMatrix2D sampledGradient) {
		return Math.pow(tau0 + updateCount, -kappa);
	}

	@Override
	public void doInference() throws IOException {
		logger.info("starting online inference");
		logger.info("using window size: " + batchSize);
		
		Iterator<Document> docIter = corpus.getShuffledDocumentIterator();
		
		HashSet<Integer> curSlice = new HashSet<>();
		int processedDocs = 0;
		int wordCount = 0;
		int runs = 0;
		logger.info("#docs\tlikelihood\tper-word likelihood\tlearnRate\titer duration (in s)");
		while(docIter.hasNext()) {
			Document d = docIter.next();
			curSlice.add(d.getId());
			wordCount += VectorUtils.sum(d.getWordFrequencies().values().elements());
			processedDocs++;
			if(processedDocs%batchSize==0) {
				runs++;
				long start = System.currentTimeMillis();
				runBatch(curSlice);
				long dur = System.currentTimeMillis() - start;
				double perWordBound = curBound*batchSize / ((double)M*(double)wordCount);
				logger.info(processedDocs + "\t" + curBound + "\t" + perWordBound + "\t" + rhoT + "\t" + (dur/1000));
				curSlice.clear();
				wordCount = 0;
			}
		}
		//sample document assignments
		//sampleDocumentAssignments();
		logger.info("done with online inference");
	}

	@Override
	public void storeModel() throws IOException {
		super.storeModel();
		long start;
		long dur;
		logger.info("saving gamma and lambda and generating topic list...");
		start = System.currentTimeMillis();
		saveTopLevelStick();
		dur = System.currentTimeMillis() - start;
		
		
		
		logger.trace("done, took " + dur + "ms\n");
	}

	/*
	private void sampleDocumentAssignments() throws IOException {
		
		Iterator<Document<DOCState, WordState>> docIter = corpus.getShuffledDocumentIterator();
		
		HashSet<Integer> docId = new HashSet<>();
		
		HashSet<Integer> seenWords = new HashSet<>();
		
		double batchLikelihood = 0.0;
		
		while(docIter.hasNext()) {
			
			int[] wordTypeIds, wordTypeCounts;

			DoubleMatrix2D zetaD, varPhiD = null;
			//DoubleMatrix2D lastVarPhi, lastZeta, zetaD, varPhiD = null;
			
			DoubleMatrix1D wordTypeCountMatrix;
			
			Document<DOCState, WordState> d = docIter.next();
			
			wordTypeIds = d.getWordFrequencies().keys().elements();
			//start with uniform distribution, each word has equal probability to be assigned to any local topic (n x T)
			zetaD = new DenseDoubleMatrix2D(wordTypeIds.length,T).assign(1./T); 
			
			
			List<Integer> _list = Arrays.asList(org.apache.commons.lang3.ArrayUtils.toObject(wordTypeIds));
			seenWords.addAll(_list);
			
			int[] localTypes = new int[wordTypeIds.length];
			for(int i=0;i<wordTypeIds.length;i++)
				localTypes[i] = localWordTypeIds.get(wordTypeIds[i]);
			
			//get term freqs of used words and construct a matrix from that
			wordTypeCounts = d.getWordFrequencies().values().elements();
			wordTypeCountMatrix = getWordTypeCountMatrix(wordTypeCounts);
			
			DoubleMatrix1D a = new DenseDoubleMatrix1D(T-1).assign(1.), b = new DenseDoubleMatrix1D(T-1).assign(getDocumentLevelBetaParam2(T-1));
			
			OnlineHDPLocalStatisticsVO batch = documentPrediction(wordTypeIds,wordTypeCountMatrix, localTypes);
			
			zetaD = batch.getZetaD();
			varPhiD = batch.getVarPhiD();
			batchLikelihood += batch.getBatchLikelihood();
			
			
		}
		
	}
	*/
	

	private void saveTopLevelStick() throws IOException {
		String filename = props.targetDir + File.separator + "stick1stLevel.dat";
		BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
		bw.write(Arrays.toString(expELogStickBeta.toArray()));
		bw.close();
	}


	
	@Override
	public void populateFinalValues() {
		finalTopicWordWeights = lambda.toArray();
		//TODO doc topic weights
//		finalDocTopicWeights = 
		
	}

	@Override
	public void loadModel(String filename) throws IOException {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.topicModels.model.GenericStaticInferencer#doPrediction(de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Document, double[])
	 */
	@Override
	public double doPrediction(Document d, double[] topicPredictions) {
		// TODO Auto-generated method stub
		return 0;
	}

}
