/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.dlngap;

import java.util.Arrays;

import org.apache.commons.math3.linear.CholeskyDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;

public class DLNGAPObjective extends ObjectiveFunction {

	private int curWord;
	private int curTopic;
	private RealMatrix KbbPlusNoiseInv;
	private RealMatrix observationsPreTerm;
	private int[] obsTimes;
	private RealMatrix Sigma;
	private RealMatrix betaHat;
	private double logDetKbbPlusNoise;
	
	private double[][] observationTimes;
	private RealMatrix V;
	private RealMatrix KbbInv;
	
	public DLNGAPObjective() {}
	
	public DLNGAPObjective(int dim, DynamicLogNormalGammaPoissonModel p) {
		super(dim, p);
	}
	
	public void setObservationData(double[][] observationTimes, int k, int w) {
		DynamicLogNormalGammaPoissonModel m = getParameters();
		this.observationTimes = observationTimes;
		//precompute covariance matrices and inverses
		RealMatrix Kbb = MatrixUtils.createRealMatrix(m.covFct.computeCovarianceMatrix(observationTimes));
		RealMatrix id = MatrixUtils.createRealIdentityMatrix(getDimensions());
		RealMatrix noise = id.scalarMultiply(m.nu);
		
		CholeskyDecomposition dec = new CholeskyDecomposition(Kbb);
		KbbInv = dec.getSolver().getInverse();
		
		dec = new CholeskyDecomposition(Kbb.add(noise));
		KbbPlusNoiseInv = dec.getSolver().getInverse();
		logDetKbbPlusNoise = Math.log(dec.getDeterminant());
		//only use covariance between observations times (we only look at posterior means at observation times)
		observationsPreTerm = Kbb.add(noise).multiply(KbbInv);
		
		V = Kbb.subtract(Kbb.multiply(KbbPlusNoiseInv).multiply(Kbb));
		m.Sigma[k][w] = VectorUtils.diag(V.getData());
		
		Sigma = observationsPreTerm.multiply(observationsPreTerm.transpose()).subtract(observationsPreTerm.transpose()).subtract(observationsPreTerm).add(id);
		this.curTopic = k;
		this.curWord = w;
				
		//store observation locations
		obsTimes = m.getNonzeroFrequencyTimes()[w];
	}


	@Override
	public double f(double[] point) {
		DynamicLogNormalGammaPoissonModel m = getParameters();
		double f = 0;
		//compute current mean value and store to the model
		double meanM = VectorUtils.mean(point);
		RealMatrix mW = MatrixUtils.createColumnRealMatrix(point);
		betaHat = observationsPreTerm.multiply(mW.scalarAdd(-meanM));
	
		//beta_hat prior likelihood
		f -= .5*betaHat.transpose().multiply(KbbInv).multiply(betaHat).getTrace();

		for(int t=0;t<obsTimes.length;t++) {
			double expEBeta = Math.exp(point[t] + .5*m.Sigma[curTopic][curWord][t]);
			f += point[t]*m.statsKWT[curTopic][curWord][t];
			f -= expEBeta*m.statsKT[curTopic][obsTimes[t]];
			f -= .5*Math.pow(point[t] - m.mean0, 2)/m.var0;
		}
//		//measurement likelihood
		f += .5*(mW.scalarAdd(-meanM).transpose().multiply(Sigma).multiply(mW.scalarAdd(-meanM))).getTrace()/m.nu; 
		return -f;
	}

	@Override
	public void df(double[] point, double[] df) {
		DynamicLogNormalGammaPoissonModel m = getParameters();
		Arrays.fill(df, 0);
		//compute current mean value and store to the model
		double meanM = VectorUtils.mean(point);
		RealMatrix mW = MatrixUtils.createColumnRealMatrix(point);
		betaHat = observationsPreTerm.multiply(mW.scalarAdd(-meanM));
	
		//beta_hat prior likelihood
		VectorUtils.subtract_(df, observationsPreTerm.transpose().multiply(KbbInv).multiply(betaHat).getColumn(0));
		
		for(int t=0;t<obsTimes.length;t++) {
			double expEBeta = Math.exp(point[t] + .5*m.Sigma[curTopic][curWord][t]);
			df[t] += -1./(m.var0) * (point[t] - m.mean0) + m.statsKWT[curTopic][curWord][t] - expEBeta*m.statsKT[curTopic][t];
		}
//		//measurement likelihood

		VectorUtils.add_(Sigma.add(Sigma.transpose()).multiply(mW.scalarAdd(-meanM)).scalarMultiply(1./(2*m.nu)).getColumn(0), df);

		VectorUtils.neg_(df);
	}

	
	@Override
	public double fdf(double[] point, double[] df) {
		DynamicLogNormalGammaPoissonModel m = getParameters();
		Arrays.fill(df, 0);
		double f = 0;
		//compute current mean value and store to the model
		double meanM = VectorUtils.mean(point);
		RealMatrix mW = MatrixUtils.createColumnRealMatrix(point);
		betaHat = observationsPreTerm.multiply(mW.scalarAdd(-meanM));
	
		//beta_hat prior likelihood
		f -= .5*betaHat.transpose().multiply(KbbInv).multiply(betaHat).getTrace();
		VectorUtils.subtract_(df, observationsPreTerm.transpose().multiply(KbbInv).multiply(betaHat).getColumn(0));
		
		for(int t=0;t<obsTimes.length;t++) {
			double expEBeta = Math.exp(point[t] + .5*m.Sigma[curTopic][curWord][t]);

			f += point[t]*m.statsKWT[curTopic][curWord][t];
			f -= expEBeta*m.statsKT[curTopic][obsTimes[t]];
			f -= .5*Math.pow(point[t] - m.mean0, 2)/m.var0;

			df[t] += -1./(m.var0) * (point[t] - m.mean0) + m.statsKWT[curTopic][curWord][t] - expEBeta*m.statsKT[curTopic][t];
		}
//		//measurement likelihood
		
		f += .5*(mW.scalarAdd(-meanM).transpose().multiply(Sigma).multiply(mW.scalarAdd(-meanM))).getTrace()/m.nu; 
		VectorUtils.add_(Sigma.add(Sigma.transpose()).multiply(mW.scalarAdd(-meanM)).scalarMultiply(1./(2*m.nu)).getColumn(0), df);

		VectorUtils.neg_(df);
		return -f;
	}
	
	
	public double[] predictFromPosteriors(double[] posteriorMean) {
		DynamicLogNormalGammaPoissonModel m = getParameters();
		double meanM = VectorUtils.mean(posteriorMean);
		RealMatrix mW = MatrixUtils.createColumnRealMatrix(posteriorMean);
		RealMatrix betaHat = observationsPreTerm.multiply(mW.scalarAdd(-meanM));
//		RealMatrix betaHat = observationsPreTerm.multiply(mW);
		
		//first, use posteriors at observed times to compute expectations
		double[] ret = new double[m.getT()];
		for(int nnz=0;nnz < obsTimes.length;nnz++) {
			//update dense posterior topic params
			m.posteriorMu[obsTimes[nnz]][curTopic][curWord] = posteriorMean[nnz];
			m.posteriorSigma[obsTimes[nnz]][curTopic][curWord] = m.Sigma[curTopic][curWord][nnz];
			//update the expected value
			ret[obsTimes[nnz]] = Math.exp(posteriorMean[nnz] + .5*m.Sigma[curTopic][curWord][nnz]);
		}
		if(obsTimes.length < m.getT()) {
			//determine non observation times
			int[] zeroFreqTimes = m.getZeroFrequencyTimes()[curWord];
			double[][] nonObservationTimes = new double[zeroFreqTimes.length][1];
			for(int n=0;n<zeroFreqTimes.length;n++) 
				nonObservationTimes[n][0] = zeroFreqTimes[n];
			
			RealMatrix predCov = MatrixUtils.createRealMatrix(m.covFct.computeCovarianceMatrix(nonObservationTimes));
			RealMatrix predCrossCov = MatrixUtils.createRealMatrix(m.covFct.computeCrossCovarianceMatrix(nonObservationTimes, observationTimes));
			
			double[] predictions = predCrossCov.multiply(KbbPlusNoiseInv).multiply(betaHat).scalarAdd(meanM).getColumn(0);
//			double[] predictions = predCrossCov.multiply(KbbPlusNoiseInv).multiply(betaHat).getColumn(0);
			double[] vars = VectorUtils.diag(predCov.subtract(predCrossCov.multiply(KbbPlusNoiseInv).multiply(predCrossCov.transpose())).getData());
			
			//compute expectations for predicted values
			for(int z=0;z<zeroFreqTimes.length;z++) {
				//update dense posterior topic params
				m.posteriorMu[zeroFreqTimes[z]][curTopic][curWord] = predictions[z];
				m.posteriorSigma[zeroFreqTimes[z]][curTopic][curWord] = vars[z];
				//update the expected value
				ret[zeroFreqTimes[z]] = Math.exp(predictions[z] + .5*vars[z]);
			}
		}
		return ret;
	}


	public double getLikelihood() {
		DynamicLogNormalGammaPoissonModel m = getParameters();
		double[] beta = m.mu[curTopic][curWord];
		double[] Sigma = m.Sigma[curTopic][curWord];
		double mean = VectorUtils.mean(beta);
		betaHat = observationsPreTerm.multiply(MatrixUtils.createColumnRealMatrix(beta).scalarAdd(-mean));
//		double likelihood = -.5*logDetKbbPlusNoise -.5*betaHat.transpose().multiply(KbbPlusNoiseInv).multiply(betaHat).getTrace() - .5*V.multiply(KbbPlusNoiseInv).getTrace();;
		double likelihood = -.5*betaHat.transpose().multiply(KbbPlusNoiseInv).multiply(betaHat).getTrace();
		for(int t=0;t<getDimensions();t++)
			likelihood += .5 * Math.pow(betaHat.getColumn(0)[t] + mean - beta[t], 2)/m.nu;
//			likelihood += .5 * (Math.log(m.nu) + (Math.pow(betaHat.getColumn(0)[t] + mean - beta[t], 2) + Sigma[t])/m.nu);
		return likelihood;
	}

	/**
	 * @param curWord the curWord to set
	 */
	public void setCurWord(int curWord) {
		this.curWord = curWord;
	}

	/**
	 * @param k
	 */
	public void setCurTopic(int k) {
		this.curTopic = k;
	}

}
