/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.sde;

/**
 * We follow the descriptions in Archambeau et.al. "Variational Inference for Diffusion Processes".
 * The goal is to define a posterior distribution over paths for a diffusion process.
 * For this, we make use of Gaussian Process approximation to the typically non-Gaussian process by
 * approximating the typically non-linear drift term f(x) in the original diffusion by a linear one (hence resulting in a GP), f_L(x).
 * The method then proceeds by defining the behavior of the GP by a set of ODEs that govern mean and variance evolution and the
 * to minimize the KL divergence between the original process and its approximation.
 * The minimization technique is based on Lagrange multipliers to ensure the constraints posed by the ODEs governing the approximation.
 * The terms involved in the KL divergence that come from the diffusion part depend on an energy function that is governed by the drift terms of
 * the original process and its approximation. Further, derivatives of the energy function w.r.t. to the variational mean and variance and
 * the variatonal functions that drive their evolution in the approximation are needed.

 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class Diffusion {
	
	public enum Type {BROWNIAN, BROWNIAN_DRIFT, ORNSTEIN_UHLENBECK, ORNSTEIN_UHLENBECK_MEAN};
	
	/**
	 * compute E_q_t[(f(x) - f_L(x))^2 / sigma^2], the SDE energy function
	 * @param diffType - the type of diffusion function
	 * @return the energy value
	 */
	public static double getDiffusionEnergy(Type diffType, double mt, double st, double at, double bt, double sigma, double... params) {
		double ret = 0;
		switch(diffType) {
		case BROWNIAN:
			ret = .5*(at*at*(mt*mt + st) - 2*at*mt*bt + bt*bt)/sigma;
			break;
		case ORNSTEIN_UHLENBECK:
			double theta = params[0];
			ret = .5*((at-theta)*(at-theta)*(mt*mt + st) - 2*(at - theta)*mt*bt + bt*bt)/sigma;
			break;
		}
		if(Double.valueOf(ret).isNaN() || ret > 1000) {
			double dummy = 1;
		}
		return ret;
	}
	
	public static double getDiffusionDerivativeMean(Type diffType, double mt, double st, double at, double bt, double sigma, double... params) {
		double ret = 0;
		switch(diffType) {
		case BROWNIAN:
			ret = (at*at*mt - at*bt)/sigma;
			break;
		case ORNSTEIN_UHLENBECK:
			double theta = params[0];
			ret = ((at-theta)*(at-theta)*mt - (at-theta)*bt)/sigma;
			break;
		}
		return ret;
	}
	
	public static double getDiffusionDerivativeVariance(Type diffType, double mt, double st, double at, double bt, double sigma, double... params) {
		double ret = 0;
		switch(diffType) {
		case BROWNIAN:
			ret = .5*(at*at)/sigma;
			break;
		case ORNSTEIN_UHLENBECK:
			double theta = params[0];
			ret = .5*(at-theta)*(at-theta)/sigma;
			break;
		}
		return ret;
	}

	public static double getDiffusionDerivativeA(Type diffType, double mt, double st, double at, double bt, double sigma, double... params) {
		double ret = 0;
		switch(diffType) {
		case BROWNIAN:
			ret = (at*(mt*mt+st) - mt*bt)/sigma;
			break;
		case ORNSTEIN_UHLENBECK:
			double theta = params[0];
			ret = ((at-theta)*(mt*mt + st) - bt*mt)/sigma;
			break;
		}
		return ret;
	}
	
	public static double getDiffusionDerivativeb(Type diffType, double mt, double st, double at, double bt, double sigma, double... params) {
		double ret = 0;
		switch(diffType) {
		case BROWNIAN:
			ret = (bt - at*mt)/sigma;
			break;
		case ORNSTEIN_UHLENBECK:
			double theta = params[0];
			ret = ((theta - at)*mt + bt)/sigma;
			break;
		}
		return ret;
	}
	
	public static double[] getDiffusionDerivativeParams(Type diffType, double mt, double st, double at, double bt, double sigma, double... params) {
		double[] derivs = new double[params.length];
		switch (diffType) {
		case BROWNIAN:
			derivs = null;
			break;
		case ORNSTEIN_UHLENBECK:
			double theta = params[0];
			derivs[0] = ((mt*mt + st)*(theta-at) + mt*bt)/sigma;
			break;
		}
		return derivs;
	}

}
