/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.lda;

import cern.colt.matrix.DoubleMatrix1D;

public class OnlineLDALocalStatisticsVO {

	DoubleMatrix1D gammaD; 
	DoubleMatrix1D phiNorm; 
	public DoubleMatrix1D getPhiNorm() {
		return phiNorm;
	}

	public void setPhiNorm(DoubleMatrix1D phiNorm) {
		this.phiNorm = phiNorm;
	}

	public DoubleMatrix1D getExpELogThetaD() {
		return expELogThetaD;
	}

	public void setExpELogThetaD(DoubleMatrix1D expELogThetaD) {
		this.expELogThetaD = expELogThetaD;
	}

	DoubleMatrix1D expELogThetaD;
	
	public DoubleMatrix1D getGammaD() {
		return gammaD;
	}

	public void setGammaD(DoubleMatrix1D gammaD) {
		this.gammaD = gammaD;
	}
}
