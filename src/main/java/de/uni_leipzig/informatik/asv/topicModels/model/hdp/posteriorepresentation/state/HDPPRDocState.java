/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.hdp.posteriorepresentation.state;

import de.uni_leipzig.informatik.asv.corpusUtils.corpus.state.DocumentState;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;


public class HDPPRDocState extends DocumentState {
	
	private double[] pi;
	private int[] wordcountByTopic;
	private int[] tableCountByTopic;
	
	public HDPPRDocState(int numTopics) {
		pi = new double[numTopics+1];
		wordcountByTopic = new int[numTopics];
		tableCountByTopic = new int[numTopics];
	}
	
	public void resamplePi(double alpha, double[] beta) {
		double[] concentrationParams = new double[pi.length];
		concentrationParams[pi.length-1] = alpha * beta[pi.length-1];
		for(int i=0;i<concentrationParams.length-1;i++)
			concentrationParams[i] = alpha * beta[i] + wordcountByTopic[i];
		pi = MathUtils.drawFromDirichlet(concentrationParams);
	}
	
	public void resampleTableCounts(double alpha, double[] beta) {
		for(int i=0;i<tableCountByTopic.length;i++) {
			tableCountByTopic[i] = MathUtils.drawNumberOfTablesFromCRP(alpha * beta[i+1], wordcountByTopic[i]);
		}
	}
	
	public void removeTopic(int id) {
		pi = VectorUtils.removeElement(pi, id);
		tableCountByTopic = VectorUtils.removeElement(tableCountByTopic, id);
		wordcountByTopic = VectorUtils.removeElement(wordcountByTopic, id);
	}
	
	public void addTopic() {
		pi = VectorUtils.addElement(pi);
		tableCountByTopic = VectorUtils.addElement(tableCountByTopic);
		wordcountByTopic = VectorUtils.addElement(wordcountByTopic);
	}
	
	public void resetPi(double vj) {
		double pi0 = pi[pi.length-2];
		pi[pi.length-1] = pi0 * vj;
		pi[pi.length-2] = pi0 * (1 - vj);
	}
	
	public void increaseWordCountForTopic(int topicId) {
		try {
			wordcountByTopic[topicId]++;
		} catch (Exception e) {
			System.out.println("h");
		}
	}
	
	public void decreaseWordCountForTopic(int topicId) {
		wordcountByTopic[topicId]--;
	}
	
	public int[] getTableCounts() {
		return tableCountByTopic;
	}
	
	public int[] getWordCountsByTopic() {
		return wordcountByTopic;
	}
	
	public double[] getPi() {
		return pi;
	}
}
