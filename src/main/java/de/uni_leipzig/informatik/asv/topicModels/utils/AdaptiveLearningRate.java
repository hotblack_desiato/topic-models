/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.utils;

import java.util.HashSet;
import java.util.Random;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.jet.math.Functions;

public class AdaptiveLearningRate {
	
	private double tau = 0; //the window size
	private DoubleMatrix1D movingAverage;
	private double movingVariance;
	
	public AdaptiveLearningRate(double initialTau, DoubleMatrix2D params) {
		computeInitialGuesses(params, (int)initialTau);
	}
	
	public double computeAdaptiveLearningRate(final DoubleMatrix2D sampledNaturalGradient) {
		double tauRec = 1./tau;

		//sum up the natural gradient
		DoubleMatrix1D gSum = new DenseDoubleMatrix1D(sampledNaturalGradient.rows());
		for(int row=0;row<sampledNaturalGradient.rows();++row) {
			gSum.set(row, sampledNaturalGradient.viewRow(row).zSum() / (double)sampledNaturalGradient.viewRow(row).cardinality());
		}
		
		//compute new moving average
		movingAverage.assign(Functions.mult(1-tauRec)).assign(gSum.copy().assign(Functions.mult(tauRec)), Functions.plus);
		
		//compute new moving variance
		double scalProd = gSum.zDotProduct(gSum);
		movingVariance *= (1. - tauRec);
		movingVariance += tauRec * scalProd;
		
		//compute new learning rate
		double rho = movingAverage.zDotProduct(movingAverage)/movingVariance;
//		System.out.println("curLearningRate = " + rho);
		//update window size
		tau *= (1 - rho);
		tau += 1;
//		System.out.println("curWindowSize = " + tau);
		return rho;
	}

	private void computeInitialGuesses(DoubleMatrix2D params, int numSamples) {
		Random rng = new Random();
		//form a expectation estimate for each topic (i.e. row)
		DoubleMatrix1D gSum = new DenseDoubleMatrix1D(params.rows());
		HashSet<Integer> seenSamples = new HashSet<>();
		for(int i=0;i<numSamples;++i) {
			int sample = getNewSample(params.columns(), seenSamples, rng);
			gSum.assign(params.viewColumn(sample), Functions.plus);
		}
		gSum.assign(Functions.div(numSamples));
		this.tau = numSamples;
		this.movingAverage = gSum;
		this.movingVariance = gSum.zDotProduct(gSum);
	}

	private int getNewSample(int rows, HashSet<Integer> seenSamples, Random rng) {
		int sample = rng.nextInt(rows);
		while(seenSamples.contains(sample))
			sample = rng.nextInt(rows);
		seenSamples.add(sample);
		return sample;
			
	}
	
}
