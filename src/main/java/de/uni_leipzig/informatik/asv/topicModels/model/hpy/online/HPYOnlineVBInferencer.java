/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.hpy.online;

import java.io.IOException;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import de.uni_leipzig.informatik.asv.corpusUtils.corpus.model.Corpus;
import de.uni_leipzig.informatik.asv.topicModels.model.hdp.online.HDPOnlineVBInferencer;
import de.uni_leipzig.informatik.asv.topicModels.utils.InferencerProperties;

public class HPYOnlineVBInferencer extends HDPOnlineVBInferencer {

	private double eta, d;
	
	public HPYOnlineVBInferencer(Corpus c,
			Corpus test, Corpus validation, InferencerProperties props) throws IOException {
		super(c, test, validation, props);
		this.d = props.d;
		this.eta = props.eta;
	}

	@Override
	protected DoubleMatrix1D getDocumentLevelBetaParam2(int dim) {
		DenseDoubleMatrix1D ret = new DenseDoubleMatrix1D(dim);
		for(int i=0;i<dim;++i)
			ret.set(i, alpha_0 + i*d); 
		return ret;
	}

	@Override
	protected double getDocumentLevelBetaParam1() {
		return 1.-d;
	}

	@Override
	protected DoubleMatrix1D getCorpusLevelBetaParam2(int dim) {
		DenseDoubleMatrix1D ret = new DenseDoubleMatrix1D(dim);
		for(int i=0;i<dim;++i)
			ret.set(i, gamma + i*eta); 
		return ret;
	}

	@Override
	protected double getCorpusLevelBetaParam1() {
		return 1.-eta;
	}


}
