/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.cdtm.state;

import de.uni_leipzig.informatik.asv.topicModels.model.cdtm.ContinuousTimeDynamicTopicModel;

public class Params {
	
	public enum Pos {FIRST, LAST, MIDDLE, FIRST_LAST}
	
	private int segmentStart,segmentEnd, optSize, k, w;
	private double initMean;
	private double[] forwardMean, forwardVariance, backwardVariance, betaHat, Pt, gradient;
	
	private Pos pos;
	private ContinuousTimeDynamicTopicModel model;
		
//	public void setValues(int segmentStart, int segmentEnd, int optSize, int topic, int word, double initMean, double[] forwardMean, double[] forwardVariance, double[] backwardVariance, double[] betaHat, double[] Pt, double[] gradient, ContinuousTimeDynamicTopicModel model) {
//		this.segmentStart = segmentStart;
//		this.segmentEnd = segmentEnd;
//		this.optSize = optSize;
//		this.k = topic;
//		this.w = word;
//		this.initMean = initMean;
//		this.forwardMean = forwardMean;
//		this.forwardVariance = forwardVariance;
//		this.backwardVariance = backwardVariance;
//		this.betaHat = betaHat;
//		this.Pt = Pt;
//		this.gradient = gradient;
//		this.model = model;		
//	}

	public int getSegmentStart() {
		return segmentStart;
	}

	public int getSegmentEnd() {
		return segmentEnd;
	}

	public int getOptSize() {
		return optSize;
	}

	public int getK() {
		return k;
	}

	public int getW() {
		return w;
	}

	public double getInitMean() {
		return initMean;
	}

	public double[] getForwardMean() {
		return forwardMean;
	}

	public double[] getForwardVariance() {
		return forwardVariance;
	}

	public double[] getBackwardVariance() {
		return backwardVariance;
	}

	public double[] getBetaHat() {
		return betaHat;
	}

	public double[] getPt() {
		return Pt;
	}

	public double[] getGradient() {
		return gradient;
	}

	public Pos getPos() {
		return pos;
	}

	public ContinuousTimeDynamicTopicModel getModel() {
		return model;
	}

	public void setSegmentStart(int segmentStart) {
		this.segmentStart = segmentStart;
	}

	public void setSegmentEnd(int segmentEnd) {
		this.segmentEnd = segmentEnd;
	}

	public void setOptSize(int optSize) {
		this.optSize = optSize;
	}

	public void setK(int k) {
		this.k = k;
	}

	public void setW(int w) {
		this.w = w;
	}

	public void setInitMean(double initMean) {
		this.initMean = initMean;
	}

	public void setForwardMean(double[] forwardMean) {
		this.forwardMean = forwardMean;
	}

	public void setForwardVariance(double[] forwardVariance) {
		this.forwardVariance = forwardVariance;
	}

	public void setBackwardVariance(double[] backwardVariance) {
		this.backwardVariance = backwardVariance;
	}

	public void setBetaHat(double[] betaHat) {
		this.betaHat = betaHat;
	}

	public void setPt(double[] pt) {
		Pt = pt;
	}

	public void setGradient(double[] gradient) {
		this.gradient = gradient;
	}

	public void setPos(Pos pos) {
		this.pos = pos;
	}

	public void setModel(ContinuousTimeDynamicTopicModel model) {
		this.model = model;
	}
	
	
}
