/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/**
 * 
 */
package de.uni_leipzig.informatik.asv.topicModels.model.gpdtm;

import java.util.Arrays;

import org.apache.commons.math3.linear.CholeskyDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.ArrayFactory.Type;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.MathUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.math.VectorUtils;
import de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction;

/**
 * @author <a href="mailto:jaehnichen@informatik.uni-leipzig.de">Patrick Jaehnichen</a>
 *
 */
public class OptimizeTopicObjectiveFunction extends ObjectiveFunction {
	
	
	private int k,w;
	private int[] obsTimes;
	private RealMatrix observationsPreTerm;
	private RealMatrix KbbPlusNoiseInv;
	private RealMatrix Sigma;
	private RealMatrix betaHat;
	private double logDetKbbPNoise;
	private RealMatrix V;
	private double[] localZeta;
	private RealMatrix KbbInv;
	private RealMatrix id;

	
	
	public OptimizeTopicObjectiveFunction(int dimensions, GaussianProcessDynamicTopicModel model) {
		super(dimensions, model);
	}
	
	public void setObservationData(double[][] observationTimes, int k, int w) {
		GaussianProcessDynamicTopicModel m = getParameters();
		//precompute covariance matrices and inverses
		RealMatrix Kbb = MatrixUtils.createRealMatrix(m.covFct.computeCovarianceMatrix(observationTimes));
		id = MatrixUtils.createRealIdentityMatrix(getDimensions());
		RealMatrix noise = id.scalarMultiply(m.nu);
		
		CholeskyDecomposition dec = new CholeskyDecomposition(Kbb);
		KbbInv = dec.getSolver().getInverse();
		
		dec = new CholeskyDecomposition(Kbb.add(noise));
		KbbPlusNoiseInv = dec.getSolver().getInverse();
		logDetKbbPNoise = Math.log(dec.getDeterminant());
		
		//only use covariance between observations times (we only look at posterior means at observation times)
		observationsPreTerm = Kbb.add(noise).multiply(KbbInv);
		
		V = Kbb.subtract(Kbb.multiply(KbbPlusNoiseInv).multiply(Kbb));
		m.v[k][w] = VectorUtils.diag(V.getData());
		
		Sigma = observationsPreTerm.multiply(observationsPreTerm.transpose()).subtract(observationsPreTerm.transpose()).subtract(observationsPreTerm).add(id);
		this.k = k;
		this.w = w;
				
		//store observation locations
		obsTimes = m.getNonzeroFrequencyTimes()[w];
	}

	
	/**
	 * @param localZeta the localZeta to set
	 */
	public void setLocalZeta(double[] localZeta) {
		this.localZeta = localZeta;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction#f(double[])
	 */
	@Override
	public double f(double[] point) {
		GaussianProcessDynamicTopicModel m = getParameters();
		double f = 0;
		//compute current mean value and store to the model
		double meanM = VectorUtils.mean(point);
		RealMatrix mW = MatrixUtils.createColumnRealMatrix(point);
//		betaHat = observationsPreTerm.multiply(mW.scalarAdd(-meanM)).scalarAdd(meanM);
		betaHat = observationsPreTerm.multiply(mW.scalarAdd(-meanM));//.scalarAdd(meanM);

		//update the zeta log sum of exp(beta)
		double[] logSumExp = localZeta.clone();
		//beta_hat prior likelihood
//		f -= .5*betaHat.scalarAdd(-meanM).transpose().multiply(KbbInv).multiply(betaHat).scalarAdd(-meanM).getTrace();
//		f -= .5*betaHat.transpose().multiply(KbbInv).multiply(betaHat).getTrace();
		f -= .5*betaHat.transpose().multiply(KbbInv).multiply(betaHat).getTrace();

		for(int t=0;t<obsTimes.length;t++) {
			//update zeta
			logSumExp[t] = MathUtils.logAdd(logSumExp[t], point[t] + .5*m.v[k][w][t]);
			//data likelihood
			f += m.ssKWT[k][w][t] * point[t] - m.ssKT[k][obsTimes[t]] * logSumExp[t];
		}
//		//measurement likelihood
//		f += .5*(mW.scalarAdd(-meanM).transpose().multiply(Sigma).multiply(mW.scalarAdd(-meanM))).getTrace()/m.nu;
		
//		double[] meanArray = new double[point.length];
//		Arrays.fill(meanArray, meanM);
//		RealMatrix muW = MatrixUtils.createColumnRealMatrix(meanArray);
//		RealMatrix u = observationsPreTerm.subtract(id).multiply(mW).subtract(observationsPreTerm.multiply(muW));
//		f += .5*u.transpose().multiply(u).getTrace()/m.nu;

		RealMatrix u = observationsPreTerm.subtract(id).multiply(mW.scalarAdd(-meanM));
		f += .5*u.transpose().multiply(u).getTrace()/m.nu;

		return -f;
	}

	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction#df(double[], double[])
	 */
	@Override
	public void df(double[] point, double[] df) {
		GaussianProcessDynamicTopicModel m = getParameters();
		Arrays.fill(df, 0);
		//compute current mean value and store to the model
		double meanM = VectorUtils.mean(point);
		RealMatrix mW = MatrixUtils.createColumnRealMatrix(point);
//		betaHat = observationsPreTerm.multiply(mW.scalarAdd(-meanM)).scalarAdd(meanM);
		betaHat = observationsPreTerm.multiply(mW.scalarAdd(-meanM));//.scalarAdd(meanM);

		//update the zeta log sum of exp(beta)
		double[] logSumExp = localZeta.clone();
		//beta_hat prior likelihood
//		VectorUtils.subtract_(df, observationsPreTerm.transpose().multiply(KbbInv).multiply(betaHat).getColumn(0));
		VectorUtils.subtract_(df, observationsPreTerm.transpose().multiply(KbbInv).multiply(betaHat).getColumn(0));
		
		for(int t=0;t<obsTimes.length;t++) {
			//update zeta
			logSumExp[t] = MathUtils.logAdd(logSumExp[t], point[t] + .5*m.v[k][w][t]);
			//data likelihood
			df[t] += m.ssKWT[k][w][t] - m.ssKT[k][obsTimes[t]] * Math.exp(point[t] + .5*m.v[k][w][t] - logSumExp[t]);
		}
		//measurement likelihood
//		VectorUtils.add_(Sigma.add(Sigma.transpose()).multiply(mW.scalarAdd(-meanM)).scalarMultiply(1./(2*m.nu)).getColumn(0), df);

//		double[] meanArray = new double[point.length];
//		Arrays.fill(meanArray, meanM);
//		RealMatrix muW = MatrixUtils.createColumnRealMatrix(meanArray);
//		RealMatrix u = observationsPreTerm.subtract(id).multiply(mW).subtract(observationsPreTerm.multiply(muW));
//		VectorUtils.add_(observationsPreTerm.subtract(id).transpose().multiply(u).getColumn(0), df);

		RealMatrix u = observationsPreTerm.subtract(id).multiply(mW.scalarAdd(-meanM));
		VectorUtils.add_(observationsPreTerm.subtract(id).transpose().multiply(u).scalarMultiply(1./m.nu).getColumn(0), df);
		
		VectorUtils.neg_(df);
	}
	
	/* (non-Javadoc)
	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction#fdf(double[], double[])
	 */
	@Override
	public double fdf(double[] point, double[] df) {
		GaussianProcessDynamicTopicModel m = getParameters();
		Arrays.fill(df, 0);
		double f = 0;
		//compute current mean value and store to the model
		double meanM = VectorUtils.mean(point);
		RealMatrix mW = MatrixUtils.createColumnRealMatrix(point);
		betaHat = observationsPreTerm.multiply(mW.scalarAdd(-meanM));//.scalarAdd(meanM);
	
		//update the zeta log sum of exp(beta)
		double[] logSumExp = localZeta.clone();
		//beta_hat prior likelihood
		f -= .5*betaHat.transpose().multiply(KbbInv).multiply(betaHat).getTrace();
		VectorUtils.subtract_(df, observationsPreTerm.transpose().multiply(KbbInv).multiply(betaHat).getColumn(0));
		
		for(int t=0;t<obsTimes.length;t++) {
			//update zeta
			logSumExp[t] = MathUtils.logAdd(logSumExp[t], point[t] + .5*m.v[k][w][t]);
			//data likelihood
			f += m.ssKWT[k][w][t] * point[t] - m.ssKT[k][obsTimes[t]] * logSumExp[t];
			df[t] += m.ssKWT[k][w][t] - m.ssKT[k][obsTimes[t]] * Math.exp(point[t] + .5*m.v[k][w][t] - logSumExp[t]);
		}
//		//measurement likelihood
//		f += .5*(mW.scalarAdd(-meanM).transpose().multiply(Sigma).multiply(mW.scalarAdd(-meanM))).getTrace()/m.nu;
//		VectorUtils.add_(Sigma.add(Sigma.transpose()).multiply(mW.scalarAdd(-meanM)).scalarMultiply(1./(2*m.nu)).getColumn(0), df);
		RealMatrix u = observationsPreTerm.subtract(id).multiply(mW.scalarAdd(-meanM));
		f += .5*u.transpose().multiply(u).getTrace()/m.nu;
		VectorUtils.add_(observationsPreTerm.subtract(id).transpose().multiply(u).scalarMultiply(1./m.nu).getColumn(0), df);

		VectorUtils.neg_(df);
		return -f;
	}

//	/* (non-Javadoc)
//	 * @see de.uni_leipzig.informatik.asv.corpusUtils.helper.optim.ObjectiveFunction#fdf(double[], double[])
//	 */
//	@Override
//	public double fdf(double[] point, double[] df) {
//		GaussianProcessDynamicTopicModel m = getParameters();
//		Arrays.fill(df, 0);
//		double f = 0;
//		//compute current mean value and store to the model
//		double meanM = VectorUtils.mean(point);
//		RealMatrix mW = MatrixUtils.createColumnRealMatrix(point);
//		betaHat = observationsPreTerm.multiply(mW.scalarAdd(-meanM));
//	
//		//update the zeta log sum of exp(beta)
//		double[] logSumExp = localZeta.clone();
//		//beta_hat prior likelihood
//		f -= .5*betaHat.transpose().multiply(KbbInv).multiply(betaHat).getTrace();
//		VectorUtils.subtract_(df, observationsPreTerm.transpose().multiply(KbbInv).multiply(betaHat).getColumn(0));
//		
//		for(int t=0;t<obsTimes.length;t++) {
//			//update zeta
//			logSumExp[t] = MathUtils.logAdd(logSumExp[t], point[t] + .5*m.v[k][w][t]);
//			//data likelihood
//			f += m.ssKWT[k][w][t] * point[t] - m.ssKT[k][obsTimes[t]] * logSumExp[t];
//			df[t] += m.ssKWT[k][w][t] - m.ssKT[k][obsTimes[t]] * Math.exp(point[t] + .5*m.v[k][w][t] - logSumExp[t]);
//		}
////		//measurement likelihood
//		f += .5*(mW.scalarAdd(-meanM).transpose().multiply(Sigma).multiply(mW.scalarAdd(-meanM))).getTrace()/m.nu;
//		VectorUtils.add_(Sigma.add(Sigma.transpose()).multiply(mW.scalarAdd(-meanM)).scalarMultiply(1./(2*m.nu)).getColumn(0), df);
////		double[] meanArray = new double[point.length];
////		Arrays.fill(meanArray, meanM);
////		RealMatrix muW = MatrixUtils.createColumnRealMatrix(meanArray);
////		RealMatrix u = observationsPreTerm.subtract(id).multiply(mW).subtract(observationsPreTerm.multiply(muW));
////		f += .5*u.transpose().multiply(u).getTrace()/m.nu;
////		VectorUtils.add_(observationsPreTerm.subtract(id).transpose().multiply(u).getColumn(0), df);
//
//		VectorUtils.neg_(df);
//		return -f;
//	}

	public void testGradients() {
		double[] x = ArrayFactory.doubleArray(Type.UNIFORM, getDimensions());
		double[] grad = new double[getDimensions()];
		double h = 1e-7;
		
		double val = fdf(x, grad);
		
		for(int i=0;i<getDimensions();i++) {
			double oldVal = x[i];
			x[i] += h;
			double val_i = f(x);
			x[i] = oldVal;
			double diff = (val_i - val)/h;
			System.out.println("diff: " + diff + ", grad: " + grad[i] + ", difference: " + Math.abs(diff - grad[i]));
		}
		System.exit(0);
	}

	public double getLikelihood() {
		GaussianProcessDynamicTopicModel m = getParameters();
		double[] beta = m.m[k][w];
		double[] Sigma = m.v[k][w];
		double meanM = VectorUtils.mean(beta);
		RealMatrix mW = MatrixUtils.createColumnRealMatrix(beta);
		betaHat = observationsPreTerm.multiply(mW.scalarAdd(-meanM));
//		double likelihood = -.5*logDetKbbPNoise -.5*betaHat.transpose().multiply(KbbPlusNoiseInv).multiply(betaHat).getTrace() - .5*V.multiply(KbbPlusNoiseInv).getTrace();
		double likelihood =-.5*betaHat.transpose().multiply(KbbInv).multiply(betaHat).getTrace();
		for(int t=0;t<getDimensions();t++) {
//			likelihood += .5*(Math.log(m.nu) + (Math.pow(betaHat.getColumn(0)[t] + meanM - beta[t], 2) + Sigma[t])/m.nu);
			likelihood += Math.pow(betaHat.getColumn(0)[t] + meanM - beta[t], 2)/m.nu;
		}
		return likelihood;
	}
	
	public double[] getObservations(double[] predictions) {
		return observationsPreTerm.multiply(MatrixUtils.createColumnRealMatrix(predictions)).getColumn(0);
	}
}
