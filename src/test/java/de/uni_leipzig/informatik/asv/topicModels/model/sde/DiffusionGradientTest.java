/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.model.sde;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import de.uni_leipzig.informatik.asv.topicModels.model.sde.Diffusion.Type;

@RunWith(JUnit4.class)
public class DiffusionGradientTest {
	double mean, var, a, b, sigma, theta, valX, valXPlusH, differential;
	double h = 1e-6;

	@Test
	public void testGradientBrownianMean() {
		mean = -5.;
		var = .5;
		sigma = .25;
		a = 10;
		b = -5;
		valX = Diffusion.getDiffusionEnergy(Type.BROWNIAN, mean, var, a, b, sigma, null);
		valXPlusH = Diffusion.getDiffusionEnergy(Type.BROWNIAN, mean+h, var, a, b, sigma, null);
		differential = (valXPlusH - valX)/h;
		
		double grad = Diffusion.getDiffusionDerivativeMean(Type.BROWNIAN, mean, var, a, b, sigma, null);
		assertEquals(null, grad, differential, 1e-3);
	}

	@Test
	public void testGradientBrownianVariance() {
		mean = -5.;
		var = .5;
		sigma = .25;
		a = 10;
		b = -5;
		valX = Diffusion.getDiffusionEnergy(Type.BROWNIAN, mean, var, a, b, sigma, null);
		valXPlusH = Diffusion.getDiffusionEnergy(Type.BROWNIAN, mean, var+h, a, b, sigma, null);
		differential = (valXPlusH - valX)/h;
		double grad = Diffusion.getDiffusionDerivativeVariance(Type.BROWNIAN, mean, var, a, b, sigma, null);
		assertEquals(null, grad, differential, 1e-3);
	}

	@Test
	public void testGradientBrownianA() {
		mean = -5.;
		var = .5;
		sigma = .25;
		a = 10;
		b = -5;
		valX = Diffusion.getDiffusionEnergy(Type.BROWNIAN, mean, var, a, b, sigma, null);
		valXPlusH = Diffusion.getDiffusionEnergy(Type.BROWNIAN, mean, var, a+h, b, sigma, null);
		differential = (valXPlusH - valX)/h;
		
		double grad = Diffusion.getDiffusionDerivativeA(Type.BROWNIAN, mean, var, a, b, sigma, null);
		assertEquals(null, grad, differential, 1e-3);
	}

	@Test
	public void testGradientBrownianB() {
		mean = -5.;
		var = .5;
		sigma = .25;
		a = 10;
		b = -5;
		valX = Diffusion.getDiffusionEnergy(Type.BROWNIAN, mean, var, a, b, sigma, null);
		valXPlusH = Diffusion.getDiffusionEnergy(Type.BROWNIAN, mean, var, a, b+h, sigma, null);
		differential = (valXPlusH - valX)/h;
		
		double grad = Diffusion.getDiffusionDerivativeb(Type.BROWNIAN, mean, var, a, b, sigma, null);
		assertEquals(null, grad, differential, 1e-3);
	}

	
	@Test
	public void testGradientOUMean() {
		mean = -5.;
		var = .5;
		sigma = .25;
		a = 10;
		b = -5;
		theta = 4.5;
		valX = Diffusion.getDiffusionEnergy(Type.ORNSTEIN_UHLENBECK, mean, var, a, b, sigma, new double[]{theta});
		valXPlusH = Diffusion.getDiffusionEnergy(Type.ORNSTEIN_UHLENBECK, mean+h, var, a, b, sigma, new double[]{theta});
		differential = (valXPlusH - valX)/h;
		
		double grad = Diffusion.getDiffusionDerivativeMean(Type.ORNSTEIN_UHLENBECK, mean, var, a, b, sigma, new double[]{theta});
		System.out.println("mean: valX = " + valX + ", valXPlusH = " + valXPlusH + ", differential = " + differential + ", gradient = " + grad);
		assertEquals(null, grad, differential, 1e-3);
	}

	@Test
	public void testGradientOUVariance() {
		mean = -5.;
		var = .5;
		sigma = .25;
		a = 10;
		b = -5;
		theta = 4.5;
		valX = Diffusion.getDiffusionEnergy(Type.ORNSTEIN_UHLENBECK, mean, var, a, b, sigma, new double[]{theta});
		valXPlusH = Diffusion.getDiffusionEnergy(Type.ORNSTEIN_UHLENBECK, mean, var+h, a, b, sigma, new double[]{theta});
		differential = (valXPlusH - valX)/h;
		double grad = Diffusion.getDiffusionDerivativeVariance(Type.ORNSTEIN_UHLENBECK, mean, var, a, b, sigma, new double[]{theta});
		System.out.println("var: valX = " + valX + ", valXPlusH = " + valXPlusH + ", differential = " + differential + ", gradient = " + grad);
		assertEquals(null, grad, differential, 1e-3);
	}

	@Test
	public void testGradientOUA() {
		mean = -5.;
		var = .5;
		sigma = .25;
		a = 10;
		b = -5;
		theta = 4.5;
		valX = Diffusion.getDiffusionEnergy(Type.ORNSTEIN_UHLENBECK, mean, var, a, b, sigma, new double[]{theta});
		valXPlusH = Diffusion.getDiffusionEnergy(Type.ORNSTEIN_UHLENBECK, mean, var, a+h, b, sigma, new double[]{theta});
		differential = (valXPlusH - valX)/h;
		
		double grad = Diffusion.getDiffusionDerivativeA(Type.ORNSTEIN_UHLENBECK, mean, var, a, b, sigma, new double[]{theta});
		System.out.println("a: valX = " + valX + ", valXPlusH = " + valXPlusH + ", differential = " + differential + ", gradient = " + grad);
		assertEquals(null, grad, differential, 1e-3);
	}

	@Test
	public void testGradientOUB() {
		mean = -5.;
		var = .5;
		sigma = .25;
		a = 10;
		b = -5;
		theta = 4.5;
		valX = Diffusion.getDiffusionEnergy(Type.ORNSTEIN_UHLENBECK, mean, var, a, b, sigma, new double[]{theta});
		valXPlusH = Diffusion.getDiffusionEnergy(Type.ORNSTEIN_UHLENBECK, mean, var, a, b+h, sigma, new double[]{theta});
		differential = (valXPlusH - valX)/h;
		
		double grad = Diffusion.getDiffusionDerivativeb(Type.ORNSTEIN_UHLENBECK, mean, var, a, b, sigma, new double[]{theta});
		System.out.println("b: valX = " + valX + ", valXPlusH = " + valXPlusH + ", differential = " + differential + ", gradient = " + grad);
		assertEquals(null, grad, differential, 1e-3);
	}

	@Test
	public void testGradientOUParams() {
		mean = -5.;
		var = .5;
		sigma = .25;
		a = 10;
		b = -5;
		theta = 4.5;
		valX = Diffusion.getDiffusionEnergy(Type.ORNSTEIN_UHLENBECK, mean, var, a, b, sigma, new double[]{theta});
		valXPlusH = Diffusion.getDiffusionEnergy(Type.ORNSTEIN_UHLENBECK, mean, var, a, b, sigma, new double[]{theta+h});
		differential = (valXPlusH - valX)/h;
		
		double[] grad = Diffusion.getDiffusionDerivativeParams(Type.ORNSTEIN_UHLENBECK, mean, var, a, b, sigma, new double[]{theta});
		System.out.println("b: valX = " + valX + ", valXPlusH = " + valXPlusH + ", differential = " + differential + ", gradient = " + grad);
		assertEquals(null, grad[0], differential, 1e-3);
	}

}
