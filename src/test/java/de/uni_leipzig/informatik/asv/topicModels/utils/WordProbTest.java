/*******************************************************************************
 * Copyright (c) 2011-2016  Patrick Jähnichen
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package de.uni_leipzig.informatik.asv.topicModels.utils;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class WordProbTest {
	@Test
	public void testSorting() {
		WordProb i1 = new WordProb("item 1", 0.3, 1);
		WordProb i2 = new WordProb("item 2", .9, 2);
		WordProb i3 = new WordProb("item 3", .001, 3);

		WordProb[] testArray = new WordProb[]{i1, i2, i3};
		Arrays.sort(testArray);
		assertEquals(i1, testArray[1]);
		assertEquals(i2, testArray[0]);
		assertEquals(i3, testArray[2]);
		
	}

}
